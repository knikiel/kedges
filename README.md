kEdges
======
Arbeit meiner HiWi-Stelle f�r Prof. Dr. Mutzel am Lehrstuhl 11: Tool f�r k-Kanten-Berechnungen.

## Wie die Tools kompiliert werden
Abh�ngigkeiten sind *Qt-5.x* und das *Open Graph Drawing Framework* (*OGDF*, Snapshot 2014-03-07, zu finden [hier](http://www.ogdf.net/doku.php/tech:download)). *qmake* sollte sich im Pfad/in der PATH-Variable befinden. 
*OGDF* (benenne heruntergeladene Version evtl. um) sollte sich im selben Verzeichnis wie *kedges* befinden.
F�r noch mehr Details, siehe die entsprechenden qmake-Projektdateien und die Online-Dokumentation.

#### Windows:
* qmake -spec win32-msvc2013 -tp vc -r (Entwicklungsplattform)
* alternativ: make.bat

#### Linux:
* qmake -spec linux-g++ -r (Release-Version)
* alternativ: make_linux.sh (evtl. muss qmake-Pfad angepasst werden)

#### Mac 
* qmake -spec macx-clang -r (Release-Version)
* alternativ: make_macx.sh (angepasst an Installation auf Karos MacBook) (evtl. muss qmake-Pfad angepasst werden)
* weitere Bemerkungen:
  * mind. MacOsX 10.9, erzwinge statisches Linken f�r Qt und Boost (wegen Boost sogar 10.11 notwendig!)
  * beachte auch CXX-Flags f�r OGDF-Make: -mmacosx-version-min=10.9 -DGTEST_USE_OWN_TR1_TUPLE=1
  * statisches Qt erfordert manuelle Kompilierung, siehe auch [Qt-Doku](http://doc.qt.io/qt-5/osx-deployment.html)
  * 2 M�glichkeiten, Programme auf anderen Macs (>= _El Capitan_) zum Laufen zu bringen:
    1. Einbindung der dynamischen Qt-Frameworks (dylibs) mit *macdeployqt*
    2. statische Verlinkung: Qt selbst kompiliert, dylibs d�rfen nicht erreichbar sein; daf�r m�ssen die Linkerflags in den von qmake generierten Makefiles manuell angepasst werden

## Hinweise zur Implementierung von weiteren Features
Erste Anlaufstellen f�r GUI-Angelegenheiten ist die [Qt-Dokumentation](http://doc.qt.io/qt-5/). Die Graphbibliothek [OGDF](http://www.ogdf.net/doc-ogdf/) bietet au�erdem noch viele Datenstrukturen und Algorithmen, die ebenfalls gut verwendet werden k�nnen.

Im Code werden auch an einigen Stellen `assert`s verwendet, die bei der Eingrenzung von Fehlerursachen im Debug-Modus helfen.
Um die Konsistenz von _VisualGraph_ in weiten Teilen zu gew�hrleisten, kann zum gew�hnlichen Debug-Flag auch noch das Flag `CONSISTENCE` per Compiler definiert werden.

#### Neue Kn�pfe und Men�punkte 
Siehe daf�r am besten die Implementierungen der Methoden `create_*` innerhalb von _KEdgesWindow_.

#### Hotkeys
```
QPushButton* widget;
QAction* action;
[...]
widget->setShortcut(Qt::CTRL + Qt::Key_X);
action->setShortcut("Ctrl+O");
```

#### Neuer Dialog
Ein neuer Dialog _CustomDialog_ muss von QDialog erben und wird �ber die Methode _show _angezeigt:
```
CustomDialog* dialog;
[...]
dialog->show();
```
Die Qt-Dokumentation von [QDialog](https://pages.github.com/) sollte erste Anlaufstelle f�r offene Fragen sein. Beispiele innerhalb des Programms finden sich in den Klassen _ColorDialog_ und _FaceInfoDialog_.

#### Features, die die Auswahl eines Knotens oder einer Kante ben�tigen
F�r die Auswahl von Knoten/Kanten wird in*kEdges* eine Statemachine genutzt.
Der Zustand wird mit dem Datentyp _SelectionState_ modelliert, dessen Instanz `m_selection_state` in _GDrawWidget_ verwaltet wird, die Transitionen erfolgen �ber die entsprechenden Kn�pfe in der GUI und das Anklicken von Graphelementen.

F�r ein neues Feature, das �hnlich wie die anderen funktionieren soll, m�ssen folgende Punkte beachtet werden:
1. Der Knopf/der Men�punkt muss (indirekt) im Konstruktor von _KEdgeWindow_ hinzugef�gt werden.
2. Die Behandlungsroutine (als Qt-_Slot_) muss implementiert werden. Dazu sollte �berpr�ft werden, ob ein Graph geladen wurde, ob die Aktion gestartet und  unterbrochen werden soll und �hnliches.
3. Abh�ngig vom aktuellen Zustand sollten der Kern der Transitionen in `GraphScene::mousePressEvent`.
4. Falls der Graph permament durch die Aktion ver�ndert wird, sollten _EditingInfo_ und _GraphChange_ angepasst werden, damit weiter auf die Undo- und Redo-Funktionalit�ten zugegriffen werden kann.

Ein einfaches Beispiel findet sich in der Knotenentfernung.
Die damit verbundene Zustandskonstante ist `SelectionState::REMOVE_VERTEX`.
Dazu sollten insbesondere die Methoden `KEdgesWindow::create_graph_modification`, `KEdgesWindow::remove_vertex_pressed`, `GDrawWidget::begin_vertex_removal`, `GDrawWidget::cancel_vertex_removal`, `GDrawWidget::finish_vertex_removal`, `GraphScene::remove_vertex`, `EditingInfo::apply_diff`, `VisualGraph::remove_vertex` untersucht werden.

F�r ein deutlich komplexeres Beispiel kann man sich auf die Kantenumleitung st�rzen, siehe dazu `KEdgesWindow::redirect_edge_pressed` usw. Dort sieht man auch, wie mehrere Zust�nde verwendet werden und eine Aktion aus bereits existierenden aufgebaut werden k�nnen.
Die grunds�tzliche Logik und die Bezeichner sind analog zur Knotenentfernung.

