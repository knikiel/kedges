Creator "ogdf::GraphIO::writeGML"
graph [
  directed 1
  node [
    id 0
    label "0"
    graphics [
      x 120.0000000
      y -780.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 1
    label "1"
    graphics [
      x 210.0000000
      y -780.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 2
    label "2"
    graphics [
      x 300.0000000
      y -780.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 3
    label "3"
    graphics [
      x 390.0000000
      y -780.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 4
    label "4"
    graphics [
      x 480.0000000
      y -780.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 5
    label "5"
    graphics [
      x 570.0000000
      y -780.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 6
    label "6"
    graphics [
      x 660.0000000
      y -780.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 7
    label "7"
    graphics [
      x 750.0000000
      y -780.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 8
    label "8"
    graphics [
      x 840.0000000
      y -780.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 9
    label "9"
    graphics [
      x 930.0000000
      y -780.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 10
    label "10"
    graphics [
      x 1020.000000
      y -780.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  edge [
    source 0
    target 10
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 120.0000000 y -780.0000000 ]
        point [ x 30.00000000 y -40.50000000 ]
        point [ x 1110.000000 y -40.50000000 ]
        point [ x 1020.000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 10
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 210.0000000 y -780.0000000 ]
        point [ x 75.00000000 y -75.00000000 ]
        point [ x 975.0000000 y -75.00000000 ]
        point [ x 1020.000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 10
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 300.0000000 y -780.0000000 ]
        point [ x 300.0000000 y -292.5000000 ]
        point [ x 786.0000000 y -292.5000000 ]
        point [ x 1020.000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 10
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 390.0000000 y -780.0000000 ]
        point [ x 466.5000000 y -735.0000000 ]
        point [ x 535.5000000 y -771.0000000 ]
        point [ x 559.5000000 y -1227.000000 ]
        point [ x 969.0000000 y -1227.000000 ]
        point [ x 1020.000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 4
    target 10
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 480.0000000 y -780.0000000 ]
        point [ x 547.5000000 y -1312.500000 ]
        point [ x 1020.000000 y -1312.500000 ]
        point [ x 1020.000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 5
    target 10
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 570.0000000 y -780.0000000 ]
        point [ x 712.5000000 y -1177.500000 ]
        point [ x 934.5000000 y -1177.500000 ]
        point [ x 1020.000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 6
    target 10
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 660.0000000 y -780.0000000 ]
        point [ x 849.0000000 y -1081.500000 ]
        point [ x 1020.000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 7
    target 10
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 750.0000000 y -780.0000000 ]
        point [ x 889.5000000 y -928.5000000 ]
        point [ x 1020.000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 8
    target 10
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 840.0000000 y -780.0000000 ]
        point [ x 918.0000000 y -859.5000000 ]
        point [ x 1020.000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 9
    target 10
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 9
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 120.0000000 y -780.0000000 ]
        point [ x 123.0000000 y -123.0000000 ]
        point [ x 957.0000000 y -123.0000000 ]
        point [ x 930.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 9
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 210.0000000 y -780.0000000 ]
        point [ x 210.0000000 y -202.5000000 ]
        point [ x 855.0000000 y -202.5000000 ]
        point [ x 930.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 9
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 300.0000000 y -780.0000000 ]
        point [ x 376.5000000 y -402.0000000 ]
        point [ x 726.0000000 y -402.0000000 ]
        point [ x 930.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 9
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 390.0000000 y -780.0000000 ]
        point [ x 390.0000000 y -453.0000000 ]
        point [ x 712.5000000 y -453.0000000 ]
        point [ x 930.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 4
    target 9
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 480.0000000 y -780.0000000 ]
        point [ x 480.0000000 y -1428.000000 ]
        point [ x 1095.000000 y -1428.000000 ]
        point [ x 1095.000000 y -646.5000000 ]
        point [ x 982.5000000 y -646.5000000 ]
        point [ x 930.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 5
    target 9
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 570.0000000 y -780.0000000 ]
        point [ x 750.0000000 y -1081.500000 ]
        point [ x 930.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 6
    target 9
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 660.0000000 y -780.0000000 ]
        point [ x 792.0000000 y -921.0000000 ]
        point [ x 930.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 7
    target 9
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 750.0000000 y -780.0000000 ]
        point [ x 841.5000000 y -837.0000000 ]
        point [ x 930.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 8
    target 9
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 8
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 120.0000000 y -780.0000000 ]
        point [ x 184.5000000 y -166.5000000 ]
        point [ x 912.0000000 y -166.5000000 ]
        point [ x 840.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 8
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 210.0000000 y -780.0000000 ]
        point [ x 268.5000000 y -246.0000000 ]
        point [ x 825.0000000 y -246.0000000 ]
        point [ x 840.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 8
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 300.0000000 y -780.0000000 ]
        point [ x 445.5000000 y -507.0000000 ]
        point [ x 708.0000000 y -507.0000000 ]
        point [ x 840.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 8
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 390.0000000 y -780.0000000 ]
        point [ x 444.0000000 y -609.0000000 ]
        point [ x 580.5000000 y -567.0000000 ]
        point [ x 736.5000000 y -612.0000000 ]
        point [ x 840.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 4
    target 8
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 480.0000000 y -780.0000000 ]
        point [ x 525.0000000 y -672.0000000 ]
        point [ x 616.5000000 y -588.0000000 ]
        point [ x 702.0000000 y -627.0000000 ]
        point [ x 840.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 5
    target 8
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 570.0000000 y -780.0000000 ]
        point [ x 700.5000000 y -927.0000000 ]
        point [ x 840.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 6
    target 8
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 660.0000000 y -780.0000000 ]
        point [ x 730.5000000 y -832.5000000 ]
        point [ x 840.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 7
    target 8
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 7
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 120.0000000 y -780.0000000 ]
        point [ x 156.0000000 y -1390.500000 ]
        point [ x 568.5000000 y -1390.500000 ]
        point [ x 615.0000000 y -775.5000000 ]
        point [ x 670.5000000 y -739.5000000 ]
        point [ x 750.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 7
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 210.0000000 y -780.0000000 ]
        point [ x 345.0000000 y -330.0000000 ]
        point [ x 772.5000000 y -330.0000000 ]
        point [ x 750.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 7
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 300.0000000 y -780.0000000 ]
        point [ x 460.5000000 y -540.0000000 ]
        point [ x 705.0000000 y -540.0000000 ]
        point [ x 750.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 7
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 390.0000000 y -780.0000000 ]
        point [ x 460.5000000 y -627.0000000 ]
        point [ x 582.0000000 y -592.5000000 ]
        point [ x 688.5000000 y -627.0000000 ]
        point [ x 750.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 4
    target 7
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 480.0000000 y -780.0000000 ]
        point [ x 628.5000000 y -634.5000000 ]
        point [ x 750.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 5
    target 7
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 570.0000000 y -780.0000000 ]
        point [ x 672.0000000 y -714.0000000 ]
        point [ x 750.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 6
    target 7
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 6
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 120.0000000 y -780.0000000 ]
        point [ x 70.50000000 y -1474.500000 ]
        point [ x 597.0000000 y -1474.500000 ]
        point [ x 660.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 6
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 210.0000000 y -780.0000000 ]
        point [ x 160.5000000 y -649.5000000 ]
        point [ x 28.50000000 y -649.5000000 ]
        point [ x 28.50000000 y -1527.000000 ]
        point [ x 729.0000000 y -1527.000000 ]
        point [ x 660.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 6
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 300.0000000 y -780.0000000 ]
        point [ x 525.0000000 y -622.5000000 ]
        point [ x 594.0000000 y -622.5000000 ]
        point [ x 660.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 6
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 390.0000000 y -780.0000000 ]
        point [ x 502.5000000 y -655.5000000 ]
        point [ x 564.0000000 y -655.5000000 ]
        point [ x 660.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 4
    target 6
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 480.0000000 y -780.0000000 ]
        point [ x 576.0000000 y -733.5000000 ]
        point [ x 660.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 5
    target 6
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 5
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 120.0000000 y -780.0000000 ]
        point [ x 210.0000000 y -1335.000000 ]
        point [ x 450.0000000 y -1335.000000 ]
        point [ x 450.0000000 y -1095.000000 ]
        point [ x 570.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 5
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 210.0000000 y -780.0000000 ]
        point [ x 270.0000000 y -1114.500000 ]
        point [ x 354.0000000 y -1114.500000 ]
        point [ x 570.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 5
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 300.0000000 y -780.0000000 ]
        point [ x 412.5000000 y -952.5000000 ]
        point [ x 570.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 5
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 390.0000000 y -780.0000000 ]
        point [ x 498.0000000 y -684.0000000 ]
        point [ x 570.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 4
    target 5
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 4
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 120.0000000 y -780.0000000 ]
        point [ x 256.5000000 y -1179.000000 ]
        point [ x 381.0000000 y -1179.000000 ]
        point [ x 480.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 4
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 210.0000000 y -780.0000000 ]
        point [ x 352.5000000 y -930.0000000 ]
        point [ x 480.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 4
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 300.0000000 y -780.0000000 ]
        point [ x 390.0000000 y -840.0000000 ]
        point [ x 480.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 4
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 3
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 120.0000000 y -780.0000000 ]
        point [ x 303.0000000 y -997.5000000 ]
        point [ x 390.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 3
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 210.0000000 y -780.0000000 ]
        point [ x 310.5000000 y -834.0000000 ]
        point [ x 390.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 3
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 2
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 120.0000000 y -780.0000000 ]
        point [ x 202.5000000 y -843.0000000 ]
        point [ x 300.0000000 y -780.0000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 2
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 1
    graphics [
      type "line"
      arrow "last"
    ]
  ]
]
