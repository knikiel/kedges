Creator "ogdf::GraphIO::writeGML"
graph [
  directed 1
  node [
    id 0
    label "0"
    graphics [
      x 791.0000000
      y -189.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 1
    label "1"
    graphics [
      x 329.0000000
      y -525.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 2
    label "2"
    graphics [
      x 504.0000000
      y -1071.000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 3
    label "3"
    graphics [
      x 1078.000000
      y -1071.000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 4
    label "4"
    graphics [
      x 1253.000000
      y -525.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 5
    label "5"
    graphics [
      x 651.0000000
      y -483.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 6
    label "6"
    graphics [
      x 560.0000000
      y -749.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 7
    label "7"
    graphics [
      x 791.0000000
      y -917.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 8
    label "8"
    graphics [
      x 1022.000000
      y -756.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 9
    label "9"
    graphics [
      x 938.0000000
      y -476.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 10
    label "10"
    graphics [
      x 791.0000000
      y -679.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  edge [
    source 0
    target 10
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 791.0000000 y -189.0000000 ]
        point [ x 742.0000000 y -511.0000000 ]
        point [ x 791.0000000 y -679.0000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 10
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 329.0000000 y -525.0000000 ]
        point [ x 616.0000000 y -665.0000000 ]
        point [ x 791.0000000 y -679.0000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 10
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 504.0000000 y -1071.000000 ]
        point [ x 742.0000000 y -847.0000000 ]
        point [ x 791.0000000 y -679.0000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 10
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1078.000000 y -1071.000000 ]
        point [ x 938.0000000 y -784.0000000 ]
        point [ x 791.0000000 y -679.0000000 ]
      ]
    ]
  ]
  edge [
    source 4
    target 10
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1253.000000 y -525.0000000 ]
        point [ x 980.0000000 y -672.0000000 ]
        point [ x 791.0000000 y -679.0000000 ]
      ]
    ]
  ]
  edge [
    source 5
    target 10
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 6
    target 10
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 7
    target 10
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 8
    target 10
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 9
    target 10
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 9
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 1
    target 9
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 329.0000000 y -525.0000000 ]
        point [ x 679.0000000 y -364.0000000 ]
        point [ x 938.0000000 y -476.0000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 9
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 504.0000000 y -1071.000000 ]
        point [ x 154.0000000 y -1029.000000 ]
        point [ x 28.00000000 y -567.0000000 ]
        point [ x 343.0000000 y -49.00000000 ]
        point [ x 770.0000000 y -14.00000000 ]
        point [ x 1169.000000 y -154.0000000 ]
        point [ x 938.0000000 y -476.0000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 9
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1078.000000 y -1071.000000 ]
        point [ x 1134.000000 y -679.0000000 ]
        point [ x 938.0000000 y -476.0000000 ]
      ]
    ]
  ]
  edge [
    source 4
    target 9
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 5
    target 9
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 6
    target 9
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 7
    target 9
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 8
    target 9
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 8
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 791.0000000 y -189.0000000 ]
        point [ x 987.0000000 y -364.0000000 ]
        point [ x 1022.000000 y -756.0000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 8
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 329.0000000 y -525.0000000 ]
        point [ x 364.0000000 y -273.0000000 ]
        point [ x 770.0000000 y -70.00000000 ]
        point [ x 1372.000000 y -280.0000000 ]
        point [ x 1372.000000 y -735.0000000 ]
        point [ x 1022.000000 y -756.0000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 8
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 504.0000000 y -1071.000000 ]
        point [ x 840.0000000 y -959.0000000 ]
        point [ x 1022.000000 y -756.0000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 8
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 4
    target 8
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 5
    target 8
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 6
    target 8
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 7
    target 8
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 7
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 791.0000000 y -189.0000000 ]
        point [ x 1148.000000 y -63.00000000 ]
        point [ x 1449.000000 y -210.0000000 ]
        point [ x 1449.000000 y -1071.000000 ]
        point [ x 1106.000000 y -1323.000000 ]
        point [ x 896.0000000 y -1323.000000 ]
        point [ x 791.0000000 y -917.0000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 7
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 329.0000000 y -525.0000000 ]
        point [ x 504.0000000 y -812.0000000 ]
        point [ x 791.0000000 y -917.0000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 7
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 3
    target 7
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 4
    target 7
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1253.000000 y -525.0000000 ]
        point [ x 1064.000000 y -903.0000000 ]
        point [ x 791.0000000 y -917.0000000 ]
      ]
    ]
  ]
  edge [
    source 5
    target 7
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 6
    target 7
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 6
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 791.0000000 y -189.0000000 ]
        point [ x 588.0000000 y -448.0000000 ]
        point [ x 560.0000000 y -749.0000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 6
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 2
    target 6
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 3
    target 6
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1078.000000 y -1071.000000 ]
        point [ x 679.0000000 y -1036.000000 ]
        point [ x 560.0000000 y -749.0000000 ]
      ]
    ]
  ]
  edge [
    source 4
    target 6
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1253.000000 y -525.0000000 ]
        point [ x 1421.000000 y -763.0000000 ]
        point [ x 1337.000000 y -1064.000000 ]
        point [ x 1008.000000 y -1288.000000 ]
        point [ x 448.0000000 y -1288.000000 ]
        point [ x 154.0000000 y -875.0000000 ]
        point [ x 560.0000000 y -749.0000000 ]
      ]
    ]
  ]
  edge [
    source 5
    target 6
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 5
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 1
    target 5
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 2
    target 5
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 504.0000000 y -1071.000000 ]
        point [ x 441.0000000 y -665.0000000 ]
        point [ x 651.0000000 y -483.0000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 5
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1078.000000 y -1071.000000 ]
        point [ x 966.0000000 y -1204.000000 ]
        point [ x 469.0000000 y -1204.000000 ]
        point [ x 280.0000000 y -966.0000000 ]
        point [ x 147.0000000 y -518.0000000 ]
        point [ x 357.0000000 y -182.0000000 ]
        point [ x 651.0000000 y -483.0000000 ]
      ]
    ]
  ]
  edge [
    source 4
    target 5
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1253.000000 y -525.0000000 ]
        point [ x 1008.000000 y -434.0000000 ]
        point [ x 651.0000000 y -483.0000000 ]
      ]
    ]
  ]
  edge [
    source 0
    target 4
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 1
    target 4
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 329.0000000 y -525.0000000 ]
        point [ x 777.0000000 y -126.0000000 ]
        point [ x 1253.000000 y -525.0000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 4
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 504.0000000 y -1071.000000 ]
        point [ x 1127.000000 y -1162.000000 ]
        point [ x 1253.000000 y -525.0000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 4
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 3
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 791.0000000 y -189.0000000 ]
        point [ x 1351.000000 y -490.0000000 ]
        point [ x 1078.000000 y -1071.000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 3
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 329.0000000 y -525.0000000 ]
        point [ x 469.0000000 y -1134.000000 ]
        point [ x 1078.000000 y -1071.000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 3
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 2
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 791.0000000 y -189.0000000 ]
        point [ x 203.0000000 y -483.0000000 ]
        point [ x 504.0000000 y -1071.000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 2
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 1
    graphics [
      type "line"
      arrow "last"
    ]
  ]
]
