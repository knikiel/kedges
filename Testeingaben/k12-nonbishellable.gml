Creator "ogdf::GraphIO::writeGML"
graph [
  directed 1
  node [
    id 0
    label "0"
    graphics [
      x 847.5000000
      y -202.5000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 1
    label "1"
    graphics [
      x 352.5000000
      y -562.5000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 2
    label "2"
    graphics [
      x 540.0000000
      y -1147.500000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 3
    label "3"
    graphics [
      x 1155.000000
      y -1147.500000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 4
    label "4"
    graphics [
      x 1342.500000
      y -562.5000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 5
    label "5"
    graphics [
      x 697.5000000
      y -517.5000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 6
    label "6"
    graphics [
      x 600.0000000
      y -802.5000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 7
    label "7"
    graphics [
      x 847.5000000
      y -982.5000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 8
    label "8"
    graphics [
      x 1095.000000
      y -810.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 9
    label "9"
    graphics [
      x 1005.000000
      y -510.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 10
    label "10"
    graphics [
      x 772.6107789
      y -771.1477895
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 11
    label "11"
    graphics [
      x 905.6151158
      y -690.7380126
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  edge [
    source 0
    target 11
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 1
    target 11
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 2
    target 11
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 540.0000000 y -1147.500000 ]
        point [ x 835.1548056 y -857.2945441 ]
        point [ x 905.6151158 y -690.7380126 ]
      ]
    ]
  ]
  edge [
    source 3
    target 11
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 4
    target 11
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 5
    target 11
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 6
    target 11
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 600.0000000 y -802.5000000 ]
        point [ x 798.4043951 y -719.6554204 ]
        point [ x 905.6151158 y -690.7380126 ]
      ]
    ]
  ]
  edge [
    source 7
    target 11
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 8
    target 11
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 9
    target 11
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 10
    target 11
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 10
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 847.5000000 y -202.5000000 ]
        point [ x 795.0000000 y -547.5000000 ]
        point [ x 772.6107789 y -771.1477895 ]
      ]
    ]
  ]
  edge [
    source 1
    target 10
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 352.5000000 y -562.5000000 ]
        point [ x 660.0000000 y -712.5000000 ]
        point [ x 772.6107789 y -771.1477895 ]
      ]
    ]
  ]
  edge [
    source 2
    target 10
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 3
    target 10
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 4
    target 10
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1342.500000 y -562.5000000 ]
        point [ x 1050.000000 y -720.0000000 ]
        point [ x 772.6107789 y -771.1477895 ]
      ]
    ]
  ]
  edge [
    source 5
    target 10
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 6
    target 10
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 7
    target 10
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 8
    target 10
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 9
    target 10
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 9
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 1
    target 9
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 352.5000000 y -562.5000000 ]
        point [ x 727.5000000 y -390.0000000 ]
        point [ x 1005.000000 y -510.0000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 9
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 540.0000000 y -1147.500000 ]
        point [ x 165.0000000 y -1102.500000 ]
        point [ x 30.00000000 y -607.5000000 ]
        point [ x 367.5000000 y -52.50000000 ]
        point [ x 825.0000000 y -15.00000000 ]
        point [ x 1252.500000 y -165.0000000 ]
        point [ x 1005.000000 y -510.0000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 9
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1155.000000 y -1147.500000 ]
        point [ x 1215.000000 y -727.5000000 ]
        point [ x 1005.000000 y -510.0000000 ]
      ]
    ]
  ]
  edge [
    source 4
    target 9
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 5
    target 9
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 6
    target 9
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 600.0000000 y -802.5000000 ]
        point [ x 737.4130687 y -583.7760099 ]
        point [ x 1005.000000 y -510.0000000 ]
      ]
    ]
  ]
  edge [
    source 7
    target 9
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 8
    target 9
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 8
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 847.5000000 y -202.5000000 ]
        point [ x 1057.500000 y -390.0000000 ]
        point [ x 1095.000000 y -810.0000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 8
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 352.5000000 y -562.5000000 ]
        point [ x 390.0000000 y -292.5000000 ]
        point [ x 825.0000000 y -75.00000000 ]
        point [ x 1470.000000 y -300.0000000 ]
        point [ x 1470.000000 y -787.5000000 ]
        point [ x 1095.000000 y -810.0000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 8
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 540.0000000 y -1147.500000 ]
        point [ x 900.0000000 y -1027.500000 ]
        point [ x 1095.000000 y -810.0000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 8
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 4
    target 8
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 5
    target 8
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 697.5000000 y -517.5000000 ]
        point [ x 937.3719740 y -599.9888941 ]
        point [ x 1095.000000 y -810.0000000 ]
      ]
    ]
  ]
  edge [
    source 6
    target 8
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 7
    target 8
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 7
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 847.5000000 y -202.5000000 ]
        point [ x 1230.000000 y -67.50000000 ]
        point [ x 1552.500000 y -225.0000000 ]
        point [ x 1552.500000 y -1147.500000 ]
        point [ x 1185.000000 y -1417.500000 ]
        point [ x 960.0000000 y -1417.500000 ]
        point [ x 847.5000000 y -982.5000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 7
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 352.5000000 y -562.5000000 ]
        point [ x 540.0000000 y -870.0000000 ]
        point [ x 847.5000000 y -982.5000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 7
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 3
    target 7
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 4
    target 7
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1342.500000 y -562.5000000 ]
        point [ x 1140.000000 y -967.5000000 ]
        point [ x 847.5000000 y -982.5000000 ]
      ]
    ]
  ]
  edge [
    source 5
    target 7
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 697.5000000 y -517.5000000 ]
        point [ x 710.2619161 y -780.4507204 ]
        point [ x 847.5000000 y -982.5000000 ]
      ]
    ]
  ]
  edge [
    source 6
    target 7
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 6
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 847.5000000 y -202.5000000 ]
        point [ x 630.0000000 y -480.0000000 ]
        point [ x 600.0000000 y -802.5000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 6
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 2
    target 6
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 3
    target 6
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1155.000000 y -1147.500000 ]
        point [ x 727.5000000 y -1110.000000 ]
        point [ x 600.0000000 y -802.5000000 ]
      ]
    ]
  ]
  edge [
    source 4
    target 6
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1342.500000 y -562.5000000 ]
        point [ x 1522.500000 y -817.5000000 ]
        point [ x 1432.500000 y -1140.000000 ]
        point [ x 1080.000000 y -1380.000000 ]
        point [ x 480.0000000 y -1380.000000 ]
        point [ x 165.0000000 y -937.5000000 ]
        point [ x 600.0000000 y -802.5000000 ]
      ]
    ]
  ]
  edge [
    source 5
    target 6
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 5
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 1
    target 5
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 2
    target 5
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 540.0000000 y -1147.500000 ]
        point [ x 472.5000000 y -712.5000000 ]
        point [ x 697.5000000 y -517.5000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 5
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1155.000000 y -1147.500000 ]
        point [ x 1035.000000 y -1290.000000 ]
        point [ x 502.5000000 y -1290.000000 ]
        point [ x 300.0000000 y -1035.000000 ]
        point [ x 157.5000000 y -555.0000000 ]
        point [ x 382.5000000 y -195.0000000 ]
        point [ x 697.5000000 y -517.5000000 ]
      ]
    ]
  ]
  edge [
    source 4
    target 5
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1342.500000 y -562.5000000 ]
        point [ x 1080.000000 y -465.0000000 ]
        point [ x 697.5000000 y -517.5000000 ]
      ]
    ]
  ]
  edge [
    source 0
    target 4
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 1
    target 4
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 352.5000000 y -562.5000000 ]
        point [ x 832.5000000 y -135.0000000 ]
        point [ x 1342.500000 y -562.5000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 4
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 540.0000000 y -1147.500000 ]
        point [ x 1207.500000 y -1245.000000 ]
        point [ x 1342.500000 y -562.5000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 4
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 3
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 847.5000000 y -202.5000000 ]
        point [ x 1447.500000 y -525.0000000 ]
        point [ x 1155.000000 y -1147.500000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 3
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 352.5000000 y -562.5000000 ]
        point [ x 502.5000000 y -1215.000000 ]
        point [ x 1155.000000 y -1147.500000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 3
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 2
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 847.5000000 y -202.5000000 ]
        point [ x 217.5000000 y -517.5000000 ]
        point [ x 540.0000000 y -1147.500000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 2
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 1
    graphics [
      type "line"
      arrow "last"
    ]
  ]
]
