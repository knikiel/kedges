Creator "ogdf::GraphIO::writeGML"
graph [
  directed 1
  node [
    id 0
    label "0"
    graphics [
      x 22.50000000
      y 472.5000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 1
    label "1"
    graphics [
      x 3322.500000
      y 462.0462146
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 2
    label "2"
    graphics [
      x 3022.500000
      y 472.5000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 3
    label "3"
    graphics [
      x 2722.500000
      y 472.5000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 4
    label "4"
    graphics [
      x 2422.500000
      y 472.5000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 5
    label "5"
    graphics [
      x 2122.500000
      y 472.5000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 6
    label "6"
    graphics [
      x 1822.500000
      y 472.5000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 7
    label "7"
    graphics [
      x 1522.500000
      y 472.5000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 8
    label "8"
    graphics [
      x 1222.500000
      y 472.5000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 9
    label "9"
    graphics [
      x 922.5000000
      y 472.5000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 10
    label "10"
    graphics [
      x 622.5000000
      y 472.5000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 11
    label "11"
    graphics [
      x 322.5000000
      y 472.5000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  edge [
    source 0
    target 11
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 1
    target 11
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 3322.500000 y 462.0462146 ]
        point [ x 3388.190056 y -1548.431634 ]
        point [ x 2.980516280 y -1561.587091 ]
        point [ x 46.83203983 y 5.135269818 ]
        point [ x 322.5000000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 11
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 3022.500000 y 472.5000000 ]
        point [ x 3022.500000 y -1439.987341 ]
        point [ x 72.43181200 y -1453.142798 ]
        point [ x 120.6684879 y -33.32629553 ]
        point [ x 322.5000000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 11
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 2722.500000 y 472.5000000 ]
        point [ x 2797.127112 y -88.66165337 ]
        point [ x 2620.885391 y -1353.209287 ]
        point [ x 166.9866596 y -1353.209287 ]
        point [ x 185.6240526 y -92.58646684 ]
        point [ x 322.5000000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 4
    target 11
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 2422.500000 y 472.5000000 ]
        point [ x 2243.050824 y -1246.526752 ]
        point [ x 264.2137095 y -1246.526752 ]
        point [ x 264.2137095 y -115.8562332 ]
        point [ x 322.5000000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 5
    target 11
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 2122.500000 y 472.5000000 ]
        point [ x 1882.993850 y -1057.633405 ]
        point [ x 374.1797850 y -1057.633405 ]
        point [ x 322.5000000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 6
    target 11
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1822.500000 y 472.5000000 ]
        point [ x 1776.412723 y -613.5368617 ]
        point [ x 452.0436465 y -613.5368617 ]
        point [ x 322.5000000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 7
    target 11
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1522.500000 y 472.5000000 ]
        point [ x 1367.322704 y 650.9882093 ]
        point [ x 1141.350322 y 1472.245024 ]
        point [ x 190.5259305 y 1472.245024 ]
        point [ x 322.5000000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 8
    target 11
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1222.500000 y 472.5000000 ]
        point [ x 1003.429358 y 1043.846676 ]
        point [ x 327.9397859 y 1043.846676 ]
        point [ x 322.5000000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 9
    target 11
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 922.5000000 y 472.5000000 ]
        point [ x 675.2326801 y 751.2115587 ]
        point [ x 322.5000000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 10
    target 11
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 10
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 22.50000000 y 472.5000000 ]
        point [ x 245.0137032 y 639.4589091 ]
        point [ x 622.5000000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 10
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 3322.500000 y 462.0462146 ]
        point [ x 3873.793056 y 1133.210940 ]
        point [ x 4022.511156 y 2909.685174 ]
        point [ x 312.2164048 y 2909.685174 ]
        point [ x 541.2403463 y 964.9127357 ]
        point [ x 622.5000000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 10
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 3022.500000 y 472.5000000 ]
        point [ x 2796.272106 y -1166.737535 ]
        point [ x 571.8257909 y -1166.737535 ]
        point [ x 410.8917341 y 310.3003050 ]
        point [ x 622.5000000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 10
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 2722.500000 y 472.5000000 ]
        point [ x 2536.608244 y -885.2655680 ]
        point [ x 615.6279138 y -885.2655680 ]
        point [ x 496.1418349 y 245.2081997 ]
        point [ x 622.5000000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 4
    target 10
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 2422.500000 y 472.5000000 ]
        point [ x 2031.298156 y -777.8293354 ]
        point [ x 670.7015099 y -777.8293354 ]
        point [ x 550.9562393 y 234.2453189 ]
        point [ x 622.5000000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 5
    target 10
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 2122.500000 y 472.5000000 ]
        point [ x 1821.327645 y -704.4182964 ]
        point [ x 990.5435597 y -704.4182964 ]
        point [ x 622.5000000 y -70.39121522 ]
        point [ x 622.5000000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 6
    target 10
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1822.500000 y 472.5000000 ]
        point [ x 1583.003337 y -487.8178839 ]
        point [ x 1077.847867 y -487.8178839 ]
        point [ x 743.4342798 y -0.4699609284 ]
        point [ x 622.5000000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 7
    target 10
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1522.500000 y 472.5000000 ]
        point [ x 1092.112938 y 94.61257037 ]
        point [ x 622.5000000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 8
    target 10
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1222.500000 y 472.5000000 ]
        point [ x 1035.243310 y 679.1441323 ]
        point [ x 622.5000000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 9
    target 10
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 9
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 22.50000000 y 472.5000000 ]
        point [ x 169.9877430 y 928.9412943 ]
        point [ x 698.2720872 y 928.9412943 ]
        point [ x 922.5000000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 9
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 3322.500000 y 462.0462146 ]
        point [ x 3592.762174 y 1173.358209 ]
        point [ x 3760.634664 y 2746.665873 ]
        point [ x 563.1541470 y 2746.665873 ]
        point [ x 735.8982358 y 1151.997219 ]
        point [ x 922.5000000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 9
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 3022.500000 y 472.5000000 ]
        point [ x 3132.407151 y 600.5621201 ]
        point [ x 3369.567103 y 2542.834401 ]
        point [ x 809.2754811 y 2542.834401 ]
        point [ x 922.5000000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 9
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 2722.500000 y 472.5000000 ]
        point [ x 2508.344567 y -320.2818439 ]
        point [ x 1132.844273 y -320.2818439 ]
        point [ x 747.0939058 y 219.0690734 ]
        point [ x 922.5000000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 4
    target 9
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 2422.500000 y 472.5000000 ]
        point [ x 2085.563823 y -222.2173744 ]
        point [ x 1328.071865 y -222.2173744 ]
        point [ x 922.5000000 y 80.63221005 ]
        point [ x 922.5000000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 5
    target 9
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 2122.500000 y 472.5000000 ]
        point [ x 1355.556625 y 56.49742778 ]
        point [ x 922.5000000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 6
    target 9
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1822.500000 y 472.5000000 ]
        point [ x 1368.343455 y 200.2646477 ]
        point [ x 922.5000000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 7
    target 9
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1522.500000 y 472.5000000 ]
        point [ x 1224.465236 y 361.8794378 ]
        point [ x 922.5000000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 8
    target 9
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 8
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 22.50000000 y 472.5000000 ]
        point [ x 73.18279062 y 1305.238072 ]
        point [ x 1017.045256 y 1305.238072 ]
        point [ x 1222.500000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 8
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 3322.500000 y 462.0462146 ]
        point [ x 3502.960286 y 2408.474881 ]
        point [ x 942.7521199 y 2408.474881 ]
        point [ x 1222.500000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 8
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 3022.500000 y 472.5000000 ]
        point [ x 3094.937837 y 720.3134426 ]
        point [ x 3184.372411 y 2214.615523 ]
        point [ x 1153.038371 y 2214.615523 ]
        point [ x 1222.500000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 8
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 2722.500000 y 472.5000000 ]
        point [ x 2717.803516 y 1983.163454 ]
        point [ x 1549.015670 y 1983.163454 ]
        point [ x 1222.500000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 4
    target 8
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 2422.500000 y 472.5000000 ]
        point [ x 1897.326152 y 35.50625160 ]
        point [ x 1339.752262 y 295.0181976 ]
        point [ x 1222.500000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 5
    target 8
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 2122.500000 y 472.5000000 ]
        point [ x 1623.048051 y 288.8645333 ]
        point [ x 1222.500000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 6
    target 8
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1822.500000 y 472.5000000 ]
        point [ x 1557.117286 y 386.4742502 ]
        point [ x 1222.500000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 7
    target 8
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 7
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 22.50000000 y 472.5000000 ]
        point [ x -120.4818172 y 1647.947319 ]
        point [ x 1261.681943 y 1647.947319 ]
        point [ x 1522.500000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 7
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 3322.500000 y 462.0462146 ]
        point [ x 3078.385684 y 2092.627222 ]
        point [ x 1453.248497 y 2092.627222 ]
        point [ x 1453.248497 y 1213.932143 ]
        point [ x 1522.500000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 7
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 3022.500000 y 472.5000000 ]
        point [ x 3022.500000 y 1672.358563 ]
        point [ x 1522.500000 y 1672.358563 ]
        point [ x 1522.500000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 7
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 2722.500000 y 472.5000000 ]
        point [ x 2562.408952 y 1276.737077 ]
        point [ x 1613.268578 y 1276.737077 ]
        point [ x 1522.500000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 4
    target 7
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 2422.500000 y 472.5000000 ]
        point [ x 2422.500000 y 901.1842270 ]
        point [ x 1696.016662 y 884.0547256 ]
        point [ x 1522.500000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 5
    target 7
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 2122.500000 y 472.5000000 ]
        point [ x 1840.964107 y 413.1635499 ]
        point [ x 1522.500000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 6
    target 7
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 6
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 22.50000000 y 472.5000000 ]
        point [ x -449.7460367 y 1821.233991 ]
        point [ x 1656.092909 y 1821.233991 ]
        point [ x 1656.092909 y 641.5177363 ]
        point [ x 1822.500000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 6
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 3322.500000 y 462.0462146 ]
        point [ x 3058.802162 y 1513.037887 ]
        point [ x 1757.637664 y 1513.037887 ]
        point [ x 1757.637664 y 743.9521545 ]
        point [ x 1822.500000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 6
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 3022.500000 y 472.5000000 ]
        point [ x 2844.017708 y 1395.123895 ]
        point [ x 1925.780246 y 1395.123895 ]
        point [ x 1822.500000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 6
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 2722.500000 y 472.5000000 ]
        point [ x 2460.902892 y 1089.380117 ]
        point [ x 2055.140946 y 1089.380117 ]
        point [ x 1822.500000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 4
    target 6
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 2422.500000 y 472.5000000 ]
        point [ x 2058.379440 y 607.9026480 ]
        point [ x 1822.500000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 5
    target 6
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 5
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 22.50000000 y 472.5000000 ]
        point [ x -77.48508863 y -605.6492923 ]
        point [ x -186.7169485 y -1754.641950 ]
        point [ x -186.7169485 y -1762.009926 ]
        point [ x 2128.270001 y -1742.928184 ]
        point [ x 2122.500000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 5
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 3322.500000 y 462.0462146 ]
        point [ x 2801.880756 y 1177.393750 ]
        point [ x 2240.324882 y 1177.393750 ]
        point [ x 2122.500000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 5
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 3022.500000 y 472.5000000 ]
        point [ x 2800.523892 y 984.9838501 ]
        point [ x 2358.544789 y 984.9838501 ]
        point [ x 2122.500000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 5
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 2722.500000 y 472.5000000 ]
        point [ x 2341.239776 y 624.3882349 ]
        point [ x 2122.500000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 4
    target 5
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 4
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 22.50000000 y 472.5000000 ]
        point [ x -187.1138975 y -603.4567161 ]
        point [ x -298.8475138 y -1875.990150 ]
        point [ x 2541.940608 y -1862.834693 ]
        point [ x 2422.500000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 4
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 3322.500000 y 462.0462146 ]
        point [ x 2803.475994 y 265.5024914 ]
        point [ x 2422.500000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 4
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 3022.500000 y 472.5000000 ]
        point [ x 2836.017330 y 632.4856936 ]
        point [ x 2422.500000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 4
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 3
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 22.50000000 y 472.5000000 ]
        point [ x -338.6629782 y -607.1705873 ]
        point [ x -412.7586980 y -2013.789992 ]
        point [ x 2890.311600 y -2000.634535 ]
        point [ x 2890.311600 y 204.5954103 ]
        point [ x 2722.500000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 3
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 3322.500000 y 462.0462146 ]
        point [ x 2868.707622 y 351.8802246 ]
        point [ x 2722.500000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 3
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 2
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 22.50000000 y 472.5000000 ]
        point [ x -571.0032526 y -606.7166555 ]
        point [ x -487.0253642 y -2205.519180 ]
        point [ x 3273.144208 y -2192.363723 ]
        point [ x 3022.500000 y 472.5000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 2
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 1
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 22.50000000 y 472.5000000 ]
        point [ x -807.0113815 y -590.8012075 ]
        point [ x -573.3619125 y -2333.929569 ]
        point [ x 3717.418298 y -2320.774112 ]
        point [ x 3322.500000 y 462.0462146 ]
      ]
    ]
  ]
]
