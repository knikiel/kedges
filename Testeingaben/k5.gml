Creator "ogdf::GraphIO::writeGML"
graph [
  directed 1
  node [
    id 0
    label "0"
    graphics [
      x 0.0000000000
      y -0.0000000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 1
    label "1"
    graphics [
      x 200.0000000
      y -800.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 2
    label "2"
    graphics [
      x 800.0000000
      y -800.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 3
    label "3"
    graphics [
      x 1000.000000
      y -0.0000000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 4
    label "4"
    graphics [
      x 500.0000000
      y -1000.000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  edge [
    source 0
    target 4
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 0.0000000000 y -0.0000000000 ]
        point [ x 0.0000000000 y -1000.000000 ]
        point [ x 500.0000000 y -1000.000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 4
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 2
    target 4
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 3
    target 4
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1000.000000 y -0.0000000000 ]
        point [ x 1000.000000 y -1000.000000 ]
        point [ x 500.0000000 y -1000.000000 ]
      ]
    ]
  ]
  edge [
    source 0
    target 3
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 1
    target 3
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 2
    target 3
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 2
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 1
    target 2
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 1
    graphics [
      type "line"
      arrow "last"
    ]
  ]
]
