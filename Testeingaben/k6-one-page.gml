Creator	"yFiles"
Version	"2.12"
graph
[
	hierarchic	1
	label	""
	directed	1
	node
	[
		id	0
		label	"1"
		graphics
		[
			x	226.0
			y	359.0
			w	30.0
			h	30.0
			type	"rectangle"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	1
		label	"2"
		graphics
		[
			x	372.0
			y	359.0
			w	30.0
			h	30.0
			type	"rectangle"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"2"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	2
		label	"3"
		graphics
		[
			x	518.0
			y	359.0
			w	30.0
			h	30.0
			type	"rectangle"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"3"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	3
		label	"4"
		graphics
		[
			x	664.0
			y	359.0
			w	30.0
			h	30.0
			type	"rectangle"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"4"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	4
		label	"5"
		graphics
		[
			x	810.0
			y	359.0
			w	30.0
			h	30.0
			type	"rectangle"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"5"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	5
		label	"6"
		graphics
		[
			x	956.0
			y	359.0
			w	30.0
			h	30.0
			type	"rectangle"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"6"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	edge
	[
		source	0
		target	1
		graphics
		[
			fill	"#000000"
		]
	]
	edge
	[
		source	1
		target	2
		graphics
		[
			fill	"#000000"
		]
	]
	edge
	[
		source	2
		target	3
		graphics
		[
			fill	"#000000"
		]
	]
	edge
	[
		source	3
		target	4
		graphics
		[
			fill	"#000000"
		]
	]
	edge
	[
		source	4
		target	5
		graphics
		[
			fill	"#000000"
		]
	]
	edge
	[
		source	0
		target	2
		graphics
		[
			fill	"#000000"
			Line
			[
				point
				[
					x	226.0
					y	359.0
				]
				point
				[
					x	372.0
					y	277.0
				]
				point
				[
					x	518.0
					y	359.0
				]
			]
		]
	]
	edge
	[
		source	0
		target	3
		graphics
		[
			fill	"#000000"
			Line
			[
				point
				[
					x	226.0
					y	359.0
				]
				point
				[
					x	362.0
					y	188.0
				]
				point
				[
					x	362.0
					y	188.0
				]
				point
				[
					x	664.0
					y	359.0
				]
			]
		]
	]
	edge
	[
		source	0
		target	4
		graphics
		[
			fill	"#000000"
			Line
			[
				point
				[
					x	226.0
					y	359.0
				]
				point
				[
					x	326.0
					y	126.0
				]
				point
				[
					x	326.0
					y	126.0
				]
				point
				[
					x	556.0
					y	126.0
				]
				point
				[
					x	810.0
					y	359.0
				]
			]
		]
	]
	edge
	[
		source	0
		target	5
		graphics
		[
			fill	"#000000"
			Line
			[
				point
				[
					x	226.0
					y	359.0
				]
				point
				[
					x	226.0
					y	55.0
				]
				point
				[
					x	226.0
					y	55.0
				]
				point
				[
					x	956.0
					y	55.0
				]
				point
				[
					x	956.0
					y	359.0
				]
			]
		]
	]
	edge
	[
		source	1
		target	3
		graphics
		[
			fill	"#000000"
			Line
			[
				point
				[
					x	372.0
					y	359.0
				]
				point
				[
					x	523.0
					y	304.0
				]
				point
				[
					x	664.0
					y	359.0
				]
			]
		]
	]
	edge
	[
		source	1
		target	4
		graphics
		[
			fill	"#000000"
			Line
			[
				point
				[
					x	372.0
					y	359.0
				]
				point
				[
					x	581.0
					y	204.0
				]
				point
				[
					x	810.0
					y	359.0
				]
			]
		]
	]
	edge
	[
		source	1
		target	5
		graphics
		[
			fill	"#000000"
			Line
			[
				point
				[
					x	372.0
					y	359.0
				]
				point
				[
					x	489.0
					y	181.0
				]
				point
				[
					x	874.0
					y	153.0
				]
				point
				[
					x	956.0
					y	359.0
				]
			]
		]
	]
	edge
	[
		source	2
		target	4
		graphics
		[
			fill	"#000000"
			Line
			[
				point
				[
					x	518.0
					y	359.0
				]
				point
				[
					x	659.0
					y	305.0
				]
				point
				[
					x	810.0
					y	359.0
				]
			]
		]
	]
	edge
	[
		source	2
		target	5
		graphics
		[
			fill	"#000000"
			Line
			[
				point
				[
					x	518.0
					y	359.0
				]
				point
				[
					x	743.0
					y	207.0
				]
				point
				[
					x	956.0
					y	359.0
				]
			]
		]
	]
	edge
	[
		source	3
		target	5
		graphics
		[
			fill	"#000000"
			Line
			[
				point
				[
					x	664.0
					y	359.0
				]
				point
				[
					x	805.0
					y	285.0
				]
				point
				[
					x	956.0
					y	359.0
				]
			]
		]
	]
]
