Creator	"yFiles"
Version	"2.12"
graph
[
	hierarchic	1
	label	""
	directed	1
	node
	[
		id	0
		label	"1"
		graphics
		[
			x	167.125
			y	260.1875
			w	30.0
			h	30.0
			type	"rectangle"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	1
		label	"2"
		graphics
		[
			x	209.75
			y	64.0
			w	30.0
			h	30.0
			type	"rectangle"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"2"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	2
		label	"4"
		graphics
		[
			x	305.5
			y	232.75
			w	30.0
			h	30.0
			type	"rectangle"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"4"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	3
		label	"5"
		graphics
		[
			x	328.5
			y	372.0
			w	30.0
			h	30.0
			type	"rectangle"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"5"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	4
		label	"7"
		graphics
		[
			x	484.0
			y	260.1875
			w	30.0
			h	30.0
			type	"rectangle"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"7"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	5
		label	"8"
		graphics
		[
			x	328.5
			y	181.5
			w	30.0
			h	30.0
			type	"rectangle"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"8"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	6
		label	"7"
		graphics
		[
			x	445.0
			y	64.0
			w	30.0
			h	30.0
			type	"rectangle"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"7"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	edge
	[
		source	0
		target	1
		graphics
		[
			fill	"#000000"
		]
	]
	edge
	[
		source	3
		target	0
		graphics
		[
			fill	"#000000"
		]
	]
	edge
	[
		source	0
		target	2
		graphics
		[
			fill	"#000000"
		]
	]
	edge
	[
		source	2
		target	3
		graphics
		[
			fill	"#000000"
		]
	]
	edge
	[
		source	2
		target	1
		graphics
		[
			fill	"#000000"
		]
	]
	edge
	[
		source	4
		target	3
		graphics
		[
			fill	"#000000"
		]
	]
	edge
	[
		source	4
		target	2
		graphics
		[
			fill	"#000000"
		]
	]
	edge
	[
		source	4
		target	0
		graphics
		[
			fill	"#000000"
		]
	]
	edge
	[
		source	4
		target	1
		graphics
		[
			fill	"#000000"
		]
	]
	edge
	[
		source	5
		target	2
		graphics
		[
			fill	"#000000"
		]
	]
	edge
	[
		source	5
		target	0
		graphics
		[
			fill	"#000000"
		]
	]
	edge
	[
		source	5
		target	1
		graphics
		[
			fill	"#000000"
		]
	]
	edge
	[
		source	5
		target	3
		graphics
		[
			fill	"#000000"
		]
	]
	edge
	[
		source	5
		target	4
		graphics
		[
			fill	"#000000"
		]
	]
	edge
	[
		source	1
		target	3
		graphics
		[
			fill	"#000000"
		]
	]
	edge
	[
		source	6
		target	5
		graphics
		[
			fill	"#000000"
		]
	]
	edge
	[
		source	6
		target	2
		graphics
		[
			fill	"#000000"
			Line
			[
				point
				[
					x	445.0
					y	64.0
				]
				point
				[
					x	359.0625
					y	221.25
				]
				point
				[
					x	305.5
					y	232.75
				]
			]
		]
	]
	edge
	[
		source	6
		target	4
		graphics
		[
			fill	"#000000"
		]
	]
	edge
	[
		source	6
		target	3
		graphics
		[
			fill	"#000000"
		]
	]
	edge
	[
		source	6
		target	1
		graphics
		[
			fill	"#000000"
		]
	]
	edge
	[
		source	6
		target	0
		graphics
		[
			fill	"#000000"
		]
	]
]
