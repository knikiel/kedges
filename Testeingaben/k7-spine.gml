Creator "ogdf::GraphIO::writeGML"
graph [
  directed 1
  node [
    id 0
    label "0"
    graphics [
      x 500.0000000
      y -1100.000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 1
    label "1"
    graphics [
      x 750.0000000
      y -1100.000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 2
    label "2"
    graphics [
      x 1000.000000
      y -1100.000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 3
    label "3"
    graphics [
      x 1250.000000
      y -1100.000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 4
    label "4"
    graphics [
      x 1500.000000
      y -1100.000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 5
    label "5"
    graphics [
      x 1750.000000
      y -1100.000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 6
    label "6"
    graphics [
      x 2000.000000
      y -1100.000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  edge [
    source 0
    target 6
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 500.0000000 y -1100.000000 ]
        point [ x 500.0000000 y -0.0000000000 ]
        point [ x 2000.000000 y -0.0000000000 ]
        point [ x 2000.000000 y -1100.000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 6
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 750.0000000 y -1100.000000 ]
        point [ x 1400.000000 y -250.0000000 ]
        point [ x 2000.000000 y -1100.000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 6
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1000.000000 y -1100.000000 ]
        point [ x 1400.000000 y -400.0000000 ]
        point [ x 2000.000000 y -1100.000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 6
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1250.000000 y -1100.000000 ]
        point [ x 1650.000000 y -1650.000000 ]
        point [ x 2000.000000 y -1100.000000 ]
      ]
    ]
  ]
  edge [
    source 4
    target 6
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1500.000000 y -1100.000000 ]
        point [ x 1750.000000 y -1300.000000 ]
        point [ x 2000.000000 y -1100.000000 ]
      ]
    ]
  ]
  edge [
    source 5
    target 6
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 5
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 500.0000000 y -1100.000000 ]
        point [ x 1400.000000 y -150.0000000 ]
        point [ x 1750.000000 y -1100.000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 5
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 750.0000000 y -1100.000000 ]
        point [ x 1400.000000 y -500.0000000 ]
        point [ x 1750.000000 y -1100.000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 5
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1000.000000 y -1100.000000 ]
        point [ x 1500.000000 y -750.0000000 ]
        point [ x 1750.000000 y -1100.000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 5
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1250.000000 y -1100.000000 ]
        point [ x 1500.000000 y -950.0000000 ]
        point [ x 1750.000000 y -1100.000000 ]
      ]
    ]
  ]
  edge [
    source 4
    target 5
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 4
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 500.0000000 y -1100.000000 ]
        point [ x 1000.000000 y -2000.000000 ]
        point [ x 1500.000000 y -1100.000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 4
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 750.0000000 y -1100.000000 ]
        point [ x 1350.000000 y -750.0000000 ]
        point [ x 1500.000000 y -1100.000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 4
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1000.000000 y -1100.000000 ]
        point [ x 1250.000000 y -950.0000000 ]
        point [ x 1500.000000 y -1100.000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 4
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 3
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 500.0000000 y -1100.000000 ]
        point [ x 900.0000000 y -1650.000000 ]
        point [ x 1250.000000 y -1100.000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 3
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 750.0000000 y -1100.000000 ]
        point [ x 1000.000000 y -1300.000000 ]
        point [ x 1250.000000 y -1100.000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 3
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 2
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 500.0000000 y -1100.000000 ]
        point [ x 750.0000000 y -1300.000000 ]
        point [ x 1000.000000 y -1100.000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 2
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 1
    graphics [
      type "line"
      arrow "last"
    ]
  ]
]
