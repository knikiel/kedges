Creator "ogdf::GraphIO::writeGML"
graph [
  directed 1
  node [
    id 0
    label "0"
    graphics [
      x 650.0000000
      y -200.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 1
    label "1"
    graphics [
      x 850.0000000
      y -400.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 2
    label "2"
    graphics [
      x 850.0000000
      y -650.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 3
    label "3"
    graphics [
      x 650.0000000
      y -850.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 4
    label "4"
    graphics [
      x 400.0000000
      y -850.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 5
    label "5"
    graphics [
      x 200.0000000
      y -650.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 6
    label "6"
    graphics [
      x 200.0000000
      y -400.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 7
    label "7"
    graphics [
      x 400.0000000
      y -200.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  edge [
    source 0
    target 7
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 1
    target 7
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 2
    target 7
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 3
    target 7
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 4
    target 7
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 400.0000000 y -850.0000000 ]
        point [ x -50.00000000 y -700.0000000 ]
        point [ x -50.00000000 y -200.0000000 ]
        point [ x 400.0000000 y -200.0000000 ]
      ]
    ]
  ]
  edge [
    source 5
    target 7
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 200.0000000 y -650.0000000 ]
        point [ x 85.00000000 y -350.0000000 ]
        point [ x 400.0000000 y -200.0000000 ]
      ]
    ]
  ]
  edge [
    source 6
    target 7
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 6
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 650.0000000 y -200.0000000 ]
        point [ x 300.0000000 y -100.0000000 ]
        point [ x 200.0000000 y -400.0000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 6
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 2
    target 6
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 3
    target 6
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 4
    target 6
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 5
    target 6
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 5
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 650.0000000 y -200.0000000 ]
        point [ x 450.0000000 y -40.00000000 ]
        point [ x 20.00000000 y -40.00000000 ]
        point [ x 20.00000000 y -570.0000000 ]
        point [ x 200.0000000 y -650.0000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 5
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 850.0000000 y -400.0000000 ]
        point [ x 1050.000000 y -400.0000000 ]
        point [ x 1050.000000 y -1150.000000 ]
        point [ x 400.0000000 y -1150.000000 ]
        point [ x 200.0000000 y -650.0000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 5
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 3
    target 5
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 4
    target 5
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 4
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 650.0000000 y -200.0000000 ]
        point [ x 650.0000000 y 50.00000000 ]
        point [ x -100.0000000 y 50.00000000 ]
        point [ x -100.0000000 y -850.0000000 ]
        point [ x 400.0000000 y -850.0000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 4
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 850.0000000 y -400.0000000 ]
        point [ x 1000.000000 y -520.0000000 ]
        point [ x 1000.000000 y -1060.000000 ]
        point [ x 400.0000000 y -1060.000000 ]
        point [ x 400.0000000 y -850.0000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 4
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 850.0000000 y -650.0000000 ]
        point [ x 850.0000000 y -980.0000000 ]
        point [ x 400.0000000 y -850.0000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 4
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 3
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 650.0000000 y -200.0000000 ]
        point [ x 1140.000000 y -200.0000000 ]
        point [ x 1140.000000 y -850.0000000 ]
        point [ x 650.0000000 y -850.0000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 3
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 850.0000000 y -400.0000000 ]
        point [ x 940.0000000 y -750.0000000 ]
        point [ x 650.0000000 y -850.0000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 3
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 2
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 1
    target 2
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 1
    graphics [
      type "line"
      arrow "last"
    ]
  ]
]
