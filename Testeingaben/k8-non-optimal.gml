Creator	"yFiles"
Version	"2.12"
graph
[
	hierarchic	1
	label	""
	directed	1
	node
	[
		id	0
		label	"1"
		graphics
		[
			x	284.0
			y	340.0
			w	30.0
			h	30.0
			type	"rectangle"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	1
		label	"2"
		graphics
		[
			x	442.5
			y	341.0
			w	30.0
			h	30.0
			type	"rectangle"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"2"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	2
		label	"3"
		graphics
		[
			x	601.0
			y	341.0
			w	30.0
			h	30.0
			type	"rectangle"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"3"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	3
		label	"4"
		graphics
		[
			x	759.5
			y	341.0
			w	30.0
			h	30.0
			type	"rectangle"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"4"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	4
		label	"5"
		graphics
		[
			x	918.0
			y	341.0
			w	30.0
			h	30.0
			type	"rectangle"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"5"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	5
		label	"6"
		graphics
		[
			x	1076.5
			y	341.0
			w	30.0
			h	30.0
			type	"rectangle"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"6"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	6
		label	"7"
		graphics
		[
			x	1235.0
			y	341.0
			w	30.0
			h	30.0
			type	"rectangle"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"7"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	7
		label	"8"
		graphics
		[
			x	1393.5
			y	341.0
			w	30.0
			h	30.0
			type	"rectangle"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"8"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	edge
	[
		source	0
		target	1
		graphics
		[
			fill	"#000000"
		]
	]
	edge
	[
		source	1
		target	2
		graphics
		[
			fill	"#000000"
		]
	]
	edge
	[
		source	2
		target	3
		graphics
		[
			fill	"#000000"
		]
	]
	edge
	[
		source	3
		target	4
		graphics
		[
			fill	"#000000"
		]
	]
	edge
	[
		source	4
		target	5
		graphics
		[
			fill	"#000000"
		]
	]
	edge
	[
		source	5
		target	6
		graphics
		[
			fill	"#000000"
		]
	]
	edge
	[
		source	6
		target	7
		graphics
		[
			fill	"#000000"
		]
	]
	edge
	[
		source	0
		target	7
		graphics
		[
			fill	"#000000"
			Line
			[
				point
				[
					x	284.0
					y	340.0
				]
				point
				[
					x	284.0
					y	-163.75
				]
				point
				[
					x	1393.5
					y	-163.75
				]
				point
				[
					x	1393.5
					y	341.0
				]
			]
		]
	]
	edge
	[
		source	1
		target	4
		graphics
		[
			fill	"#000000"
			Line
			[
				point
				[
					x	442.5
					y	341.0
				]
				point
				[
					x	678.75
					y	111.25
				]
				point
				[
					x	918.0
					y	341.0
				]
			]
		]
	]
	edge
	[
		source	3
		target	6
		graphics
		[
			fill	"#000000"
			Line
			[
				point
				[
					x	759.5
					y	341.0
				]
				point
				[
					x	1013.75
					y	115.0
				]
				point
				[
					x	1235.0
					y	341.0
				]
			]
		]
	]
	edge
	[
		source	4
		target	7
		graphics
		[
			fill	"#000000"
			Line
			[
				point
				[
					x	918.0
					y	341.0
				]
				point
				[
					x	1228.75
					y	105.0
				]
				point
				[
					x	1393.5
					y	341.0
				]
			]
		]
	]
	edge
	[
		source	4
		target	6
		graphics
		[
			fill	"#000000"
			Line
			[
				point
				[
					x	918.0
					y	341.0
				]
				point
				[
					x	1116.25
					y	258.75
				]
				point
				[
					x	1235.0
					y	341.0
				]
			]
		]
	]
	edge
	[
		source	4
		target	2
		graphics
		[
			fill	"#000000"
			Line
			[
				point
				[
					x	918.0
					y	341.0
				]
				point
				[
					x	711.25
					y	216.25
				]
				point
				[
					x	601.0
					y	341.0
				]
			]
		]
	]
	edge
	[
		source	0
		target	2
		graphics
		[
			fill	"#000000"
			Line
			[
				point
				[
					x	284.0
					y	340.0
				]
				point
				[
					x	433.4375
					y	202.5
				]
				point
				[
					x	601.0
					y	341.0
				]
			]
		]
	]
	edge
	[
		source	0
		target	3
		graphics
		[
			fill	"#000000"
			Line
			[
				point
				[
					x	284.0
					y	340.0
				]
				point
				[
					x	464.6875
					y	115.0
				]
				point
				[
					x	759.5
					y	341.0
				]
			]
		]
	]
	edge
	[
		source	0
		target	4
		graphics
		[
			fill	"#000000"
			Line
			[
				point
				[
					x	284.0
					y	340.0
				]
				point
				[
					x	488.4375
					y	7.5
				]
				point
				[
					x	488.4375
					y	7.5
				]
				point
				[
					x	788.4375
					y	46.25
				]
				point
				[
					x	918.0
					y	341.0
				]
			]
		]
	]
	edge
	[
		source	2
		target	5
		graphics
		[
			fill	"#000000"
			Line
			[
				point
				[
					x	601.0
					y	341.0
				]
				point
				[
					x	664.6875
					y	177.5
				]
				point
				[
					x	664.6875
					y	177.5
				]
				point
				[
					x	902.1875
					y	130.0
				]
				point
				[
					x	1076.5
					y	341.0
				]
			]
		]
	]
	edge
	[
		source	0
		target	5
		graphics
		[
			fill	"#000000"
			Line
			[
				point
				[
					x	284.0
					y	340.0
				]
				point
				[
					x	512.1875
					y	725.9375
				]
				point
				[
					x	900.3125
					y	643.75
				]
				point
				[
					x	1076.5
					y	341.0
				]
			]
		]
	]
	edge
	[
		source	0
		target	6
		graphics
		[
			fill	"#000000"
			Line
			[
				point
				[
					x	284.0
					y	340.0
				]
				point
				[
					x	429.6875
					y	-86.25
				]
				point
				[
					x	1135.9375
					y	20.0
				]
				point
				[
					x	1235.0
					y	341.0
				]
			]
		]
	]
	edge
	[
		source	1
		target	3
		graphics
		[
			fill	"#000000"
			Line
			[
				point
				[
					x	442.5
					y	341.0
				]
				point
				[
					x	601.0
					y	486.0
				]
				point
				[
					x	759.5
					y	341.0
				]
			]
		]
	]
	edge
	[
		source	1
		target	5
		graphics
		[
			fill	"#000000"
			Line
			[
				point
				[
					x	442.5
					y	341.0
				]
				point
				[
					x	558.4375
					y	516.0
				]
				point
				[
					x	558.4375
					y	516.0
				]
				point
				[
					x	782.1875
					y	526.0
				]
				point
				[
					x	1076.5
					y	341.0
				]
			]
		]
	]
	edge
	[
		source	1
		target	6
		graphics
		[
			fill	"#000000"
			Line
			[
				point
				[
					x	442.5
					y	341.0
				]
				point
				[
					x	540.9375
					y	604.75
				]
				point
				[
					x	540.9375
					y	604.75
				]
				point
				[
					x	918.0
					y	711.0
				]
				point
				[
					x	1194.6875
					y	618.5
				]
				point
				[
					x	1235.0
					y	341.0
				]
			]
		]
	]
	edge
	[
		source	1
		target	7
		graphics
		[
			fill	"#000000"
			Line
			[
				point
				[
					x	442.5
					y	341.0
				]
				point
				[
					x	587.5
					y	-110.0625
				]
				point
				[
					x	1290.625
					y	-100.6875
				]
				point
				[
					x	1393.5
					y	341.0
				]
			]
		]
	]
	edge
	[
		source	6
		target	2
		graphics
		[
			fill	"#000000"
			Line
			[
				point
				[
					x	1235.0
					y	341.0
				]
				point
				[
					x	1025.0
					y	49.3125
				]
				point
				[
					x	1025.0
					y	49.3125
				]
				point
				[
					x	581.25
					y	80.5625
				]
				point
				[
					x	531.25
					y	211.8125
				]
				point
				[
					x	601.0
					y	341.0
				]
			]
		]
	]
	edge
	[
		source	5
		target	7
		graphics
		[
			fill	"#000000"
			Line
			[
				point
				[
					x	1076.5
					y	341.0
				]
				point
				[
					x	1243.75
					y	242.4375
				]
				point
				[
					x	1393.5
					y	341.0
				]
			]
		]
	]
	edge
	[
		source	5
		target	3
		graphics
		[
			fill	"#000000"
			Line
			[
				point
				[
					x	1076.5
					y	341.0
				]
				point
				[
					x	943.75
					y	248.6875
				]
				point
				[
					x	759.5
					y	341.0
				]
			]
		]
	]
	edge
	[
		source	7
		target	3
		graphics
		[
			fill	"#000000"
			Line
			[
				point
				[
					x	1393.5
					y	341.0
				]
				point
				[
					x	1271.875
					y	547.125
				]
				point
				[
					x	759.5
					y	341.0
				]
			]
		]
	]
	edge
	[
		source	2
		target	7
		graphics
		[
			fill	"#000000"
			Line
			[
				point
				[
					x	601.0
					y	341.0
				]
				point
				[
					x	601.0
					y	420.5625
				]
				point
				[
					x	1304.6875
					y	762.75
				]
				point
				[
					x	1393.5
					y	341.0
				]
			]
		]
	]
]
