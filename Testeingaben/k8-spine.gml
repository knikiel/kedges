Creator "ogdf::GraphIO::writeGML"
graph [
  directed 1
  node [
    id 0
    label "0"
    graphics [
      x 0.0000000000
      y -0.0000000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 1
    label "1"
    graphics [
      x 215.0000000
      y -0.0000000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 2
    label "2"
    graphics [
      x 430.0000000
      y -0.0000000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 3
    label "3"
    graphics [
      x 645.0000000
      y -0.0000000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 4
    label "4"
    graphics [
      x 860.0000000
      y -0.0000000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 5
    label "5"
    graphics [
      x 1075.000000
      y -0.0000000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 6
    label "6"
    graphics [
      x 1290.000000
      y -0.0000000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 7
    label "7"
    graphics [
      x 1505.000000
      y -0.0000000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  edge [
    source 0
    target 7
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 0.0000000000 y -0.0000000000 ]
        point [ x 0.0000000000 y 740.0000000 ]
        point [ x 1505.000000 y 740.0000000 ]
        point [ x 1505.000000 y -0.0000000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 7
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 215.0000000 y -0.0000000000 ]
        point [ x 0.0000000000 y -285.0000000 ]
        point [ x 0.0000000000 y -750.0000000 ]
        point [ x 1505.000000 y -750.0000000 ]
        point [ x 1505.000000 y -0.0000000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 7
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 430.0000000 y -0.0000000000 ]
        point [ x 140.0000000 y -380.0000000 ]
        point [ x 140.0000000 y -660.0000000 ]
        point [ x 1380.000000 y -660.0000000 ]
        point [ x 1505.000000 y -0.0000000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 7
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 645.0000000 y -0.0000000000 ]
        point [ x 660.0000000 y -560.0000000 ]
        point [ x 1250.000000 y -560.0000000 ]
        point [ x 1505.000000 y -0.0000000000 ]
      ]
    ]
  ]
  edge [
    source 4
    target 7
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 860.0000000 y -0.0000000000 ]
        point [ x 1235.000000 y -360.0000000 ]
        point [ x 1505.000000 y -0.0000000000 ]
      ]
    ]
  ]
  edge [
    source 5
    target 7
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1075.000000 y -0.0000000000 ]
        point [ x 1375.000000 y 200.0000000 ]
        point [ x 1505.000000 y -0.0000000000 ]
      ]
    ]
  ]
  edge [
    source 6
    target 7
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 6
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 0.0000000000 y -0.0000000000 ]
        point [ x 200.0000000 y 645.0000000 ]
        point [ x 1290.000000 y 645.0000000 ]
        point [ x 1290.000000 y -0.0000000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 6
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 215.0000000 y -0.0000000000 ]
        point [ x 415.0000000 y 410.0000000 ]
        point [ x 1145.000000 y 410.0000000 ]
        point [ x 1290.000000 y -0.0000000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 6
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 430.0000000 y -0.0000000000 ]
        point [ x 430.0000000 y -400.0000000 ]
        point [ x 1165.000000 y -400.0000000 ]
        point [ x 1290.000000 y -0.0000000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 6
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 645.0000000 y -0.0000000000 ]
        point [ x 780.0000000 y -290.0000000 ]
        point [ x 1070.000000 y -290.0000000 ]
        point [ x 1290.000000 y -0.0000000000 ]
      ]
    ]
  ]
  edge [
    source 4
    target 6
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 860.0000000 y -0.0000000000 ]
        point [ x 1105.000000 y -120.0000000 ]
        point [ x 1290.000000 y -0.0000000000 ]
      ]
    ]
  ]
  edge [
    source 5
    target 6
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 5
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 0.0000000000 y -0.0000000000 ]
        point [ x 335.0000000 y 560.0000000 ]
        point [ x 1075.000000 y 560.0000000 ]
        point [ x 1075.000000 y -0.0000000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 5
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 215.0000000 y -0.0000000000 ]
        point [ x 480.0000000 y 350.0000000 ]
        point [ x 990.0000000 y 350.0000000 ]
        point [ x 1075.000000 y -0.0000000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 5
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 430.0000000 y -0.0000000000 ]
        point [ x 790.0000000 y 250.0000000 ]
        point [ x 1075.000000 y -0.0000000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 5
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 645.0000000 y -0.0000000000 ]
        point [ x 890.0000000 y -170.0000000 ]
        point [ x 1075.000000 y -0.0000000000 ]
      ]
    ]
  ]
  edge [
    source 4
    target 5
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 4
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 0.0000000000 y -0.0000000000 ]
        point [ x 380.0000000 y 480.0000000 ]
        point [ x 860.0000000 y -0.0000000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 4
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 215.0000000 y -0.0000000000 ]
        point [ x 555.0000000 y 240.0000000 ]
        point [ x 860.0000000 y -0.0000000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 4
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 430.0000000 y -0.0000000000 ]
        point [ x 645.0000000 y 70.00000000 ]
        point [ x 860.0000000 y -0.0000000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 4
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 3
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 0.0000000000 y -0.0000000000 ]
        point [ x 335.0000000 y -360.0000000 ]
        point [ x 645.0000000 y -0.0000000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 3
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 215.0000000 y -0.0000000000 ]
        point [ x 515.0000000 y 105.0000000 ]
        point [ x 645.0000000 y -0.0000000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 3
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 2
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 0.0000000000 y -0.0000000000 ]
        point [ x 235.0000000 y -115.0000000 ]
        point [ x 430.0000000 y -0.0000000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 2
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 1
    graphics [
      type "line"
      arrow "last"
    ]
  ]
]
