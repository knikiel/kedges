Creator "ogdf::GraphIO::writeGML"
graph [
  directed 1
  node [
    id 0
    label "0"
    graphics [
      x 1415.000000
      y -1365.000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 1
    label "1"
    graphics [
      x 1615.000000
      y -1015.000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 2
    label "2"
    graphics [
      x 1540.000000
      y -615.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 3
    label "3"
    graphics [
      x 1240.000000
      y -365.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 4
    label "4"
    graphics [
      x 840.0000000
      y -365.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 5
    label "5"
    graphics [
      x 540.0000000
      y -615.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 6
    label "6"
    graphics [
      x 465.0000000
      y -1015.000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 7
    label "7"
    graphics [
      x 665.0000000
      y -1365.000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 8
    label "8"
    graphics [
      x 1040.000000
      y -1490.000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  edge [
    source 0
    target 8
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 1
    target 8
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 2
    target 8
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 3
    target 8
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 4
    target 8
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 840.0000000 y -365.0000000 ]
        point [ x 190.0000000 y -530.0000000 ]
        point [ x 190.0000000 y -1700.000000 ]
        point [ x 1040.000000 y -1700.000000 ]
        point [ x 1040.000000 y -1490.000000 ]
      ]
    ]
  ]
  edge [
    source 5
    target 8
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 540.0000000 y -615.0000000 ]
        point [ x 340.0000000 y -940.0000000 ]
        point [ x 340.0000000 y -1560.000000 ]
        point [ x 680.0000000 y -1560.000000 ]
        point [ x 1040.000000 y -1490.000000 ]
      ]
    ]
  ]
  edge [
    source 6
    target 8
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 465.0000000 y -1015.000000 ]
        point [ x 465.0000000 y -1490.000000 ]
        point [ x 1040.000000 y -1490.000000 ]
      ]
    ]
  ]
  edge [
    source 7
    target 8
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 7
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 1
    target 7
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 2
    target 7
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 3
    target 7
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 4
    target 7
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 5
    target 7
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 540.0000000 y -615.0000000 ]
        point [ x 390.0000000 y -1015.000000 ]
        point [ x 575.0000000 y -1365.000000 ]
        point [ x 665.0000000 y -1365.000000 ]
      ]
    ]
  ]
  edge [
    source 6
    target 7
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 6
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1415.000000 y -1365.000000 ]
        point [ x 1290.000000 y -1640.000000 ]
        point [ x 390.0000000 y -1640.000000 ]
        point [ x 390.0000000 y -1145.000000 ]
        point [ x 465.0000000 y -1015.000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 6
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 2
    target 6
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 3
    target 6
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 4
    target 6
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 5
    target 6
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 5
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1415.000000 y -1365.000000 ]
        point [ x 1415.000000 y -1800.000000 ]
        point [ x 120.0000000 y -1800.000000 ]
        point [ x 120.0000000 y -615.0000000 ]
        point [ x 540.0000000 y -615.0000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 5
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1615.000000 y -1015.000000 ]
        point [ x 2060.000000 y -1015.000000 ]
        point [ x 2060.000000 y -20.00000000 ]
        point [ x 540.0000000 y -20.00000000 ]
        point [ x 540.0000000 y -615.0000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 5
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 3
    target 5
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 4
    target 5
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 4
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1415.000000 y -1365.000000 ]
        point [ x 1990.000000 y -1365.000000 ]
        point [ x 1990.000000 y -80.00000000 ]
        point [ x 840.0000000 y -80.00000000 ]
        point [ x 840.0000000 y -365.0000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 4
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1615.000000 y -1015.000000 ]
        point [ x 1760.000000 y -900.0000000 ]
        point [ x 1760.000000 y -160.0000000 ]
        point [ x 1035.000000 y -160.0000000 ]
        point [ x 840.0000000 y -365.0000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 4
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1540.000000 y -615.0000000 ]
        point [ x 1385.000000 y -220.0000000 ]
        point [ x 840.0000000 y -365.0000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 4
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 3
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1415.000000 y -1365.000000 ]
        point [ x 1915.000000 y -1260.000000 ]
        point [ x 1915.000000 y -365.0000000 ]
        point [ x 1240.000000 y -365.0000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 3
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1615.000000 y -1015.000000 ]
        point [ x 1615.000000 y -500.0000000 ]
        point [ x 1240.000000 y -365.0000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 3
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 2
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1415.000000 y -1365.000000 ]
        point [ x 1835.000000 y -1180.000000 ]
        point [ x 1835.000000 y -615.0000000 ]
        point [ x 1540.000000 y -615.0000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 2
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 1
    graphics [
      type "line"
      arrow "last"
    ]
  ]
]
