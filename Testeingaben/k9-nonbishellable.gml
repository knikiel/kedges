Creator "ogdf::GraphIO::writeGML"
graph [
  directed 1
  node [
    id 0
    label "0"
    graphics [
      x 1510.000000
      y -390.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 1
    label "1"
    graphics [
      x 1190.000000
      y -640.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 2
    label "2"
    graphics [
      x 1040.000000
      y -1400.000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 3
    label "3"
    graphics [
      x 1910.000000
      y -230.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 4
    label "4"
    graphics [
      x 980.0000000
      y -1810.000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 5
    label "5"
    graphics [
      x 70.00000000
      y -240.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 6
    label "6"
    graphics [
      x 390.0000000
      y -490.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 7
    label "7"
    graphics [
      x 770.0000000
      y -640.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  node [
    id 8
    label "8"
    graphics [
      x 980.0000000
      y -990.0000000
      w 20.00000000
      h 20.00000000
      type "rectangle"
    ]
  ]
  edge [
    source 0
    target 8
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1510.000000 y -390.0000000 ]
        point [ x 1010.000000 y -610.0000000 ]
        point [ x 980.0000000 y -990.0000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 8
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 2
    target 8
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 3
    target 8
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 4
    target 8
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 5
    target 8
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 70.00000000 y -240.0000000 ]
        point [ x 450.0000000 y -700.0000000 ]
        point [ x 750.0000000 y -930.0000000 ]
        point [ x 980.0000000 y -990.0000000 ]
      ]
    ]
  ]
  edge [
    source 6
    target 8
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 390.0000000 y -490.0000000 ]
        point [ x 700.0000000 y -810.0000000 ]
        point [ x 980.0000000 y -990.0000000 ]
      ]
    ]
  ]
  edge [
    source 7
    target 8
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 7
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1510.000000 y -390.0000000 ]
        point [ x 1030.000000 y -500.0000000 ]
        point [ x 770.0000000 y -640.0000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 7
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 2
    target 7
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1040.000000 y -1400.000000 ]
        point [ x 1090.000000 y -970.0000000 ]
        point [ x 770.0000000 y -640.0000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 7
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1910.000000 y -230.0000000 ]
        point [ x 1010.000000 y -410.0000000 ]
        point [ x 770.0000000 y -640.0000000 ]
      ]
    ]
  ]
  edge [
    source 4
    target 7
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 5
    target 7
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 70.00000000 y -240.0000000 ]
        point [ x 630.0000000 y -510.0000000 ]
        point [ x 770.0000000 y -640.0000000 ]
      ]
    ]
  ]
  edge [
    source 6
    target 7
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 6
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 1
    target 6
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1190.000000 y -640.0000000 ]
        point [ x 830.0000000 y -800.0000000 ]
        point [ x 390.0000000 y -490.0000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 6
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 3
    target 6
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1910.000000 y -230.0000000 ]
        point [ x 0.0000000000 y -0.0000000000 ]
        point [ x 0.0000000000 y -490.0000000 ]
        point [ x 390.0000000 y -490.0000000 ]
      ]
    ]
  ]
  edge [
    source 4
    target 6
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 5
    target 6
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 5
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 1
    target 5
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 2
    target 5
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1040.000000 y -1400.000000 ]
        point [ x 1450.000000 y -1950.000000 ]
        point [ x 70.00000000 y -1950.000000 ]
        point [ x 70.00000000 y -240.0000000 ]
      ]
    ]
  ]
  edge [
    source 3
    target 5
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 4
    target 5
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 4
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1510.000000 y -390.0000000 ]
        point [ x 1530.000000 y -0.0000000000 ]
        point [ x 2000.000000 y -250.0000000 ]
        point [ x 980.0000000 y -1810.000000 ]
      ]
    ]
  ]
  edge [
    source 1
    target 4
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1190.000000 y -640.0000000 ]
        point [ x 1250.000000 y -1200.000000 ]
        point [ x 980.0000000 y -1810.000000 ]
      ]
    ]
  ]
  edge [
    source 2
    target 4
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 3
    target 4
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 3
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 1
    target 3
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 2
    target 3
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 0
    target 2
    graphics [
      type "line"
      arrow "last"
    ]
  ]
  edge [
    source 1
    target 2
    graphics [
      type "line"
      arrow "last"
      Line [
        point [ x 1190.000000 y -640.0000000 ]
        point [ x 1150.000000 y -1050.000000 ]
        point [ x 1040.000000 y -1400.000000 ]
      ]
    ]
  ]
  edge [
    source 0
    target 1
    graphics [
      type "line"
      arrow "last"
    ]
  ]
]
