CONFIG += staticlib
TEMPLATE = lib
CONFIG += debug_and_release
QT -= gui

win32{
    CONFIG += debug_and_release
    CONFIG(debug, debug|release){
        DEFINES += OGDF_DEBUG
        TARGET = commond
    } else {
        TARGET = common
    } 
}

linux-g++{
    contains(CXXVERSION, "4.6") {
        QMAKE_CXXFLAGS += -std=c++0x
    } else {
        QMAKE_CXXFLAGS += -std=c++11
    }
    CONFIG(debug, debug|release){
        OBJECTS_DIR = "debug"
        DEFINES += OGDF_DEBUG
        TARGET = commond
    } else {
        OBJECTS_DIR = "release"
        TARGET = common
    } 

}

macx-g++ | macx-clang{
    QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.9
    QMAKE_CXXFLAGS += -std=c++11

    OBJECTS_DIR = "release"
}

OGDF_INCLUDE = ../../OGDF/include

INCLUDEPATH += include/
INCLUDEPATH += $$OGDF_INCLUDE 
HEADERS += include/*
SOURCES += source/*
