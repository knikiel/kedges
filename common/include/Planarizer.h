#ifndef PLANARIZER_H
#define PLARANIZER_H

#include <list>
#include <vector>

#include "ogdf/basic/Graph.h"

#include "common_types.h"
#include "drawing_types.h"

/// Erzeugt und sammelt alle Segmente einer urspr�nglich unbearbeiteten Zeichnung, um sie zu planarisieren.
class Planarizer {
public:
    /// Sammelt Knoteninformation f�r interne Berechnungen.
    struct Vertex {
        enum class Type {
            Node, ///! echter Knoten
            Bend, ///! Kantenknick
            Crossing, ///! Kreuzungsknoten
        };

        Type type;
        int id;
        ogdf::node vertex;

        Vertex(Type type, int id, ogdf::node vertex);
        Vertex(const Vertex& other);
        Vertex();

        bool operator==(const Vertex &r) const;
        bool operator<(const Vertex &r) const;
        bool operator!=(const Vertex &r) const;
    };


    /// Sammelt Segmentinformationen f�r interne Berechnungen
    struct Segment {
        Vertex u;
        Vertex v;
        ogdf::edge e;
        
        Segment(Vertex u, Vertex v, ogdf::edge e);
        Segment(const Segment& other);
        Segment();

        bool operator==(const Segment &other) const;
        bool operator!=(const Segment &other) const;
        bool operator<(const Segment &other) const;
    };

    /// Informationen �ber eine durchzuf�hrende Kreuzung.
    /// Gibt an, welche Kante an welcher Stelle geschnitten wird.
    struct CrossingInfo {
        Segment seg;
        Point pos;
        // seg schneidet Referenz e "von unten"
        //    ^ seg
        // ------> e
        //    | true
        bool bottom_up;
    };

    Planarizer(const UnprocessedDrawing &drawing, ogdf::PlanRep* planrep);

    /// Position eines Knotens.
    Point point_position(Vertex p) const;

    /// Gibt alle Segmente (inkl. Kreuzungspositionen) sortiert zur�ck, die angegebenes Segment kreuzen.
    std::vector<CrossingInfo> crossed_segments(Segment seg);

    /// Gibt alle Segmente zwischen 2 echten oder Kreuzungs-Knoten zur�ck.
    std::list<Segment> segments(node_id u, node_id v) const;

    /// F�ge eine neue Kreuzung ein, indem Segmente geteilt werden.
    /// Gibt Kante eines hinteren Segments zur�ck.
    ogdf::edge add_crossing(Segment seg1, Segment seg2, Point pos, bool top_down);

    /// Speichert die interne Repr�sentation der Segmente als planarisierte Zeichnung ab.
    void fill_planarized_drawing(PlanarizedDrawing* drawing);

private:
    /// Segmente der in der Ordnung sp�teren Kanten.
    std::vector<Segment> other_unprocessed_segments(node_id u, node_id v);

    /// Gibt Kreuzungs- oder echten Knoten vor Knick zur�ck; au�erdem alle Knicke auf dem Weg.
    std::pair<ogdf::node, std::vector<Point>> find_real_node_before(Segment seg);

    /// Gibt Kreuzungs- oder echten Knoten nach Knick zur�ck; au�erdem alle Knicke auf dem Weg.
    std::pair<ogdf::node, std::vector<Point>> find_real_node_after(Segment seg);


    /// Berechnet, ob und wie sich zwei Segmente kreuzen.
    /// Falls ja, gilt 0 < ret.first < 1 (entspricht skalierter Positionierung im ersten Segment)
    /// und ret.second erh�lt Position der Kreuzung.
    std::pair<double, Point> are_crossing(Segment seg, Segment other_seg);

    ogdf::PlanRep* m_planrep;
    std::map<Vertex, Point> m_point_to_position; // Positionen aller Punkte (auch zus�tzliche)
    std::map<Segment, node_pair> m_segment_to_end_vertices; // weist Segmenten die Endknoten ihrer Kante zu
    edge_map<std::list<Segment>> m_segments_map; // geordnete Folge der Segmente zu einer Kante (als Endknoten einer echten Kante)

    int m_num_vertices;
    int m_num_nodes;
    node_id m_curr_id_bending;
};

#endif // PLANARIZER_H