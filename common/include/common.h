#ifndef COMMON_H
#define COMMON_H

#include "ogdf/basic/Graph.h"
#include "ogdf/planarity/PlanRep.h"

#include "common_types.h"
#include "drawing_types.h"

//#include "../gui/include/MessageSystem.h"

#include <cmath>

// forward declarations
class UnprocessedDrawing;
class PlanarizedDrawing;

using namespace ogdf;

/// Makro fürs durchlaufen aller Adjazenzen eines Faces
#define forall_adjEdges(other_adj, first_adj, block) \
    (other_adj) = (first_adj); \
    do { \
        block \
        (other_adj) = (other_adj)->faceCycleSucc(); \
    } while ((other_adj) != (first_adj));


/// Berechnet die Mindestanzahl der Kreuzungen nach der Harary-Hill-Vermutung.
int harary_hill_crossings(int num_vertices);

/// Zählt die Anzahl der k-Kanten in info.
void count_num_k_edges(const KEdgeInformation& info, std::vector<int>* num_kedges);

/// Zählt die Anzahl der k-Kanten in info mit Beachtung der Vorzeichen.
void count_num_signed_k_edges(const KEdgeInformation& info, std::vector<int>* minus_numbers, std::vector<int> *plus_numbers, std::vector<int> *both_numbers);

/// Berechnet die beiden Seiten der mit type angegebenen Ungleichung.
KEdgeInequality test_single_condition(KEdgeSumType type, const std::vector<int>& num_k_edges, int k);

/// Überprüft, ob die mit type angegebene Bedingung für alle k erfüllt ist.
bool is_no_condition_violated(KEdgeSumType type, const std::vector<int>& num_k_edges);

/// Zählt die Anzahl der verletzten Bedingungen des angegeben Typs.
int count_violated_conditions(int num_vertices, KEdgeSumType type, const std::vector<int>& num_k_edges);

/// Berechnet das k (einschließlich), bis zu welchem alle Summenbedingungen erfüllt sein müssen.
int compute_relevant_upper_k(int num_vertices);

/// Der Algorithmus für das Berechnen der k-Kanten.
KEdgeInformation compute_k_numbers(PlanRep& planrep, adjEntry ref_face_adjacency);

/// Findet alle per ref_faces angegebenen Adjazenzen in planrep.
/// Falls keine angegeben sind, wird eine Adjazenz pro Face gesucht.
ErrorInfo search_for_adj_entries(const PlanRep& planrep,
    const std::vector<node_pair>& ref_faces, std::vector<adjEntry>* adj_entries);

/// Findet alle Flächen in der Einbettung und speichert sie in faces.
void search_for_faces(const ConstCombinatorialEmbedding& embedding, std::vector<face>* faces);

/// Konstruiert einen planarisierten Graphen aus einer Zeichnung (Knotenposition + Knickpositionen).
/// Der Graph graph wird mit den eingelesenen Knoten und Kanten gefüllt.
/// Speichert die planarisierte Zeichnung im planarized.
ogdf::PlanRep* planarize(const UnprocessedDrawing& drawing,
    ogdf::Graph* graph, PlanarizedDrawing* planarized);

/// Gibt die gewünschte Adjazenz aus.
/// Falls sie nicht gefunden werden kann, ist Ausgabe "nullptr".
ogdf::adjEntry search_adjEntry(const PlanRep& plan_rep, node_pair e_uv);

/// Berechnet den Winkel der x-Achse zum Vektor e.
double angle_to_x_axis(Point e_u, Point e_v);

/// Berechnet den Winkel von Vektor e zu Vektor f.
double angle(Point e_u, Point e_v, Point f_u, Point f_v);

/// Gibt Kreuzungsposition der beschriebenen Strecken (s1, s2), (t1, t2) zurück.
/// Kreuzung existiert nur genau dann, wenn ret.first zwischen 0 und 1 (entprechen s1 und s2).
std::pair<double, Point> crossing_position(Point s1, Point s2, Point t1, Point t2);

/// Speichert die kombinatorische Einbettung inkl. Positionen in einer Datei.
void write_gml_file(const Graph& graph, const UnprocessedDrawing& drawing, std::string path);

/// Liest die Positionen einer einfachen Zeichnung aus einer GML-Datei.
UnprocessedDrawing read_gml_drawing(std::string path, bool* ok, std::string* err_msg);

#endif
