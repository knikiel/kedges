#ifndef COMMON_TYPES_H
#define COMMON_TYPES_H

#include <cassert>
#include <string>
#include <utility>
#include <map>
#include <vector>

#include "ogdf/basic/geometry.h"
#include "qpoint.h"

// einige Aliasse, um die Verwendung der primitiven Typen klarer zu machen
using node_id = int;
using adj_id = int;
using node_pair = std::pair<node_id, node_id>;
using adj_pair = std::pair<adj_id, adj_id>;
template<class T>
using edge_map = std::map<node_pair, T>;


/// Gibt an, auf welche Art die Kanten gefärbt werden sollen.
enum class EdgeColoringMode {
    NO_COLORING, ///! keine Färbung
    K_NUMBER, ///! Färbung nach k-Zahl
    K_DIFFERENCE_TO_PREV, ///! Färbung nach Differenz der k-Zahl zu vorigem Graphen
    K_DIFFERENCE_TO_NEXT ///! Färbung nach Differenz der k-Zahl zu nachfolgendem Graphen
};


/// Struktur, um mögliche Fehler wären einer Funktion zu beschreiben.
struct ErrorInfo {
    bool ok;
    std::string err_msg;
};


/// Gibt an, welche Seite der Kante weniger Knoten enthält.
enum class DominatingSide {
    NONE,
    LEFT,
    RIGHT,
};


/// Speichert die k-Zahl und dominierende Seite aller Kanten eines Graphen.
struct KEdgeInformation {
    int num_vertices;
    int num_edges;
    edge_map<std::pair<DominatingSide, int>> edge_numbers;
    std::string infoStr;
};


/// Adapter, um die Konvertierung zwischen den Punkt-Klassen von Qt und OGDF zu vereinfachen.
struct Point {
    double x;
    double y;

    Point();
    Point(double x, double y);
    Point(const QPointF& other);
    Point(const ogdf::DPoint& other);

    bool operator== (const Point& other) const;
    bool operator<= (const Point& other) const;
    bool operator< (const Point& other) const;
    Point& operator= (const QPointF& other);
    Point& operator= (const ogdf::DPoint& other);
    operator QPointF() const;
    operator ogdf::DPoint() const;

    std::string to_string() const;
};



/// Die verschiedenen k-Kantensummierungen.
enum class KEdgeSumType {
    NONE,
    EXACT, ///! exakte Anzahl von Kanten
    CUMULATED, ///! kumuliert
    C2UMULATED, ///! 2-fach kumuliert
    C3UMULATED ///! 3-fach kumuliert
};


/// Beschreibung der Bedingungen als Terme der Ungleichungen.
struct KEdgeInequality {
    // Summe k-Kanten
    long sum_term;
    // Binomialwert
    long binom_term;
    // Beitrag der k-Kanten
    std::vector<long> coefficents;

    bool is_ok() { return sum_term >= binom_term; }
};


/// Oberklasse der Graphmodifikationen
struct GraphChange {
    /// Art der Graphmodifikation.
    enum class Type {
        NO_CHANGE, ///! nicht initializiert oder fehlerhaft.
        REROUTING,
        VERTEX_ADDITION,
        EDGE_ADDITION,
        VERTEX_REMOVAL,
        EDGE_REMOVAL
    };

    Type type; 

    // Informationen, um Umleitungen wiederherzustellen, alter Kanten zu löschen
    // oder neue Kanten zu erstellen.
    /// ausgewählte Kante (Richtung implizit)
    node_pair vertices;
    /// neuer Kantenverlauf, der sich aus den angegebenen Face-Adjazenzen ergibt
    std::vector<adj_id> adjacencies;
    /// die Knicke
    std::vector<std::vector<Point>> bend_positions;

    // Informationen, um Knoten zu löschen.
    /// ID des entfernten Knoten.
    node_id vertex_id;

    // Informationen, um Knoten hinzuzufügen
    /// Position des neuen Knotens.
    Point position;

    GraphChange()
        : adjacencies()
        , bend_positions()
    {
        type = Type::NO_CHANGE;
        vertices = {-1, -1};
        vertex_id = -1;
    }
};

#endif
