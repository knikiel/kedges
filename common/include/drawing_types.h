#ifndef DRAWING_TYPES_H
#define DRAWING_TYPES_H

#include <deque>
#include <map>

#include "common.h"

/// Speichert eine *unbearbeitete* Zeichnung eines ungerichteten Graphen.
/// Sie enth�lt alle vorhandenen Kanten und die Positionen der Knoten und der Kantenknicke.
/// Objekt muss mit der Anzahl der Knoten initialisert werden, den Knoten m�ssen Positionen
/// zugewiesen werden, Kanten m�ssen hinzugef�gt und evtl. mit Positionen der Kantenknicke
/// beschrieben werden.
class UnprocessedDrawing {
public:
    /// Konstruiert eine mit der Knotenanzahl initialisierte Zeichnung.
    UnprocessedDrawing(int num_vertices);

    /// Konstruiert eine nicht initialisierten Zeichnung. (siehe initialize(...))
    UnprocessedDrawing();

    /// Copy-Konstruktor.
    UnprocessedDrawing(const UnprocessedDrawing& drawing);

    /// Initialisiere die Zeichnung mit der Anzahl der vorhandenen Knoten.
    /// Muss nach parameterlosem Konstruktor aufgerufen werden, damit andere Operationen vern�nftig funktionieren.
    void initialize(int num_vertices);

    // einfache Transformationen der Zeichnung
    void invert_y_positions();
    void scale_by(double scale_factor);

    // vervollst�ndige Zeichnung
    void set_position(node_id v, Point pos);
    void add_edge(node_pair edge);

    /// F�gt einen weiteren Knick in impliziter Kantenrichtung ein.
    void add_bending(node_pair edge, Point pos);

    // Anfragen an die Zeichnung
    std::vector<node_pair> all_edges() const;
    bool exists(node_pair edge) const;
    int number_vertices() const;
    Point position(node_id v) const;
    std::deque<Point> bendings(node_pair edge) const;

private:
    /// Bestimmt, ob die implizite Richtung der angegebene Kante der gespeicherten entspricht
    /// und korrigiert diese evtl.
    bool get_correct_direction(node_pair* edge) const;

private:
    int m_num_vertices;
    std::vector<Point> m_positions;
    edge_map<std::deque<Point>> m_bend_positions;
};


/// Speichert eine *planarisierte* Zeichnung inklusiver Knicke und Kantenkreuzungen.
/// Sie enth�lt alle vorhandenen Kanten und die Positionen der Knoten, der Kantenknicke
/// und der .
/// Objekt muss mit der Anzahl der Knoten initialisert werden, den Knoten m�ssen Positionen
/// zugewiesen werden, Kanten m�ssen hinzugef�gt und evtl. mit Positionen der Kantenknicke
/// beschrieben werden.
class PlanarizedDrawing {
public:
    /// Konstruiert eine nicht initialisierten Zeichnung. (siehe initialize(...))
    PlanarizedDrawing();

    /// Initialisiere die Zeichnung mit der Anzahl der vorhandenen Knoten.
    /// Muss nach Konstruktor aufgerufen werden, damit andere Operationen vern�nftig funktionieren.
    void initialize(int num_nodes);

    // vervollst�ndige Zeichnung
    void set_position(node_id v, Point pos);
    void add_edge(ogdf::edge real_edge);

    /// F�gt einen weiteren Knick in impliziter Kantenrichtung ein.
    void add_bending(ogdf::edge real_edge, Point pos);

    // Anfragen an die Zeichnung
    bool exists(ogdf::edge real_edge) const;
    int number_nodes() const;
    Point position(node_id v) const;
    std::deque<Point> bendings(ogdf::edge real_edge) const;

private:
    int m_num_nodes;
    std::vector<Point> m_positions;
    std::map<ogdf::edge, std::deque<Point>> m_bend_positions;
};

#endif // DRAWING_TYPES_H