#include "Planarizer.h"

#include <vector>

#include "ogdf/planarity/PlanRep.h"

#include "drawing_types.h"

using namespace std;


#define PI 3.14159265359

Planarizer::Planarizer(const UnprocessedDrawing &drawing, PlanRep* planrep)
    : m_planrep(planrep)
    , m_curr_id_bending(0)
{
    m_num_vertices = drawing.number_vertices();
    m_num_nodes = m_num_vertices;
    List<node> all_nodes;
    planrep->allNodes(all_nodes);
    vector<node> all_vertices;
    for (auto v : all_nodes) {
        all_vertices.push_back(v);
    }

    // f�ge alle Kanten ab und ihre Knicke zur Datenstruktur ein
    for (int u_copy_id = 0; u_copy_id < m_num_nodes; ++u_copy_id) {
        node u_copy = all_vertices[u_copy_id];
        node u_orig = planrep->original(u_copy);
        node_id u_orig_id = u_orig->index();
        Vertex p = { Vertex::Type::Node, u_copy_id, u_copy };
        m_point_to_position[p] = drawing.position(u_orig_id);
        for (int v_copy_id = u_copy_id + 1; v_copy_id < m_num_nodes; ++v_copy_id) {
            node v_copy = all_vertices[v_copy_id];
            node v_orig = planrep->original(v_copy);
            edge e = m_planrep->searchEdge(u_copy, v_copy);
            if (!e) {
                assert(!m_planrep->searchEdge(v_copy, u_copy));
                continue;
            }
            if (e->source()->index() > e->target()->index()) {
                m_planrep->reverseEdge(e);
            }
            Vertex prev_point = p;
            Vertex curr_point;
            Segment seg;
            for (Point pos : drawing.bendings({ u_orig->index(), v_orig->index() })) {
                curr_point = { Vertex::Type::Bend, m_curr_id_bending++, nullptr };
                m_point_to_position[curr_point] = pos;
                seg = { prev_point, curr_point, e };
                m_segments_map[{u_copy_id, v_copy_id}].push_back(seg);
                m_segment_to_end_vertices[seg] = { u_copy_id, v_copy_id };
                prev_point = curr_point;
            }
            curr_point = { Vertex::Type::Node, v_copy_id, all_vertices[v_copy_id] };
            seg = { prev_point, curr_point, e };
            m_segments_map[{u_copy_id, v_copy_id}].push_back(seg);
            m_segment_to_end_vertices[seg] = { u_copy_id, v_copy_id };
        }
    }
}

Point Planarizer::point_position(Planarizer::Vertex p) const {
    return m_point_to_position.at(p);
}

std::list<Planarizer::Segment> Planarizer::segments(node_id u, node_id v) const {
    return m_segments_map.at({u, v});
}

vector<Planarizer::CrossingInfo> Planarizer::crossed_segments(Segment seg) {
    node_pair e = m_segment_to_end_vertices[seg];
    vector<Segment> segments = other_unprocessed_segments(e.first, e.second);
    vector<pair<double, CrossingInfo>> crossed;

    // bestimme die gekreuzten Segmente
    for (Segment other_seg : segments) {
        pair<double, Point> scalar_and_pos = are_crossing(seg, other_seg);
        double scalar = scalar_and_pos.first;
        Point pos = scalar_and_pos.second;
        if (0.00001f < scalar && scalar < 0.9999f) {
            // es wird nur gekreuzt, wenn der Skalar zwischen 0 und 1 liegt (epsilon-Toleranz)
            bool bottom_up = angle(pos, m_point_to_position[seg.v],
                pos, m_point_to_position[other_seg.v]) <= PI;
            crossed.push_back({ scalar, { other_seg, pos, bottom_up } });
        }
    }
    
    // sortiere die gekreuzten Segmente nach der Reihenfolge, in denen sie gekreuzt werden
    std::sort(crossed.begin(), crossed.end(),
        [](pair<double, CrossingInfo> l, pair<double, CrossingInfo> r) -> bool {
        return l.first < r.first;
    });

    vector<CrossingInfo> crossings;
    for (auto seg : crossed) {
        crossings.push_back(seg.second);
    }
    return crossings;
}


edge Planarizer::add_crossing(Segment seg1, Segment seg2, Point pos, bool topdown) {
#if OGDF_DEBUG
    assert(seg1.e->graphOf() == (const Graph*) m_planrep); 
    assert(seg2.e->graphOf() == (const Graph*) m_planrep); 
#endif
    assert(seg1 != seg2);

    // f�ge Kreuzung in interne PlanRep korrekt ein
    Segment old_seg1 = seg1;
    Segment old_seg2 = seg2;
    node_pair old_ends1 = { old_seg1.e->source()->index(), old_seg1.e->target()->index() };
    node_pair old_ends2 = { old_seg2.e->source()->index(), old_seg2.e->target()->index() };
    node_pair e1 = m_segment_to_end_vertices.at(seg1);
    node_pair e2 = m_segment_to_end_vertices.at(seg2);
    assert(!((e1.first == 0 && e1.second == 0) || (e2.first == 0 && e2.second == 0)));
    Vertex u1 = seg1.u;
    Vertex v1 = seg1.v;
    Vertex u2 = seg2.u;
    Vertex v2 = seg2.v;
    edge seg1_edge = seg1.e;
    edge new_edge = m_planrep->insertCrossing(seg1_edge, seg2.e, topdown);
    edge e_u1p = seg1_edge->adjSource()->cyclicSucc()->cyclicSucc()->theEdge();
    edge e_pv1 = seg1_edge;
    edge e_u2p = new_edge->adjSource()->cyclicSucc()->cyclicSucc()->theEdge();
    edge e_pv2 = new_edge;

    assert(e_pv1->source() == e_u2p->target());
    assert(e_pv1->source() != e_pv1->target() && e_u1p->source() != e_u1p->target());
    assert(e_pv2->source() != e_pv2->target() && e_u2p->source() != e_u2p->target());
    assert(e_u1p->source()->index() == old_ends1.first && e_pv1->target()->index() == old_ends1.second);
    assert(e_u2p->source()->index() == old_ends2.first && e_pv2->target()->index() == old_ends2.second);
    assert(e_u1p != e_u2p && e_u1p != e_pv2 && e_pv1 != e_u2p && e_pv1 != e_u2p);
    Vertex p = { Vertex::Type::Crossing, e_pv1->source()->index(), e_pv1->source()};
    m_point_to_position[p] = pos;
    ++m_num_nodes;

    // teile seg1 korrekt in beide Teilsegmente auf uns speichere notwendige Informationen
    list<Segment> &first_edge_segments = m_segments_map[e1];
    auto iter = first_edge_segments.begin();
    while (iter->v != v1) {
        if (iter->e == old_seg1.e) {
            m_segment_to_end_vertices.erase(*iter);
            iter->e = e_u1p;
            m_segment_to_end_vertices[*iter] = e1;
        }
        ++iter;
    }
    assert(iter != first_edge_segments.end());
    first_edge_segments.insert(iter, { u1, p, e_u1p});
    first_edge_segments.insert(iter, { p, v1, e_pv1});
    m_segment_to_end_vertices[{u1, p, e_u1p}] = e1;
    m_segment_to_end_vertices[{p, v1, e_pv1}] = e1;
    m_segment_to_end_vertices.erase(*iter);
    iter = first_edge_segments.erase(iter);
    while (iter != first_edge_segments.end() && iter->e == old_seg1.e) {
        m_segment_to_end_vertices.erase(*iter);
        iter->e = e_pv1;
        m_segment_to_end_vertices[*iter] = e1;
        ++iter;
    }

    // teile seg2 korrekt in beide Teilsegmente auf uns speichere notwendige Informationen
    list<Segment> &second_edge_segments = m_segments_map[e2];
    iter = second_edge_segments.begin();
    while (iter->v != v2) {
        if (iter->e == old_seg2.e) {
            m_segment_to_end_vertices.erase(*iter);
            iter->e = e_u2p;
            m_segment_to_end_vertices[*iter] = e2;
        }
        ++iter;
    }
    assert(iter != second_edge_segments.end());
    second_edge_segments.insert(iter, { u2, p, e_u2p});
    second_edge_segments.insert(iter, { p, v2, e_pv2});
    m_segment_to_end_vertices[{u2, p, e_u2p}] = e2;
    m_segment_to_end_vertices[{p, v2, e_pv2}] = e2;
    m_segment_to_end_vertices.erase(*iter);
    iter = second_edge_segments.erase(iter);
    while (iter != second_edge_segments.end() && iter->e == old_seg2.e) {
        m_segment_to_end_vertices.erase(*iter);
        iter->e = e_pv2;
        m_segment_to_end_vertices[*iter] = e2;
        ++iter;
    }
    assert(!((e1.first == 0 && e1.second == 0) || (e2.first == 0 && e2.second == 0)));

    return e_pv1;
}

void Planarizer::fill_planarized_drawing(PlanarizedDrawing* drawing) {
    drawing->initialize(m_planrep->numberOfNodes());
    node curr_node;
    forall_nodes(curr_node, *m_planrep) {
    }
    for (auto point_pos : m_point_to_position) {
        if (point_pos.first.type != Vertex::Type::Bend) {
            drawing->set_position(point_pos.first.vertex->index(), point_pos.second);
        }
    }

    vector<Point> bendings;
    for (int u_copy_id = 0; u_copy_id < m_num_vertices; ++u_copy_id) {
        for (int v_copy_id = u_copy_id + 1; v_copy_id < m_num_vertices; ++v_copy_id) {
            node_pair end_nodes = { u_copy_id, v_copy_id };
            assert(m_segments_map.count(end_nodes) == m_segments_map.count({ end_nodes.first, end_nodes.second }));
            if (m_segments_map.count(end_nodes) == 0) {
                // keine Kante vorhanden
                continue;
            }
            node u_copy = nullptr, v_copy = nullptr;
            forall_nodes(curr_node, *m_planrep) {
                if (curr_node->index() == u_copy_id) {
                    u_copy = curr_node;
                }
                else if (curr_node->index() == v_copy_id) {
                    v_copy = curr_node;
                }
            }
            node u_orig = m_planrep->original(u_copy);
            node v_orig = m_planrep->original(v_copy);
            edge edge_orig = m_planrep->original().searchEdge(u_orig, v_orig);
            auto chain = m_planrep->chain(edge_orig);
            bool is_reversed = false;
            if (chain.front()->source()->index() > chain.back()->target()->index()) {
                is_reversed = true;
            }

            // f�ge alle Segmente in der Kante mit evtl. korrigierten Richtungen hinzu
            for (auto segment : m_segments_map[{u_copy_id, v_copy_id}]) {
                if (segment.u.type != Vertex::Type::Bend) {
                }
                if (segment.v.type == Vertex::Type::Bend) {
                    bendings.push_back(m_point_to_position[segment.v]);
                }
                else {
                    edge edge_copy = chain.popFrontRet();
                    drawing->add_edge(edge_copy);
                    if (!is_reversed) {
                        for (Point bend : bendings) {
                            drawing->add_bending(edge_copy, bend);
                        }
                    }
                    else {
                        vector<Point> reversed(bendings.size());
                        std::reverse_copy(bendings.begin(), bendings.end(), reversed.begin());
                        for (Point bend : reversed) {
                            drawing->add_bending(edge_copy, bend);
                        }
                    }
                    bendings.clear();
                }
            }
        }
    }
}

vector<Planarizer::Segment> Planarizer::other_unprocessed_segments(node_id u, node_id v) {
    vector<Segment> segments;
    for (int i = u; i < m_num_vertices-1; ++i) {
        for (int j = i + 1; j < m_num_vertices; ++j) {
            if ((i == u && j == v) || (i == v && j == u)
                || m_segments_map.count({ i, j }) == 0)
            {
                continue;
            }
            for (Segment seg : m_segments_map[{i, j}]) {
                segments.push_back(seg);
            }
        }
    }
    return segments;
}


pair<double, Point> Planarizer::are_crossing(Segment seg, Segment other_seg) {
    return crossing_position(m_point_to_position[seg.u], m_point_to_position[seg.v],
        m_point_to_position[other_seg.u], m_point_to_position[other_seg.v]);
}


pair<node, vector<Point>> Planarizer::find_real_node_before(Segment seg) {
    auto seg_edge = m_segment_to_end_vertices[seg];
    auto segments = m_segments_map[seg_edge];
    node real_node_before = nullptr;
    vector<Point> bends_inbetween;
    for (const auto& segment : segments) {
        if (segment.u.type == Vertex::Type::Bend){
            // f�ge Knick hinzu
            bends_inbetween.push_back(m_point_to_position[segment.u]);
        }
        else {
            // ein Kreuzungsknoten befindet sich auf dem Segmentpfad, bisherige Knicke geh�ren nicht zu seg
            real_node_before = segment.u.vertex;
            bends_inbetween.clear();
        }
        
        // breche ab, sobald das Segment in der Segmentfolge gefunden wird
        if (segment.v.type == seg.v.type && segment.v.id == seg.u.id) {
            break;
        }
    }
    return {real_node_before, bends_inbetween};
}


pair<node, vector<Point>> Planarizer::find_real_node_after(Segment seg) {
    auto seg_edge = m_segment_to_end_vertices[seg];
    auto segments = m_segments_map[seg_edge];
    vector<Point> bends_inbetween;
    auto iter = segments.begin();
    // suche das Segment in der Segmentfolge
    while (iter->v.type != seg.v.type || iter->v.id != seg.v.id) ++iter;
    // f�ge alle Knicke bis zum n�chsten echte Knoten/Kreuzungsknoten hinzu
    for (; iter->v.type == Vertex::Type::Bend; ++iter) {
        bends_inbetween.push_back(m_point_to_position[iter->v]);
    }
    node real_node_after = iter->v.vertex;
    return {real_node_after, bends_inbetween};
}

Planarizer::Vertex::Vertex(Type type, int id, ogdf::node vertex) :
    type(type),
    id(id),
    vertex(vertex)
{}

Planarizer::Vertex::Vertex(const Vertex& other) :
    type(other.type),
    id(other.id),
    vertex(other.vertex)
{}

Planarizer::Vertex::Vertex() :
    type(Type::Node),
    vertex(nullptr)
{}

bool Planarizer::Vertex::operator==(const Vertex &r) const {
    return type == r.type && id == r.id && vertex == r.vertex;
}

bool Planarizer::Vertex::operator<(const Vertex &r) const {
    return type < r.type || (type == r.type && id < r.id);
}

bool Planarizer::Vertex::operator!=(const Vertex &r) const {
    return !(*this == r);
}

Planarizer::Segment::Segment(Vertex u, Vertex v, ogdf::edge e) :
    u(u),
    v(v),
    e(e)
{}

Planarizer::Segment::Segment(const Segment& other) :
    u(other.u),
    v(other.v),
    e(other.e)
{}

Planarizer::Segment::Segment() :
    e(nullptr)
{}

bool Planarizer::Segment::operator==(const Segment &other) const {
    return u.type == other.u.type && u.id == other.u.id
        && v.type == other.v.type && v.id == other.v.id
        && e->index() == other.e->index();
}

bool Planarizer::Segment::operator!=(const Segment &other) const {
    return !(*this == other);
}

bool Planarizer::Segment::operator<(const Segment &other) const {
    return u < other.u || (u == other.u && v < other.v)
        || (u == other.u && v == other.v && e->index() < other.e->index());
}
