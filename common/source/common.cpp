//#include <regex>
#include <algorithm>
#include <cassert>
#include <iostream>
#include <cmath>
#include <sstream>
#include <string>

#include "ogdf/basic/DualGraph.h"
#include "ogdf/basic/List.h"
#include "ogdf/fileformats/GraphIO.h"

#include "common.h"
#include "Planarizer.h"

using namespace std;

#define PI 3.14159265359


void create_graph(const UnprocessedDrawing& drawing, Graph* graph) {
	graph->clear();
	List<node> vertices;
	int n = drawing.number_vertices();
	for (int i = 0; i < n; i++) {
		vertices.pushBack(graph->newNode());
	}
	for (node v : vertices) {
		for (node u : vertices) {
			if (u->index() < v->index() && drawing.exists({ u->index(), v->index() })) {
				graph->newEdge(u, v);
			}
		}
	}
}


void count_num_k_edges(const KEdgeInformation& info, std::vector<int>* numbers) {
	int num_vertices = info.num_vertices;
	numbers->resize(num_vertices / 2, 0);
    for (auto key_val : info.edge_numbers) {
        DominatingSide number_type = key_val.second.first;
        int k_number = key_val.second.second;
        numbers->at(k_number) = numbers->at(k_number) + 1;
	}
}


void count_num_signed_k_edges(const KEdgeInformation& info,
	std::vector<int>* minus_numbers, std::vector<int>* plus_numbers,
	std::vector<int>* both_numbers)
{
	int num_vertices = info.num_vertices;
	minus_numbers->clear();
	minus_numbers->resize(num_vertices / 2, 0);
	plus_numbers->clear();
	plus_numbers->resize(num_vertices / 2, 0);
	both_numbers->clear();
	both_numbers->resize(num_vertices / 2, 0);
    for (auto key_val : info.edge_numbers) {
        int k_number = key_val.second.second;
        switch (key_val.second.first) {
        case DominatingSide::NONE:	{
            both_numbers->at(k_number) += 1;
        } break;
        case DominatingSide::LEFT:	{
            minus_numbers->at(k_number) += 1;
        } break;
        case DominatingSide::RIGHT:	{
            plus_numbers->at(k_number) += 1;
        } break;
        }
	}
}

int harary_hill_crossings(int num_vertices) {
	return (num_vertices / 2) * ((num_vertices - 1) / 2) * ((num_vertices - 2) / 2)
        * ((num_vertices - 3) / 2) / 4;
}

KEdgeInequality test_single_condition(KEdgeSumType type, const std::vector<int>& num_k_edges, int k) {
	long sum = 0;
	vector<long> coefficients;
	KEdgeInequality ineq;
    // Formeln lassen sich mit Rechenregeln zu summierten Binomialkoeffizienten berechnen
	switch (type) {
	case KEdgeSumType::C3UMULATED: {
		for (int i = 0; i <= k; ++i) {
			long factor = (k + 2 - i) * (k + 1 - i) / 2;
			coefficients.push_back(factor);
			sum += factor * num_k_edges[i];
		}
		long bin = (k + 4) * (k + 3) * (k + 2) * (k + 1) / 24 * 3;
		ineq.binom_term = bin;
	} break;
	case KEdgeSumType::C2UMULATED: {
		for (int i = 0; i <= k; ++i) {
			long factor = (k + 1 - i);
			coefficients.push_back(factor);
			sum += factor * num_k_edges[i];
		}
		long bin = (k + 3) * (k + 2) * (k + 1) / 6 * 3;
		ineq.binom_term = bin;
	} break;
	case KEdgeSumType::CUMULATED: {
		for (int i = 0; i <= k; ++i) {
			long factor = 1;
			coefficients.push_back(factor);
			sum += factor * num_k_edges[i];
		}
		long bin = (k + 2) * (k + 1) / 2 * 3;
		ineq.binom_term = bin;
	} break;
	case KEdgeSumType::EXACT: {
		for (int i = 0; i <= k; ++i) {
			long factor = (i == k) ? 1 : 0;
			coefficients.push_back(factor);
			sum += factor * num_k_edges[i];
		}
		long bin = 3 * (k + 1);
		ineq.binom_term = bin;
	} break;
    default: {
        // falsche Eingabe
        assert(false);
    } break;
    }
	ineq.sum_term = sum;
	ineq.coefficents = coefficients;
	return ineq;
}


bool is_no_condition_violated(int num_vertices, KEdgeSumType type, const std::vector<int>& num_k_edges) {
	return count_violated_conditions(num_vertices, type, num_k_edges) == 0;
}

int count_violated_conditions(int num_vertices, KEdgeSumType type, const std::vector<int>& num_k_edges) {
	int count = 0;
	int upper_bound = compute_relevant_upper_k(num_vertices);
	for (int k = 0; k <= upper_bound; ++k) {
		if (!test_single_condition(type, num_k_edges, k).is_ok()) {
			count += 1;
		}
	}
	return count;
}


ErrorInfo search_for_adj_entries(const PlanRep& planrep,
	const std::vector<node_pair>& ref_faces, std::vector<adjEntry>* adjacencies)
{
	if (ref_faces.empty()) {
		// alle Flächen durchlaufen
		ConstCombinatorialEmbedding embedding(planrep);
		std::vector<face> faces;
		search_for_faces(embedding, &faces);
		for (face f : faces) {
			adjacencies->push_back(f->firstAdj());
		}
	}
	else {
		// spezifizierte Flächen suchen;
		for (auto e : ref_faces) {
			if (e.first >= planrep.maxNodeIndex() || e.second >= planrep.maxNodeIndex()) {
                return ErrorInfo{ false, "Keine Knoten (" + std::to_string(e.first)
					+ " und/oder " + std::to_string(e.second) + ") in Planarisierung!" };
			}
            
            node u = nullptr;
            forall_nodes(u, planrep) {
                if (u->index() == e.first) {
                    break;
                }
            }
            assert(u);
			adjEntry adjacency = nullptr;
			forall_adj(adjacency, u) {
				if (adjacency->twinNode()->index() == e.second) {
                    break;
				}
			}
			if (!adjacency) {
                return ErrorInfo{ false, "Keine Kante (" + std::to_string(e.first)
					+ ", " + std::to_string(e.second) + ") in Planarisierung!" };
			}
			adjacencies->push_back(adjacency);
		}
	}

    return ErrorInfo{ true, "OK" };
}

void search_for_faces(const ConstCombinatorialEmbedding& embedding, std::vector<face>* faces) {
	face f;
	forall_faces(f, embedding) {
		faces->push_back(f);
	}
}

double angle_to_x_axis(Point e_u, Point e_v) {
    // einfache Trigonometrie
	double e_x = e_v.x - e_u.x;
	double e_y = e_v.y - e_u.y;
	double radiants = acos(e_x / sqrt(e_x*e_x + e_y*e_y));
	if (e_y < 0) {
        radiants = 2 * PI - radiants;
    }
	return 2 * PI - radiants;
}

double angle(Point e_u, Point e_v, Point f_u, Point f_v) {
	double temp = angle_to_x_axis(f_u, f_v) - angle_to_x_axis(e_u, e_v);
	return temp > 0 ? temp : 2 * PI + temp;
}
pair<double, Point> crossing_position(Point s1, Point s2, Point t1, Point t2) {
	double f_uX = t1.x;
	double f_uY = t1.y;
	double f_vX = t2.x;
	double f_vY = t2.y;
    double e_uX = s1.x;
    double e_uY = s1.y;
    double e_vX = s2.x;
    double e_vY = s2.y;
	double s;
	// kompliziertere Trigonometrie
	if (f_vY - f_uY == 0.0) {
		if (e_vY > e_uY) {
			s = (f_vY - e_uY) / (e_vY - e_uY);
		} else {
			s = (e_uY - f_vY ) / (e_uY - e_vY);
		}
	} else {
		double c = (f_vX - f_uX) / (f_vY - f_uY);
		double b = f_uX - c * f_uY;
		double a = e_uX - c * e_uY;
		double d = (e_vX - e_uX) - c * (e_vY - e_uY);
		s = (b - a) / d;
	}
    Point pos = {
		e_uX + s * (e_vX - e_uX),
		e_uY + s * (e_vY - e_uY),
	};
	// entsprechender Skalarfaktor des zweiten Segmentes muss nich bestimmt werden
	// prüfe einfach, ob pos zwischen beiden Endknoten liegt, beachte einfache Rundungsfehler
	if (!(min(f_uX, f_vX)-0.01 <= pos.x && pos.x <= max(f_uX, f_vX)+0.01
			&& min(f_uY, f_vY)-0.01 <= pos.y && pos.y <= max(f_uY, f_vY)+0.01)) {
		s = -1;
	}
	return{ s, pos };
}


PlanRep* planarize(const UnprocessedDrawing& drawing, 
	Graph* graph, PlanarizedDrawing* planarized)
{
	int num_vertices = drawing.number_vertices();

	create_graph(drawing, graph);
	// die Kopie des Graphen bildet nicht unbedingt Knoten auf selbe ID ab!
	PlanRep* planrep = new PlanRep(*graph);
	planrep->initCC(0);
	
	Planarizer collection(drawing, planrep);

	// berechne Rotationen der echten Knoten (betrachte Kante von beiden Endknoten)
	List<node> all_nodes;
	planrep->allNodes(all_nodes);
	vector<node_id> copy_to_orig_map(planrep->numberOfNodes());
	for (node u_copy : all_nodes) {
		node_id u_orig_id = planrep->original(u_copy)->index();
		copy_to_orig_map[u_copy->index()] = u_orig_id;
        Point u_pos = drawing.position(u_orig_id);
		vector<pair<node_id, double>> node_rotation;
		adjEntry adj;
		forall_adj(adj, u_copy) {
			node v_copy = adj->twinNode();
			node_id v_copy_id = v_copy->index();
			node_id v_orig_id = planrep->original(v_copy)->index();
			assert(drawing.exists({ u_orig_id, v_orig_id }));
            // bestimme den Winkel des ausgehenden Segments
            Point v_pos = drawing.position(v_orig_id);
			auto bendings = drawing.bendings({ u_orig_id, v_orig_id });
			if (bendings.size() > 0) {
				v_pos = bendings.front();
			}
			double angle_radiants = angle_to_x_axis(u_pos, v_pos);
			node_rotation.push_back({ v_copy_id, angle_radiants });
		}

		if (node_rotation.size() <= 2) {
			continue;
		}
        // sortiere Knoten nach Winkeln der Segmente
		std::sort(node_rotation.begin(), node_rotation.end(),
			[](const pair<int, double> &l, const pair<int, double> &r) -> bool {
				return l.second > r.second;
		});
		vector<node_id> ordered_targets;
		for (auto v : node_rotation) {
			ordered_targets.push_back(v.first);
		}

		// setze korrekte Kantenrotation um
		node_id first_target_id = *ordered_targets.begin();
		adjEntry first_adj = u_copy->firstAdj();
		while (first_target_id != first_adj->twinNode()->index()) { first_adj = first_adj->cyclicSucc(); }
		adjEntry next_adj = first_adj->cyclicPred();
		for (auto target_iter = ordered_targets.rbegin(); target_iter != ordered_targets.rend(); ++target_iter) {
			node_id next_target_id = *target_iter;
			if (next_target_id == first_adj->twinNode()->index()) {
				break;
			}
			while (next_target_id != next_adj->twinNode()->index()) {
                next_adj = next_adj->cyclicPred();
            }
			planrep->moveAdjAfter(next_adj, first_adj);
			next_adj = first_adj->cyclicPred();
		}
	}

#if OGDF_DEBUG
	edge e;
	forall_edges(e, *planrep) {
		node_id u_id_copy = e->source()->index();
		node_id v_id_copy = e->target()->index();
		node_id u_id_orig = copy_to_orig_map[u_id_copy];
		node_id v_id_orig = copy_to_orig_map[v_id_copy];
		cerr << "Copy nodes: " << u_id_copy << " " << v_id_copy;
		cerr << ", orig. nodes: " << u_id_orig << " " << v_id_orig << endl;
		assert(drawing.exists({ u_id_orig, v_id_orig }));
	}
#endif

	// laufe alle Kantensegmente der Kanten ab und prüfe, ob sie sich schneiden
	for (int u_copy_id = 0; u_copy_id < num_vertices; ++u_copy_id) {
		for (int v_copy_id = u_copy_id + 1; v_copy_id < num_vertices; ++v_copy_id) {
			if (!drawing.exists({ copy_to_orig_map[u_copy_id], copy_to_orig_map[v_copy_id] })) {
				continue;
			}
			auto segments = collection.segments(u_copy_id, v_copy_id);
			Planarizer::Segment seg = segments.front();
			for (auto segments_iter = segments.begin(); segments_iter != segments.end(); ++segments_iter) {
				if (segments_iter->u.type != Planarizer::Vertex::Type::Bend) {
					seg = *segments_iter;
				}
				else {
					seg.u = segments_iter->u;
					seg.v = segments_iter->v;
				}
				// finde alle geschnittenen Segmente
				auto crossed_segments = collection.crossed_segments(seg);
				for (auto it = crossed_segments.begin(); it != crossed_segments.end(); ++it) {
					Planarizer::CrossingInfo crossed = *it;
					// füge Kreuzungsknoten ein
					edge next_segment_edge = collection.add_crossing(seg, crossed.seg, crossed.pos, !crossed.bottom_up);
					node u_next = next_segment_edge->source();
					copy_to_orig_map.push_back(u_next->index());
					seg = { { Planarizer::Vertex::Type::Crossing,
                        u_next->index(), u_next }, seg.v, next_segment_edge };
				}
			}
		}
	}
	assert(planrep->representsCombEmbedding());
	collection.fill_planarized_drawing(planarized);

	return planrep;
};


int compute_relevant_upper_k(int num_vertices) {
	return num_vertices / 2 - 2;
}


// die eigentliche (rekursive) Tiefensuche
bool recursive_dfs(node start, node dest, const EdgeArray<bool>& is_triangle_edge,
    NodeArray<bool>* visited)
{
	(*visited)[start] = true;
    if (start == dest) {
        return true;
    }
	adjEntry adj;
	forall_adj(adj, start) {
		if (is_triangle_edge[adj->theEdge()]) {
            continue;
		}
		node next = adj->twinNode();
		if ((*visited)[next]) continue;
		if (recursive_dfs(next, dest, is_triangle_edge, visited)) {
			return true;
		}
	}
	return false;
}


// Tiefensuche im dualen Graphen, um Knoten dest von Knoten start zu erreichen.
// Die Kanten, die zum Dreieck gehören (is_triangle_edge), werden dabei nicht verwendet.
// Das Array is_visited wird zur Optimierung von außen verwendet.
bool search_node(node start, node dest,
    const EdgeArray<bool>& is_triangle_edge,
    NodeArray<bool>* is_visited)
{
    is_visited->fill(false);
	return recursive_dfs(start, dest, is_triangle_edge, is_visited);
}

KEdgeInformation compute_k_numbers(PlanRep& pr, adjEntry left_to_external_face) {
	const Graph &graph = pr.original();
	int num_vertices = graph.numberOfNodes();
	OGDF_ASSERT(pr.representsCombEmbedding());
	OGDF_ASSERT(left_to_external_face->graphOf() == &pr);
	CombinatorialEmbedding embedding(pr);
	ogdf::DualGraph dualEmbedding(embedding);

	face ref_face = embedding.rightFace(left_to_external_face);
	embedding.setExternalFace(ref_face);

    edge_map<pair<DominatingSide, int>> k_edges;

    // aus Performancegründen hier deklariert und einfach immer wieder reinitialisiert.
    const Graph& dual_graph = dualEmbedding.getGraph();
    NodeArray<bool> is_visited(dual_graph);
    EdgeArray<bool> is_triangle_edge(dual_graph);

    edge e_uv;

//    node n;
//    forall_nodes(n, graph) {
//        forall_nodes(n, graph) {
//            graph.chooseEdge()
//        }
//    }

    std::ostringstream infoText;

    forall_edges(e_uv, graph) {
        int num_L_nodes = 0;
        int num_R_nodes = 0;

        node u = e_uv->source();
        node v = e_uv->target();
        auto e_uv_segments = pr.chain(e_uv);

        // prüfe für alle Knoten w != u, v, ob sie links oder rechts von e_uv liegen
        adjEntry e_vw_adj;
        forall_adj(e_vw_adj, v) {
            node w = e_vw_adj->twinNode();
            if (w == u) {
                continue;
            }

            adjEntry e_wu_adj = nullptr;
            adjEntry adj;
            forall_adj(adj, w) {
                if (adj->twinNode() == u) {
                    e_wu_adj = adj;
                    break;
                }
            }
            if (e_wu_adj == nullptr) {
                // kein Dreieck mit Knoten w zu e_uv
                continue;
            }

            // suche "rechts" von e_uv die Referenzfläche
            // das aufgespannte Dreieck in der primalen Einbettung darf nicht durchquert werden
            // falls vorhanden, befinden wir uns im "unbeschränktem" face bzgl. uvw
            // dann ist w L-Knoten

            // segmenente verlaufen uU nicht in gleicher richtung wie original kante
            face right_face;
            if (pr.original(e_uv_segments.back()->target()) == v) {
                right_face = embedding.rightFace(e_uv_segments.front()->adjSource());
            } else {
                right_face = embedding.leftFace(e_uv_segments.front()->adjSource());
            }

            node dual_start = dualEmbedding.dualNode(right_face);
            node dual_dest = dualEmbedding.dualNode(ref_face);
            is_triangle_edge.fill(false);
//            infoText << "e_uv_seg: ";
            for (edge e : e_uv_segments) {
                is_triangle_edge[dualEmbedding.dualEdge(e)] = true;
//                infoText << "s: " << e->source() << "  t: " << e->target() << "\n";
            }
            for (edge e : pr.chain(e_vw_adj->theEdge())) {
                is_triangle_edge[dualEmbedding.dualEdge(e)] = true;
            }
            for (edge e : pr.chain(e_wu_adj->theEdge())) {
                is_triangle_edge[dualEmbedding.dualEdge(e)] = true;
            }
            bool is_left = search_node(dual_start, dual_dest,
                is_triangle_edge, &is_visited);
            if (is_left) {
                ++num_L_nodes;
            }
            else {
                ++num_R_nodes;
            }
        }

        // sammle Informationen über alle Knoten w bzgl. e_uv
        int k_number = std::min(num_L_nodes, num_R_nodes);
        string edgeInfo = std::to_string(k_number);
        DominatingSide type1, type2;
        if (num_L_nodes == k_number) {
            if (num_L_nodes == num_R_nodes) {
                type1 = DominatingSide::NONE;
                type2 = DominatingSide::NONE;
            }
            else {
                type1 = DominatingSide::LEFT;
                type2 = DominatingSide::NONE;
                edgeInfo.append("+");
            }
        }
        else {
            type1 = DominatingSide::NONE;
            type2 = DominatingSide::LEFT;
            edgeInfo.append("-");
        }

//        infoText << "e: " << u->index() << " " << v->index() <<
//                "  L: " << num_L_nodes << "  R: " << num_R_nodes << "  k: " << edgeInfo << "\n";

        node_id u_id = u->index();
        node_id v_id = v->index();
        if (u_id < v_id) {
            k_edges[{u_id, v_id}] = { type1, k_number };
            infoText << "e: " << u_id << " " << v_id <<
                      "  L: " << num_L_nodes << "  R: " << num_R_nodes << "  k: " << edgeInfo << "\n";

        }
        else {
            k_edges[{v_id, u_id}] = { type2, k_number };
            infoText << "e: " << u_id << " " << v_id <<
                   "  L: " << num_R_nodes << "  R: " << num_L_nodes << "  k: " << edgeInfo << "\n";

          }
	}

    KEdgeInformation info = { num_vertices,
        num_vertices*(num_vertices - 1) / 2, k_edges, infoText.str() };
	return info;
}


adjEntry search_adjEntry(const PlanRep& plan_rep, node_pair e_uv) {
	node u = nullptr;
	node u_i;
	forall_nodes(u_i, plan_rep) {
		if (u_i->index() == e_uv.first) {
			u = u_i;
		}
	}
	adjEntry adjacency;
	forall_adj(adjacency, u) {
		if (adjacency->twinNode()->index() == e_uv.second) {
			return adjacency;
		}
	}

	return nullptr;
}


void write_gml_file(const Graph& graph, const UnprocessedDrawing &drawing, std::string path) {
	GraphAttributes attr(graph,
        GraphAttributes::nodeGraphics | GraphAttributes::edgeGraphics | GraphAttributes::nodeLabel);
	node v;
	forall_nodes(v, graph) {
		auto pos = drawing.position(v->index());
		attr.x(v) = pos.x;
		attr.y(v) = pos.y;
		attr.label(v) = to_string(v->index());
	}
	edge e;
	forall_edges(e, graph) {
		auto bends = drawing.bendings({ e->source()->index(), e->target()->index() });
		for (auto bend : bends) {
			attr.bends(e).pushBack(bend);
		}
	}
	GraphIO::writeGML(attr, path);
}


UnprocessedDrawing read_gml_drawing(std::string path, bool* ok, std::string* err_msg) {
	UnprocessedDrawing drawing;
	*ok = true;
	*err_msg = "";

	Graph graph;
	GraphAttributes attributes(graph);
	bool no_error = GraphIO::readGML(attributes, graph, path);
	if (!no_error) {
		*ok = false;
		*err_msg = "Datei nicht im GML-Format! Laden abgebrochen.";
		return drawing;
	}
	node v1, v2;
	adjEntry adj;
	forall_nodes(v1, graph) {
		forall_nodes(v2, graph) {
			if (v1 == v2) continue;
			bool found = false;
			forall_adj(adj, v1) {
				if (adj->twinNode() == v2) {
					if (!found) {
						found = true;
					}
					else {
						*ok = false;
						*err_msg = "Graph hat mehr als eine Kante zwischen 2 Knoten! Laden abgebrochen.";
						return drawing;
					}
				}
			}
		}
	}

	int num_vertices = graph.numberOfNodes();
	drawing.initialize(num_vertices);

	// überprüfe Positionen im Graphen, damit es keine negativen Koordinaten gibt
	double min_x_pos = 0;
	double min_y_pos = 0;
	node v;
	forall_nodes(v, graph) {
		min_x_pos = min(min_x_pos, attributes.x(v));
		min_y_pos = min(min_y_pos, attributes.y(v));
	}
	edge e;
	forall_edges(e, graph) {
		for (auto& bend : attributes.bends(e)) {
			min_x_pos = min(min_x_pos, bend.m_x);
			min_y_pos = min(min_y_pos, bend.m_y);
		}
	}

	forall_nodes(v, graph) {
		drawing.set_position(v->index(),
            Point(attributes.x(v) - min_x_pos, attributes.y(v) - min_y_pos));
	}
	forall_edges(e, graph) {
		node_pair pair = { e->source()->index(), e->target()->index() };
		drawing.add_edge(pair);
		auto bends = attributes.bends(e);
		if (bends.size() > 0) {
			assert(bends.size() > 2);
			bends.popFront();
			bends.popBack();
			DPoint prev_bend = DPoint(-1e32, -1e32);
			for (auto& bend : bends) {
				if (bend != prev_bend) {
					drawing.add_bending(pair, bend - DPoint(min_x_pos, min_y_pos));
					prev_bend = bend;
				}
				else {
					// ignoriere doppelten Knick
				}
			}
		}
	}

	return drawing;
}
