#include "common_types.h"

Point::Point() {
    x = 0.;
    y = 0.;
}

Point::Point(double x, double y) {
    this->x = x;
    this->y = y;
}

Point::Point(const QPointF& other) {
    this->x = other.x();
    this->y = other.y();
}

Point::Point(const ogdf::DPoint& other) {
    this->x = other.m_x; 
    this->y = other.m_y;
}

std::string Point::to_string() const {
    return std::to_string(x) + ", " + std::to_string(y);
}

bool Point::operator== (const Point& other) const {
    return x == other.x && y == other.y;
}

bool Point::operator<= (const Point& other) const {
    return x <= other.x || (x == other.x && y <= other.y);
}

bool Point::operator< (const Point& other) const {
    return x < other.x || (x == other.x && y < other.y);
}

Point& Point::operator= (const QPointF& other) {
    x = other.x();
    y = other.y();
    return *this;
}

Point& Point::operator= (const ogdf::DPoint& other) {
    x = other.m_x;
    y = other.m_y;
    return *this;
}

Point::operator QPointF() const {
    return QPointF(x, y);
}

Point::operator ogdf::DPoint() const {
    return ogdf::DPoint(x, y);
}
