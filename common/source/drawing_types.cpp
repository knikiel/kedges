#include "drawing_types.h"

PlanarizedDrawing::PlanarizedDrawing()
    : m_num_nodes(-1)
    , m_positions()
    , m_bend_positions()
{ }

void PlanarizedDrawing::initialize(int num_nodes) {
    m_num_nodes = num_nodes;
    m_positions.resize(num_nodes);
    m_bend_positions.clear();
}

bool PlanarizedDrawing::exists(ogdf::edge real_edge) const {
    return m_bend_positions.find(real_edge) != m_bend_positions.end();
}

int PlanarizedDrawing::number_nodes() const {
    return m_num_nodes;
}

Point PlanarizedDrawing::position(node_id v) const {
    return m_positions[v];
}

std::deque<Point> PlanarizedDrawing::bendings(ogdf::edge real_edge) const {
    assert(exists(real_edge));
    return m_bend_positions.at(real_edge);
}

void PlanarizedDrawing::set_position(node_id v, Point pos) {
    m_positions[v] = pos;
}

void PlanarizedDrawing::add_edge(ogdf::edge real_edge) {
    assert(!exists(real_edge));
    m_bend_positions[real_edge] = std::deque<Point>();
}

void PlanarizedDrawing::add_bending(ogdf::edge real_edge, Point pos) {
    m_bend_positions[real_edge].push_back(pos);
}

UnprocessedDrawing::UnprocessedDrawing(int num_vertices)
    : m_num_vertices(num_vertices)
    , m_positions(num_vertices)
    , m_bend_positions()
{ }

UnprocessedDrawing::UnprocessedDrawing()
    : m_num_vertices(0)
    , m_positions()
    , m_bend_positions()
{ }

UnprocessedDrawing::UnprocessedDrawing(const UnprocessedDrawing& drawing)
    : m_num_vertices(drawing.m_num_vertices)
    , m_positions(drawing.m_positions)
    , m_bend_positions(drawing.m_bend_positions)
{ }

void UnprocessedDrawing::initialize(int num_vertices) {
    m_num_vertices = num_vertices;
    m_positions.clear();
    m_positions.resize(num_vertices);
    m_bend_positions.clear();
}

void UnprocessedDrawing::invert_y_positions() {
    for (size_t i = 0; i < m_positions.size(); ++i) {
        double inverted_y = -m_positions[i].y;
        m_positions[i].y = inverted_y;
    }
    for (auto& keyval : m_bend_positions) {
        auto bendings = keyval.second;
        for (auto& pos : bendings) {
            double inverted_y = -pos.y;
            pos.y *= inverted_y;
        }
    }
}

void UnprocessedDrawing::scale_by(double scale_factor) {
    assert(scale_factor > 0.0);

    for (size_t i = 0; i < m_positions.size(); ++i) {
        m_positions[i].x *= scale_factor;
        m_positions[i].y *= scale_factor;
    }
    for (auto& keyval : m_bend_positions) {
        auto& bendings = keyval.second;
        for (auto& pos : bendings) {
            pos.x *= scale_factor;
            pos.y *= scale_factor;
        }
    }
}

std::vector<node_pair> UnprocessedDrawing::all_edges() const {
    std::vector<node_pair> edges;
    for (auto& keyval : m_bend_positions) {
        edges.push_back(keyval.first);
    }
    return edges;
}

bool UnprocessedDrawing::exists(node_pair edge) const {
    get_correct_direction(&edge);
    return m_bend_positions.count(edge) > 0;
}

int UnprocessedDrawing::number_vertices() const {
    return m_num_vertices;
}

Point UnprocessedDrawing::position(node_id v) const {
    return m_positions[v];
}

std::deque<Point> UnprocessedDrawing::bendings(node_pair edge) const {
    bool was_correct = get_correct_direction(&edge);
    assert(exists(edge));
    if (was_correct) {
        return m_bend_positions.at(edge);
    }
    else {
        std::deque<Point> other_direction;
        for (auto pos : m_bend_positions.at(edge)) {
            other_direction.push_front(pos);
        }
        return other_direction;
    }
}

void UnprocessedDrawing::set_position(node_id v, Point pos) {
    m_positions[v] = pos;
}

void UnprocessedDrawing::add_edge(node_pair edge) {
    assert(!exists(edge));
    get_correct_direction(&edge);
    m_bend_positions[edge] = std::deque<Point>();
}

void UnprocessedDrawing::add_bending(node_pair edge, Point pos) {
    bool was_correct = get_correct_direction(&edge);
    if (was_correct) {
        m_bend_positions[edge].push_back(pos);
    }
    else {
        m_bend_positions[edge].push_front(pos);
    }
}

bool UnprocessedDrawing::get_correct_direction(node_pair* edge) const {
    bool was_correct = true;
    // Kanten verlaufen vom kleineren zum gr��eren Knoten
    if (edge->first > edge->second) {
        // Kante wird aus der anderen Richtung betrachtet 
        was_correct = false;
        *edge = { edge->second, edge->first };
    } 
    return was_correct;
}
