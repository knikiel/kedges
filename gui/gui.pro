CONFIG += debug_and_release
QT += widgets

win32{
    CONFIG(debug, debug|release){
        OGDF_LIB = -L../../OGDF/Win32/Debug/ -lOGDF
        COMMON_LIB = -L../common/debug -lcommond
        DEFINES += OGDF_DEBUG
        TARGET = guid
    } else {
        OGDF_LIB = -L../../OGDF/Win32/Release/ -lOGDF
        COMMON_LIB = -L../common/release -lcommon
        TARGET = gui
    }
}

linux-g++{
    QMAKE_CXXFLAGS += -std=c++11
    CONFIG(debug, debug|release){
        OGDF_LIB = -L../../OGDF/_debug/ -lOGDF
        COMMON_LIB = -L../common/ -lcommond
        OBJECTS_DIR = "debug"
        DEFINES += OGDF_DEBUG
        TARGET = guid
    } else {
        OGDF_LIB = -L../../OGDF/_release/ -lOGDF
        COMMON_LIB = -L../common/ -lcommon
        OBJECTS_DIR = "release"
        TARGET = gui
    }
}

macx-clang{
    QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.9
    QMAKE_CXXFLAGS += -std=c++11 -stdlib=libc++
    OGDF_LIB = -L../../OGDF/_release/ -lOGDF
    COMMON_LIB = -L../common/ -lcommon

    OBJECTS_DIR = "release"
}

OGDF_INCLUDE = ../../OGDF/include/

INCLUDEPATH += $$OGDF_INCLUDE
INCLUDEPATH += ../common/include/
INCLUDEPATH += include/
LIBS += $$COMMON_LIB $$OGDF_LIB
HEADERS += include/*
SOURCES += source/*
