#ifndef COLOR_DIALOG_H
#define COLOR_DIALOG_H

#include <QDialog>
#include <QFormLayout>

/// Dialog, um die Kodierungen der Zahlen in Farben darzustellen.
class ColorDialog : public QDialog {
public:
    ColorDialog();
};

#endif // COLOR_DIALOG_H
