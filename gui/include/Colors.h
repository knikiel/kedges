#ifndef COLORS_H
#define COLORS_H

#include <QColor>

/// Enth�lt einige Konstanten zur Farbdarstellung.
namespace colors {
    // Farben f�r darzustellenden Zahlen
    const QColor COLOR_0 = QColor(Qt::GlobalColor::green);
    const QColor COLOR_1 = QColor(Qt::GlobalColor::yellow);
    const QColor COLOR_2 = QColor(Qt::GlobalColor::red);
    const QColor COLOR_3 = QColor(Qt::GlobalColor::blue);
    const QColor COLOR_4 = QColor(Qt::GlobalColor::darkGreen);
    const QColor COLOR_5 = QColor(Qt::GlobalColor::darkYellow);
    const QColor COLOR_6 = QColor(Qt::GlobalColor::darkRed);
    const QColor COLOR_AT_LEAST_7 = QColor(Qt::GlobalColor::black);

    // Farbvariationen f�r number_color
    const float EDGE_COLOR_DEVIATION = 0.9f;
    const float FACE_COLOR_DEVIATION = 1.f;
};

/// Gibt die Farbe zur�ck, die der Zahl number in den Visualisierungen entspricht.
/// Parameter variation <= 1.0 kann verwendet werden, um die Farbe ein wenig zu variieren.
QColor number_color(int number, float variation = 1.0);

#endif