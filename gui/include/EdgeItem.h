#ifndef KEDGES_EDGEITEM_H
#define KEDGES_EDGEITEM_H

#include "ogdf/basic/geometry.h"
#include "ogdf/basic/Graph.h"
#include <QGraphicsLineItem>
#include <QPen>

// Verwaltung der graphischen Repräsentation einer Kante in der Planarisierung.
class EdgeItem : public QGraphicsItemGroup {
public:
    /// Standardbreite der Kanten.
    static const double DEFAULT_WIDTH;

    /// Standardstift fürs Zeichnen der Kanten.
    static QPen default_pen();

    /// Stift für Markierung der Kante bei der Anzeige von alternativen Verläufen.
    static QPen alternative_course_pen();

    /// Stift für Markierung der Kante bei der Anzeige von möglichen extended Flips.
    static QPen extended_flips_pen();

    /// Stift für Markierung der Kante bei der Umleitung.
    static QPen redirect_edge_pen();

    /// Stift für Markierung von inzidenten Kanten bei der Umleitung.
    static QPen incident_edge_pen();
    
    /// Stiftstyle, wenn k-Zahl durch links bestimmt.
    static const Qt::PenStyle LEFT_K_NUM_STYLE;

    /// Stiftstyle, wenn k-Zahl durch rechts bestimmt.
    static const Qt::PenStyle RIGHT_K_NUM_STYLE;

    /// Stiftstyle, wenn k-Zahl von beiden Seiten bestimmt.
    static const Qt::PenStyle BOTH_K_NUM_STYLE;

public:
    /// Erstelle EdgeItem zu Kante e, mit Kantenverlauf über line.
    EdgeItem(ogdf::edge e, const ogdf::DPolyline& line, int thickness);
    EdgeItem(const EdgeItem& other) = delete;

    virtual ~EdgeItem();

    virtual bool contains(const QPointF& point) const override;

    /// Ändere den Pen, übernehme aber eingestellte Breite.
    void set_pen(const QPen& pen);

    /// Ändere die Kantendicke. Muss für Aktualisierung für jede Kante aufgerufen werden.
    void set_width(double new_thickness);

    /// Standardbrush wird für Kante genutzt
    void reset_brush() ;

    ogdf::edge edge() const { return m_e; }

    QPen pen() const { return m_segments.front()->pen(); }

    /// Gibt Breite der Kanten zurück.
    static int thickness() { return m_thickness; }

    ogdf::DPolyline all_points() const { return m_points; }

private:
    ogdf::edge m_e;
    ogdf::DPolyline m_points;
    std::vector<QGraphicsLineItem*> m_segments;

    // globale Dicke der Kanten
    static double m_thickness;
};

#endif //KEDGES_EDGEITEM_H
