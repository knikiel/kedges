#ifndef EDITINGINFO_H
#define EDITINGINFO_H

#include <string>
#include <vector>

#include <QGraphicsScene>

#include "common.h"
#include "common_types.h"
#include "VisualGraph.h"

    
/// Informationen �ber geladenen Graphen, seine Ver�nderungen und bereits gecachte Berechnungen.
/// Erm�glicht auch das Wiederherstellen von Graphen anhand der gespeicherten Ver�nderungen.
class EditingInfo {
public:
    static const int NOT_EXISTING = -1;
    static const int NO_UNIQUE_FACE = -2;

    EditingInfo();

    UnprocessedDrawing original_drawing() const { return m_orig_drawing; }

    /// Ist der aktuelle Graph modifiziert worden?
    /// Voraussetzung: add_new_change wird immer korrekt nach einer Ver�nderung aufgerufen.
    bool has_older_graph_state() const;

    /// Ist die aktuelle Graphversion eine mittels Redo rekonstruierte Version?
    /// Voraussetzung: add_new_change wird immer korrekt nach einer Ver�nderung aufgerufen.
    bool has_newer_graph_state() const;

    /// Ist die gespeicherte Referenzfl�che auch tats�chlich ausgew�hlt?
    bool is_reference_face_chosen();

    /// Ist die Referenzfl�che die Au�enfl�che?
    bool is_reference_face_external();

    /// Ist die k-Kantenzahl zur ausgew�hlten Fl�che im Cache?
    bool is_k_edge_info_cached() const;

    /// Gibt gecachete k-Kantenzahl zur ausgew�hlten Fl�che zur�ck.
    /// Muss vorhanden sein.
    KEdgeInformation k_edge_info(adjEntry adj);

    /// Gibt die vorige k-Zahl der angegebenen Kante zur�ck, bezogen auf die angebene Referenzfl�che.
    /// Falls die Kante vorher nicht existiert hat, wird NOT_EXISTING zur�ckgegeben.
    /// Falls es keine eindeutig zuordnungsbare Fl�che vorher gab, wird NO_UNIQUE_FACE zur�ckgegeben.
    int previous_k_number(node_id u_id, node_id v_id);

    /// Gibt die folgende k-Zahl der angegebenen Kante zur�ck, bezogen auf die angebene Referenzfl�che.
    /// Falls die Kante danach nicht mehr existiert, wird NOT_EXISTING zur�ckgegeben.
    /// Falls es keine eindeutig zuordnungsbare Fl�che danach gibt, wird NO_UNIQUE_FACE zur�ckgegeben.
    int next_k_number(node_id u_id, node_id v_id);

    /// Initialisiert mit einem neuen Graphen.
    void initialize(std::string path_to_graph, UnprocessedDrawing drawing);

    /// Cachet die k-Kantenzahl zur ausgew�hlten Fl�che.
    void set_k_edge_info(adjEntry adj, KEdgeInformation info);

    /// Setzt das ausgew�hlte Polygon, um dieses bei Graphver�nderungen m�glichst beizubehalten.
    /// Auch kompatibel mit Undo und Redo.
    void set_reference_face_polygon(const QPolygonF& ref_face_polygon, bool is_external = false);

    QPolygonF reference_face_polygon();

    /// L�scht alle gespeicherten Berechnungen zum Graphenzustand. 
    void clear_caches();

    /// L�dt den zugrundeliegenden Graphen neu und gibt ihn zur�ck.
    VisualGraph* recreate_current(QGraphicsScene* scene, int vertex_width, int edge_width);
    
    /// Speichert die n�chste Graphenver�nderung.
    void add_new_change(const GraphChange& diff);

    /// F�hrt die n�chste gespeicherte Ver�nderung durch.
    /// Graph muss konsistent mit den durchzuf�hrenden �nderungen sein!
    /// Gibt neue Referenzfl�che zur�ck, die nach M�glichkeit der angegebenen entspricht.
    /// Eingabegraph wird direkt modifiziert.
    void apply_next_change(VisualGraph* graph);

    /// F�hrt alle vorherigen gespeicherten Ver�nderungen durch, au�er die letzte.
    /// Graph befindet sich dann effektiv im letzten Zustand.
    /// Graph muss dem gespeicherten Originalgraphen entsprechen!
    /// Gibt neue Referenzfl�che zur�ck, die nach M�glichkeit der angegebenen entspricht.
    /// Eingabegraph wird direkt modifiziert.
    void apply_previous_changes(VisualGraph* graph);

private:
    /// F�hrt die (indirekt) angegebene �nderung am Graphen vor.
    void apply_diff(int idx_diff, VisualGraph* graph) const;

    /// Hilfsfunktion f�r die Umsetzung der Kantenumleitung.
    void remove_edge(VisualGraph *graph, const GraphChange& diff) const;

    /// Hilfsfunktion f�r die Umsetzung der Kantenerstellung.
    void add_edge(VisualGraph *graph, const GraphChange& diff) const;

private:
    enum GraphDiffIndex {
        NOT_LOADED = -2,
        ORIGINAL = -1
    };

    std::string m_path_to_loaded_graph;
    UnprocessedDrawing m_orig_drawing;
    std::vector<GraphChange> m_all_diffs; // Informationen, um Graphzust�nde wiederherzustellen
    int m_curr_graph_index; // aktueller Index in all_diffs
    bool m_is_face_chosen;
    bool m_is_ref_face_external;
    QPolygonF m_ref_face_polygon;
    edge_map<int> m_previous_k_numbers;
    edge_map<int> m_next_k_numbers;

    // Caching-Informationen
    bool m_are_k_edges_cached;
    std::map<adjEntry, KEdgeInformation> m_cached_k_edges;
};

#endif // EDITINGINFO_H
