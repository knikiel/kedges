#ifndef FACE_INFO_DIALOG_H
#define FACE_INFO_DIALOG_H

#include <sstream>

#include <QDialog>
#include <QGridLayout>
#include <QToolBox>

#include "VisualGraph.h"
#include "FaceItem.h"

/// Ein Dialog, der Information von k-Zahlen der Kanten bzgl. einer gew�hlten Fl�che darstellt.
class FaceInfoDialog : public QDialog {
    Q_OBJECT

public:
    FaceInfoDialog(const VisualGraph& graph, const FaceItem& face);

    ~FaceInfoDialog();

public slots:
    /// Speichert die angezeigten Informationen in einer Datei.
    void save_info();

private slots:
    /// Aktiviert alle ge�ffneten Tabs mit dem angegebenen Index.
    static void selectTabIndex(int idx);

private:
    /// Erzeugt die Tabelle f�r die angegebene Ungleichung.
    /// Gibt die Anzahl der verletzten Bedingungen zur�ck.
    void create_inequality_tab(int num_vertices, const KEdgeInformation& info,
        const KEdgeSumType& cond_type, const char* description, QToolBox* toolbox);

    static KEdgeSumType selectedType;

    /// enth�lt alle offenen Dialoge
    static std::set<FaceInfoDialog*> openDialogs;

    QPalette m_ok_palette;
    QPalette m_wrong_palette;
    QPalette m_unimportant_palette;
    QPushButton* m_save_button;
    QToolBox* m_toolbox;
    std::stringstream m_print_text;
};

#endif // FACE_INFO_DIALOG_H
