#ifndef KEDGES_FACEITEM_H
#define KEDGES_FACEITEM_H

#include "ogdf/basic/Graph.h"
#include "ogdf/basic/geometry.h"

#include <QGraphicsPolygonItem>
#include <QPainter>

// Verwaltung der graphischen Repräsentation einer Fläche.
class FaceItem : public QGraphicsPolygonItem {
public:
    // statische Methoden
    /// Standardbrush.
    static QBrush default_brush();

    /// Brush für die Referenzfläche.
    static QBrush reference_face_brush();

    /// Brush für die beim Umleiten vorübergehend gemergten Faces.
    static QBrush redirect_edge_brush();
   
    /// Brush für die bei der Umleitung durchlaufenen Faces.
    static QBrush chosen_face_brush();

    /// Brush für Markierung alternativer Kantenverläufe.
    static QBrush alternative_course_brush(int max_idx = 0, int idx = 0);

    /// Brush für Markierung möglicher erweiterter Kantenflips.
    static QBrush extended_flip_brush(int max_idx = 0, int idx = 0);

public:
    /// Erstellt ein FaceItem zur Fläche, die rechts von face_repr liegt.
    /// Zusätzlich werden die Koordinaten der Knoten zur Fläche mitgegeben,
    /// und ob es sich um die äußere Fläche handelt
    FaceItem(ogdf::adjEntry face_repr, bool is_external, QPolygonF polygon);

    virtual ~FaceItem();

    /// Stelle Face etwas anders da, falls als Referenzface gewählt.
    virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem* option,
        QWidget* widget = nullptr) override;

    bool is_external() const { return m_is_external; }
    bool is_reference() const { return m_is_reference;  }
    ogdf::adjEntry first_adjacency() const { return m_face_repr; }
    std::vector<ogdf::adjEntry> adjacencies() const;
    std::vector<ogdf::node> adjacent_nodes() const;

    /// Standardbrush wird für Face genutzt.
    void reset_brush();

    /// Wählt die Fläche als Referenz, passt auch die Darstellung an.
    void set_reference(bool is_ref);

    /// Passt auch die Darstellung an.
    void set_external(bool is_external);

private:
    ogdf::adjEntry m_face_repr;
    bool m_is_external;
    bool m_is_reference;
};

#endif //KEDGES_FACEITEM_H
