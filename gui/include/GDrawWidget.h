#ifndef GDRAWWIDGET_H
#define GDRAWWIDGET_H

#include <QGraphicsView>

#include "EditingInfo.h"
#include "VisualizationParameters.h"
#include "VisualGraph.h"
#include "GraphScene.h"

/// Auswahlmodus der Anwendung.
enum class SelectionState {
    /// initialer Zustand
    NO_GRAPH_LOADED,
    /// Standardzustand mit geladenem Graphen, neue Ref-Fläche kann ausgewählt werden
    REF_FACE,
    /// Zustand zum Hinzufügen eines Knotens
    ADD_VERTEX,
    /// Zustand zum Entfernen eines Knotens
    REMOVE_VERTEX,
    /// Zustände, um eine Kante zum Hinzufügen auszuwählen
    ADD_EDGE_U, ADD_EDGE_V,
    /// Zustand, um den Verlauf einer neuen Kante zu bestimmen
    ROUTE_THROUGH_FACES,
    /// Zustände, um eine zu entfernende Kante auszuwählen
    REMOVE_EDGE_U, REMOVE_EDGE_V,
    /// Zustände zur Auswahl einer Kante, bei der alternative Pfade angezeigt werden sollen
    ALTERNATE_U, ALTERNATE_V, 
    /// Zustände, um eine Kante zum Umleiten auszuwählen
    REROUTE_U, REROUTE_V,
    /// Zustand, um den Verlauf der umzuleitenden Kante zu bestimmen
    REROUTE_THROUGH_FACE
};


/// Art und Anzahl der berechneten Shellings oder Bishellings.
enum class ShellingOutputType {
    // ein minimales Shelling
    MINIMAL,
    // alle minimalen Shellings
    ALL_MINIMAL,
    // alle Shellings
    ALL
};


/// Ermöglicht Operationen über GUI an dem zugrundeliegenden Graphen.
class GDrawWidget : public QGraphicsView {
    Q_OBJECT

    friend class GraphScene;

public:
    GDrawWidget();

    VisualGraph& visual_graph() const { return *m_graph; }

    FaceItem& reference_face() const { return *m_options.reference_face; }

    /// Stellt den angegebenen Graphen dar und initialisiert Widget.
    void set_new_graph(string path, VisualGraph* graph, UnprocessedDrawing drawing);

    /// Löscht die in der Szene dargestellten Elemente.
    void clear_layout();

    /// Berechnet die Layoutgröße für den Bildexport.
    QRect layout_dimensions();

    /// Schreibt den Graphen in aktueller Erscheinung auf den Painter.
    void render(QPainter &painter);

    /// Gehe in den Knotenerstellungsmodus über.
    void begin_vertex_addition();

    /// Bricht die Knotenerstellung ab.
    void cancel_vertex_addition();

    /// Gehe in den Knotenentfernungsmodus über.
    void begin_vertex_removal();

    /// Bricht die Knotenentfernung ab.
    void cancel_vertex_removal();

    /// Starte Auswahl der zu löschenden Kante.
    void begin_edge_removal();

    /// Bricht den Vorgang der Kantenentfernung vorzeitig ab.
    void cancel_edge_removal();

    /// Starte Auswahl der zu erzeugenden Kante.
    void begin_edge_addition();

    /// Bricht den Vorgang der Kantenerstellung vorzeitig ab.
    void cancel_edge_addition();

    /// Gehe in den Kantenumleitungsmodus über.
    void begin_edge_rerouting();

    /// Bricht die Kantenumleitung vorzeitig ab.
    void cancel_rerouting();

    /// Gehe in den Modus zur Berechnung alternativer Pfade einer Kante über.
    void begin_showing_alternative_routes();

    /// Bricht die Berechnung eines alternativen Pfades ab.
    void cancel_showing_alternative_routes();

    /// Berechnet angegebene Menge von Shellings (Größe >=n/2).
    void test_shellability(ShellingOutputType output_amount);

    /// Berechnet angegebene Menge von Bishellings (Größe >=n/2-2).
    void test_bishellability(ShellingOutputType output_amount);

    /// Berechnet Menge von Bishellings zum Refface (Größe >=n/2-2) und gibt sie zurück.
    string test_bishellability_of_ref_face();

    /// Zeige alle Kanten an, die alternative Pfade haben.
    /// Parameter beschreibt, ob nur erweiterte Flips berücksichtigt werden.
    void show_edges_with_alternative_routes(bool only_extended_flips);

    /// Gibt es eine ältere Version des geladenen Graphen?
    bool has_older_graph_version();

    /// Gibt es eine neuere Version des geladenen Graphen?
    bool has_newer_graph_version();

    /// Mache die aktuelle Umleitung rückgängig
    void undo_modification();

    /// Stelle die direkte folgende Umleitung wieder her.
    void redo_modification();

    /// Methode, um den aktuell gespeicherten Zustand wiederherzustellen.
    void reload_graph();

    /// Im Standardzustand des Programms mit geladenem Graphen?
    bool is_in_default_state();

    /// Knoten wird hinzugefügt?
    bool is_in_vertex_addition_state();

    /// Knoten wird entfernt?
    bool is_in_vertex_deletion_state();

    /// Kante wird entfernt?
    bool is_in_edge_deletion_state();

    /// Kante wird erstellt?
    bool is_in_edge_addition_state();

    /// Kantenumleitung findet statt?
    bool is_in_rerouting_state();
    
    /// Auswahl der Knoten für Alternativverlauf findet statt?
    bool is_in_alternate_state();

    /// Färbe die Kanten entsprechend des gegebenen Modus.
    void show_edge_colors(EdgeColoringMode coloring_mode);
    
    /// Graph geladen?
    bool is_graph_loaded() { return m_graph != nullptr; };

public slots:
    // Zeige Kreuzungsknoten.
    void show_crossings(bool marked);

    // Zeige die Faces ohne besondere Markierung.
    void show_simple_faces(bool marked);

    // Zeige die Faces in den Farben, die die Anzahl der verletzten einfachen Bedingungen wiederspiegelt.
    void show_ineq_faces(bool marked);

    // Zeige die Faces in den Farben, die die Anzahl der verletzten c-Bedingungen wiederspiegelt.
    void show_cineq_faces(bool marked);

    // Zeige die Faces in den Farben, die die Anzahl der verletzten cc-Bedingungen wiederspiegelt.
    void show_ccineq_faces(bool marked);

    // Zeige die Faces in den Farben, die die Anzahl der verletzten ccc-Bedingungen wiederspiegelt.
    void show_cccineq_faces(bool marked);

    // Zoome in den Graphen hinein.
    void zoom_in();

    // Zoome aus dem Graphen hinaus.
    void zoom_out();
    
    // Zoome zum Graphen, d.h. passe Skalierung und View-Zentrum an.
    void zoom_to_entire_graph();

    /// Ändere die Breite der Knoten in der Visualisierung.
    void set_vertex_width(double new_width);

    /// Ändere die Breite der Kanten in der Visualisierung.
    void set_edge_width(double new_width);

signals:
    /// Wird gesendet, sobald eine Berechnung am Graphen beendet wird
    void computation_finished();


protected:
    /// Ermöglicht das Zoomen mit dem Mausrad.
    void wheelEvent(QWheelEvent* wheel_event) override;


private:
    /// Initialisiere den Zustand des Widgets, wenn neuer Graph gelesen wurde.
    void initialize(string path, UnprocessedDrawing original_drawing);

    /// Passt die Darstellung des Graphen gemäß der gespeicherten Visualisierungseinstellungen an.
    void visualize_properly();

    /// Markiere die Flächen entsprechend der Anzahl der verletzten Ungleichungen.
    void show_colors_of_faces(KEdgeSumType cum_ineq);

    /// Markiere die Kanten entsprechend ihrer k-Zahlen und des ausgewählten Modus.
    void show_colors_of_edges(EdgeColoringMode color_mode);

    /// Berechnung und Ausgabe von (Bi-)Shellings.
    /// Wenn Bishellings gesucht werden, muss bishellable == true gelten.
    void test_specific_shellability(bool bishellable, ShellingOutputType output_amount);

    /// Vergisst Performance verbessernde Speicherungen wie gecachete Farben, aber nicht Umleitungsdiffs.
    void clear_graph_cache();
    
    /// Räumt nach erfolgreicher Knotenerstellung auf.
    void finish_vertex_addition(const GraphChange &diff);

    /// Räumt nach erfolgreicher Knotenentfernung auf.
    void finish_vertex_removal(const GraphChange &diff);

    /// Räumt nach erfolgreicher Kantenentfernung auf.
    void finish_edge_removal(const GraphChange &diff);

    /// Räumt nach erfolgreicher Kantenerstellung auf.
    void finish_edge_addition(const GraphChange& diff);

    /// Räumt nach dem Rerouting anstängig auf.
    void finish_rerouting(const GraphChange& new_diff);

    /// Stellt einen älteren Kantenverlauf wieder her.
    void reconstruct_old_edge_course();

    /// Befindet sich das angegebene Face unter den bereits ausgewählten?
    bool is_already_chosen(FaceItem* face);

private:

    struct EdgeSelectionInfo {
        VertexItem* u;
        VertexItem* v;

        EdgeSelectionInfo() {
            u = nullptr;
            v = nullptr;
        }

        void clear() {
            u = nullptr;
            v = nullptr;
        }
    };

    struct EdgeRoutingInfo {
        EdgeSelectionInfo vertex_pair;
        int num_removed_crossings;
        std::vector<adjEntry> crossed_adjacencies;
        std::vector<FaceItem*> chosen_faces;
        std::vector<std::vector<Point>> positions;
        std::vector<QGraphicsLineItem*> segments;
        QPointF last_point;

        void clear() {
            vertex_pair.clear();
            crossed_adjacencies.clear();
            chosen_faces.clear();
            positions.clear();
            for (auto segment : segments) {
                auto scene = segment->scene();
                scene->removeItem(segment);
                delete segment;
            }
            segments.clear();
        }
    };

    // Attribute
    GraphScene* m_scene;
    double m_curr_scale;

    VisualGraph* m_graph;

    VisualizationParameters m_options;
    SelectionState m_selection_state;
    EdgeRoutingInfo m_edge_routing_info;
    EdgeSelectionInfo m_edge_selection_info;

    EditingInfo m_editing_info;

public:
    // Konstanten
    static const double ZOOM_FACTOR;
    static const double MIN_EXPORTING_SCALE;
};

#endif
