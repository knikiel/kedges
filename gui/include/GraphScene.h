#ifndef GRAPH_SCENE_H
#define GRAPH_SCENE_H

#include "ogdf/basic/Graph.h"
#include "qgraphicsitem.h"
#include "qgraphicsscene.h"

#include "common_types.h"
#include "VertexItem.h"
#include "FaceItem.h"

class GDrawWidget;

/// Stellt den geladenen Graphen dar und erm�glicht Auswahl der Graphelemente f�r Modifikationen.
class GraphScene : public QGraphicsScene {
public:
    GraphScene(GDrawWidget* widget);

protected:
    /// Erm�glicht die Interaktionen mit den Graph-Items.
    void mousePressEvent(QGraphicsSceneMouseEvent* mouseEvent) override;

private:
    /// W�hlt ein passendes Referenzface aus.
    void choose_reference_face(const QList<QGraphicsItem*>& items);

    /// F�ge einen neuen Knoten an der ausgew�hlten Stelle hinzu.
    void add_vertex(const Point& position);

    /// L�sche den ausgew�hlten Knoten.
    void remove_vertex(VertexItem* v_item);

    /// Findet die echten Endknoten einer Kante anhand eines Segments.
    std::pair<VertexItem*, VertexItem*> find_vertex_items(ogdf::edge e_copy);

    /// W�hle Kante, zu der alternative Wege gefunden werden sollen.
    void choose_first_vertex_for_alternative_paths(VertexItem* v_item);
    void choose_second_vertex_for_alternative_paths(VertexItem* v_item);

    /// W�hle Kante, die gel�scht werden soll.
    void choose_first_vertex_for_edge_removal(VertexItem* v_item);
    void choose_second_vertex_for_edge_removal(VertexItem* v_item);

    /// W�hle Kante, die hinzugef�gt werden soll, und dessen Verlauf.
    void choose_first_vertex_for_edge_addition(VertexItem* v_item, QPointF v_pos);
    void choose_second_vertex_for_edge_addition(VertexItem* v_item);
    void finish_edge_addition(VertexItem* v_item, QPointF pos);

    /// W�hle den neuen Verlauf der umzuleitenden Kanten.
    void choose_first_vertex_for_rerouting(VertexItem* v_item, QPointF v_pos);
    void choose_second_vertex_for_rerouting(VertexItem* v_item);
    void choose_next_face(FaceItem* f_item, bool external, QPointF pos);
    void finish_rerouting(VertexItem* v_item, QPointF pos);

    /// W�hlt das angegebene Face als Referenzface aus.
    void set_new_reference_face(FaceItem* f_item);

    /// Markiere entsprechende Knoten als ausw�hlbar.
    void mark_adjacent_v_items(VertexItem* v_item);
    void mark_non_adjacent_v_items(VertexItem* v_item);

    GDrawWidget* gdraw;
};

#endif