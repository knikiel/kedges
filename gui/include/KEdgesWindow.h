#ifndef KEDGESWINDOW_H
#define KEDGESWINDOW_H

#include <cassert>
#include <string>

#include <QMainWindow>
#include <QPushButton>
#include <QCheckBox>
#include <QLineEdit>
#include <QSpinBox>
#include <QTextEdit>
#include <QLabel>
#include <QRadioButton>

#include "GDrawWidget.h"
#include "ColorDialog.h"

// Darstellung der Oberfläche und Controllerfunktionen.
class KEdgesWindow : public QMainWindow {
    Q_OBJECT

public:
    KEdgesWindow(QWidget *parent = nullptr);

private:
    /// Erstellt ein DockWidget für die Ausgaben.
    void create_message_system();

    /// Erstellt ein DockWidget für die Visualisierungsmöglichkeiten.
    void create_visualization_options();
    
    /// Erstellt ein DockWidget für einige Grapheigenschaften.
    void create_graph_properties_view();

    /// Erstellt ein DockWidget, um Graph zu modifizieren.
    void create_graph_modifications();

#ifdef OGDF_DEBUG
    /// Erstellt Debug-Optionen.
    void create_debug_options();
#endif

    /// Erstellt das Menü, inkl. Untermenüs.
    void create_menu();

private slots:
    /// Slot, um Darstellung der Kreuzungsknoten an- oder auszuschalten. 
    void show_crossings(bool show);

    // Slots, um Kantenfarben nach k-Zahlen zu bestimmen
    void show_no_edge_colors(bool show);
    void show_kdiffs_to_prev(bool show);
    void show_kdiffs_to_next(bool show);
    void show_knumbers_with_colors(bool show);

    /// Slot, um aktualisierte Grapheigenschaften anzuzeigen.
    void recompute_graph_properties();

    // Slots für die Graphmodifikationen.
    void remove_vertex_pressed();
    void remove_edge_pressed();
    void add_vertex_pressed();
    void add_edge_pressed();
    void reroute_edge_pressed();

    // Slots, um besondere Kanten hervorzuheben.
    void show_alternative_routes_pressed();
    void show_edges_with_alternative_routes_pressed();
    void show_edges_with_extended_flips_pressed();

    /// Slot, um die Verletzungen zu einer guten Zeichnung anzuzeigen.
    void print_violations_to_good_drawing();

    // Slots fürs Rückgängigmachen und Wiederherstellen
    void undo_pressed();
    void redo_pressed();

#ifdef OGDF_DEBUG
    // Slots um die Graphelemente zufällig zu färben
    void debug_faces_pressed();
    void debug_edges_pressed();
#endif

    /// Slot, um alle Ausgaben zu speichern.
    void save_output();

    /// Slot, um einen Graph zu laden.
    void load_graph();

    /// Slot, um ein Bild vom geladenen Graphen zu speichern.
    void export_picture();

    /// Slot, um den geladenen Graphen zu speichern.
    void save_in_file();

    // Slots, um (Bi-)Shellings zu berechnen
    void compute_one_minimal_shelling_pressed();
    void compute_minimal_shellings_pressed();
    void compute_all_shellings_pressed();
    void compute_one_minimal_bishelling_pressed();
    void compute_minimal_bishellings_pressed();
    void compute_all_bishellings_pressed();
    void compute_all_bishellings_of_face_pressed();

    // Slots, um bestimmte Dialoge zu öffnen.
    void show_color_help();
    void show_program_help();
    void show_info_reference_face();

    /// Reinitialisert den Status der Buttons, sodass nur korrekte Operationen gemacht werden können.
    void reinitialize_button_states();

private:
    /// Deaktiviert alle Buttons, die einen geladenen Graphen benötigen.
    void disable_all_buttons();

    GDrawWidget* m_gdraw_widget;
    QDoubleSpinBox* m_edge_width_widget;
    QDoubleSpinBox* m_vertex_width_widget;
    QLabel* m_number_crossings_label;
    QLabel* m_number_vertices_label;
    QLabel* m_number_edges_label;
    QLabel* m_harary_hill_label;
    QLabel* m_good_drawing_label;
    ColorDialog* m_color_info_dialog;
    QAction* m_export_action;
    QAction* m_export_plandescr_action;
    QAction* m_ref_face_info_action;
    QAction* m_alternate_all_action;
    QAction* m_compute_one_minimal_shelling_action;
    QAction* m_compute_minimal_shellings_action;
    QAction* m_compute_all_shellings_action;
    QAction* m_compute_one_minimal_bishelling_action;
    QAction* m_compute_minimal_bishellings_action;
    QAction* m_compute_all_bishellings_action;
    QAction* m_compute_all_bishellings_of_face_action;
    QAction* m_violations_good_drawing_action;
    QPushButton* m_alternative_courses_button;
    QPushButton* m_rerouting_button;
    QPushButton* m_remove_vertex_button;
    QPushButton* m_remove_edge_button;
    QPushButton* m_add_vertex_button;
    QPushButton* m_add_edge_button;
    QPushButton* m_undo_button;
    QPushButton* m_redo_button;
    QTextEdit* m_output;
    QRadioButton* m_diff_to_prev_colored_button;
    QRadioButton* m_diff_to_next_colored_button;

    QDockWidget* m_message_system_dock;
    QDockWidget* m_visualization_options_dock;
    QDockWidget* m_graph_properties_dock;
    QDockWidget* m_modifications_dock;
#ifdef OGDF_DEBUG
    QDockWidget* m_debug_options_dock;
#endif

    const char* BEGIN_REMOVING_VERTEX_TEXT = "Knoten entfernen";
    const char* ABORT_REMOVING_VERTEX_TEXT = "Knotenentfernung abbrechen";
    const char* BEGIN_REMOVING_EDGE_TEXT= "Kante entfernen";
    const char* ABORT_REMOVING_EDGE_TEXT = "Kantenentfernung abbrechen";
    const char* BEGIN_ADDING_VERTEX_TEXT= "Knoten hinzufügen";
    const char* ABORT_ADDING_VERTEX_TEXT = "Knotenerstellung abbrechen";
    const char* BEGIN_ADDING_EDGE_TEXT= "Kante hinzufügen";
    const char* ABORT_ADDING_EDGE_TEXT = "Kantenerstellung abbrechen";
    const char* BEGIN_REROUTING_TEXT = "Kante umleiten";
    const char* ABORT_REROUTING_TEXT = "Kantenumleitung abbrechen";
    const char* BEGIN_ALTERNATIVE_TEXT = "Alternativen Kantenverlauf zeigen";
    const char* ABORT_ALTERNATIVE_TEXT = "Alternativenberechnung abbrechen";

    const char* NOTHING_LOADED_TEXT = "Aktion nicht möglich, da kein Graph geladen.";

    static const double DEFAULT_EDGE_WIDTH;
    static const double STEP_EDGE_WIDTH;
    static const double MIN_EDGE_WIDTH;
    static const double MAX_EDGE_WIDTH;

    static const double DEFAULT_VERTEX_WIDTH;
    static const double STEP_VERTEX_WIDTH;
    static const double MIN_VERTEX_WIDTH;
    static const double MAX_VERTEX_WIDTH;
};

#endif
