#include <QTextEdit>
#include <QMessageBox>

#include "GDrawWidget.h"

class MessageSystem {
public:
    /// Zeigt einen modalen Fragedialog.
    /// Gibt true zur�ck, wenn die Frage bejaht wurde.
    static bool show_question_box(const char* text, const char* sub_text,
        QMessageBox::Icon icon = QMessageBox::Question);

    /// Gibt vor, welche Zustandsnachricht ausgegeben werden soll.
    static void print_state_message(SelectionState state);

    /// Schreibt eine Nachricht in das Ausgabewidget.
    static void print_message(const char* text);

    /// Schreibt eine Nachricht in das Ausgabewidget.
    static void print_message(std::string text);

    /// wird f�r Zustandsnachrichten benutzt
    static QTextEdit* state_text_widget;
    /// wird f�r andere Nachrichten benutzt
    static QTextEdit* sub_text_widget;

private:
    // Meldungen zu den entsprechenden SelectionStates
    static const char* NO_GRAPH_LOADED_TEXT;
    static const char* REF_FACE_TEXT;
    static const char* ADD_VERTEX_TEXT;
    static const char* REMOVE_VERTEX_TEXT;
    static const char* ADD_EDGE_U_TEXT;
    static const char* ADD_EDGE_V_TEXT;
    static const char* ROUTE_THROUGH_FACES_TEXT;
    static const char* REMOVE_EDGE_U_TEXT;
    static const char* REMOVE_EDGE_V_TEXT;
    static const char* ALTERNATE_U_TEXT;
    static const char* ALTERNATE_V_TEXT;
    static const char* REROUTE_U_TEXT;
    static const char* REROUTE_V_TEXT;
    static const char* REROUTE_THROUGH_FACE_TEXT;
};