#ifndef KEDGES_VERTEXITEM_H
#define KEDGES_VERTEXITEM_H

#include "ogdf/basic/Graph.h"
#include <QGraphicsRectItem>
#include <QGraphicsEllipseItem>

/// Verwaltung der graphischen Repräsentation eines echten Knotens (liegt schon im Originalgraphen vor)
/// oder Kreuzungsknotens (aus der Planarisierung). 
class VertexItem : public QGraphicsEllipseItem {
public:
    /// Standardbrush für echte Knoten
    static QBrush default_brush();
    
    /// Standardbrush für Kreuzungsknoten
    static QBrush crossing_brush();

    /// Brush für ausgewählbare Knoten.
    static QBrush selectable_brush();

    /// Brush für ausgewählte Knoten.
    static QBrush chosen_brush();

    /// Standardbreite eines VertexItems.
    static const int DEFAULT_WIDTH;

public:
    /// Erstelle VertexItem zu echtem Knoten oder Kreuzungsknoten v,
    /// mit Label text, Position (x, y) und Breite width.
    VertexItem(ogdf::node v, QString text, bool not_crossing,
        qreal x, qreal y, qreal width);

    virtual ~VertexItem();

    ogdf::node node() const { return m_node; }
    bool is_not_crossing() const { return m_is_not_crossing; }

    // Workarounds für fehlerhafte Rückgaben von x() und y().
    double x_pos() const { return m_x_pos; }
    double y_pos() const { return m_y_pos; }

    void reset_brush(bool visible); 
    void set_width(double new_width);

private:
    ogdf::node m_node;

    QGraphicsTextItem* m_text;

    bool m_is_not_crossing;
    double m_x_pos;
    double m_y_pos;
};

#endif //KEDGES_VERTEXITEM_H
