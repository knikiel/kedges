#ifndef KEDGES_VISUALGRAPH_H
#define KEDGES_VISUALGRAPH_H

#include <set>

#include "ogdf/basic/Graph.h"
#include "ogdf/planarity/PlanRep.h"
#include <QGraphicsScene>

#include "common.h"
#include "EdgeItem.h"
#include "VertexItem.h"
#include "FaceItem.h"

#ifdef OGDF_DEBUG
// Hilfsfunktion, um Faces zufällig einzufärben.
void debug_color(FaceItem* f_item, int val = 1);

// Hilfsfunktion, um Kanten zufällig einzufärben.
void debug_color(EdgeItem* e_item, int val = 1);
#endif

// Datenstruktur, welche die interne Struktur des Graphen nach OGDF mit seiner visuellen Darstellung mit dem Qt-Framework vereinigt.
// Ermöglicht dynamische Anpassung des zugrundeliegenden Graphen und Interaktion in der GUI.
class VisualGraph {
public:
    /// Erstellt einen neuen VisualGraph.
    /// Der planarisierte Graph planrep soll durch drawing aus graph entstanden sein.
    VisualGraph(QGraphicsScene* scene,
        ogdf::Graph* graph, ogdf::PlanRep* planrep,
        const PlanarizedDrawing& drawing,
        double vertex_width, double edge_width);

    ~VisualGraph();

    /// Planarisierter Graph wird ausgeliehen.
    ogdf::PlanRep* planar_repr() const { return m_planrep; }

    /// Graph wird ausgeliehen.
    const ogdf::Graph* graph() const { return m_graph; }

    /// Gibt die Segmente einer Kante zurück.
    std::vector<EdgeItem*> edge_path(VertexItem* u_item, VertexItem* v_item) const;

    /// Gibt alle echten Knoten-Items zurück (keine Kreuzungen).
    std::set<VertexItem*> vertex_items() const { return m_vertices; } 

    /// Gibt alle Kreuzung-Items zurück.
    std::set<VertexItem*> crossing_items() const { return m_crossings; } 

    /// Anzahl der echten Knoten.
    int number_vertices() const { return m_graph->numberOfNodes(); }

    /// Anzahl der Kreuzungen.
    int number_crossings() const { return m_crossings.size(); }

    /// Anzahl der Kanten.
    int number_edges() const { return m_graph->numberOfEdges(); }

    std::set<EdgeItem*> edge_items() const { return m_edges; }

    std::set<FaceItem*> face_items() const { return m_faces; }

    /// Gibt Fläche zurück, die zur Adjazenz gehört.
    FaceItem* face_item_for_adjacency(adjEntry adj) const;

    FaceItem* external_face_item() const { return m_external_face; }

    FaceItem* reference_face_item() const { return m_reference_face; }

    double vertex_width() const { return m_vertex_width; }

    double edge_width() const { return m_edge_width; }

    /// Gibt zurück, ob die beiden angegebnen Knoten im zugrundeliegenden Graphen verbunden sind.
    bool original_edge_exists(VertexItem* u_item, VertexItem* v_item) const;

    /// Füge einen neuen Knoten an der angegeben Stelle ein.
    GraphChange add_vertex(const Point& position);

    /// Entfernt den angegeben echten Knoten und seine inzidenten Kanten.
    GraphChange remove_vertex(VertexItem* v_item);

    /// Fügt die Kante mit dem angegebenen Verlauf zwischen den Knoten hinzu.
    /// chosen_faces beschreibt die von der Kante durchlaufenen Flächen, die durch die Adjazenzen in crossed_adjacencies
    /// verbunden sind. bend_positions beinhaltet alle Knicke der Kante in den entsprechenden Flächen.
    GraphChange add_edge(node u_copy, node v_copy, const std::vector<FaceItem*>& chosen_faces,
        const std::vector<adjEntry>& crossed_adjacencies,
        const std::vector<std::vector<Point>>& bend_positions);

    /// Setzt die Referenzfläche mit Hilfe des angegeben Polygons.
    /// Setzt die richtigen Flags in der Fläche.
    /// Falls eine eindeutige Fläche gefunden wird, wird true zurückgegeben, ansonsten false
    bool set_reference_face(const QPolygonF& ref_face_polygon, bool is_ref_face_external);

    /// Setzt die Referenzfläche.
    /// Setzt die richtigen Flags in der Fläche.
    void set_reference_face(FaceItem* ref_face);

    /// Entfernt die Kante vollständig aus dem Graphen.
    GraphChange remove_edge(VertexItem* first_vertex, VertexItem* second_vertex);

    // Überprüft, ob die angebenen Flächen benachbart sind.
    bool are_faces_adjacent(FaceItem* f1, FaceItem* f2) const;

    // Überprüft, ob die Fläche den Knoten berührt.
    bool is_face_adjacent_to_vertex(VertexItem* u_item, FaceItem* f_item) const;

    /// Gibt die Positionen der echten Knoten und die Knicke der Kanten zurück, ohne Berücksichtung der Kreuzungen.
    UnprocessedDrawing to_crossingless_drawing() const;

    /// Berechnet alternative Pfade von Startknoten u zum Zielknoten v über Flächen.
    std::vector<std::vector<FaceItem*>> compute_alternative_courses(
        VertexItem* u_item, VertexItem* v_item, bool only_extended_flips) const;

    /// Testet, ob der gegebene Graph shellable ist und berechnet evtl. Shellings.
    bool compute_shellings(FaceItem* f_item, int r, node u_1, node u_r, std::vector<std::vector<node>>* shellings);

    /// Testet, ob der gegebene Graph bishellable ist und berechnet evtl. Shellings.
    bool compute_bishellings(FaceItem* f_item, int r, node a_0, node b_0, std::vector<std::vector<node>>* bishellings);

    /// Berechnet die Anzahl der Zusammenhangskomponenten.
    int number_connected_components();

    /// Gibt alle im Originalgraphen benachbarten Knoten zurück (nicht Kreuzungsknoten!).
    std::vector<VertexItem*> adjacent_v_items(VertexItem* v_item);

    /// Gibt an, welche Adjazenz vom angegebenen Kantenverlauf zwischen 2 Flächen geschnitten wird.
    adjEntry crossed_adjacency(Point pos1, Point pos2, FaceItem* f1_item, FaceItem* f2_item);

    /// Markiert alle inzidenten Kanten.
    void mark_incident_edges(const VertexItem& v_item);

    /// Gibt zurück, ob die zugrundeliegende Zeichnung/Planarisierung gutartig ist.
    /// Gutartig heißt, dass (1) sich keine Kanten mehr als 1 mal kreuzen,
    /// (2) sich Kanten mit gemeinsamen Endknoten nicht kreuzen, und
    /// (3) keine Kante sich selbst kreuzt.
    bool is_drawing_good() const;

    /// Gibt Verletzungen an, wenn zugrundeliegende Zeichnung/Planarisierung nicht gutartig ist.
    std::vector<string> determine_good_drawing_violations() const;

    /// Ändert die Breite der Knoten.
    void set_vertex_width(double new_thickness);

    /// Ändert die Breite der Kanten.
    void set_edge_width(double new_thickness);


private:
    /// Fügt alle Items neu zur Szene hinzu, damit sie korrekt übereinander gestapelt sind.
    void reset_overlapping_items();

    /// Initialisiert die Graphkomponenten mit ihren Positionen.
    void compute_initial_layout(QGraphicsScene* scene, const GraphAttributes& geom,
        adjEntry external_adj); 

    /// Initialisiert die Graphkomponenten mit ihren Positionen.
    void compute_initial_layout(QGraphicsScene* scene, const PlanarizedDrawing& geom,
        adjEntry external_adj); 

    /// Erstellt alle Face-Items für korrekte Initialisierung.
    void create_faces(QGraphicsScene* scene, const GraphAttributes &attrs, adjEntry external_adj);

    /// Erstellt alle Kanten-Items für korrekte Initialisierung.
    void create_edges(QGraphicsScene* scene, const GraphAttributes &attrs);

    /// Erstellt alle Knoten-Items für echte Knoten für korrekte Initialisierung.
    void create_vertices(QGraphicsScene* scene, const GraphAttributes &attrs);

    /// Erstellt alle Knoten-Items für Kreuzungen für korrekte Initialisierung.
    void create_crossings(QGraphicsScene* scene, const GraphAttributes &attrs);

    /// Für Shelling-Berechnung: Bestimmt die an einer erweiterten Referenzfläche liegenden echten Knoten.
    void compute_outer_vertices(const std::vector<FaceItem*>& reference_faces, node u_last,
        const std::vector<node>& other_vertices, std::vector<node>* outer_vertices) const;

    /// Für Shelling-Berechnung: Löscht den Knoten logisch und erweitert die Menge der Referenz-Faces entsprechend.
    int delete_vertex_for_shelling_tests(node outer_vertex, std::vector<FaceItem*>* reference_faces,
        std::vector<edge>* surrounding_edges) const;

    /// Teste alle Kombinationen für Shelling-Paare.
    bool test_shelling_pairs(FaceItem *reference_face, const std::vector<node> &shelling_sequence) const;

    /// Für Bishelling-Berechnung: Berechne eine Folge von Knoten für Bishellings.
    bool compute_removal_orders_rec(int r, node other_end_vertex,
        std::vector<node>* curr_tested_order,
        std::vector<FaceItem*>* reference_faces, std::vector<edge>* surrounding_edges,
        std::vector<std::vector<node>>* removal_orders) const;

    /// Testet erste und zweite Knotenreihenfolgen auf Shellbarkeit, korrekte Bishellings werden hintereinander in bishellings gespeichert.
    bool compute_bishellings_from_removal_orders(const std::vector<std::vector<node>>& first_removal_orders,
        const std::vector<std::vector<node>>& second_removal_orders, std::vector<std::vector<node>>* bishellings) const;

    /// Für Shelling-Berechnung: Berechne eine korrekte Shelling-Sequenz.
    bool compute_shellings_rec(int r, node u_last,
        std::vector<node>* curr_tested_shelling,
        std::vector<FaceItem*>* reference_faces, std::vector<edge>* surrounding_edges,
        std::vector<std::vector<node>>* shellings) const;

    /// Fügt die Positionen zusammen, die das angegebene Face umschließen (Anfang == Ende).
    DPolyline positions_of_face_vertices(adjEntry first);

    /// Lösche das Flächen-Item und entferne es aus allen internen Datenstrukten.
    void delete_face_item(FaceItem* f_item);

    /// Lösche das dazugehörige Kanten-Item und entferne es aus allen internen Datenstrukten.
    void delete_edge_item(edge e);

    /// Lösche das Knoten-Item und entferne es aus allen internen Datenstrukten.
    void delete_vertex_item(node v, bool not_crossing);

    /// falls CONSISTENCE-Flag aktiv: Prüft Konsistenz der verwalteten Datenstrukturen.
    void assert_consistencies();

private:
    // zugrundeliegende Graphstrukturen aus OGDF
    ogdf::Graph *m_graph;
    ogdf::PlanRep *m_planrep;

    // graphische Elemente für die Graphkomponenten
    QGraphicsScene *m_scene;
    std::set<FaceItem*> m_faces;
    std::set<EdgeItem*> m_edges;
    std::set<VertexItem*> m_vertices;
    std::set<VertexItem*> m_crossings;
    FaceItem* m_external_face;
    FaceItem* m_reference_face;

    double m_edge_width;
    double m_vertex_width;

    // Mappings der verschiedenen Graphrepräsentationen aufeinander
    // die Graphelemente stammen aus der Planarisierung
    std::map<node, VertexItem*> m_node_to_v_item;
    std::map<edge, EdgeItem*> m_edge_to_e_item;
    std::map<adjEntry, FaceItem*> m_adj_to_f_item;
};

#endif //KEDGES_VISUALGRAPH_H
