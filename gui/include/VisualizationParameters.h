#ifndef KEDGES_LAYOUT_OPTIONS_H
#define KEDGES_LAYOUT_OPTIONS_H

#include "ogdf/basic/Graph_d.h"
#include "common_types.h"
#include "FaceItem.h"


/// Parameter f�r Darstellung des Graphen.
struct VisualizationParameters {
    FaceItem* reference_face;
    bool cross_ids_shown;
    EdgeColoringMode edge_coloring;
    KEdgeSumType cum_ineq_shown;
};

#endif //KEDGES_LAYOUT_OPTIONS_H