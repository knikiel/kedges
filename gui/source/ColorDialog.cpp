#include "ColorDialog.h"

#include <QPalette>
#include <QLabel>

#include "Colors.h"

QWidget* create_color_widget(QColor color) {
    QWidget* widget = new QWidget();
    widget->setMinimumSize(QSize(40, 15));
    QPalette palette;
    palette.setColor(QPalette::ColorRole::Background, color);
    widget->setPalette(palette);
    widget->setAutoFillBackground(true);
    return widget;
}

ColorDialog::ColorDialog()
    : QDialog()
{
    setWindowTitle("Farbkodierungen");
    setMinimumWidth(200);
    auto layout = new QFormLayout();
    layout->addRow(QString("0"), create_color_widget(colors::COLOR_0));
    layout->addRow(QString("1"), create_color_widget(colors::COLOR_1));
    layout->addRow(QString("2"), create_color_widget(colors::COLOR_2));
    layout->addRow(QString("3"), create_color_widget(colors::COLOR_3));
    layout->addRow(QString("4"), create_color_widget(colors::COLOR_4));
    layout->addRow(QString("5"), create_color_widget(colors::COLOR_5));
    layout->addRow(QString("6"), create_color_widget(colors::COLOR_6));
    layout->addRow(QString("7 oder mehr"), create_color_widget(colors::COLOR_AT_LEAST_7));
    setLayout(layout);
}
