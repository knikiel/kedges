#include "Colors.h"

#include <cassert>

QColor number_color(int num, float scale) {
    assert(0 < scale && scale <= 1);
    QColor color;
    switch (num) {
    case 0: color = colors::COLOR_0; break;
    case 1:	color = colors::COLOR_1; break;
    case 2:	color = colors::COLOR_2; break;
    case 3:	color = colors::COLOR_3; break;
    case 4:	color = colors::COLOR_4; break;
    case 5:	color = colors::COLOR_5; break;
    case 6:	color = colors::COLOR_6; break;
    default: color = colors::COLOR_AT_LEAST_7; break;
    }
    QColor qcolor = QColor(color);
    return QColor((int) qcolor.red() * scale, 
        (int) qcolor.green() * scale, (int) qcolor.blue() * scale);
}
