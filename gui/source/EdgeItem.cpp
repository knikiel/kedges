#include "EdgeItem.h" 

#include "ogdf/basic/geometry.h"
#include "ogdf/basic/Graph.h"
#include <QPen>

using namespace ogdf;

const Qt::PenStyle EdgeItem::LEFT_K_NUM_STYLE = Qt::DotLine;
const Qt::PenStyle EdgeItem::RIGHT_K_NUM_STYLE = Qt::SolidLine;
const Qt::PenStyle EdgeItem::BOTH_K_NUM_STYLE = Qt::DashLine;
const double EdgeItem::DEFAULT_WIDTH = 7;
double EdgeItem::m_thickness = EdgeItem::DEFAULT_WIDTH;

EdgeItem::EdgeItem(ogdf::edge e, const DPolyline& line, int thickness)
    :
    m_e(e),
    m_points(line),
    m_segments()
{
    m_thickness = thickness;
    DPoint prev_point(-1e10, -1e10);
    for (auto point : line) {
        if (prev_point != DPoint(-1e10, -1e10)) {
            QGraphicsLineItem* segment = new QGraphicsLineItem(prev_point.m_x, prev_point.m_y,
                point.m_x, point.m_y);
            m_segments.push_back(segment);
            addToGroup(segment);
        }
        prev_point = point;
    }
    reset_brush();
}

EdgeItem::~EdgeItem() {
    m_segments.clear();
}

bool EdgeItem::contains(const QPointF& point) const {
    for (auto segment : m_segments) {
        if (segment->contains(point)) {
            return true;
        }
    }
    return false;
}

void EdgeItem::reset_brush() {
    QPen pen = default_pen();
    pen.setWidth(m_thickness);
    set_pen(pen);
}

QPen EdgeItem::default_pen() {
    QPen pen;
    pen.setColor(Qt::GlobalColor::black);
    pen.setStyle(Qt::SolidLine);
    return pen;
}

void EdgeItem::set_pen(const QPen& pen) {
    QPen new_pen = pen;
    new_pen.setWidth(m_thickness);
    for (auto segment : m_segments) {
        segment->setPen(new_pen);
    }
}

void EdgeItem::set_width(double new_thickness) {
    m_thickness = new_thickness;
    QPen new_pen = m_segments.front()->pen();
    new_pen.setWidthF(m_thickness);
    for (auto segment : m_segments) {
        segment->setPen(new_pen);
    }
}

QPen EdgeItem::redirect_edge_pen() {
    QPen pen;
    pen.setWidth(m_thickness+1);
    pen.setStyle(Qt::DashDotDotLine);
    pen.setColor(Qt::cyan);
    return pen;
}

QPen EdgeItem::incident_edge_pen() {
    QPen pen;
    pen.setWidth(m_thickness);
    pen.setStyle(Qt::DashDotDotLine);
    pen.setColor(Qt::darkMagenta);
    return pen;
}

QPen EdgeItem::alternative_course_pen() {
    QPen pen;
    pen.setWidth(m_thickness+1);
    pen.setColor(Qt::GlobalColor::gray);
    pen.setStyle(Qt::DashDotDotLine);
    return pen;
}

QPen EdgeItem::extended_flips_pen() {
    QPen pen;
    pen.setWidth(m_thickness+1);
    pen.setColor(Qt::GlobalColor::black);
    pen.setStyle(Qt::DashDotDotLine);
    return pen;
}
