#include "EditingInfo.h"

EditingInfo::EditingInfo()
{
    m_path_to_loaded_graph = "";
    m_curr_graph_index = NOT_LOADED;
    m_is_face_chosen = false;
    m_are_k_edges_cached = false;
}

void EditingInfo::initialize(std::string path_to_graph, UnprocessedDrawing drawing) {
    m_path_to_loaded_graph = path_to_graph;
    m_orig_drawing = drawing;
    m_all_diffs.clear();
    m_curr_graph_index = ORIGINAL;
    m_is_face_chosen = false;
    m_is_ref_face_external = false;
    m_ref_face_polygon.clear();
    m_previous_k_numbers.clear();
    m_next_k_numbers.clear();

    m_are_k_edges_cached = false;
    m_cached_k_edges.clear();
}

bool EditingInfo::is_reference_face_chosen() {
    return m_is_face_chosen;
}

bool EditingInfo::is_reference_face_external() {
    return m_is_ref_face_external || !m_is_face_chosen;
}

QPolygonF EditingInfo::reference_face_polygon() {
    return m_ref_face_polygon;
}

int EditingInfo::previous_k_number(node_id u_id, node_id v_id) {
    assert(has_older_graph_state());
    // falls nicht im Cache: berechne k-Zahlen der vorangehenden Graphversion und cache sie
    if (m_previous_k_numbers.empty()) {
        // lade Graph tempor�r neu
        QGraphicsScene dummy_scene;
        Graph* graph = new Graph();
        PlanarizedDrawing drawing;
        PlanRep* planrep = planarize(m_orig_drawing, graph, &drawing);
        VisualGraph visual_graph(&dummy_scene, graph, planrep, drawing, 1, 1);
        // stelle vorangehende Graphversion her
        for (int i = 0; i < m_curr_graph_index; ++i) {
            apply_diff(i, &visual_graph);
        }
        // teste, ob k-Zahl in der vorherigen Version wohl definiert ist
        bool face_is_found = visual_graph.set_reference_face(m_ref_face_polygon, m_is_ref_face_external);
        if (!face_is_found) {
            return NO_UNIQUE_FACE;
        }
        int num_connected_components = visual_graph.number_connected_components();
        if (num_connected_components > 1) {
            return NO_UNIQUE_FACE;
        }
        // berechne und cache
        FaceItem* ref_face = visual_graph.reference_face_item();
        auto k_numbers = compute_k_numbers(*planrep, ref_face->first_adjacency());
        for (auto key_val : k_numbers.edge_numbers) {
            m_previous_k_numbers[key_val.first] = key_val.second.second;
        }
    }
    if (v_id < u_id) {
        swap(u_id, v_id);
    }
    if (m_previous_k_numbers.find({u_id, v_id}) != m_previous_k_numbers.end()) {
        return m_previous_k_numbers[{u_id, v_id}];
    }
    else {
        return NOT_EXISTING;
    }
}

int EditingInfo::next_k_number(node_id u_id, node_id v_id) {
    assert(has_newer_graph_state());
    // falls nicht im Cache: berechne k-Zahlen der folgenden Graphversion und cache sie
    if (m_next_k_numbers.empty()) {
        // lade Graph tempor�r neu
        QGraphicsScene dummy_scene;
        Graph* graph = new Graph();
        PlanarizedDrawing drawing;
        PlanRep* planrep = planarize(m_orig_drawing, graph, &drawing);
        VisualGraph visual_graph(&dummy_scene, graph, planrep, drawing, 1, 1);
        // stelle folgende Graphversion her
        for (int i = 0; i < m_curr_graph_index+2; ++i) {
            apply_diff(i, &visual_graph);
        }
        // teste, ob k-Zahl in der folgenden Version wohl definiert ist
        bool face_is_found = visual_graph.set_reference_face(m_ref_face_polygon, m_is_ref_face_external);
        if (!face_is_found) {
            return NO_UNIQUE_FACE;
        }
        int num_connected_components = visual_graph.number_connected_components();
        if (num_connected_components > 1) {
            return NO_UNIQUE_FACE;
        }
        // berechne und cache
        FaceItem* ref_face = visual_graph.reference_face_item();
        auto k_numbers = compute_k_numbers(*planrep, ref_face->first_adjacency());
        for (auto key_val : k_numbers.edge_numbers) {
            m_next_k_numbers[key_val.first] = key_val.second.second;
        }
    }
    if (v_id < u_id) {
        swap(u_id, v_id);
    }
    if (m_next_k_numbers.find({u_id, v_id}) != m_next_k_numbers.end()) {
        return m_next_k_numbers[{u_id, v_id}];
    }
    else {
        return NOT_EXISTING;
    }
}

void EditingInfo::set_reference_face_polygon(const QPolygonF& ref_face_polygon, bool is_external) {
    m_is_face_chosen = true;
    m_previous_k_numbers.clear();
    m_next_k_numbers.clear();
    m_is_ref_face_external = is_external;
    m_ref_face_polygon = ref_face_polygon;
}

bool EditingInfo::has_older_graph_state() const {
    return m_curr_graph_index > ORIGINAL;
}

bool EditingInfo::has_newer_graph_state() const {
    return m_curr_graph_index+1 < (int) m_all_diffs.size();
}

bool EditingInfo::is_k_edge_info_cached() const	{
    return m_are_k_edges_cached;
}

KEdgeInformation EditingInfo::k_edge_info(adjEntry adj) {
    assert(is_k_edge_info_cached());
    return m_cached_k_edges[adj];
}

void EditingInfo::set_k_edge_info(adjEntry adj, KEdgeInformation info) {
    m_are_k_edges_cached = true;
    m_cached_k_edges[adj] = info;
}

void EditingInfo::clear_caches() {
    m_previous_k_numbers.clear();
    m_next_k_numbers.clear();
    m_are_k_edges_cached = false;
    m_cached_k_edges.clear();
}

void EditingInfo::add_new_change(const GraphChange& diff) {
    clear_caches();
    m_curr_graph_index += 1;

    if (m_curr_graph_index == (int) m_all_diffs.size()) {
        m_all_diffs.push_back(diff);
    }
    else {
        // andere folgende �nderungen m�ssen vergessen werden
        m_all_diffs[m_curr_graph_index] = diff;
        for (int i = m_all_diffs.size()-1; i > m_curr_graph_index; --i) {
            m_all_diffs.pop_back();
        }
    }

    // sp�tere Kantenverl�ufe sind nicht mehr passend
    while ((int) m_all_diffs.size() > m_curr_graph_index + 1) {
        m_all_diffs.pop_back();
    }
}

void EditingInfo::apply_next_change(VisualGraph* graph) {
    clear_caches();
    ++m_curr_graph_index;
    apply_diff(m_curr_graph_index, graph);
}

void EditingInfo::apply_previous_changes(VisualGraph* graph) {
    clear_caches();
    --m_curr_graph_index;
    for (int i = 0; i <= m_curr_graph_index; ++i) {
        apply_diff(i, graph);
    }
}

VisualGraph* EditingInfo::recreate_current(QGraphicsScene* scene, int vertex_width, int edge_width) {
    Graph* graph = new Graph();
    PlanarizedDrawing planarized;
    PlanRep* planrep = planarize(m_orig_drawing, graph, &planarized);
    VisualGraph* visual_graph = new VisualGraph(scene, graph, planrep, planarized, vertex_width, edge_width);
    for (int i = 0; i <= m_curr_graph_index; ++i) {
        apply_diff(i, visual_graph);
    }
    return visual_graph;
}

void EditingInfo::apply_diff(int idx_diff, VisualGraph* graph) const {
    GraphChange diff = m_all_diffs[idx_diff];
    if (diff.type == GraphChange::Type::REROUTING) {
        remove_edge(graph, diff);
        add_edge(graph, diff);
    }
    else if (diff.type == GraphChange::Type::EDGE_ADDITION) {
        add_edge(graph, diff);
    }
    else if (diff.type == GraphChange::Type::EDGE_REMOVAL) {
        remove_edge(graph, diff);
    }
    else if (diff.type == GraphChange::Type::VERTEX_ADDITION) {
        auto pos = diff.position;
        graph->add_vertex(pos);
    }
    else if (diff.type == GraphChange::Type::VERTEX_REMOVAL) {
        node_id id = diff.vertex_id;
        VertexItem* v_item = nullptr;
        for (VertexItem* v : graph->vertex_items()) {
            if (v->node()->index() == id) {
                v_item = v;
                break;
            }
        }
        assert(v_item);
        graph->remove_vertex(v_item);
    }
    else {
        // nicht erreichbar
        assert(false);
    }
}

void EditingInfo::add_edge(VisualGraph *graph, const GraphChange& diff) const {
    assert(diff.type == GraphChange::Type::EDGE_ADDITION || diff.type == GraphChange::Type::REROUTING);
    // bestimme die Knotenitems
    node_pair end_vertices = diff.vertices;
    auto v_items = graph->vertex_items();
    VertexItem* u_item = nullptr;
    VertexItem* v_item = nullptr;
    for (auto item : v_items) {
        if (item->node()->index() == end_vertices.first) {
            u_item = item;
        } else if (item->node()->index() == end_vertices.second) {
            v_item = item;
        }
    }
    assert(u_item && v_item);

    // bestimme die Folge von Faces
    std::vector<FaceItem*> faces;
    std::vector<adjEntry> adjacencies;
    for (adj_id adj_id : diff.adjacencies) {
        adjEntry face_adj = nullptr;
        edge e;
        forall_edges(e, *graph->planar_repr()) {
            if (e->adjSource()->index() == adj_id) {
                face_adj = e->adjSource();
                break;
            }
            else if (e->adjTarget()->index() == adj_id) {
                face_adj = e->adjTarget();
                break;
            }
        }
        assert(face_adj);
        if (faces.size() > 0) {
            // die erste Adjazenz ist ausschlie�lich f�r die Fl�chenfolge vorgesehen
            adjacencies.push_back(face_adj);
        }
        FaceItem* f_item = graph->face_item_for_adjacency(face_adj);
        assert(f_item);
        faces.push_back(f_item);
    }

    // f�hre Umleitung durch
    graph->add_edge(u_item->node(), v_item->node(), faces, adjacencies, diff.bend_positions);
}

void EditingInfo::remove_edge(VisualGraph* graph, const GraphChange& diff) const {
    assert(diff.type == GraphChange::Type::EDGE_REMOVAL || diff.type == GraphChange::Type::REROUTING);
    node_id u_id = diff.vertices.first;
    node_id v_id = diff.vertices.second;
    VertexItem* u_item = nullptr;
    VertexItem* v_item = nullptr;
    for (VertexItem* v : graph->vertex_items()) {
        if (v->node()->index() == u_id) {
            u_item = v;
        }
        if (v->node()->index() == v_id) {
            v_item = v;
        }
    }
    assert(u_item && v_item);
    graph->remove_edge(u_item, v_item);
}
