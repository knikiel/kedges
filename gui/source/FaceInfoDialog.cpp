#include "FaceInfoDialog.h"

#include <QFileDialog>
#include <QFrame>
#include <QLabel>
#include <QPushButton>

#include "common.h"
#include "Colors.h"
#include "GDrawWidget.h"

using namespace std;

KEdgeSumType FaceInfoDialog::selectedType = KEdgeSumType::CUMULATED;
set<FaceInfoDialog*> FaceInfoDialog::openDialogs;

/// Gibt ein Icon in der Farbe zurück, die der Anzahl an verletzten k-Bedingungen entspricht.
QIcon get_colored_icon(int num_violated_conditions) {
    QImage image(10, 10, QImage::Format::Format_RGB888);
    image.fill(number_color(num_violated_conditions));
    QIcon icon(QPixmap::fromImage(image));
    return icon;
}

FaceInfoDialog::FaceInfoDialog(const VisualGraph& graph, const FaceItem& face) {
    setWindowTitle("Flächeninformationen");
    m_toolbox = new QToolBox();

    m_save_button = new QPushButton("Speichern");
    connect(m_save_button, SIGNAL(pressed()), this, SLOT(save_info()));

    m_ok_palette.setColor(QPalette::ColorRole::Background, Qt::green);
    m_wrong_palette.setColor(QPalette::ColorRole::Background, Qt::red);
    m_unimportant_palette.setColor(QPalette::ColorRole::Background, Qt::gray);

    QGridLayout* layout = new QGridLayout();
    setLayout(layout);

    // Fläche soll einfacher zu identifizieren sein
    int vert_offset = 0;
    stringstream vertices_string;
    adjEntry curr_adj = nullptr;
    bool first = true;
    forall_adjEdges(curr_adj, face.first_adjacency(), {
        if (!first) {
            vertices_string << ", ";
        }
        node v_copy = curr_adj->theNode();
        node v_orig = graph.planar_repr()->original(curr_adj->theNode());
        if (v_orig) {
            vertices_string << v_orig->index();
        }
        else {
            vertices_string << v_copy->index();
        }
        first = false;
    });
    layout->addWidget(new QLabel("benachbarte Knoten:"), vert_offset, 0);
    layout->addWidget(new QLabel(vertices_string.str().c_str()), vert_offset, 1);
    ++vert_offset;
    layout->addWidget(new QLabel("Knotenzahl"), vert_offset, 0);
    layout->addWidget(new QLabel(QString::number(graph.graph()->numberOfNodes())), vert_offset, 1);
    ++vert_offset;
    layout->addWidget(new QLabel("Kantenzahl:"), vert_offset, 0);
    layout->addWidget(new QLabel(QString::number(graph.graph()->numberOfEdges())), vert_offset, 1);
    ++vert_offset;

    std::stringstream print_text;
    print_text << "benachbarte Knoten: " << vertices_string.str() << '\n';

    KEdgeInformation info = compute_k_numbers(*graph.planar_repr(), face.first_adjacency());
    vector<int> num_k_edges_minus;
    vector<int> num_k_edges_plus;
    vector<int> num_k_edges_both;
    count_num_signed_k_edges(info, &num_k_edges_minus, &num_k_edges_plus, &num_k_edges_both);

    // stelle Anzahl der k-Kanten dar
    layout->addWidget(new QLabel("Anzahl der k-Kanten"), vert_offset, 0);
    ++vert_offset;

    QLabel* minus_label = new QLabel("-");
    minus_label->setAlignment(Qt::AlignRight);
    QLabel* plus_label = new QLabel("+");
    plus_label->setAlignment(Qt::AlignCenter);
    QLabel* both_label = new QLabel("beide");
    both_label->setAlignment(Qt::AlignLeft);
    print_text << "Kantennummer\t-\t+\tbeide\n";
    layout->addWidget(minus_label, vert_offset, 1);
    layout->addWidget(plus_label, vert_offset, 2);
    layout->addWidget(both_label, vert_offset, 3);
    ++vert_offset;

    for (size_t k = 0; k < num_k_edges_minus.size(); ++k) {
        QLabel* left_label = new QLabel((std::to_string(k) + "-Kanten:").c_str());
        left_label->setAlignment(Qt::AlignRight);
        print_text << k << "-Kanten:";
        QLabel* right_label_minus = new QLabel((std::to_string(num_k_edges_minus[k])).c_str());
        right_label_minus->setAlignment(Qt::AlignRight);
        QLabel* right_label_plus = new QLabel((std::to_string(num_k_edges_plus[k])).c_str());
        right_label_plus->setAlignment(Qt::AlignCenter);
        QLabel* right_label_both = new QLabel((std::to_string(num_k_edges_both[k])).c_str());
        right_label_both->setAlignment(Qt::AlignLeft);
        print_text << '\t' << num_k_edges_minus[k] << '\t' << num_k_edges_plus[k] << '\t' << num_k_edges_both[k] << '\n';
        layout->addWidget(left_label, vert_offset, 0);
        layout->addWidget(right_label_minus, vert_offset, 1);
        layout->addWidget(right_label_plus, vert_offset, 2);
        layout->addWidget(right_label_both, vert_offset, 3);
        ++vert_offset;
    }

    QFrame* seperating_line = new QFrame();
    seperating_line->setFrameShape(QFrame::HLine);
    seperating_line->setFrameShadow(QFrame::Sunken);
    layout->addWidget(seperating_line, vert_offset, 0, 1, -1);
    ++vert_offset;

    // erstelle Tabs für Ungleichungsbedingungen
    layout->addWidget(m_toolbox, vert_offset, 0, 1, -1);
    int num_vertices = graph.number_vertices();
    create_inequality_tab(num_vertices, info, KEdgeSumType::EXACT, "=-Bedingungen", m_toolbox);
    create_inequality_tab(num_vertices, info, KEdgeSumType::CUMULATED, "≤-Bedingungen", m_toolbox);
    create_inequality_tab(num_vertices, info, KEdgeSumType::C2UMULATED, "≤≤-Bedingungen", m_toolbox);
    create_inequality_tab(num_vertices, info, KEdgeSumType::C3UMULATED, "≤≤≤-Bedingungen", m_toolbox);
    connect(m_toolbox, SIGNAL(currentChanged(int)), this, SLOT(selectTabIndex(int)));
    ++vert_offset;

    layout->addWidget(m_save_button);

    FaceInfoDialog::openDialogs.insert(this);
}

FaceInfoDialog::~FaceInfoDialog() {
    auto dialogs_iter = FaceInfoDialog::openDialogs.find(this);
    FaceInfoDialog::openDialogs.erase(dialogs_iter);
}

void FaceInfoDialog::create_inequality_tab(int num_vertices, const KEdgeInformation& info,
    const KEdgeSumType& kedge_type, const char* description, QToolBox* toolbox)
{
    QGridLayout* ineq_layout = new QGridLayout();
    QWidget* widget = new QWidget();
    widget->setLayout(ineq_layout);

    int vert_offset = 0;
    vector<int> num_k_edges;
    count_num_k_edges(info, &num_k_edges);
    int num_violated_conditions = count_violated_conditions(num_vertices, kedge_type, num_k_edges);
    if (num_violated_conditions == 0) {
        QWidget* widget = new QLabel("Face erfüllt alle Bedingungen:");
        ineq_layout->addWidget(widget, vert_offset, 0, 1, -1);
    }
    else {
        QWidget* widget = new QLabel(("Face verletzt " + to_string(num_violated_conditions) + " Bedingungen:").c_str());
        ineq_layout->addWidget(widget, vert_offset, 0, 1, -1);
    }
    ++vert_offset;
    for (int k = 0; k < (int) num_k_edges.size(); ++k) {
        KEdgeInequality ineq = test_single_condition(kedge_type, num_k_edges, k);
        stringstream terms;
        for (size_t i = 0; i < ineq.coefficents.size(); ++i) {
            long term = ineq.coefficents[i];
            if (i > 0) {
                terms << '+' << to_string(term) << '*' << to_string(num_k_edges[i]);
            }
            else {
                terms << to_string(term) << '*' << to_string(num_k_edges[i]);
            }
        }
        terms << "= " << to_string(ineq.sum_term);
        QLabel* left_label = new QLabel(terms.str().c_str());
        m_print_text << terms.str();
        left_label->setAlignment(Qt::AlignRight);
        ineq_layout->addWidget(left_label, vert_offset, 0);

        QLabel* center_label = nullptr;
        if (ineq.is_ok()) {
            center_label = new QLabel("≥");
            center_label->setPalette(m_ok_palette);
            m_print_text << " ≥ ";
        }
        else {
            center_label = new QLabel("<");
            center_label->setPalette(m_wrong_palette);
            m_print_text << " < ";
        }
        center_label->setAutoFillBackground(true);
        center_label->setAlignment(Qt::AlignHCenter);
        ineq_layout->addWidget(center_label, vert_offset, 1);

        string binom_text = to_string(ineq.binom_term);
        QLabel* right_label = new QLabel(binom_text.c_str());
        m_print_text << binom_text;
        right_label->setAlignment(Qt::AlignLeft);
        ineq_layout->addWidget(right_label, vert_offset, 2);

        if (k > compute_relevant_upper_k(num_vertices)) {
            left_label->setAutoFillBackground(true);
            left_label->setPalette(m_unimportant_palette);
            right_label->setAutoFillBackground(true);
            right_label->setPalette(m_unimportant_palette);
        }
        m_print_text << '\n';
        ++vert_offset;
    }

    auto tab_index = toolbox->addItem(widget, get_colored_icon(num_violated_conditions), description);
    if (FaceInfoDialog::selectedType == kedge_type) {
        toolbox->setCurrentIndex(tab_index);
    }
}

void FaceInfoDialog::save_info() {
    QString filename = QFileDialog::getSaveFileName(this,
        "Speichern der Face-Informationen",
        "./unbenannt.txt",
        "Text-Datei (*.txt)");
    if (filename.isNull() || filename.isEmpty()) {
        return;
    }
    
    ofstream file(filename.toStdString());
    file << m_print_text.str();
}

void FaceInfoDialog::selectTabIndex(int idx) {
    FaceInfoDialog::selectedType = (KEdgeSumType) ((int) KEdgeSumType::EXACT + idx);
    for (auto dialog : FaceInfoDialog::openDialogs) {
        dialog->m_toolbox->setCurrentIndex(idx);
    }
}
