#include <FaceItem.h>

#include <sstream>

#include "ogdf/basic/Graph.h"
#include <QGraphicsPolygonItem>
#include <QBrush>
#include <QPen>

#include "common.h"

using namespace ogdf;

FaceItem::FaceItem(adjEntry face_repr, bool is_external, QPolygonF polygon)
    : QGraphicsPolygonItem(polygon),
    m_face_repr(face_repr),
    m_is_external(is_external),
    m_is_reference(false)
{
    setFillRule(Qt::OddEvenFill);
    reset_brush();
    if (is_external) {
        setVisible(false);
    }
}

FaceItem::~FaceItem() {
}

void FaceItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) {
    QGraphicsPolygonItem::paint(painter, option, widget);
    if (m_is_external || !m_is_reference) {
        return;
    }
    painter->save();
    QBrush brush = QBrush(Qt::black);
    brush.setStyle(Qt::BDiagPattern);
    painter->setBrush(brush);
    painter->drawPolygon(polygon());
    brush = QBrush(Qt::white);
    brush.setStyle(Qt::FDiagPattern);
    painter->setBrush(brush);
    painter->drawPolygon(polygon());
    painter->restore();
}

void FaceItem::reset_brush() {
    setBrush(default_brush());
    QPen pen;
    pen.setWidth(1);
    setPen(pen);
}

void FaceItem::set_reference(bool is_ref) {
    m_is_reference = is_ref;
}

void FaceItem::set_external(bool is_external) {
    m_is_external = is_external; 
    if (is_external) {
        setVisible(false);
    }
    else {
        setVisible(true);
    }
}

std::vector<adjEntry> FaceItem::adjacencies() const {
    adjEntry first_adj = first_adjacency();
    adjEntry curr_adj;
    std::vector<adjEntry> adjacencies;
    forall_adjEdges(curr_adj, first_adj, {
        adjacencies.push_back(curr_adj);
    });
    return adjacencies;
}

std::vector<node> FaceItem::adjacent_nodes() const {
    adjEntry first_adj = first_adjacency();
    adjEntry curr_adj = nullptr;
    std::vector<node> nodes;
    forall_adjEdges(curr_adj, first_adj, {
        nodes.push_back(curr_adj->theNode());
    });
    return nodes;
}

QBrush FaceItem::default_brush() {
    QBrush brush;
    brush.setColor(Qt::white);
    brush.setStyle(Qt::SolidPattern);
    return brush;
}

QBrush FaceItem::reference_face_brush() {
    QBrush brush;
    brush.setColor(Qt::darkGray);
    brush.setStyle(Qt::Dense3Pattern);
    return brush;
}

QBrush FaceItem::chosen_face_brush() {
    QBrush brush;
    brush.setColor(Qt::gray);
    brush.setStyle(Qt::SolidPattern);
    return brush;
}

QBrush FaceItem::alternative_course_brush(int max_idx, int idx) {
    QBrush brush;
    QColor color(Qt::lightGray);
    color.setRedF(color.redF() * ((float)(idx + 3) / (float)(max_idx + 3)));
    color.setGreenF(color.greenF() * ((float)(idx + 2) / (float)(max_idx + 2)));
    color.setBlueF(color.blueF() * ((float)(idx + 1) / (float)(max_idx + 1)));
    brush.setColor(color);
    brush.setStyle(Qt::SolidPattern);
    return brush;
}

QBrush FaceItem::extended_flip_brush(int max_idx, int idx) {
    QBrush brush;
    QColor color(Qt::lightGray);
    color.setRedF(color.redF() * ((float)(idx + 1) / (float)(max_idx + 1)));
    color.setGreenF(color.greenF() * ((float)(idx + 2) / (float)(max_idx + 2)));
    color.setBlueF(color.blueF() * ((float)(idx + 3) / (float)(max_idx + 3)));
    brush.setColor(color);
    brush.setStyle(Qt::SolidPattern);
    return brush;
}

QBrush FaceItem::redirect_edge_brush() {
    QBrush brush;
    brush.setColor(Qt::darkCyan);
    brush.setStyle(Qt::Dense5Pattern);
    return brush;
}