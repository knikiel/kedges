#include "GDrawWidget.h"

#include <cassert>
#include <sstream>

#include "ogdf/planarity/PlanarizationLayout.h"
#include "QWheelEvent"

#include "common.h"
#include "Colors.h"
#include "MessageSystem.h"

using namespace ogdf;
using namespace std;

const double GDrawWidget::ZOOM_FACTOR = 1.2;
const double GDrawWidget::MIN_EXPORTING_SCALE = 10.0;


GDrawWidget::GDrawWidget() :
    QGraphicsView(),
    m_curr_scale(1.0),
    m_graph(nullptr),
    m_options(),
    m_selection_state(SelectionState::NO_GRAPH_LOADED),
    m_edge_selection_info()
{
    m_scene = new GraphScene(this);
    setScene(m_scene);
    MessageSystem::print_state_message(m_selection_state);
}

void GDrawWidget::set_new_graph(string path, VisualGraph* graph, UnprocessedDrawing drawing) {
    m_graph = graph;
    initialize(path, drawing);
}

void GDrawWidget::wheelEvent(QWheelEvent* wheel_event) {
    if (wheel_event->delta() > 0) {
        zoom_in();
    }
    else {
        zoom_out();
    }
}

void GDrawWidget::initialize(string path, UnprocessedDrawing drawing) {
    if (!m_graph) {
        return;
    }

    m_options.reference_face = m_graph->external_face_item();

    m_editing_info.initialize(path, drawing);

    m_selection_state = SelectionState::REF_FACE;
    MessageSystem::print_state_message(m_selection_state);

    visualize_properly();
    zoom_to_entire_graph();
}


void GDrawWidget::visualize_properly() {
    show_colors_of_faces(m_options.cum_ineq_shown);
    show_colors_of_edges(m_options.edge_coloring);
    show_crossings(m_options.cross_ids_shown);

    QBrush external_brush = m_graph->external_face_item()->brush();
    m_scene->setBackgroundBrush(external_brush);
}


void GDrawWidget::show_colors_of_faces(KEdgeSumType cum_ineq) {
    if (cum_ineq == KEdgeSumType::NONE
        || m_graph->number_connected_components() > 1)
    {
        // es sollen oder dürfen keine Flächen angemessen gefärbt werden
        for (auto f_item : m_graph->face_items()) {
            f_item->reset_brush();
        }
        m_scene->setBackgroundBrush(m_graph->external_face_item()->brush());
        return;
    }

    // bestimme Gültigkeit der k-Ungleichungsbedingungen
    KEdgeInformation info;
    if (!m_editing_info.is_k_edge_info_cached()) {
        for (auto f_item : m_graph->face_items()) {
            adjEntry adj = f_item->first_adjacency();
            info = compute_k_numbers(*m_graph->planar_repr(), adj);
            m_editing_info.set_k_edge_info(adj, info);
        }
    }

    // färbe alle Flächen angemessen
    for (auto f_item : m_graph->face_items()) {
        adjEntry adj = f_item->first_adjacency();

        std::vector<int> num_k_edges;
        info = m_editing_info.k_edge_info(adj);
        count_num_k_edges(info, &num_k_edges);
        int num_violated_conditions = count_violated_conditions(m_graph->number_vertices(), cum_ineq, num_k_edges);
        QColor color = number_color(num_violated_conditions, colors::FACE_COLOR_DEVIATION);

        QBrush brush = FaceItem::default_brush();
        brush.setColor(color);
        f_item->setBrush(brush);
    }

    // externes Face
    m_scene->setBackgroundBrush(m_graph->external_face_item()->brush());
}


void GDrawWidget::show_colors_of_edges(EdgeColoringMode color_mode) {
    if (color_mode == EdgeColoringMode::NO_COLORING
        || (color_mode == EdgeColoringMode::K_DIFFERENCE_TO_PREV && !m_editing_info.has_older_graph_state())
        || (color_mode == EdgeColoringMode::K_DIFFERENCE_TO_NEXT && !m_editing_info.has_newer_graph_state())
        || m_graph->number_connected_components() > 1)
    {
        // es sollen oder dürfen keine Kanten angemessen gefärbt werden
        for (auto e_item : m_graph->edge_items()) {
            e_item->reset_brush();
            e_item->set_width(m_graph->edge_width());
        }
        return;
    }

    PlanRep& p = *m_graph->planar_repr();
    KEdgeInformation info = compute_k_numbers(p, m_options.reference_face->first_adjacency());
    // info über k-edges output
    MessageSystem::print_message(info.infoStr.c_str());

    for (auto e_item : m_graph->edge_items()) {
        edge e_orig = m_graph->planar_repr()->original(e_item->edge());
        node_id u_orig_id = min(e_orig->source()->index(), e_orig->target()->index());
        node_id v_orig_id = max(e_orig->source()->index(), e_orig->target()->index());
        DominatingSide type = info.edge_numbers[{u_orig_id, v_orig_id}].first;
        int k_number = info.edge_numbers[{u_orig_id, v_orig_id}].second;

        switch (color_mode) {
        case EdgeColoringMode::K_NUMBER: {
            QColor color = number_color(k_number, colors::EDGE_COLOR_DEVIATION);
            QPen pen = e_item->pen();
            pen.setColor(color);
            switch (type) {
            case DominatingSide::LEFT:{
                pen.setStyle(EdgeItem::LEFT_K_NUM_STYLE);
            } break;
            case DominatingSide::RIGHT:{
                pen.setStyle(EdgeItem::RIGHT_K_NUM_STYLE);
            } break;
            case DominatingSide::NONE:{
                pen.setStyle(EdgeItem::BOTH_K_NUM_STYLE);
            } break;
            }
            e_item->set_pen(pen);
            break;
        }
        case EdgeColoringMode::K_DIFFERENCE_TO_PREV: {
            int prev_k_number = m_editing_info.previous_k_number(u_orig_id, v_orig_id);
            if (prev_k_number == EditingInfo::NO_UNIQUE_FACE) {
                // es existiert keine eindeutige Referenzfläche in der vorigen Version
                show_colors_of_edges(EdgeColoringMode::NO_COLORING);
                return;
            }
            int k_number_change = k_number - prev_k_number;
            QPen pen = e_item->pen();
            QColor color;
            if (prev_k_number == EditingInfo::NOT_EXISTING) {
                // Kante hat vorher nicht existiert
                color = number_color(k_number, colors::EDGE_COLOR_DEVIATION);
                pen.setStyle(EdgeItem::BOTH_K_NUM_STYLE);
            }
            else if (k_number_change >= 0) {
                color = number_color(k_number_change, colors::EDGE_COLOR_DEVIATION);
                pen.setStyle(EdgeItem::RIGHT_K_NUM_STYLE);
            }
            else {
                color = number_color(-k_number_change, colors::EDGE_COLOR_DEVIATION);
                pen.setStyle(EdgeItem::LEFT_K_NUM_STYLE);
            }
            pen.setColor(color);
            e_item->set_pen(pen);
            e_item->set_width(m_graph->edge_width());
            break;
        }
        case EdgeColoringMode::K_DIFFERENCE_TO_NEXT: {
            int next_k_number = m_editing_info.next_k_number(u_orig_id, v_orig_id);
            if (next_k_number == EditingInfo::NO_UNIQUE_FACE) {
                // es existiert keine eindeutige Referenzfläche in der nächsten Version
                show_colors_of_edges(EdgeColoringMode::NO_COLORING);
                return;
            }
            int k_number_change = next_k_number - k_number;
            QPen pen = e_item->pen();
            QColor color;
            if (next_k_number == EditingInfo::NOT_EXISTING) {
                // Kante existiert danach nicht
                color = number_color(k_number, colors::EDGE_COLOR_DEVIATION);
                pen.setStyle(EdgeItem::BOTH_K_NUM_STYLE);
            }
            else if (k_number_change >= 0) {
                color = number_color(k_number_change, colors::EDGE_COLOR_DEVIATION);
                pen.setStyle(EdgeItem::RIGHT_K_NUM_STYLE);
            }
            else {
                color = number_color(-k_number_change, colors::EDGE_COLOR_DEVIATION);
                pen.setStyle(EdgeItem::LEFT_K_NUM_STYLE);
            }
            pen.setColor(color);
            e_item->set_pen(pen);
            e_item->set_width(m_graph->edge_width());
            break;
        }
        default: {
            // nicht erreichbar
            assert(false);
        }
        }
    }
}

bool GDrawWidget::is_in_default_state() {
    return m_selection_state == SelectionState::REF_FACE;
}

bool GDrawWidget::is_in_vertex_addition_state() {
    return m_selection_state == SelectionState::ADD_VERTEX;
}

bool GDrawWidget::is_in_vertex_deletion_state() {
    return m_selection_state == SelectionState::REMOVE_VERTEX;
}

bool GDrawWidget::is_in_edge_deletion_state() {
    return m_selection_state == SelectionState::REMOVE_EDGE_U
        || m_selection_state == SelectionState::REMOVE_EDGE_V;
}

bool GDrawWidget::is_in_edge_addition_state() {
    return m_selection_state == SelectionState::ADD_EDGE_U
        || m_selection_state == SelectionState::ADD_EDGE_V
        || m_selection_state == SelectionState::ROUTE_THROUGH_FACES;
}

bool GDrawWidget::is_in_rerouting_state() {
    return m_selection_state == SelectionState::REROUTE_THROUGH_FACE
        || m_selection_state == SelectionState::REROUTE_U
        || m_selection_state == SelectionState::REROUTE_V;
}

bool GDrawWidget::is_in_alternate_state() {
    return m_selection_state == SelectionState::ALTERNATE_U
        || m_selection_state == SelectionState::ALTERNATE_V;
}

void GDrawWidget::clear_graph_cache() {
    if (is_in_rerouting_state()) {
        cancel_rerouting();
    }
    m_editing_info.clear_caches();
}


void GDrawWidget::clear_layout() {
    m_scene->clear();
}

void GDrawWidget::render(QPainter &painter) {
    double additional_scale_factor = MIN_EXPORTING_SCALE / m_curr_scale;
    if (additional_scale_factor > 1.0) {
        scale(additional_scale_factor, additional_scale_factor);
    }
    QGraphicsView::render(&painter, QRect(), layout_dimensions());
    if (additional_scale_factor > 1.0) {
        scale(1.0 / additional_scale_factor, 1.0 / additional_scale_factor);
    }
}

QRect GDrawWidget::layout_dimensions() {
    QRectF view_rect = m_scene->itemsBoundingRect();
    QRect mod_rect = mapFromScene(view_rect).boundingRect();
    mod_rect.adjust(-50, -50, 50, 50);
    return mod_rect;
}

void GDrawWidget::begin_edge_removal() {
    m_selection_state = SelectionState::REMOVE_EDGE_U;
    MessageSystem::print_state_message(m_selection_state);
}

void GDrawWidget::finish_edge_removal(const GraphChange &diff) {
    assert(m_selection_state == SelectionState::REMOVE_EDGE_V);
    m_edge_selection_info.clear();
    for (auto v_item : m_graph->vertex_items()) {
        v_item->reset_brush(true);
    }

    m_editing_info.add_new_change(diff);

    m_graph->set_reference_face(m_editing_info.reference_face_polygon(),
        m_editing_info.is_reference_face_external());
    m_options.reference_face = m_graph->reference_face_item();
    m_selection_state = SelectionState::REF_FACE;

    visualize_properly();
    MessageSystem::print_state_message(m_selection_state);
}

void GDrawWidget::cancel_edge_removal() {
    m_edge_selection_info.clear();
    for (auto v_item : m_graph->vertex_items()) {
        v_item->reset_brush(true);
    }
    m_selection_state = SelectionState::REF_FACE;
    MessageSystem::print_state_message(m_selection_state);
}

void GDrawWidget::begin_edge_addition() {
    m_selection_state = SelectionState::ADD_EDGE_U;
    MessageSystem::print_state_message(m_selection_state);
}

void GDrawWidget::finish_edge_addition(const GraphChange &diff) {
    assert(m_selection_state == SelectionState::ROUTE_THROUGH_FACES);
    m_edge_routing_info.clear();
    for (auto v_item : m_graph->vertex_items()) {
        v_item->reset_brush(true);
    }

    m_editing_info.add_new_change(diff);

    m_graph->set_reference_face(m_editing_info.reference_face_polygon(),
        m_editing_info.is_reference_face_external());
    m_options.reference_face = m_graph->reference_face_item();
    m_selection_state = SelectionState::REF_FACE;

    visualize_properly();
    MessageSystem::print_state_message(m_selection_state);
}

void GDrawWidget::cancel_edge_addition() {
    switch (m_selection_state) {
    case SelectionState::ROUTE_THROUGH_FACES:
    case SelectionState::ADD_EDGE_V:
    case SelectionState::ADD_EDGE_U: {
        m_edge_routing_info.clear();
        for (auto v_item : m_graph->vertex_items()) {
            v_item->reset_brush(true);
        }
        break;
    }
    default: { }
    }

    m_selection_state = SelectionState::REF_FACE;
    MessageSystem::print_state_message(m_selection_state);
    visualize_properly();
}

void GDrawWidget::begin_vertex_addition() {
    m_selection_state = SelectionState::ADD_VERTEX;
}

void GDrawWidget::finish_vertex_addition(const GraphChange& diff) {
    assert(m_selection_state == SelectionState::ADD_VERTEX);
    m_editing_info.add_new_change(diff);

    m_graph->set_reference_face(m_editing_info.reference_face_polygon(),
        m_editing_info.is_reference_face_external());
    m_options.reference_face = m_graph->reference_face_item();

    m_selection_state = SelectionState::REF_FACE;
    MessageSystem::print_state_message(m_selection_state);
    visualize_properly();
}

void GDrawWidget::cancel_vertex_addition() {
    m_selection_state = SelectionState::REF_FACE;
    MessageSystem::print_state_message(m_selection_state);
}

void GDrawWidget::begin_vertex_removal() {
    m_selection_state = SelectionState::REMOVE_VERTEX;
}

void GDrawWidget::finish_vertex_removal(const GraphChange& diff) {
    assert(m_selection_state == SelectionState::REMOVE_VERTEX);
    m_edge_routing_info.clear();
    for (auto v_item : m_graph->vertex_items()) {
        v_item->reset_brush(true);
    }

    m_editing_info.add_new_change(diff);

    m_graph->set_reference_face(m_editing_info.reference_face_polygon(),
        m_editing_info.is_reference_face_external());
    m_options.reference_face = m_graph->reference_face_item();

    m_selection_state = SelectionState::REF_FACE;
    MessageSystem::print_state_message(m_selection_state);
    visualize_properly();
}

void GDrawWidget::cancel_vertex_removal() {
    m_selection_state = SelectionState::REF_FACE;
    MessageSystem::print_state_message(m_selection_state);
}

void GDrawWidget::cancel_rerouting() {
    switch (m_selection_state) {
    case SelectionState::REROUTE_THROUGH_FACE: {
        m_edge_routing_info.clear();
        for (auto v_item : m_graph->vertex_items()) {
            v_item->reset_brush(true);
        }
        auto new_graph = m_editing_info.recreate_current(m_scene,
            m_graph->vertex_width(), m_graph->edge_width());
        delete m_graph;
        m_graph = new_graph;
        break;
    }
    case SelectionState::REROUTE_V:
    case SelectionState::REROUTE_U: {
        m_edge_routing_info.clear();
        for (auto v_item : m_graph->vertex_items()) {
            v_item->reset_brush(true);
        }
        break;
    }
    default: { }
    }

    m_selection_state = SelectionState::REF_FACE;
    MessageSystem::print_state_message(m_selection_state);

    visualize_properly();
}

void GDrawWidget::cancel_showing_alternative_routes() {
    m_edge_selection_info.clear();
    for (auto v_item : m_graph->vertex_items()) {
        v_item->reset_brush(true);
    }
    m_selection_state = SelectionState::REF_FACE;
    MessageSystem::print_state_message(m_selection_state);

    visualize_properly();
}

void GDrawWidget::finish_rerouting(const GraphChange& new_diff) {
    assert(new_diff.type == GraphChange::Type::REROUTING);
    assert(m_selection_state == SelectionState::REROUTE_THROUGH_FACE);

    m_scene->setBackgroundBrush(m_graph->external_face_item()->brush());
    m_edge_routing_info.clear();
    for (auto v_item : m_graph->vertex_items()) {
        v_item->reset_brush(true);
    }

    m_selection_state = SelectionState::REF_FACE;
    MessageSystem::print_state_message(m_selection_state);

    m_editing_info.add_new_change(new_diff);

    m_graph->set_reference_face(m_editing_info.reference_face_polygon(),
        m_editing_info.is_reference_face_external());
    m_options.reference_face = m_graph->reference_face_item();

    visualize_properly();
}

string GDrawWidget::test_bishellability_of_ref_face() {
    std::ostringstream text;
    int num_vertices = m_graph->vertex_items().size();
    int min_length_shelling = (num_vertices / 2 - 2);
    int max_length_shelling = (num_vertices - 1);
    FaceItem* f_item = m_options.reference_face;
    for (int r = min_length_shelling; r <= max_length_shelling; ++r) {
        // verwende alle Paare von Knoten
        vector<node> outer_vertices;
        adjEntry first_adj = f_item->first_adjacency();
        adjEntry curr_adj = nullptr;
        forall_adjEdges(curr_adj, first_adj, {
            node curr_vertex = curr_adj->theNode();
            if (!m_graph->planar_repr()->isCrossingType(curr_vertex)) {
                outer_vertices.push_back(curr_adj->theNode());
            }
        });
        for (node u_1 : outer_vertices) {
            for (node u_r : outer_vertices) {
                if (u_1->index() >= u_r->index()) {
                    continue;
                }
                vector<vector<node>> shellings;
                // die eigentliche Berechnung der (Bi-)Shellings
                if (!m_graph->compute_bishellings(f_item, r, u_1, u_r, &shellings)) {
                    continue;
                }
                // falls ein (Bi-)Shelling der Mindestgröße existiert, gebe dieses aus
                stringstream face_name;
                auto face_nodes = f_item->adjacent_nodes();
                for (node v_copy : face_nodes) {
                    node v_orig = m_graph->planar_repr()->original(v_copy);
                    if (v_orig) {
                        face_name << v_orig->index();
                    }
                    else {
                        face_name << v_copy->index();
                    }
                    face_name << ' ';
                }
                text << "Bishelling der Größe " << r << " mit Referenz-Face " << face_name.str() << ":\n";
                for (const auto &shelling : shellings) {
                    assert((int) shelling.size() == 2*r+2);
                    text << '<';
                    int i = 0;
                    for (node v_copy : shelling) {
                        node v_orig = m_graph->planar_repr()->original(v_copy);
                        if (v_orig) {
                            text << v_orig->index();
                        }
                        else {
                            text << v_copy->index();
                        }
                        if (i == r) {
                            // beginne zweite Sequenz
                            text << "><";
                        }
                        else if (i < 2*r+1) {
                            text << ' ';
                        }
                        i += 1;
                    }
                    text << ">\n";
                }
            }
        }
    }
    return text.str();
}

void GDrawWidget::test_bishellability(ShellingOutputType output_type) {
    test_specific_shellability(true, output_type);
}

void GDrawWidget::test_shellability(ShellingOutputType output_type) {
    test_specific_shellability(false, output_type);
}

void GDrawWidget::test_specific_shellability(bool bishellable, ShellingOutputType output_type) {
    int num_vertices = m_graph->vertex_items().size();
    int min_length_shelling = bishellable ? (num_vertices / 2 - 2) : (num_vertices / 2);
    int max_length_shelling = bishellable ? (num_vertices - 1) : (num_vertices);
    for (int r = min_length_shelling; r <= max_length_shelling; ++r) {
        bool example_found = false;
        bool minimum_found = false;
        for (FaceItem* f_item : m_graph->face_items()) {
            // verwende alle Paare von Knoten
            vector<node> outer_vertices;
            adjEntry first_adj = f_item->first_adjacency();
            adjEntry curr_adj = nullptr;
            forall_adjEdges(curr_adj, first_adj, {
                node curr_vertex = curr_adj->theNode();
                if (!m_graph->planar_repr()->isCrossingType(curr_vertex)) {
                    outer_vertices.push_back(curr_adj->theNode());
                }
            });
            for (node u_1 : outer_vertices) {
                for (node u_r : outer_vertices) {
                    if (u_1->index() >= u_r->index()) {
                        continue;
                    }
                    // die eigentliche Berechnung der (Bi-)Shellings
                    vector<vector<node>> shellings;
                    if (bishellable) {
                        if (!m_graph->compute_bishellings(f_item, r, u_1, u_r, &shellings)) {
                            continue;
                        }
                    }
                    else {
                        if (!m_graph->compute_shellings(f_item, r, u_1, u_r, &shellings)) {
                            continue;
                        }
                    }
                    // falls ein (Bi-)Shelling der Mindestgröße existiert, gebe dieses aus
                    std::ostringstream text;
                    stringstream face_name;
                    auto face_nodes = f_item->adjacent_nodes();
                    for (node v_copy : face_nodes) {
                        node v_orig = m_graph->planar_repr()->original(v_copy);
                        if (v_orig) {
                            face_name << v_orig->index();
                        }
                        else {
                            face_name << v_copy->index();
                        }
                        face_name << ' ';
                    }
                    if (bishellable) {
                        text << "Bishelling der Größe " << r << " mit Referenz-Face " << face_name.str() << ":\n";
                    }
                    else {
                        text << "Shelling der Größe " << r << " mit Referenz-Face " << face_name.str() << ":\n";
                    }
                    for (const auto &shelling : shellings) {
                        if (bishellable) {
                            assert((int) shelling.size() == 2*r+2);
                            text << '<';
                            int i = 0;
                            for (node v_copy: shelling) {
                                node v_orig = m_graph->planar_repr()->original(v_copy);
                                if (v_orig) {
                                    text << v_orig->index();
                                }
                                else {
                                    text << v_copy->index();
                                }
                                if (i == r) {
                                    // beginne zweite Sequenz
                                    text << "><";
                                }
                                else if (i < 2*r+1) {
                                    text << ' ';
                                }
                                i += 1;
                            }
                            text << '>';
                        }
                        else {
                            for (node v_copy : shelling) {
                                node v_orig = m_graph->planar_repr()->original(v_copy);
                                if (v_orig) {
                                    text << v_orig->index();
                                }
                                else {
                                    text << v_copy->index();
                                }
                                text << ' ';
                            }
                            text << ';';
                        }
                        MessageSystem::print_message(text.str().c_str());
                        text.str("");
                        if (output_type == ShellingOutputType::ALL_MINIMAL) {
                            minimum_found = true;
                        }
                        if (output_type == ShellingOutputType::MINIMAL) {
                            return;
                        }
                    }
                }
                if (example_found) {
                    break;
                }
            }
            if (example_found) {
                break;
            }
        }
        if (minimum_found) {
            return;
        }
    }
}

void GDrawWidget::show_crossings(bool marked) {
    if (!is_graph_loaded()) {
        return;
    }
    m_options.cross_ids_shown = marked;
    if (m_graph == nullptr) {
        return;
    }
    for (auto v_item : m_graph->crossing_items()) {
        v_item->setVisible(marked);
    }
}

void GDrawWidget::show_edge_colors(EdgeColoringMode coloring_mode) {
    m_options.edge_coloring = coloring_mode;
    if (!is_graph_loaded()) {
        return;
    }
    show_colors_of_edges(m_options.edge_coloring); 
}

void GDrawWidget::show_simple_faces(bool marked) {
    if (!marked) {
        return;
    }
    m_options.cum_ineq_shown = KEdgeSumType::NONE;
    if (!is_graph_loaded()) {
        return;
    }
    show_colors_of_faces(KEdgeSumType::NONE);
}

void GDrawWidget::show_ineq_faces(bool marked) {
    if (!marked) {
        m_options.cum_ineq_shown = KEdgeSumType::NONE;
        return;
    }

    m_options.cum_ineq_shown = KEdgeSumType::EXACT;
    if (!is_graph_loaded()) {
        return;
    }
    show_colors_of_faces(KEdgeSumType::EXACT);
}

void GDrawWidget::show_cineq_faces(bool marked) {
    if (!marked) {
        m_options.cum_ineq_shown = KEdgeSumType::NONE;
        return;
    }

    m_options.cum_ineq_shown = KEdgeSumType::CUMULATED;
    if (!is_graph_loaded()) {
        return;
    }
    show_colors_of_faces(KEdgeSumType::CUMULATED);
}

void GDrawWidget::show_ccineq_faces(bool marked) {
    if (!marked) {
        m_options.cum_ineq_shown = KEdgeSumType::NONE;
        return;
    }

    m_options.cum_ineq_shown = KEdgeSumType::C2UMULATED;
    if (!is_graph_loaded()) {
        return;
    }
    show_colors_of_faces(KEdgeSumType::C2UMULATED);
}

void GDrawWidget::show_cccineq_faces(bool marked) {
    if (!marked) {
        m_options.cum_ineq_shown = KEdgeSumType::NONE;
        return;
    }

    m_options.cum_ineq_shown = KEdgeSumType::C3UMULATED;
    if (!is_graph_loaded()) {
        return;
    }
    show_colors_of_faces(KEdgeSumType::C3UMULATED);
}

void GDrawWidget::begin_edge_rerouting() {
    m_selection_state = SelectionState::REROUTE_U;
    MessageSystem::print_state_message(m_selection_state);
}

void GDrawWidget::begin_showing_alternative_routes() {
    visualize_properly();
    m_selection_state = SelectionState::ALTERNATE_U;
    MessageSystem::print_state_message(m_selection_state);
}

void GDrawWidget::show_edges_with_alternative_routes(bool only_extended_flips) {
    auto vertices = m_graph->vertex_items();
    QPen pen;
    if (only_extended_flips) {
        pen = EdgeItem::extended_flips_pen();
    }
    else {
        pen = EdgeItem::alternative_course_pen();
    }
    for (VertexItem* v1 : vertices) {
        for (VertexItem* v2 : vertices) {
            if (v1->node()->index() < v2->node()->index()) {
                auto courses = m_graph->compute_alternative_courses(v1, v2, only_extended_flips);
                if (courses.size() > 0) {
                    auto segments = m_graph->edge_path(v1, v2);
                    for (auto e_item : segments) {
                        e_item->set_pen(pen);
                    }
                }
            }
        }
    }
}

void GDrawWidget::zoom_in() {
    m_curr_scale *= ZOOM_FACTOR;
    scale(ZOOM_FACTOR, ZOOM_FACTOR);
}

void GDrawWidget::zoom_out() {
    m_curr_scale *= 1./ZOOM_FACTOR;
    scale(1./ZOOM_FACTOR, 1./ZOOM_FACTOR);
}

void GDrawWidget::zoom_to_entire_graph() {
    QRectF layout_rect = m_scene->itemsBoundingRect();
    fitInView(layout_rect.left(), layout_rect.top(), layout_rect.width(), layout_rect.height(),
        Qt::KeepAspectRatio);
    m_curr_scale = transform().m11();
}

void GDrawWidget::set_vertex_width(double new_thickness) {
    if (!is_graph_loaded()) {
        return;
    }
    m_graph->set_vertex_width(new_thickness);
}

void GDrawWidget::set_edge_width(double new_thickness) {
    if (!is_graph_loaded()) {
        return;
    }
    m_graph->set_edge_width(new_thickness);
}

bool GDrawWidget::is_already_chosen(FaceItem* face) {
    return std::find(m_edge_routing_info.chosen_faces.begin(), m_edge_routing_info.chosen_faces.end(), face)
        != m_edge_routing_info.chosen_faces.end();
}

bool GDrawWidget::has_older_graph_version() {
    return m_editing_info.has_older_graph_state();
}

bool GDrawWidget::has_newer_graph_version() {
    return m_editing_info.has_newer_graph_state();
}

void GDrawWidget::undo_modification() {
    if (!m_editing_info.has_older_graph_state()) {
        return;
    }
    auto ref_face_polygon = m_editing_info.reference_face_polygon();

    clear_layout();
    reconstruct_old_edge_course();

    m_graph->set_reference_face(ref_face_polygon, m_editing_info.is_reference_face_external());
    m_options.reference_face = m_graph->reference_face_item();
    m_selection_state = SelectionState::REF_FACE;
    visualize_properly();
    emit computation_finished();
}

void GDrawWidget::redo_modification() {
    if (!m_editing_info.has_newer_graph_state()) {
        return;
    }
    auto ref_face_polygon = m_editing_info.reference_face_polygon();

    clear_graph_cache();
    m_editing_info.apply_next_change(m_graph);

    m_graph->set_reference_face(ref_face_polygon, m_editing_info.is_reference_face_external());
    m_options.reference_face = m_graph->reference_face_item();
    m_selection_state = SelectionState::REF_FACE;
    visualize_properly();
    emit computation_finished();
}

void GDrawWidget::reconstruct_old_edge_course() {
    // aktueller Graph wird vergessen
    clear_graph_cache();
    clear_layout();
    double vertex_width = m_graph->vertex_width();
    double edge_width = m_graph->edge_width();
    delete m_graph;

    // ursprünglicher Graph wird geladen
    Graph *graph = new Graph();
    UnprocessedDrawing drawing = m_editing_info.original_drawing();
    PlanarizedDrawing planarized;
    PlanRep* planrep = planarize(drawing, graph, &planarized);
    VisualGraph* visual_graph = new VisualGraph(m_scene, graph, planrep, planarized, vertex_width, edge_width);

    // Änderungen werden aufgespielt
    m_editing_info.apply_previous_changes(visual_graph);

    m_graph = visual_graph;
    m_options.reference_face = m_graph->external_face_item();
}

