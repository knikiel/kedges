#include "GraphScene.h"

#include <QGraphicsSceneMouseEvent>
#include <sstream>

#include "MessageSystem.h"

using namespace ogdf;
using namespace std;

/// Sucht in einer Liste von Items nach dem ersten Item eines bestimmten Typs
template<typename ITEM_TYPE>
ITEM_TYPE* search_items(const QList<QGraphicsItem*>& items) {
    for (auto item : items) {
        ITEM_TYPE* item_ = dynamic_cast<ITEM_TYPE*>(item);
        if (item_) {
            return item_;
        }
    }
    return nullptr;
}

GraphScene::GraphScene(GDrawWidget* widget) {
    gdraw = widget;
}

void GraphScene::set_new_reference_face(FaceItem* face) {
   bool is_ref_face_external = (face == gdraw->m_graph->external_face_item());
   gdraw->m_graph->set_reference_face(face);
   gdraw->m_options.reference_face = face;
   gdraw->m_editing_info.set_reference_face_polygon(face->polygon(), is_ref_face_external);

   // passe einige Visualisierungselemente an
   gdraw->show_colors_of_edges(gdraw->m_options.edge_coloring);
   for (FaceItem* f_item : gdraw->m_graph->face_items()) {
       f_item->update();
   }
}

/// Markiere die möglichen gefundenen Pfade bei der Bestimmung von alternativen Pfaden.
void mark_alternative_courses(const vector<vector<FaceItem*>>& face_item_paths, bool extended_flips) {
    int max_idx = 0;
    for (auto path : face_item_paths) {
        int path_size = path.size();
        if (path_size > max_idx) {
            max_idx = path_size;
        }
    }
    max_idx -= 1;
    for (auto path : face_item_paths) {
        QBrush brush;
        if (path.front()) {
            int idx = 0;
            for (FaceItem* f_item : path) {
                if (extended_flips) {
                    brush = FaceItem::extended_flip_brush(max_idx, idx);
                }
                else {
                    brush = FaceItem::alternative_course_brush(max_idx, idx);
                }
                f_item->setBrush(brush);
                idx += 1;
            }
        }
    }
}

void GraphScene::choose_reference_face(const QList<QGraphicsItem*>& items)
{
    if (items.size() == 0) {
       if (gdraw->m_graph != nullptr) {
           // es wurde außerhalb des Graphen geklickt, wähle äußere Fläche
           FaceItem* face = gdraw->m_graph->external_face_item();
           set_new_reference_face(face);
           gdraw->m_scene->setBackgroundBrush(face->brush());
        }
        return;
    };

    FaceItem* face = search_items<FaceItem>(items);
    if (face != nullptr) {
        set_new_reference_face(face);
    }
}

pair<VertexItem*, VertexItem*> GraphScene::find_vertex_items(edge e_copy) {
    auto planrep = gdraw->m_graph->planar_repr();
    edge e_orig = planrep->original(e_copy);
    node u_copy = planrep->copy(e_orig->source());
    node v_copy = planrep->copy(e_orig->target());
    VertexItem* u_item = nullptr;
    VertexItem* v_item = nullptr;
    for (auto item : gdraw->m_graph->vertex_items()) {
        if (item->node() == u_copy) {
            u_item = item;
        }
        if (item->node() == v_copy) {
            v_item = item;
        }
    }
    assert(u_item && v_item);
    return{ u_item, v_item };
}

void GraphScene::add_vertex(const Point& position) {
    MessageSystem::print_message("Knoten wird erstellt.");
    GraphChange diff = gdraw->m_graph->add_vertex(position);
    assert(diff.type == GraphChange::Type::VERTEX_ADDITION);
    MessageSystem::print_message("Knoten an der Position " + position.to_string() + " hinzugefügt!");
    gdraw->finish_vertex_addition(diff);
    emit gdraw->computation_finished();
}

void GraphScene::remove_vertex(VertexItem* v_item) {
    MessageSystem::print_message("Zu löschenden Knoten ausgewählt.");
    GraphChange diff = gdraw->m_graph->remove_vertex(v_item);
    assert(diff.type == GraphChange::Type::VERTEX_REMOVAL);
    MessageSystem::print_message("Knoten gelöscht!");
    gdraw->finish_vertex_removal(diff);
    emit gdraw->computation_finished();
}

void GraphScene::mark_adjacent_v_items(VertexItem* v_item) {
    for (VertexItem* v_item : gdraw->m_graph->adjacent_v_items(v_item)) {
        v_item->setBrush(VertexItem::selectable_brush());
    }
}

void GraphScene::mark_non_adjacent_v_items(VertexItem* v_item) {
    for (VertexItem* other_item : gdraw->m_graph->vertex_items()) {
        if (other_item != v_item) {
            other_item->setBrush(VertexItem::selectable_brush());
        }
    }
    for (VertexItem* adjacent_v_item : gdraw->m_graph->adjacent_v_items(v_item)) {
        adjacent_v_item->setBrush(VertexItem::default_brush());
    }
}

void GraphScene::choose_first_vertex_for_alternative_paths(VertexItem* v_item) {
    v_item->setBrush(VertexItem::chosen_brush());
    mark_adjacent_v_items(v_item);

    // speichere wichtige Informationen
    gdraw->m_edge_selection_info.u = v_item;
    gdraw->m_selection_state = SelectionState::ALTERNATE_V;
    MessageSystem::print_state_message(gdraw->m_selection_state);

    // Schreibe, was getan wird
    MessageSystem::print_message("Startknoten ausgewählt.");
}

void GraphScene::choose_second_vertex_for_alternative_paths(VertexItem* v_item) {
    v_item->setBrush(VertexItem::chosen_brush());

    // speichere wichtige Informationen
    gdraw->m_edge_selection_info.v = v_item;

    // schreibe, was getan wird
    gdraw->m_selection_state = SelectionState::REF_FACE;
    MessageSystem::print_state_message(gdraw->m_selection_state);
    MessageSystem::print_message("Zielknoten ausgewählt, es wird gerechnet.");

    VertexItem* u = gdraw->m_edge_selection_info.u;
    VertexItem* v = gdraw->m_edge_selection_info.v;
    auto segments = gdraw->visual_graph().edge_path(u, v);
    for (auto e_item : segments) {
        e_item->set_pen(EdgeItem::alternative_course_pen());
    }
    auto faces = gdraw->visual_graph().compute_alternative_courses(u, v, false);
    auto faces_for_flips = gdraw->visual_graph().compute_alternative_courses(u, v, true);
    for (auto v_item : gdraw->m_graph->vertex_items()) {
        v_item->reset_brush(true);
    }

    mark_alternative_courses(faces, false);
    mark_alternative_courses(faces_for_flips, true);
    emit gdraw->computation_finished();
    MessageSystem::print_message("Berechnung abgeschlossen, siehe Ergebnis.");
}

void GraphScene::choose_first_vertex_for_edge_removal(VertexItem* v_item) {
    v_item->setBrush(VertexItem::chosen_brush());
    mark_adjacent_v_items(v_item);

    // speichere wichtige Informationen
    gdraw->m_edge_selection_info.u = v_item;
    gdraw->m_selection_state = SelectionState::REMOVE_EDGE_V;
    MessageSystem::print_state_message(gdraw->m_selection_state);

    // schreibe, was getan wird
    MessageSystem::print_message("Startknoten ausgewählt.");
}

void GraphScene::choose_second_vertex_for_edge_removal(VertexItem* v_item) {
    v_item->setBrush(VertexItem::chosen_brush());

    gdraw->m_edge_selection_info.v = v_item;

    MessageSystem::print_state_message(gdraw->m_selection_state);
    MessageSystem::print_message("Zielknoten ausgewählt, Kante wird gelöscht.");

    VertexItem* u = gdraw->m_edge_selection_info.u;
    VertexItem* v = gdraw->m_edge_selection_info.v;
    GraphChange diff = gdraw->m_graph->remove_edge(u, v);

    gdraw->finish_edge_removal(diff);

    emit gdraw->computation_finished();
    MessageSystem::print_message("Ausgewählte Kante gelöscht!");
}

void GraphScene::choose_first_vertex_for_edge_addition(VertexItem* v_item, QPointF v_pos) {
    v_item->setBrush(VertexItem::chosen_brush());
    mark_non_adjacent_v_items(v_item);

    gdraw->m_edge_routing_info.vertex_pair.u = v_item;
    gdraw->m_edge_routing_info.last_point = v_pos;
    gdraw->m_selection_state = SelectionState::ADD_EDGE_V;
    MessageSystem::print_state_message(gdraw->m_selection_state);

    MessageSystem::print_message("Startknoten ausgewählt.");
}

void GraphScene::choose_second_vertex_for_edge_addition(VertexItem* v_item) {
    gdraw->m_edge_routing_info.vertex_pair.v = v_item;
    gdraw->m_selection_state = SelectionState::ROUTE_THROUGH_FACES;
    MessageSystem::print_state_message(gdraw->m_selection_state);
    auto u = gdraw->m_edge_routing_info.vertex_pair.u;
    auto v = gdraw->m_edge_routing_info.vertex_pair.v;

    // markiere Knoten
    v_item->setBrush(VertexItem::chosen_brush());

    // markiere nicht unabhängige Kanten
    gdraw->m_graph->mark_incident_edges(*u);
    gdraw->m_graph->mark_incident_edges(*v);

    // Schreibe, was getan wird
    MessageSystem::print_message("Zielknoten ausgewählt, Kante ist festgelegt.");
}

void GraphScene::choose_next_face(FaceItem* f_item, bool external, QPointF pos) {
    bool first_face = gdraw->m_edge_routing_info.chosen_faces.empty();
    if (first_face || gdraw->m_edge_routing_info.chosen_faces.back() != f_item) {
        if (first_face) {
            VertexItem* u = gdraw->m_edge_routing_info.vertex_pair.u;
            if (!gdraw->m_graph->is_face_adjacent_to_vertex(u, f_item)) {
                MessageSystem::print_message("Fläche nicht benachbart zum Startknoten.");
                return;
            }
        }
        else {
            // neue Fläche, mind. 2., teste auf Fehler
            FaceItem* last_face = gdraw->m_edge_routing_info.chosen_faces.back();
            if (!gdraw->m_graph->are_faces_adjacent(f_item, last_face)) {
                MessageSystem::print_message("Fläche nicht benachbart zur letzen Fläche.");
                return;
            }
            else if (gdraw->is_already_chosen(f_item)) {
                MessageSystem::print_message("Fläche bereits durchlaufen.");
                return;
            }
            else {
                adjEntry crossed_adj = 
                    gdraw->m_graph->crossed_adjacency( gdraw->m_edge_routing_info.last_point,
                        pos, last_face, f_item);
                if (crossed_adj == nullptr) {
                    MessageSystem::print_message("Kante zwischen beiden Flächen wird nicht gekreuzt.");
                    return;
                }
                node u = gdraw->m_graph->planar_repr()->original(gdraw->m_edge_routing_info.vertex_pair.u->node());
                node v = gdraw->m_graph->planar_repr()->original(gdraw->m_edge_routing_info.vertex_pair.v->node());
                edge crossed_e_orig = gdraw->m_graph->planar_repr()->original(crossed_adj->theEdge());
                node crossed_u = crossed_e_orig->source();
                node crossed_v = crossed_e_orig->target();
                if (crossed_u == u || crossed_u == v
                    || crossed_v == u || crossed_v == v)
                {
                    if (!MessageSystem::show_question_box("Kante zwischen beiden Flächen ist inzident zu einem Endknoten",
                        "Soll die Kante dennoch so verlaufen?", QMessageBox::Warning))
                    {
                        return;
                    }
                }
                gdraw->m_edge_routing_info.crossed_adjacencies.push_back(crossed_adj);
            }
        }
        MessageSystem::print_message("Nächste zu durchlaufende Fläche ausgewählt.");
        gdraw->m_edge_routing_info.chosen_faces.push_back(f_item);
        gdraw->m_edge_routing_info.positions.push_back(vector<Point>());
    }
    else {
        MessageSystem::print_message("Neuer Knickpunkt zur ausgewählten Fläche.");
    }

    // speichere zusätzlichen Knickpunkt und stelle neues Kantensegment dar
    gdraw->m_edge_routing_info.positions.back().push_back(pos);
    QLineF line(gdraw->m_edge_routing_info.last_point, pos);
    auto seg = new QGraphicsLineItem(line);
    seg->setPen(QPen(Qt::blue));
    gdraw->m_scene->addItem(seg);
    gdraw->m_edge_routing_info.segments.push_back(seg);
    gdraw->m_edge_routing_info.last_point = pos;
    
    // markiere Face
    if (!external) {
        QBrush brush = FaceItem::chosen_face_brush();
        f_item->setBrush(brush);
    }
    else {
        QBrush brush = FaceItem::chosen_face_brush();
        gdraw->m_scene->setBackgroundBrush(brush);
    }
}

void GraphScene::finish_edge_addition(VertexItem* v_item, QPointF pos) {
    // zeige auch letztes Kantensegment an 
    MessageSystem::print_message("Kantenverlauf vollständig angegeben, Berechnung des Layouts.");
    QLineF line(gdraw->m_edge_routing_info.last_point, pos);
    auto seg = new QGraphicsLineItem(line);
    seg->setPen(QPen(Qt::blue));
    gdraw->m_scene->addItem(seg);
    gdraw->m_scene->update();
    gdraw->m_edge_routing_info.segments.push_back(seg);
   
    // vervollständige Kantenerstellungsinfo
    VertexItem* u = gdraw->m_edge_routing_info.vertex_pair.u;
    auto chosen_faces = gdraw->m_edge_routing_info.chosen_faces;
    auto crossed_adj = gdraw->m_edge_routing_info.crossed_adjacencies;
    auto bend_positions = gdraw->m_edge_routing_info.positions;

    // füge Kante hinzu
    GraphChange diff = gdraw->m_graph->add_edge(u->node(), v_item->node(),
        chosen_faces, crossed_adj, bend_positions);
    if (diff.type == GraphChange::Type::NO_CHANGE) {
        MessageSystem::print_message("Numerischer Fehler bei den Berechnungen mit dem angegebenen Verlauf, Änderungen werden nicht durgeführt!");
        gdraw->cancel_edge_addition();
        auto new_graph = gdraw->m_editing_info.recreate_current(gdraw->m_scene,
            gdraw->m_graph->vertex_width(), gdraw->m_graph->edge_width());
        delete gdraw->m_graph;
        gdraw->m_graph = new_graph;
        return;
    }
    gdraw->finish_edge_addition(diff);
    emit gdraw->computation_finished();

    MessageSystem::print_message("Kantenerstellung abgeschlossen.");
}

void GraphScene::choose_first_vertex_for_rerouting(VertexItem* v_item, QPointF v_pos) {
    v_item->setBrush(VertexItem::chosen_brush());
    mark_adjacent_v_items(v_item);

    //speichere Rerouting-Informationen
    gdraw->m_edge_routing_info.vertex_pair.u = v_item;
    gdraw->m_edge_routing_info.last_point = v_pos;
    gdraw->m_selection_state = SelectionState::REROUTE_V;
    MessageSystem::print_state_message(gdraw->m_selection_state);

    // Schreibe, was getan wird
    MessageSystem::print_message("Startknoten ausgewählt.");
}

void GraphScene::choose_second_vertex_for_rerouting(VertexItem* v_item) {
    // entferne temporär die angegebene Kante
    gdraw->m_edge_routing_info.vertex_pair.v = v_item;
    gdraw->m_selection_state = SelectionState::REROUTE_THROUGH_FACE;
    MessageSystem::print_state_message(gdraw->m_selection_state);
    auto u = gdraw->m_edge_routing_info.vertex_pair.u;
    auto v = gdraw->m_edge_routing_info.vertex_pair.v;

    int num_crossings_before = gdraw->m_graph->crossing_items().size();
    gdraw->m_graph->remove_edge(u, v);
    int num_crossings_after = gdraw->m_graph->crossing_items().size();
    gdraw->m_edge_routing_info.num_removed_crossings = num_crossings_before - num_crossings_after;

    v_item->setBrush(VertexItem::chosen_brush());
    gdraw->m_graph->mark_incident_edges(*u);
    gdraw->m_graph->mark_incident_edges(*v);

    MessageSystem::print_message("Zielknoten ausgewählt, Kante ist festgelegt.");
}

void GraphScene::finish_rerouting(VertexItem* v_item, QPointF pos) {
    // zeige auch letztes Kantensegment an 
    MessageSystem::print_message("Neuer Kantenverlauf vollständig angegeben, Neuberechnung des Layouts.");
    QLineF line(gdraw->m_edge_routing_info.last_point, pos);
    auto seg = new QGraphicsLineItem(line);
    seg->setPen(QPen(Qt::blue));
    gdraw->m_scene->addItem(seg);
    gdraw->m_scene->update();
    gdraw->m_edge_routing_info.segments.push_back(seg);
   
    // vervollständige Kantenerstellungsinfo
    VertexItem* u = gdraw->m_edge_routing_info.vertex_pair.u;
    auto chosen_faces = gdraw->m_edge_routing_info.chosen_faces;
    auto crossed_adj = gdraw->m_edge_routing_info.crossed_adjacencies;
    auto bend_positions = gdraw->m_edge_routing_info.positions;

    QMessageBox msg_box;
    const char* text = "Wollen Sie die Umleitung durchführen?";
    std::stringstream sub_text;
    QMessageBox::Icon icon;
    int additional_crossings = (gdraw->m_edge_routing_info.chosen_faces.size() - 1) 
            - gdraw->m_edge_routing_info.num_removed_crossings;
    if (additional_crossings == 0) {
        icon = QMessageBox::Question;
        sub_text << "Kreuzungszahl bleibt erhalten.";
    }
    else {
        icon = QMessageBox::Warning;
        if (additional_crossings > 0) {
            sub_text << "Kreuzungszahl wird um " << additional_crossings << " erhöht!";
        }
        else {
            sub_text << "Kreuzungszahl wird um " << -additional_crossings << " verringert!";
        }
    }
    if (!MessageSystem::show_question_box(text, sub_text.str().c_str(), icon)) {
        gdraw->cancel_rerouting();
        emit gdraw->computation_finished();
        return;
    }

    // führe Routing durch
    GraphChange diff = gdraw->m_graph->add_edge(u->node(), v_item->node(),
            chosen_faces, crossed_adj, bend_positions);
    if (diff.type == GraphChange::Type::NO_CHANGE) {
        MessageSystem::print_message("Numerischer Fehler bei den Berechnungen mit dem angegebenen Verlauf, Änderungen werden nicht durgeführt!");
        gdraw->cancel_rerouting();
    }
    diff.type = GraphChange::Type::REROUTING;
    gdraw->finish_rerouting(diff);
    emit gdraw->computation_finished();

    gdraw->m_selection_state = SelectionState::REF_FACE;
    MessageSystem::print_state_message(gdraw->m_selection_state);
    MessageSystem::print_message("Umleitung abgeschlossen.");
}

void GraphScene::mousePressEvent(QGraphicsSceneMouseEvent* mouse_event) {
    QGraphicsScene::mousePressEvent(mouse_event);
    QList<QGraphicsItem*> clicked_items = items(mouse_event->scenePos());
    switch (gdraw->m_selection_state) {
    case SelectionState::REF_FACE: {
        choose_reference_face(clicked_items);
    }; break;
    case SelectionState::REMOVE_VERTEX: {
        VertexItem* v_item = search_items<VertexItem>(clicked_items);
        if (v_item) {
            remove_vertex(v_item);
        }
    }; break;
    case SelectionState::ADD_VERTEX: {
        VertexItem* v_item = search_items<VertexItem>(clicked_items);
        EdgeItem* e_item = search_items<EdgeItem>(clicked_items);
        if (!v_item && !e_item) {
            add_vertex(mouse_event->scenePos());
        }
    }; break;
    case SelectionState::REMOVE_EDGE_U: {
        VertexItem* v_item = search_items<VertexItem>(clicked_items);
        if (v_item) {
            // wähle richtigen Knoten aus (kein Kreuzungsknoten o.ä.)
            if (!v_item->is_not_crossing()) {
                return;
            }
            choose_first_vertex_for_edge_removal(v_item);
        }
        else {
            EdgeItem* e_item = search_items<EdgeItem>(clicked_items);
            if (e_item) {
                auto vertices = find_vertex_items(e_item->edge());
                choose_first_vertex_for_edge_removal(vertices.first);
                choose_second_vertex_for_edge_removal(vertices.second);
            }
        }
    }; break;
    case SelectionState::REMOVE_EDGE_V: {
        VertexItem* v_item = search_items<VertexItem>(clicked_items);
        if (v_item) {
            // wähle richtigen Knoten aus (kein Kreuzungsknoten o.ä.)
            if (!v_item->is_not_crossing()) {
                return;
            }
            else if (!gdraw->m_graph->original_edge_exists(gdraw->m_edge_selection_info.u, v_item)) {
                MessageSystem::print_message("Kante existiert nicht!");
                return;
            }
            choose_second_vertex_for_edge_removal(v_item);
        }
    } break;
    case SelectionState::ADD_EDGE_U: {
        VertexItem* v_item = search_items<VertexItem>(clicked_items);
        if (v_item) {
            // wähle richtigen Knoten aus (kein Kreuzungsknoten o.ä.)
            if (!v_item->is_not_crossing()) {
                return;
            }
            choose_first_vertex_for_edge_addition(v_item, mouse_event->scenePos());
        }
    }; break;
    case SelectionState::ADD_EDGE_V: {
        VertexItem* v_item = search_items<VertexItem>(clicked_items);
        if (v_item) {
            // wähle richtigen Knoten aus (kein Kreuzungsknoten o.ä.)
            if (!v_item->is_not_crossing()) {
                return;
            }
            else if (gdraw->m_graph->original_edge_exists(gdraw->m_edge_routing_info.vertex_pair.u, v_item)) {
                MessageSystem::print_message("Kante existiert bereits!");
                return;
            }
            choose_second_vertex_for_edge_addition(v_item);
        }
    } break;
    case SelectionState::ROUTE_THROUGH_FACES: {
        VertexItem* v_item = search_items<VertexItem>(clicked_items);
        // Endknoten ausgewählt
        if (v_item) {
            // beende Rerouting: überprüfe, ob Pfad vollständig, setze um
            VertexItem* v = gdraw->m_edge_routing_info.vertex_pair.v;

            // wähle richtigen Endknoten v aus
            if (v_item != v) {
                MessageSystem::print_message("Knoten ist nicht Zielknoten.");
                return;
            }
            if ( gdraw->m_edge_routing_info.chosen_faces.empty()
                || !gdraw->m_graph->is_face_adjacent_to_vertex( v_item,
                    gdraw->m_edge_routing_info.chosen_faces.back()))
            {
                MessageSystem::print_message("Knoten noch nicht erreichbar.");
                return;
            }

            finish_edge_addition(v_item, mouse_event->scenePos());
            return;
        }

        // inneres oder äußeres Face ausgewählt
        FaceItem* f_item = search_items<FaceItem>(clicked_items);
        QGraphicsItem* item = itemAt(mouse_event->scenePos(), QTransform());
        if (clicked_items.size() == 1 && dynamic_cast<EdgeItem*>(item) != nullptr) {
            //entspricht dem Fall, dass außeres Face gewählt wurde
            item = nullptr;
        }

        // füge nächstes Face (inneres oder außeres) hinzu, falls sinnvoll gewählt
        if (f_item != nullptr || item == nullptr) {
            bool extern_face_chosen = (item == nullptr);
            if (extern_face_chosen) {
                f_item = gdraw->m_graph->external_face_item();
            }

            QPointF pos = mouse_event->scenePos();
            choose_next_face(f_item, extern_face_chosen, pos);
        }
    }; break; 
    case SelectionState::ALTERNATE_U: {
        VertexItem* v_item = search_items<VertexItem>(clicked_items);
        if (v_item) {
            // wähle richtigen Knoten aus (kein Kreuzungsknoten o.ä.)
            if (!v_item->is_not_crossing()) {
                return;
            }
            choose_first_vertex_for_alternative_paths(v_item);
        }
        else {
            EdgeItem* e_item = search_items<EdgeItem>(clicked_items);
            if (e_item) {
                auto vertices = find_vertex_items(e_item->edge());
                choose_first_vertex_for_alternative_paths(vertices.first);
                choose_second_vertex_for_alternative_paths(vertices.second);
            }
        }
    }; break;
    case SelectionState::ALTERNATE_V: {
        VertexItem* v_item = search_items<VertexItem>(clicked_items);
        if (v_item) {
            // wähle richtigen Knoten aus (kein Kreuzungsknoten o.ä.)
            if (!v_item->is_not_crossing() || v_item == gdraw->m_edge_selection_info.u) {
                return;
            }
            else if (!gdraw->m_graph->original_edge_exists(gdraw->m_edge_selection_info.u, v_item)) {
                MessageSystem::print_message("Kante existiert nicht!");
                return;
            }
            choose_second_vertex_for_alternative_paths(v_item);
        }
    }; break;
    case SelectionState::REROUTE_U: {
        VertexItem* v_item = search_items<VertexItem>(clicked_items);
        if (v_item) {
            // wähle richtigen Knoten aus (kein Kreuzungsknoten o.ä.)
            if (!v_item->is_not_crossing()) {
                return;
            }
            choose_first_vertex_for_rerouting(v_item, mouse_event->scenePos());
        }
        else {
            EdgeItem* e_item = search_items<EdgeItem>(clicked_items);
            if (e_item) {
                auto vertices = find_vertex_items(e_item->edge());
                auto first_vertex = vertices.first;
                choose_first_vertex_for_rerouting(first_vertex, { first_vertex->x_pos(), first_vertex->y_pos() });
                choose_second_vertex_for_rerouting(vertices.second);
            }
        }
    }; break;
    case SelectionState::REROUTE_V: {
        VertexItem* v_item = search_items<VertexItem>(clicked_items);
        if (v_item) {
            // wähle richtigen Knoten aus (kein Kreuzungsknoten, nicht der selbe o.ä.)
            if (!v_item->is_not_crossing() || 
                    v_item->node() == gdraw->m_edge_routing_info.vertex_pair.u->node()) {
                return;
            }
            else if (!gdraw->m_graph->original_edge_exists(gdraw->m_edge_routing_info.vertex_pair.u, v_item)) {
                MessageSystem::print_message("Kante existiert nicht!");
                return;
            }
            choose_second_vertex_for_rerouting(v_item);
        }
    }; break;
    case SelectionState::REROUTE_THROUGH_FACE: {
        VertexItem* v_item = search_items<VertexItem>(clicked_items);
        // Endknoten ausgewählt
        if (v_item) {
            // beende Rerouting: überprüfe, ob Pfad vollständig, setze um
            VertexItem* v = gdraw->m_edge_routing_info.vertex_pair.v;

            // wähle richtigen Endknote
            if (v_item != v) {
                MessageSystem::print_message("Knoten ist nicht Zielknoten.");
                return;
            }
            if ( gdraw->m_edge_routing_info.chosen_faces.empty()
                || !gdraw->m_graph->is_face_adjacent_to_vertex( v_item,
                    gdraw->m_edge_routing_info.chosen_faces.back()))
            {
                MessageSystem::print_message("Knoten noch nicht erreichbar.");
                return;
            }

            finish_rerouting(v_item, mouse_event->scenePos());
            return;
        }

        // inneres oder äußeres Face ausgewählt
        FaceItem* f_item = search_items<FaceItem>(clicked_items);
        QGraphicsItem* item = itemAt(mouse_event->scenePos(), QTransform());
        if (clicked_items.size() == 1 && dynamic_cast<EdgeItem*>(item) != nullptr) {
            //entspricht dem Fall, dass außeres Face gewählt wurde
            item = nullptr;
        }

        // füge nächstes Face (inneres oder außeres) hinzu, falls sinnvoll gewählt
        if (f_item != nullptr || item == nullptr) {
            bool extern_face_chosen = (item == nullptr);
            if (extern_face_chosen) {
                f_item = gdraw->m_graph->external_face_item();
            }

            QPointF pos = mouse_event->scenePos();
            choose_next_face(f_item, extern_face_chosen, pos);
        }
    }; break; 
    case SelectionState::NO_GRAPH_LOADED: {};
    };
} 
