#include "KEdgesWindow.h"

#include <cassert>
#include <string>

#include <QApplication>
#include <QDockWidget>
#include <QMainWindow>
#include <QMessageBox>
#include <QVBoxLayout>
#include <QPushButton>
#include <QAction>
#include <QMenuBar>
#include <QFileDialog>
#include <QCheckBox>
#include <QImage>
#include <QInputDialog>
#include <QLineEdit>
#include <QButtonGroup>
#include <QSpinBox>
#include <QSplitter>
#include <QTextEdit>
#include <QLabel>
#include <QGroupBox>

#include "common.h"
#include "ColorDialog.h"
#include "FaceInfoDialog.h"
#include "MessageSystem.h"
#include "GDrawWidget.h"

const double KEdgesWindow::DEFAULT_EDGE_WIDTH = EdgeItem::DEFAULT_WIDTH;
const double KEdgesWindow::STEP_EDGE_WIDTH = 0.5;
const double KEdgesWindow::MIN_EDGE_WIDTH = 0.25;
const double KEdgesWindow::MAX_EDGE_WIDTH = 30.25;
const double KEdgesWindow::DEFAULT_VERTEX_WIDTH = VertexItem::DEFAULT_WIDTH;
const double KEdgesWindow::STEP_VERTEX_WIDTH = 10.0;
const double KEdgesWindow::MIN_VERTEX_WIDTH = 10.0;
const double KEdgesWindow::MAX_VERTEX_WIDTH = 150.0;

KEdgesWindow::KEdgesWindow(QWidget *parent)
    : QMainWindow(parent)
{
    setLocale(QLocale::system().name());

    // Hauptwidget
    m_gdraw_widget = new GDrawWidget();
    connect(m_gdraw_widget, SIGNAL(computation_finished()),
        this, SLOT(reinitialize_button_states()));

    create_message_system();
    create_graph_properties_view();
    create_visualization_options();
    create_graph_modifications();
#ifdef OGDF_DEBUG
    create_debug_options();
#endif

    create_menu();

    this->setCentralWidget(m_gdraw_widget);

    // erzeuge die eine Instanz des statischen Farbinformationsdialogs
    m_color_info_dialog = new ColorDialog();

    // kein Graph geladen, schalte also zunächst entsprechende Buttons ab
    disable_all_buttons();

    connect(m_gdraw_widget, SIGNAL(computation_finished()),
        this, SLOT(recompute_graph_properties()));

    MessageSystem::print_message("Programm gestartet.");
}

void KEdgesWindow::create_message_system() {
    QTextEdit* state_text_widget = new QTextEdit();
    state_text_widget->setReadOnly(true);
    state_text_widget->setMaximumHeight(80);
    state_text_widget->setAlignment(Qt::AlignCenter);
    MessageSystem::state_text_widget = state_text_widget;

    QTextEdit* sub_text_widget = new QTextEdit();
    m_output = sub_text_widget;
    QPushButton* clear_text_button = new QPushButton("Lösche Ausgabe");
    connect(clear_text_button, SIGNAL(pressed()), sub_text_widget, SLOT(clear()));
    QPushButton* save_button = new QPushButton("Speichere Ausgabe in Datei");
    connect(save_button, SIGNAL(pressed()), this, SLOT(save_output()));
    MessageSystem::sub_text_widget = sub_text_widget;
    QVBoxLayout* sub_text_layout = new QVBoxLayout();
    sub_text_layout->addWidget(state_text_widget);
    sub_text_layout->addWidget(sub_text_widget);
    sub_text_layout->addWidget(clear_text_button);
    sub_text_layout->addWidget(save_button);
    sub_text_widget->setReadOnly(true);
    QWidget* sub_text_entire_widget = new QWidget();
    sub_text_entire_widget->setLayout(sub_text_layout);

    m_message_system_dock = new QDockWidget("Ausgabe", this);
    m_message_system_dock->setWidget(sub_text_entire_widget);
    m_message_system_dock->setFeatures(QDockWidget::DockWidgetFloatable | QDockWidget::DockWidgetMovable);
    m_message_system_dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    addDockWidget(Qt::RightDockWidgetArea, m_message_system_dock);
}

void KEdgesWindow::create_visualization_options() {
    // besondere Visualisierungsmöglichkeiten
    QCheckBox* crossingIDs = new QCheckBox("Kreuzungsknoten anzeigen");
    connect(crossingIDs, SIGNAL(toggled(bool)),
        this, SLOT(show_crossings(bool)));

    // Anzeige der Kanteneigenschaften
    QGroupBox* edge_coloring_group = new QGroupBox("Kantenfärbungen");
    auto no_edge_colors_button = new QRadioButton("ungefärbt", edge_coloring_group);
    connect(no_edge_colors_button, SIGNAL(toggled(bool)),
        this, SLOT(show_no_edge_colors(bool)));
    auto k_number_colored_button = new QRadioButton("k-Zahlen", edge_coloring_group);
    connect(k_number_colored_button, SIGNAL(toggled(bool)),
        this, SLOT(show_knumbers_with_colors(bool)));
    k_number_colored_button->setToolTip("Färbt die Kanten nach ihrer k-Zahl relativ zur ausgewählten Fläche.");
    m_diff_to_prev_colored_button = new QRadioButton("k-Differenzen (zu davor)", edge_coloring_group);
    connect(m_diff_to_prev_colored_button, SIGNAL(toggled(bool)),
        this, SLOT(show_kdiffs_to_prev(bool)));
    m_diff_to_prev_colored_button->setToolTip("Färbt die Kanten nach der Differenz der k-Zahlen zur vorherigen Graphversion, sofern Kante und Referenzfläche vorher eindeutig existiert hat.");
    m_diff_to_next_colored_button = new QRadioButton("k-Differenzen (zu danach)", edge_coloring_group);
    connect(m_diff_to_next_colored_button, SIGNAL(toggled(bool)),
        this, SLOT(show_kdiffs_to_next(bool)));
    m_diff_to_next_colored_button->setToolTip("Färbt die Kanten nach der Differenz der k-Zahlen zur nächsten Graphversion, sofern Kante und Referenzfläche nachher eindeutig existiert.");
    no_edge_colors_button->setChecked(true);
    auto edge_coloring_layout = new QVBoxLayout();
    edge_coloring_layout->addWidget(no_edge_colors_button);
    edge_coloring_layout->addWidget(k_number_colored_button);
    edge_coloring_layout->addWidget(m_diff_to_prev_colored_button);
    edge_coloring_layout->addWidget(m_diff_to_next_colored_button);
    edge_coloring_group->setLayout(edge_coloring_layout);

    // Anzeige der Face-Eigenschaften
    auto ineq_group = new QGroupBox("Flächenfärbungen");
    QRadioButton* no_ineq_conds = new QRadioButton("ungefärbt", ineq_group);
    connect(no_ineq_conds, SIGNAL(toggled(bool)),
        m_gdraw_widget, SLOT(show_simple_faces(bool)));
    QRadioButton* ineq_conds = new QRadioButton("nach =k-Bedingungen", ineq_group);
    connect(ineq_conds, SIGNAL(toggled(bool)),
        m_gdraw_widget, SLOT(show_ineq_faces(bool)));
    ineq_conds->setToolTip("Färbt die Fläche nach der Anzahl der verletzten =k-Bedingungen, mit ihr als Referenzfläche.\n=k-Bedingungen: E_k ≥ 3(k+1), (0 ≤ k ≤ (n-2)/2)");
    QRadioButton* cineq_conds = new QRadioButton("nach ≤k-Bedingungen", ineq_group);
    connect(cineq_conds, SIGNAL(toggled(bool)),
        m_gdraw_widget, SLOT(show_cineq_faces(bool)));
    cineq_conds->setToolTip("Färbt die Fläche nach der Anzahl der verletzten ≤k-Bedingungen, mit ihr als Referenzfläche.\n≤k-Bedingungen: E_≤k ≥ 3(k+2 über 2) , (0 ≤ k ≤ (n-2)/2)");
    QRadioButton* ccineq_conds = new QRadioButton("nach ≤≤k-Bedingungen", ineq_group);
    connect(ccineq_conds, SIGNAL(toggled(bool)),
        m_gdraw_widget, SLOT(show_ccineq_faces(bool)));
    ccineq_conds->setToolTip("Färbt die Fläche nach der Anzahl der verletzten ≤≤k-Bedingungen, mit ihr als Referenzfläche.\n≤≤k-Bedingungen: E_≤≤k ≥ 3(k+3 über 3), (0 ≤ k ≤ (n-2)/2)");
    QRadioButton* cccineq_conds = new QRadioButton("nach ≤≤≤k-Bedingungen", ineq_group);
    connect(cccineq_conds, SIGNAL(toggled(bool)),
        m_gdraw_widget, SLOT(show_cccineq_faces(bool)));
    no_ineq_conds->setChecked(true);
    cccineq_conds->setToolTip("Färbt die Fläche nach der Anzahl der verletzten ≤≤≤k-Bedingungen, mit ihr als Referenzfläche.\n≤≤≤k-Bedingungen: E_k≤≤≤ ≥ 3(k+4 über 4), (0 ≤ k ≤ (n-2)/2)");

    // Zooming
    QPushButton* zoom_rein = new QPushButton("Zoom in");
    QKeySequence zoom_shortcut = Qt::CTRL + Qt::Key_Plus;
    zoom_rein->setAutoRepeat(true);
    zoom_rein->setShortcut(zoom_shortcut);
    zoom_rein->setToolTip("Hotkey: " + zoom_shortcut.toString());
    connect(zoom_rein, SIGNAL(pressed()),
        m_gdraw_widget, SLOT(zoom_in()));
    QPushButton* zoom_raus = new QPushButton("Zoom out");
    zoom_shortcut = Qt::CTRL + Qt::Key_Minus;
    zoom_raus->setAutoRepeat(true);
    zoom_raus->setShortcut(zoom_shortcut);
    zoom_raus->setToolTip("Hotkey: " + zoom_shortcut.toString());
    connect(zoom_raus, SIGNAL(pressed()),
        m_gdraw_widget, SLOT(zoom_out()));
    QPushButton* zoom_to_entire = new QPushButton("Zoom zum Graphen");
    zoom_shortcut = Qt::CTRL + Qt::Key_Z;
    zoom_to_entire->setShortcut(zoom_shortcut);
    zoom_to_entire->setToolTip("In anderen Worten: Passe Größe und Position des Graphen so an, dass er vollständig im Fenster liegt.\nHotkey: " + zoom_shortcut.toString());
    connect(zoom_to_entire, SIGNAL(pressed()),
        m_gdraw_widget, SLOT(zoom_to_entire_graph()));
    QHBoxLayout* zoom_layout = new QHBoxLayout();
    zoom_layout->addWidget(zoom_rein);
    zoom_layout->addWidget(zoom_raus);

    // Knotenbreite
    QLabel* vertex_width_label = new QLabel("Knotenbreite");
    m_vertex_width_widget = new QDoubleSpinBox();
    m_vertex_width_widget->setSingleStep(STEP_VERTEX_WIDTH);
    m_vertex_width_widget->setValue(DEFAULT_VERTEX_WIDTH);
    m_vertex_width_widget->setMinimum(MIN_VERTEX_WIDTH);
    m_vertex_width_widget->setMaximum(MAX_VERTEX_WIDTH);
    connect(m_vertex_width_widget, SIGNAL(valueChanged(double)),
        m_gdraw_widget, SLOT(set_vertex_width(double)));
    QHBoxLayout* vertex_width_layout = new QHBoxLayout();
    vertex_width_layout->addWidget(vertex_width_label);
    vertex_width_layout->addWidget(m_vertex_width_widget);

    // Kantendicke
    QLabel* edge_width_label = new QLabel("Kantenbreite");
    m_edge_width_widget = new QDoubleSpinBox();
    m_edge_width_widget->setSingleStep(STEP_EDGE_WIDTH);
    m_edge_width_widget->setValue(DEFAULT_EDGE_WIDTH);
    m_edge_width_widget->setMinimum(MIN_EDGE_WIDTH);
    m_edge_width_widget->setMaximum(MAX_EDGE_WIDTH);
    connect(m_edge_width_widget, SIGNAL(valueChanged(double)),
        m_gdraw_widget, SLOT(set_edge_width(double)));
    QHBoxLayout* edge_width_layout = new QHBoxLayout();
    edge_width_layout->addWidget(edge_width_label);
    edge_width_layout->addWidget(m_edge_width_widget);

    auto ineq_layout = new QVBoxLayout();
    ineq_layout->addWidget(no_ineq_conds);
    ineq_layout->addWidget(ineq_conds);
    ineq_layout->addWidget(cineq_conds);
    ineq_layout->addWidget(ccineq_conds);
    ineq_layout->addWidget(cccineq_conds);
    ineq_group->setLayout(ineq_layout);

    QVBoxLayout* layout = new QVBoxLayout();
    layout->addWidget(crossingIDs);
    layout->addWidget(edge_coloring_group);
    layout->addWidget(ineq_group);
    layout->addLayout(zoom_layout);
    layout->addWidget(zoom_to_entire);
    layout->addLayout(vertex_width_layout);
    layout->addLayout(edge_width_layout);
    QWidget* visualization_options_widget = new QWidget();
    visualization_options_widget->setLayout(layout);

    m_visualization_options_dock = new QDockWidget("Visualisierung", this);
    m_visualization_options_dock->setWidget(visualization_options_widget);
    m_visualization_options_dock->setFeatures(QDockWidget::DockWidgetFloatable | QDockWidget::DockWidgetMovable);
    m_visualization_options_dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    addDockWidget(Qt::LeftDockWidgetArea, m_visualization_options_dock);
}

void KEdgesWindow::create_graph_properties_view() {
    // Ganzzahlige Eigenschaften über den Graphen
    m_number_vertices_label = new QLabel("-");
    QHBoxLayout* number_vertices_layout = new QHBoxLayout();
    number_vertices_layout->addWidget(new QLabel("Knotenzahl:"));
    number_vertices_layout->addWidget(m_number_vertices_label);
    m_number_edges_label = new QLabel("-");
    QHBoxLayout* number_edges_layout = new QHBoxLayout();
    number_edges_layout->addWidget(new QLabel("Kantenzahl:"));
    number_edges_layout->addWidget(m_number_edges_label);
    m_number_crossings_label = new QLabel("-");
    QHBoxLayout* number_crossings_layout = new QHBoxLayout();
    number_crossings_layout->addWidget(new QLabel("Kreuzungszahl:"));
    number_crossings_layout->addWidget(m_number_crossings_label);
    connect(m_gdraw_widget, SIGNAL(crossing_number_changed(int)),
        m_number_crossings_label, SLOT(setNum(int)));

    // Harary-Hill-Zahl
    QHBoxLayout* harary_hill_layout = new QHBoxLayout();
    auto tooltip = "Die Harary-Hill-Zahl ist die vermutete Kreuzungszahl für den vollständigen Graphen mit vorliegender Knotenanzahl.";
    auto harary_hill_label = new QLabel("Harary-Hill-Zahl:");
    harary_hill_label->setToolTip(tooltip);
    harary_hill_layout->addWidget(harary_hill_label);
    m_harary_hill_label = new QLabel("-");
    m_harary_hill_label->setToolTip(tooltip);
    harary_hill_layout->addWidget(m_harary_hill_label);
    connect(m_gdraw_widget, SIGNAL(crossing_number_changed(int)),
        this, SLOT(recompute_graph_properties()));

    // gute Zeichnung?
    QHBoxLayout* good_drawing_layout = new QHBoxLayout();
    tooltip = "Eine Zeichnung ist gutartig, wenn \n (1) eine Kante sich nicht selbst schneidet,\n"
        " (2) Kanten mit dem selben Endknoten sich nicht schneiden, und\n"
        " (3) keine Kanten sich mehrfach schneiden.";
    auto good_drawing_label = new QLabel("Gutartige Zeichnung:");
    good_drawing_label->setToolTip(tooltip);
    good_drawing_layout->addWidget(good_drawing_label);
    m_good_drawing_label = new QLabel("-");
    m_good_drawing_label->setToolTip(tooltip);
    good_drawing_layout->addWidget(m_good_drawing_label);

    QVBoxLayout* layout = new QVBoxLayout();
    layout->addLayout(number_vertices_layout);
    layout->addLayout(number_edges_layout);
    layout->addLayout(number_crossings_layout);
    layout->addLayout(harary_hill_layout);
    layout->addLayout(good_drawing_layout);
    QWidget* graph_properties_view_widget = new QWidget();
    graph_properties_view_widget->setLayout(layout);
    
    m_graph_properties_dock = new QDockWidget("Eigenschaften", this);
    m_graph_properties_dock->setWidget(graph_properties_view_widget);
    m_graph_properties_dock->setFeatures(QDockWidget::DockWidgetFloatable | QDockWidget::DockWidgetMovable);
    m_graph_properties_dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    addDockWidget(Qt::LeftDockWidgetArea, m_graph_properties_dock);
}


void KEdgesWindow::create_graph_modifications() {
    // Knoten aus Zeichnung entfernen
    m_remove_vertex_button = new QPushButton(BEGIN_REMOVING_VERTEX_TEXT);
    connect(m_remove_vertex_button, SIGNAL(pressed()), this, SLOT(remove_vertex_pressed()));

    // Kanten aus der Zeichnung entfernen
    m_remove_edge_button = new QPushButton(BEGIN_REMOVING_EDGE_TEXT);
    connect(m_remove_edge_button, SIGNAL(pressed()), this, SLOT(remove_edge_pressed()));

    // Knoten hinzufügen
    m_add_vertex_button = new QPushButton(BEGIN_ADDING_VERTEX_TEXT);
    connect(m_add_vertex_button, SIGNAL(pressed()), this, SLOT(add_vertex_pressed()));

    // Kanten hinzufügen
    m_add_edge_button = new QPushButton(BEGIN_ADDING_EDGE_TEXT);
    connect(m_add_edge_button, SIGNAL(pressed()), this, SLOT(add_edge_pressed()));

    // Kantenverläufe austesten oder durchführen
    m_rerouting_button = new QPushButton(BEGIN_REROUTING_TEXT);
    connect(m_rerouting_button, SIGNAL(pressed()),
        this, SLOT(reroute_edge_pressed()));
    m_alternative_courses_button = new QPushButton(BEGIN_ALTERNATIVE_TEXT);
    m_alternative_courses_button->setToolTip("Stellt alternative Kantenverläufe einer auszuwählenden Kante mit gleicher oder kleineren Kreuzungszahl durch einen Farbgradienten dar.");
    connect(m_alternative_courses_button, SIGNAL(pressed()),
        this, SLOT(show_alternative_routes_pressed()));

    // Undo, Redo
    m_undo_button = new QPushButton("Undo");
    QKeySequence undo_shortcut = Qt::CTRL + Qt::Key_Left;
    m_undo_button->setShortcut(undo_shortcut);
    m_undo_button->setToolTip("Hotkey: " + undo_shortcut.toString());
    connect(m_undo_button, SIGNAL(pressed()),
        this, SLOT(undo_pressed()));

    m_redo_button = new QPushButton("Redo");
    QKeySequence redo_shortcut = Qt::CTRL + Qt::Key_Right;
    m_redo_button->setShortcut(redo_shortcut);
    m_redo_button->setToolTip("Hotkey: " + redo_shortcut.toString());
    connect(m_redo_button, SIGNAL(pressed()),
        this, SLOT(redo_pressed()));

    QHBoxLayout* undo_redo_layout = new QHBoxLayout();
    undo_redo_layout->addWidget(m_undo_button);
    undo_redo_layout->addWidget(m_redo_button);

    QVBoxLayout* layout = new QVBoxLayout();
    layout->addWidget(m_remove_vertex_button);
    layout->addWidget(m_remove_edge_button);
    layout->addWidget(m_add_vertex_button);
    layout->addWidget(m_add_edge_button);
    layout->addWidget(m_rerouting_button);
    layout->addWidget(m_alternative_courses_button);
    layout->addLayout(undo_redo_layout);
    QWidget* modifications_widget = new QWidget();
    modifications_widget->setLayout(layout);

    m_modifications_dock = new QDockWidget("Modifikationen", this);
    m_modifications_dock->setWidget(modifications_widget);
    m_modifications_dock->setFeatures(QDockWidget::DockWidgetFloatable | QDockWidget::DockWidgetMovable);
    m_modifications_dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    addDockWidget(Qt::LeftDockWidgetArea, m_modifications_dock);
}


#ifdef OGDF_DEBUG
void KEdgesWindow::create_debug_options() {
    //Debugging
    QPushButton* debug_faces_button = new QPushButton("COLOR FACES");
    connect(debug_faces_button, SIGNAL(pressed()), this, SLOT(debug_faces_pressed()));
    QPushButton* debug_edges_button = new QPushButton("COLOR EDGES");
    connect(debug_edges_button, SIGNAL(pressed()), this, SLOT(debug_edges_pressed()));

    QVBoxLayout* layout = new QVBoxLayout();
    layout->addWidget(debug_faces_button);
    layout->addWidget(debug_edges_button);
    QWidget* debug_widget = new QWidget();
    debug_widget->setLayout(layout);
    
    m_debug_options_dock = new QDockWidget("Debug-Hilfen", this);
    m_debug_options_dock->setWidget(debug_widget);
    m_debug_options_dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    addDockWidget(Qt::RightDockWidgetArea, m_debug_options_dock);
}
#endif

void KEdgesWindow::create_menu() {
    // Menü-Optionen
    // alle Datei-Actions
    QAction* load_action = new QAction("&Lade Layout", this);
    load_action->setShortcut(QKeySequence::Open);
    connect(load_action, SIGNAL(triggered()),
        this, SLOT(load_graph()));
    m_export_plandescr_action = new QAction("&Speichere Layout", this);
    m_export_plandescr_action->setShortcut(QKeySequence::Save);
    connect(m_export_plandescr_action, SIGNAL(triggered()),
        this, SLOT(save_in_file()));
    m_export_action = new QAction("&Exportiere Layout als Grafik", this);
    m_export_action->setShortcut(QKeySequence::SaveAs);
    connect(m_export_action, SIGNAL(triggered()),
        this, SLOT(export_picture()));

    // alle Analyse-Actions
    m_ref_face_info_action = new QAction("Zeige Informationen über ausgewählte &Fläche", this);
    m_ref_face_info_action->setToolTip("Öffnet einen Dialog, der die Anzahl der k-Kanten und die verschiedenen Bedingungen anzeigt.");
    connect(m_ref_face_info_action, SIGNAL(triggered()),
        this, SLOT(show_info_reference_face()));
    m_ref_face_info_action->setShortcut(Qt::CTRL + Qt::Key_F);
    m_violations_good_drawing_action = new QAction("&Verletzungen zu einer gutartigen Zeichnung", this);
    m_violations_good_drawing_action->setToolTip("Zeigt an, welche Bedingungen an eine gutartige Zeichnung verletzt sind.\n"
        "Eine Zeichnung ist gutartig, wenn \n (1) eine Kante sich nicht selbst schneidet,\n"
        " (2) Kanten mit dem selben Endknoten sich nicht schneiden, und\n"
        " (3) keine Kanten sich mehrfach schneiden.");
    connect(m_violations_good_drawing_action, SIGNAL(triggered()),
        this, SLOT(print_violations_to_good_drawing()));
    m_alternate_all_action = new QAction("Kanten mit &alternativen Verläufen anzeigen", this);
    connect(m_alternate_all_action, SIGNAL(triggered()),
        this, SLOT(show_edges_with_alternative_routes_pressed()));
    m_compute_one_minimal_shelling_action = new QAction("Minimales Shelling", this);
    connect(m_compute_one_minimal_shelling_action, SIGNAL(triggered()),
        this, SLOT(compute_one_minimal_shelling_pressed()));
    m_compute_minimal_shellings_action = new QAction("Alle minimalen Shellings", this);
    connect(m_compute_minimal_shellings_action, SIGNAL(triggered()),
        this, SLOT(compute_minimal_shellings_pressed()));
    m_compute_all_shellings_action = new QAction("Alle Shellings", this);
    connect(m_compute_all_shellings_action, SIGNAL(triggered()),
        this, SLOT(compute_all_shellings_pressed()));
    m_compute_one_minimal_bishelling_action = new QAction("Minimales Bishelling", this);
    connect(m_compute_one_minimal_bishelling_action, SIGNAL(triggered()),
        this, SLOT(compute_one_minimal_bishelling_pressed()));
    m_compute_minimal_bishellings_action = new QAction("Alle minimalen Bishellings", this);
    connect(m_compute_minimal_bishellings_action, SIGNAL(triggered()),
        this, SLOT(compute_minimal_bishellings_pressed()));
    m_compute_all_bishellings_action = new QAction("Alle Bishellings", this);
    connect(m_compute_all_bishellings_action, SIGNAL(triggered()),
        this, SLOT(compute_all_bishellings_pressed()));
    m_compute_all_bishellings_of_face_action = new QAction("Alle Bishellings der ausgewählten Fläche", this);
    connect(m_compute_all_bishellings_of_face_action, SIGNAL(triggered()),
        this, SLOT(compute_all_bishellings_of_face_pressed()));

    // alle Hilfe-Aktionen
    QAction* color_help_action = new QAction("Zeige &Farbkodierungen", this);
    color_help_action->setShortcut(Qt::CTRL + Qt::Key_H);
    connect(color_help_action, SIGNAL(triggered()),
        this, SLOT(show_color_help()));
    QAction* about_program_action = new QAction("Über kEdges...", this);
    connect(about_program_action, SIGNAL(triggered()),
        this, SLOT(show_program_help()));

    QMenu* menu = this->menuBar()->addMenu("&Datei");
    menu->setToolTipsVisible(true);
    menu->addAction(load_action);
    menu->addSeparator();
    menu->addAction(m_export_plandescr_action);
    menu->addAction(m_export_action);
    menu = this->menuBar()->addMenu("&Analyse");
    menu->setToolTipsVisible(true);
    menu->addAction(m_ref_face_info_action);
    menu->addAction(m_violations_good_drawing_action);
    menu->addSeparator();
    menu->addAction(m_alternate_all_action);
    menu->addSeparator();
    QMenu* shelling_menu = menu->addMenu("Shelling-Berechnungen");
    shelling_menu->setToolTipsVisible(true);
    shelling_menu->addAction(m_compute_one_minimal_shelling_action);
    shelling_menu->addAction(m_compute_minimal_shellings_action);
    shelling_menu->addAction(m_compute_all_shellings_action);
    shelling_menu->addSeparator();
    shelling_menu->addAction(m_compute_one_minimal_bishelling_action);
    shelling_menu->addAction(m_compute_minimal_bishellings_action);
    shelling_menu->addAction(m_compute_all_bishellings_action);
    shelling_menu->addSeparator();
    shelling_menu->addAction(m_compute_all_bishellings_of_face_action);
    menu = this->menuBar()->addMenu("&Hilfe");
    menu->setToolTipsVisible(true);
    menu->addAction(color_help_action);
    menu->addAction(about_program_action);
}

void KEdgesWindow::recompute_graph_properties() {
    int number = m_gdraw_widget->visual_graph().number_crossings();
    string number_string = std::to_string(number);
    m_number_crossings_label->setText(number_string.c_str());

    number = m_gdraw_widget->visual_graph().number_vertices();
    number_string = std::to_string(number);
    m_number_vertices_label->setText(number_string.c_str());

    number = m_gdraw_widget->visual_graph().number_edges();
    number_string = std::to_string(number);
    m_number_edges_label->setText(number_string.c_str());

    m_harary_hill_label->setText(to_string(
        harary_hill_crossings(m_gdraw_widget->visual_graph().vertex_items().size())).c_str());
    m_good_drawing_label->setText((m_gdraw_widget->visual_graph().is_drawing_good()) ?
        "ja" : "nein");
}

void KEdgesWindow::remove_vertex_pressed() {
    if (!m_gdraw_widget->is_graph_loaded()) {
        MessageSystem::print_message(NOTHING_LOADED_TEXT);
        return;
    }
    if (m_gdraw_widget->is_in_default_state()) {
        m_remove_vertex_button->setText(ABORT_REMOVING_VERTEX_TEXT);
        disable_all_buttons();
        m_remove_vertex_button->setDisabled(false);
        MessageSystem::print_state_message(SelectionState::REMOVE_VERTEX);
        m_gdraw_widget->begin_vertex_removal();
    }
    else if (m_gdraw_widget->is_in_vertex_deletion_state()) {
        m_remove_vertex_button->setText(BEGIN_REMOVING_VERTEX_TEXT);
        reinitialize_button_states();
        MessageSystem::print_message("Knotenentfernung abgebrochen!");
        m_gdraw_widget->cancel_vertex_removal();
    }
    else {
        MessageSystem::print_message("Nicht möglich, andere Operation im Gange.");
    }
}

void KEdgesWindow::remove_edge_pressed() {
    if (!m_gdraw_widget->is_graph_loaded()) {
        MessageSystem::print_message(NOTHING_LOADED_TEXT);
        return;
    }
    if (m_gdraw_widget->is_in_default_state()) {
        m_remove_edge_button->setText(ABORT_REMOVING_EDGE_TEXT);
        disable_all_buttons();
        m_remove_edge_button->setDisabled(false);
        MessageSystem::print_state_message(SelectionState::REMOVE_EDGE_U);
        m_gdraw_widget->begin_edge_removal();
    }
    else if (m_gdraw_widget->is_in_edge_deletion_state()) {
        m_remove_edge_button->setText(BEGIN_REMOVING_EDGE_TEXT);
        reinitialize_button_states();
        MessageSystem::print_message("Kantenentfernung abgebrochen!");
        m_gdraw_widget->cancel_edge_removal();
    }
    else {
        MessageSystem::print_message("Nicht möglich, andere Operation im Gange.");
    }
}

void KEdgesWindow::add_edge_pressed() {
    if (!m_gdraw_widget->is_graph_loaded()) {
        MessageSystem::print_message(NOTHING_LOADED_TEXT);
        return;
    }
    if (m_gdraw_widget->is_in_default_state()) {
        m_add_edge_button->setText(ABORT_ADDING_EDGE_TEXT);
        disable_all_buttons();
        m_add_edge_button->setDisabled(false);
        MessageSystem::print_state_message(SelectionState::ADD_EDGE_U);
        m_gdraw_widget->begin_edge_addition();
    }
    else if (m_gdraw_widget->is_in_edge_addition_state()) {
        m_gdraw_widget->cancel_edge_addition();
        reinitialize_button_states();
        m_rerouting_button->setText(BEGIN_ADDING_EDGE_TEXT);
        MessageSystem::print_message("Auswahl der Knoten für Kantenerstellung abgebrochen");
    }
    else {
        m_gdraw_widget->cancel_edge_addition();
        reinitialize_button_states();
        MessageSystem::print_message("Kantenerstellung abgebrochen.");
    }
}

void KEdgesWindow::add_vertex_pressed() {
    if (!m_gdraw_widget->is_graph_loaded()) {
        MessageSystem::print_message(NOTHING_LOADED_TEXT);
        return;
    }
    if (m_gdraw_widget->is_in_default_state()) {
        m_add_vertex_button->setText(ABORT_ADDING_VERTEX_TEXT);
        disable_all_buttons();
        m_add_vertex_button->setDisabled(false);
        MessageSystem::print_state_message(SelectionState::ADD_VERTEX);
        m_gdraw_widget->begin_vertex_addition();
    }
    else if (m_gdraw_widget->is_in_vertex_addition_state()) {
        m_gdraw_widget->cancel_vertex_addition();
        reinitialize_button_states();
        m_rerouting_button->setText(BEGIN_ADDING_VERTEX_TEXT);
        MessageSystem::print_message("Auswahl der Position für Knotenerstellung abgebrochen");
    }
    else {
        m_gdraw_widget->cancel_vertex_addition();
        reinitialize_button_states();
        MessageSystem::print_message("Knotenerstellung abgebrochen.");
    }
}

void KEdgesWindow::show_crossings(bool show) {
    if (!m_gdraw_widget->is_graph_loaded()) {
        MessageSystem::print_message(NOTHING_LOADED_TEXT);
        return;
    }
    m_gdraw_widget->show_crossings(show);
}

void KEdgesWindow::show_knumbers_with_colors(bool show) {
    if (show) {
        m_gdraw_widget->show_edge_colors(EdgeColoringMode::K_NUMBER);
    }
}

void KEdgesWindow::show_kdiffs_to_prev(bool show) {
    if (show) {
        m_gdraw_widget->show_edge_colors(EdgeColoringMode::K_DIFFERENCE_TO_PREV);
    }
}

void KEdgesWindow::show_kdiffs_to_next(bool show) {
    if (show) {
        m_gdraw_widget->show_edge_colors(EdgeColoringMode::K_DIFFERENCE_TO_NEXT);
    }
}

void KEdgesWindow::show_no_edge_colors(bool show) {
    if (show) {
        m_gdraw_widget->show_edge_colors(EdgeColoringMode::NO_COLORING);
    }
}

void KEdgesWindow::reroute_edge_pressed() {
    if (!m_gdraw_widget->is_graph_loaded()) {
        MessageSystem::print_message(NOTHING_LOADED_TEXT);
        return;
    }
    if (m_gdraw_widget->is_in_alternate_state()) {
        m_gdraw_widget->cancel_showing_alternative_routes();
        m_rerouting_button->setText(BEGIN_ALTERNATIVE_TEXT);
        MessageSystem::print_message("Auswahl der Knoten für alternative Umleitung abgebrochen");
    }
    else if (!m_gdraw_widget->is_in_rerouting_state()) {
        m_gdraw_widget->begin_edge_rerouting();
        m_rerouting_button->setText(ABORT_REROUTING_TEXT);
        disable_all_buttons();
        m_rerouting_button->setDisabled(false);
    }
    else {
        m_gdraw_widget->cancel_rerouting();
        reinitialize_button_states();
        MessageSystem::print_message("Umleitung abgebrochen.");
    }
}

void KEdgesWindow::print_violations_to_good_drawing() {
    if (!m_gdraw_widget->is_graph_loaded()) {
        MessageSystem::print_message(NOTHING_LOADED_TEXT);
        return;
    }
    auto&& violations = m_gdraw_widget->visual_graph().determine_good_drawing_violations();
    if (violations.size() > 0) {
        MessageSystem::print_message("Bedingungen einer gutartigen Zeichnung sind verletzt:");
        for (string violation : violations) {
            MessageSystem::print_message(violation.c_str());
        }
    }
    else {
        MessageSystem::print_message("Keine Bedingungen einer gutartigen Zeichnung verletzt.");
    }
}

void KEdgesWindow::show_alternative_routes_pressed() {
    if (!m_gdraw_widget->is_graph_loaded()) {
        MessageSystem::print_message(NOTHING_LOADED_TEXT);
        return;
    }
    if (m_gdraw_widget->is_in_rerouting_state()) {
        MessageSystem::print_message("Umleitung abbrechen oder zu Ende führen.");
        return;
    }
    if (!m_gdraw_widget->is_in_alternate_state()) {
        m_gdraw_widget->begin_showing_alternative_routes();
        m_alternative_courses_button->setText(ABORT_REROUTING_TEXT);
        disable_all_buttons();
        m_alternative_courses_button->setDisabled(false);
        //MessageSystem::print_message("Wählen sie die umzuleitende Kante, indem Sie beide Knoten auswählen.");
    }
    else {
        m_gdraw_widget->cancel_showing_alternative_routes();
        reinitialize_button_states();
        MessageSystem::print_message("Abgebrochen.");
    }
}

void KEdgesWindow::show_edges_with_alternative_routes_pressed() {
    if (!m_gdraw_widget->is_graph_loaded()) {
        MessageSystem::print_message(NOTHING_LOADED_TEXT);
        return;
    }
    QApplication::setOverrideCursor(Qt::WaitCursor);
    m_gdraw_widget->show_edges_with_alternative_routes(false);
    QApplication::restoreOverrideCursor();
}

void KEdgesWindow::show_edges_with_extended_flips_pressed() {
    if (!m_gdraw_widget->is_graph_loaded()) {
        MessageSystem::print_message(NOTHING_LOADED_TEXT);
        return;
    }
    QApplication::setOverrideCursor(Qt::WaitCursor);
    m_gdraw_widget->show_edges_with_alternative_routes(true);
    QApplication::restoreOverrideCursor();
}

void KEdgesWindow::compute_one_minimal_shelling_pressed() {
    if (!m_gdraw_widget->is_graph_loaded()) {
        MessageSystem::print_message(NOTHING_LOADED_TEXT);
        return;
    }
    MessageSystem::print_message("Berechne ein minimales Shelling...");
    QApplication::setOverrideCursor(Qt::WaitCursor);
    m_gdraw_widget->test_shellability(ShellingOutputType::MINIMAL);
    QApplication::restoreOverrideCursor();
    MessageSystem::print_message("Berechnung beendet.");
}

void KEdgesWindow::compute_one_minimal_bishelling_pressed() {
    if (!m_gdraw_widget->is_graph_loaded()) {
        MessageSystem::print_message(NOTHING_LOADED_TEXT);
        return;
    }
    MessageSystem::print_message("Berechne ein minimales Bishelling...");
    QApplication::setOverrideCursor(Qt::WaitCursor);
    m_gdraw_widget->test_bishellability(ShellingOutputType::MINIMAL);
    QApplication::restoreOverrideCursor();
    MessageSystem::print_message("Berechnung beendet.");
}

void KEdgesWindow::compute_minimal_shellings_pressed() {
    if (!m_gdraw_widget->is_graph_loaded()) {
        MessageSystem::print_message(NOTHING_LOADED_TEXT);
        return;
    }
    MessageSystem::print_message("Berechne alle minimalen Shellings...");
    QApplication::setOverrideCursor(Qt::WaitCursor);
    m_gdraw_widget->test_shellability(ShellingOutputType::ALL_MINIMAL);
    QApplication::restoreOverrideCursor();
    MessageSystem::print_message("Berechnung beendet.");
}

void KEdgesWindow::compute_minimal_bishellings_pressed() {
    if (!m_gdraw_widget->is_graph_loaded()) {
        MessageSystem::print_message(NOTHING_LOADED_TEXT);
        return;
    }
    MessageSystem::print_message("Berechne alle minimalen Bishellings...");
    QApplication::setOverrideCursor(Qt::WaitCursor);
    m_gdraw_widget->test_bishellability(ShellingOutputType::ALL_MINIMAL);
    QApplication::restoreOverrideCursor();
    MessageSystem::print_message("Berechnung beendet.");
}

void KEdgesWindow::compute_all_shellings_pressed() {
    if (!m_gdraw_widget->is_graph_loaded()) {
        MessageSystem::print_message(NOTHING_LOADED_TEXT);
        return;
    }
    MessageSystem::print_message("Berechne alle Shellings...");
    QApplication::setOverrideCursor(Qt::WaitCursor);
    m_gdraw_widget->test_shellability(ShellingOutputType::ALL);
    QApplication::restoreOverrideCursor();
    MessageSystem::print_message("Berechnung beendet.");
}

void KEdgesWindow::compute_all_bishellings_pressed() {
    if (!m_gdraw_widget->is_graph_loaded()) {
        MessageSystem::print_message(NOTHING_LOADED_TEXT);
        return;
    }
    MessageSystem::print_message("Berechne alle Bishellings...");
    QApplication::setOverrideCursor(Qt::WaitCursor);
    m_gdraw_widget->test_bishellability(ShellingOutputType::ALL);
    QApplication::restoreOverrideCursor();
    MessageSystem::print_message("Berechnung beendet.");
}

void KEdgesWindow::compute_all_bishellings_of_face_pressed() {
    if (!m_gdraw_widget->is_graph_loaded()) {
        MessageSystem::print_message(NOTHING_LOADED_TEXT);
        return;
    }
    MessageSystem::print_message("Berechne alle Bishellings der ausgewählten Fläche...");
    QApplication::setOverrideCursor(Qt::WaitCursor);
    string output = m_gdraw_widget->test_bishellability_of_ref_face();
    MessageSystem::print_message(output.c_str());
    QApplication::restoreOverrideCursor();
    MessageSystem::print_message("Berechnung beendet.");
}

void KEdgesWindow::save_output() {
    QString filename = QFileDialog::getSaveFileName(this,
        "Speichern der Ausgabe",
        "./unbenannt.txt",
        "Text-Datei (*.txt)");
    if (filename.isNull() || filename.isEmpty()) {
        return;
    }
    
    ofstream file(filename.toStdString());
    file << m_output->toPlainText().toStdString();
}

void KEdgesWindow::undo_pressed() {
    if (!m_gdraw_widget->is_graph_loaded()) {
        MessageSystem::print_message(NOTHING_LOADED_TEXT);
        return;
    }
    if (!m_gdraw_widget->has_older_graph_version()) {
        MessageSystem::print_message("Nicht möglich, da nichts rückgängig zu machen ist.");
    }
    m_gdraw_widget->undo_modification();
    reinitialize_button_states();
}

void KEdgesWindow::redo_pressed() {
    if (!m_gdraw_widget->is_graph_loaded()) {
        MessageSystem::print_message(NOTHING_LOADED_TEXT);
        return;
    }
    if (!m_gdraw_widget->has_newer_graph_version()) {
        MessageSystem::print_message("Nicht möglich, da nichts wiederherzustellen ist.");
    }
    m_gdraw_widget->redo_modification();
    reinitialize_button_states();
}

#ifdef OGDF_DEBUG
void KEdgesWindow::debug_faces_pressed() {
    if (!m_gdraw_widget->is_graph_loaded()) {
        MessageSystem::print_message(NOTHING_LOADED_TEXT);
        return;
    }
     
    for (auto f_item : m_gdraw_widget->visual_graph().face_items()) {
        if (!f_item->is_external()) {
            debug_color(f_item, f_item->first_adjacency()->index());
        }
    }
}

void KEdgesWindow::debug_edges_pressed() {
    if (!m_gdraw_widget->is_graph_loaded()) {
        MessageSystem::print_message(NOTHING_LOADED_TEXT);
        return;
    }
     
    for (auto e_item : m_gdraw_widget->visual_graph().edge_items()) {
            debug_color(e_item, e_item->edge()->index());
    }
}
#endif

void KEdgesWindow::load_graph() {
    QString filename = QFileDialog::getOpenFileName(this,
        "Wählen Sie eine geometrische Zeichnung (Positionen der Knoten und Knicke)!", "",
        "Graph Modelling Language (*.gml)");

    if (filename.isNull() || filename.isEmpty()) {
        return;
    }
    MessageSystem::print_message(string("Lade Graph in \"") + filename.toStdString() + "\"...");
    QString fileformat = filename.section('.', -1);

    bool ok;
    string err_msg;
    UnprocessedDrawing orig_drawing = read_gml_drawing(filename.toStdString(), &ok, &err_msg);
    if (!ok) {
        MessageSystem::print_message(err_msg.c_str());
        return;
    }

    QString scale_text = QInputDialog::getText(this, "Skalierung",
        "Skalierungsfaktor der angegebenen Positionen:", QLineEdit::Normal,
        "1", &ok);
    double scale = scale_text.toDouble();
    if (!ok) {
        return;
    }
    if (scale_text.isEmpty() || scale <= 0.0) {
        MessageSystem::print_message("Skalierungsfaktor muss größer als 0.0 sein!");
    }
    orig_drawing.scale_by(scale);

    Graph* graph = new Graph();
    PlanarizedDrawing planarized;
    PlanRep* planrep = planarize(orig_drawing, graph, &planarized);
    m_gdraw_widget->clear_layout();

    // Berechnung der äußeren Hülle
    VisualGraph* visual_graph = new VisualGraph(m_gdraw_widget->scene(), graph, planrep, planarized, m_vertex_width_widget->value(), m_edge_width_widget->value());
    m_gdraw_widget->set_new_graph(filename.toStdString(), visual_graph, orig_drawing);

    recompute_graph_properties();
    MessageSystem::print_message("Graph geladen.");

    setWindowTitle("kEdges - " + filename);
    reinitialize_button_states();
}

void KEdgesWindow::export_picture() {
    if (!m_gdraw_widget->is_graph_loaded()) {
        MessageSystem::print_message(NOTHING_LOADED_TEXT);
        return;
    }

    QString filename = QFileDialog::getSaveFileName(this,
        "Exportieren des Layouts",
        "./untitled.png",
        "Bilder (*.png *.jpeg *.jpg *.bmp)");
    if (filename.isNull() || filename.isEmpty()) {
        return;
    }
    QRect size = m_gdraw_widget->layout_dimensions();
    size.moveTo(0, 0);
    QImage device(size.width(), size.height(), QImage::Format_ARGB32);
    QPainter painter(&device);
    painter.setRenderHint(QPainter::Antialiasing);
    m_gdraw_widget->render(painter);
    device.save(filename);
    MessageSystem::print_message("Graphlayout exportiert.");
}

void KEdgesWindow::save_in_file() {
    if (!m_gdraw_widget->is_graph_loaded()) {
        MessageSystem::print_message(NOTHING_LOADED_TEXT);
        return;
    }
    QString filename = QFileDialog::getSaveFileName(this,
        "Speichern der Planarisierung",
        "./untitled.gml",
        "Graph Modeling Language (*.gml)");
    if (filename.isNull() || filename.isEmpty()) {
        return;
    }
    VisualGraph& graph = m_gdraw_widget->visual_graph();
    UnprocessedDrawing drawing = graph.to_crossingless_drawing();
    write_gml_file(*graph.graph(), drawing, filename.toStdString());
    MessageSystem::print_message("Graph im \'gml\'-Format gespeichert.");
    setWindowTitle("kEdges - " + filename);
}

void KEdgesWindow::show_color_help() {
    m_color_info_dialog->show();
}

void KEdgesWindow::show_info_reference_face() {
    if (!m_gdraw_widget->is_graph_loaded()) {
        MessageSystem::print_message(NOTHING_LOADED_TEXT);
        return;
    }
    FaceInfoDialog* info_dialog = new FaceInfoDialog(m_gdraw_widget->visual_graph(), m_gdraw_widget->reference_face());
    info_dialog->show();
}

void KEdgesWindow::reinitialize_button_states() {
    if (!m_gdraw_widget->is_graph_loaded()) {
        disable_all_buttons();
        return;
    }
    m_export_action->setDisabled(false);
    m_export_plandescr_action->setDisabled(false);
    m_ref_face_info_action->setDisabled(false);
    m_compute_one_minimal_shelling_action->setDisabled(false);
    m_compute_minimal_shellings_action->setDisabled(false);
    m_compute_all_shellings_action->setDisabled(false);
    m_compute_one_minimal_bishelling_action->setDisabled(false);
    m_compute_minimal_bishellings_action->setDisabled(false);
    m_compute_all_bishellings_action->setDisabled(false);
    m_compute_all_bishellings_of_face_action->setDisabled(false);
    m_add_vertex_button->setText(BEGIN_ADDING_VERTEX_TEXT);
    m_add_vertex_button->setDisabled(false);
    m_remove_vertex_button->setText(BEGIN_REMOVING_VERTEX_TEXT);
    m_remove_vertex_button->setDisabled(false);
    m_add_edge_button->setText(BEGIN_ADDING_EDGE_TEXT);
    m_add_edge_button->setDisabled(false);
    m_remove_edge_button->setText(BEGIN_REMOVING_EDGE_TEXT);
    m_remove_edge_button->setDisabled(false);
    m_alternative_courses_button->setDisabled(false);
    m_alternative_courses_button->setText(BEGIN_ALTERNATIVE_TEXT);
    m_rerouting_button->setDisabled(false);
    m_rerouting_button->setText(BEGIN_REROUTING_TEXT);
    m_alternate_all_action->setDisabled(false);
    m_undo_button->setDisabled(!m_gdraw_widget->has_older_graph_version());
    m_redo_button->setDisabled(!m_gdraw_widget->has_newer_graph_version());
    m_violations_good_drawing_action->setDisabled(m_gdraw_widget->visual_graph().is_drawing_good());
    m_diff_to_prev_colored_button->setDisabled(!m_gdraw_widget->has_older_graph_version());
    m_diff_to_next_colored_button->setDisabled(!m_gdraw_widget->has_newer_graph_version());
}

void KEdgesWindow::disable_all_buttons() {
    m_export_action->setDisabled(true);
    m_export_plandescr_action->setDisabled(true);
    m_ref_face_info_action->setDisabled(true);
    m_compute_one_minimal_shelling_action->setDisabled(true);
    m_compute_minimal_shellings_action->setDisabled(true);
    m_compute_all_shellings_action->setDisabled(true);
    m_compute_one_minimal_bishelling_action->setDisabled(true);
    m_compute_minimal_bishellings_action->setDisabled(true);
    m_compute_all_bishellings_action->setDisabled(true);
    m_compute_all_bishellings_of_face_action->setDisabled(true);
    m_alternative_courses_button->setDisabled(true);
    m_rerouting_button->setDisabled(true);
    m_alternate_all_action->setDisabled(true);
    m_undo_button->setDisabled(true);
    m_redo_button->setDisabled(true);
    m_remove_vertex_button->setDisabled(true);
    m_remove_edge_button->setDisabled(true);
    m_add_vertex_button->setDisabled(true);
    m_add_edge_button->setDisabled(true);
    m_violations_good_drawing_action->setDisabled(true);
    m_diff_to_prev_colored_button->setDisabled(!m_gdraw_widget->has_older_graph_version());
    m_diff_to_next_colored_button->setDisabled(!m_gdraw_widget->has_newer_graph_version());
}

void KEdgesWindow::show_program_help() {
    QMessageBox::information(this, "Über kEdges...",
        "kEdges ist ein Programm, das entwickelt wurde, um die Harary-Hill-Vermutung bezüglich der optimalen Kreuzungszahl von vollständigen Graphen genauer zu untersuchen.\n\n"
        "Zu diesem Zweck können Graphzeichnungen im GML-Format geladen werden, um sogenannte k-Zahlen automatisch berechnen zu lassen. "
        "Diese Informationen werden aktualisiert, wenn der Graph oder die Zeichnung aktualisiert wird, um den Einfluss dieser Veränderungen am konkreten Beispiel zu testen.\n\n"
        "Die meisten Visualisierungen verwenden dafür eine einheitliche Farbkodierung, die in der Hilfe vorgefunden werden kann. "
        "Zu den komplexeren Begriffen und Aktionen können Tooltips angezeigt werden.",
        QMessageBox::Ok);
}