#include "MessageSystem.h"

QTextEdit* MessageSystem::state_text_widget = nullptr;
QTextEdit* MessageSystem::sub_text_widget = nullptr;

const char* MessageSystem::NO_GRAPH_LOADED_TEXT = "Laden sie zunächst einen planarisierten Graphen.";
const char* MessageSystem::REF_FACE_TEXT = "Sie können eine neue Referenzfläche auswählen.";
const char* MessageSystem::ADD_VERTEX_TEXT = "Wählen Sie den Ort, an dem sie den neuen Knoten hinzufügen wollen.";
const char* MessageSystem::REMOVE_VERTEX_TEXT = "Wählen Sie einen zu löschenden Knoten!";
const char* MessageSystem::REMOVE_EDGE_U_TEXT = "Kantenentfernung: Wählen Sie die Kante oder dessen Startknoten.";
const char* MessageSystem::REMOVE_EDGE_V_TEXT = "Kantenentfernung: Wählen Sie den Zielknoten der Kante.";
const char* MessageSystem::ADD_EDGE_U_TEXT = "Kantenerstellung: Wählen Sie den Startknoten der Kante.";
const char* MessageSystem::ADD_EDGE_V_TEXT = "Kantenerstellung: Wählen Sie den Zielknoten der Kante.";
const char* MessageSystem::ROUTE_THROUGH_FACES_TEXT = "Kantenerstellung: Wählen Sie die Route der Kante.";
const char* MessageSystem::ALTERNATE_U_TEXT = "Alternative Routen: Wählen Sie die Kante oder dessen Startknoten.";
const char* MessageSystem::ALTERNATE_V_TEXT = "Alternative Routen: Wählen Sie den Zielknoten der Kante.";
const char* MessageSystem::REROUTE_U_TEXT = "Umleitung: Wählen Sie die Kante oder dessen Startknoten.";
const char* MessageSystem::REROUTE_V_TEXT = "Umleitung: Wählen Sie den Zielknoten der Kante.";
const char* MessageSystem::REROUTE_THROUGH_FACE_TEXT = "Umleitung: Wählen Sie die neue Route der Kante.";

bool MessageSystem::show_question_box(const char* text, const char* sub_text, QMessageBox::Icon icon) {
    QMessageBox msg_box;
    msg_box.setText(text);
    msg_box.setInformativeText(sub_text);
    msg_box.setIcon(icon);
    msg_box.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    msg_box.setDefaultButton(QMessageBox::Yes);
    msg_box.setButtonText(QMessageBox::Yes, "Ja");
    msg_box.setButtonText(QMessageBox::No, "Nein");
    int chosen_option = msg_box.exec();
    switch (chosen_option) {
    case QMessageBox::Yes: {
        return true;
    }; break;
    case QMessageBox::No: {
        return false;
    }; break;
    }
    return false;
}

void MessageSystem::print_state_message(SelectionState state) {
    if (state_text_widget) {
        const char* text;
        switch (state) {
            case SelectionState::NO_GRAPH_LOADED: text = NO_GRAPH_LOADED_TEXT; break;
            case SelectionState::REF_FACE: text = REF_FACE_TEXT; break;
            case SelectionState::REMOVE_VERTEX: text = REMOVE_VERTEX_TEXT; break;
            case SelectionState::ADD_VERTEX: text = ADD_VERTEX_TEXT; break;
            case SelectionState::REMOVE_EDGE_U: text = REMOVE_EDGE_U_TEXT; break;
            case SelectionState::REMOVE_EDGE_V: text = REMOVE_EDGE_V_TEXT; break;
            case SelectionState::ADD_EDGE_U: text = ADD_EDGE_U_TEXT; break;
            case SelectionState::ADD_EDGE_V: text = ADD_EDGE_V_TEXT; break;
            case SelectionState::ROUTE_THROUGH_FACES: text = ROUTE_THROUGH_FACES_TEXT; break;
            case SelectionState::ALTERNATE_U: text = ALTERNATE_U_TEXT; break;
            case SelectionState::ALTERNATE_V: text = ALTERNATE_V_TEXT; break;
            case SelectionState::REROUTE_U: text = REROUTE_U_TEXT; break;
            case SelectionState::REROUTE_V: text = REROUTE_V_TEXT; break;
            case SelectionState::REROUTE_THROUGH_FACE: text = REROUTE_THROUGH_FACE_TEXT; break;
            default:
                text = "TEXT NOCH NICHT IMPLEMENTIERT!";
        }
        state_text_widget->setText(text);
    }
}

void MessageSystem::print_message(const char* text) {
    if (sub_text_widget) {
        sub_text_widget->append(text);
    }
}

void MessageSystem::print_message(std::string text) {
    if (sub_text_widget) {
        sub_text_widget->append(text.c_str());
    }
}
