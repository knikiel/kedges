#include "VertexItem.h"

#include "ogdf/basic/Graph.h"
#include <QBrush>

using namespace ogdf;

const int VertexItem::DEFAULT_WIDTH = 40;

VertexItem::VertexItem(ogdf::node v, QString text, bool not_crossing,
    qreal x, qreal y, qreal width) :
    QGraphicsEllipseItem(),
    m_node(v),
    m_text(new QGraphicsTextItem(text)),
    m_is_not_crossing(not_crossing),
    m_x_pos(x),
    m_y_pos(y)
{
    m_text->setParentItem(this);
    setRect(x - width/2, y - width/2, width, width);
    m_text->setPos(x - width/2, y - width/2);
    m_text->setScale(width/20);

    reset_brush(true);
}

VertexItem::~VertexItem() { } 

void VertexItem::reset_brush(bool visible) {
    if (m_is_not_crossing) {
        setBrush(default_brush());
    }
    else {
        setBrush(crossing_brush());
    }
    setVisible(visible);
}

void VertexItem::set_width(double new_width) {
    QRectF r = rect();
    double new_x = r.x() + r.width() / 2 - new_width / 2;
    double new_y = r.y() + r.height() / 2 - new_width / 2;
    setRect(new_x, new_y, new_width, new_width);
    m_text->setPos(new_x, new_y);
    m_text->setScale(new_width/20);
}

QBrush VertexItem::default_brush() {
    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(Qt::GlobalColor::yellow);
    return brush;
}

QBrush VertexItem::crossing_brush() {
    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(Qt::GlobalColor::cyan);
    return brush;
}

QBrush VertexItem::selectable_brush() {
    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(Qt::GlobalColor::green);
    return brush;
}

QBrush VertexItem::chosen_brush() {
    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(Qt::GlobalColor::red);
    return brush;
}
