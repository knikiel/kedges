#include "VisualGraph.h"

#include "ogdf/basic/simple_graph_alg.h"
#include "ogdf/basic/DualGraph.h"
#include "ogdf/graphalg/ShortestPathAlgorithms.h"

#include <cassert>
#include <sstream>

using namespace std;

#ifdef OGDF_DEBUG
void debug_color(FaceItem* f_item, int val) {
    assert(f_item);
    f_item->setVisible(true);
    auto brush = f_item->brush();
    brush.setStyle(Qt::LinearGradientPattern);
    brush.setColor(QColor(val * 91 & 0xFF, val * 23 & 0xFF, val * 208 & 0xFF));
    f_item->setBrush(brush);
}

void debug_color(EdgeItem* e_item, int val) {
    assert(e_item);
    e_item->setVisible(true);
    auto brush = e_item->pen();
    brush.setStyle(Qt::DotLine);
    brush.setColor(QColor(val * 91 & 0xFF, val * 23 & 0xFF, val * 208 & 0xFF));
    e_item->set_pen(brush);
}
#endif

/// Berechnet das Kreuzprodukt zweier Punkte.
inline double cross(Point p1, Point p2) {
    return p1.x * p2.y - p2.x * p1.y;
}


/// Gibt zurück, ob man von der Streck p1-p2 zum Punkt p3 nach links geht.
inline bool turns_left(Point p1, Point p2, Point p3) {
    // siehe z.B. Cormen
    Point p12 = {p2.x - p1.x, p2.y - p1.y};
    Point p13 = {p3.x - p1.x, p3.y - p1.y};
    return cross(p13, p12) < 0;
}

/// Sortiert eine Sequenz gegen den Uhrzeigersinn bzgl. des Referenzpunktes.
/// Referenzpunkt muss als erster Punkt in points enthalten sein.
/// Referenzpunkt ist letzter Punkt im Ergebnis.
void sort_by_polar_angle(const std::vector<Point>& points, Point reference_point, std::vector<size_t>* sorted) {
    // Initialisierung
    sorted->clear();
    for (size_t i = 0; i < points.size(); ++i) {
        sorted->push_back(i);
    }

    // Sortierung über std::sort
    std::sort(sorted->begin(), sorted->end(),
        [&points, reference_point](node_id u, node_id v) {
        if (u == 0) {
            return false;
        }
        else if (v == 0) {
            return true;
        }
        else {
            return turns_left(reference_point, points.at(u), points.at(v));
        }
    } );
}


/// Berechnung einer äußeren Kante für korrekte Darstellung.
node_pair compute_external_adjacency(const std::vector<Point>& points, const set<pair<Point, Point>>& are_connected) {
    assert(points.size() > 2);

    // finde tiefsten Punkt curr_node (bei Gleichstand den linksten)
    Point reference_point = {0., -100000000000.};
    node_id reference_id;
    node_id curr_node = 0;
    for (Point curr_point : points) {
        if (curr_point.y > reference_point.y ||
            (curr_point.y == reference_point.y && curr_point.x < reference_point.x))
        {
            reference_point = curr_point;
            reference_id = curr_node;
        }
        curr_node += 1;
    }

    // bestimme unter allen mit curr_node verbundenen Punkten, welcher in
    // der Rotation am weitesten rechts steht; zusammen beschreiben sie die geforderte Adjazenz.
    vector<Point> connected_points;
    vector<node_id> connected_ids;
    connected_points.push_back(reference_point);
    connected_ids.push_back(reference_id);
    for (size_t curr_node = 0; curr_node < points.size(); ++curr_node) {
        Point other_point = points[curr_node];
        if (are_connected.find({ reference_point, other_point }) != are_connected.end()) {
            connected_points.push_back(other_point);
            connected_ids.push_back(curr_node);
        }
    }
    std::vector<size_t> sorted_vertices;
    sort_by_polar_angle(connected_points, reference_point, &sorted_vertices);

    return { connected_ids[sorted_vertices.front()], reference_id };
}

VisualGraph::VisualGraph(QGraphicsScene* scene,
    ogdf::Graph* graph, ogdf::PlanRep* planrep,
    const PlanarizedDrawing& drawing,
    double vertex_width, double edge_width) :
        m_graph(graph),
        m_planrep(planrep),
        m_scene(scene),
        m_edge_width(edge_width),
        m_vertex_width(vertex_width)
{
    int num_nodes = drawing.number_nodes();
    vector<Point> copy_ids_to_points;
    set<pair<Point, Point>> connected_points;
    map<Point, node_id> point_to_id;
    for (int v_copy_id = 0; v_copy_id < num_nodes; ++v_copy_id) {
        auto pos = drawing.position(v_copy_id);
        copy_ids_to_points.push_back(pos);
        point_to_id[pos] = v_copy_id;
    }
    node_id curr_bend_id = copy_ids_to_points.size();
    std::map<node_id, node_pair> end_points_for_bendings;
    edge e;
    forall_edges(e, *m_planrep) {
        node_id u_copy_id = e->source()->index();
        node_id v_copy_id = e->target()->index();
        Point prev_point = drawing.position(u_copy_id);
        assert(drawing.exists(e));
        auto bendings = drawing.bendings(e);
        for (Point bend_pos : bendings) {
            copy_ids_to_points.push_back(bend_pos);
            connected_points.insert({ prev_point, bend_pos });
            connected_points.insert({ bend_pos, prev_point });
            end_points_for_bendings[curr_bend_id] = { u_copy_id, v_copy_id };
            point_to_id[bend_pos] = curr_bend_id;
            curr_bend_id += 1;
            prev_point = bend_pos;
        }
        connected_points.insert({ prev_point, drawing.position(v_copy_id) });
        connected_points.insert({ drawing.position(v_copy_id), prev_point });
    }

    auto external_segment = compute_external_adjacency(copy_ids_to_points, connected_points);
    // suche echte Kantenenden, von denen das äußere Segment abstammt;
    node_id u_copy_id = -1;
    node_id v_copy_id = -1;
    if (external_segment.first < num_nodes) {
        u_copy_id = external_segment.first;
        if (external_segment.second < num_nodes) {
            v_copy_id = external_segment.second;
        }
        else {
            auto end_nodes = end_points_for_bendings[external_segment.second];
            v_copy_id = (u_copy_id == end_nodes.first) ? end_nodes.second : end_nodes.first;
        }
    }
    else {
        auto end_nodes1 = end_points_for_bendings[external_segment.first];
        node u1 = nullptr, v1 = nullptr;
        node u;
        forall_nodes(u, *m_planrep) {
            if (u->index() == end_nodes1.first) {
                u1 = u;
            }
            else if (u->index() == end_nodes1.second) {
                v1 = u;
            }
        }
        edge real_edge = m_planrep->searchEdge(u1, v1);
        assert(real_edge);
        if (external_segment.second < num_nodes) {
            v_copy_id = external_segment.second;
            u_copy_id = (v_copy_id == end_nodes1.first) ? end_nodes1.second : end_nodes1.first;
        }
        else {
            // komplizierter Fall: beide "Knoten" sind Knicke auf der selben Kante
            auto end_nodes2 = end_points_for_bendings[external_segment.second];
            assert(end_nodes1 == end_nodes2);
            bool u_found = false;
            bool v_found = false;
            for (auto bend_pos : drawing.bendings(real_edge)) {
                if (point_to_id[bend_pos] == external_segment.first) {
                    u_found = true;
                    if (v_found) {
                        u_copy_id = end_nodes1.second;
                        v_copy_id = end_nodes2.first;
                        break;
                    }
                }
                if (point_to_id[bend_pos] == external_segment.second) {
                    v_found = true;
                    if (u_found) {
                        u_copy_id = end_nodes1.first;
                        v_copy_id = end_nodes2.second;
                        break;
                    }
                }
            }
            assert(u_found && v_found);
        }
    }

    node u;
    forall_nodes (u, *planrep) {
        if (u->index() == u_copy_id) {
            break;
        }
    }
    adjEntry external_adj = nullptr;
    forall_adj (external_adj, u) {
        if (external_adj->twinNode()->index() == v_copy_id) {
            break;
        }
    }
    assert(external_adj);

    compute_initial_layout(scene, drawing, external_adj);
}

VisualGraph::~VisualGraph() {
    delete m_planrep;
    delete m_graph;
}

GraphChange VisualGraph::add_vertex(const Point& position) {
    node new_vertex_orig = m_graph->newNode();
    node new_vertex_copy = m_planrep->newCopy(new_vertex_orig, Graph::NodeType::vertex);
    auto vertex_label = QString("%1").arg(new_vertex_orig->index());
    VertexItem* new_vertex_item = new VertexItem(new_vertex_copy, vertex_label,
        true, position.x, position.y, m_vertex_width);
    m_scene->addItem(new_vertex_item);
    m_node_to_v_item[new_vertex_copy] = new_vertex_item;
    m_vertices.insert(new_vertex_item);

    assert_consistencies();
    reset_overlapping_items();

    GraphChange diff;
    diff.type = GraphChange::Type::VERTEX_ADDITION;
    diff.position = position;
    return diff;
}

GraphChange VisualGraph::remove_vertex(VertexItem* v_item) {
    node v_copy = v_item->node();
    node v_orig = m_planrep->original(v_copy);

    bool ref_face_external = m_reference_face == m_external_face;
    auto ref_face_polygon = m_reference_face->polygon();
    auto ref_face_adjs = m_reference_face->adjacencies();

    // entferne anliegende Kanten
    while (v_orig->degree() > 0) {
        adjEntry adj = v_orig->firstAdj();
        node other_v = m_planrep->copy(adj->twinNode());
        VertexItem* other_v_item = m_node_to_v_item[other_v];
        remove_edge(v_item, other_v_item);
        assert(m_planrep->representsCombEmbedding());
        assert(m_external_face);
        assert(m_reference_face);
    }

    // entferne schließlich den betreffenden Knoten
    delete_vertex_item(v_item->node(), true);
    m_planrep->delNode(v_copy);
    m_graph->delNode(v_orig);

    set_reference_face(ref_face_polygon, ref_face_external);
    assert(m_planrep->representsCombEmbedding());
    assert(m_external_face);
    assert(m_reference_face);

    assert_consistencies();
    reset_overlapping_items();
    GraphChange diff;
    diff.type = GraphChange::Type::VERTEX_REMOVAL;
    diff.vertex_id = v_copy->index();
    return diff;
}

/// Für Shelling-Tests: Fügt die Menge an begrenzenden Kanten für das angegebene Face hinzu.
void compute_surrounding_edges(FaceItem* f_item, vector<edge>* surrounding_edges) {
    adjEntry first_adj = f_item->first_adjacency();
    adjEntry curr_adj = nullptr;
    forall_adjEdges(curr_adj, first_adj, {
        if (std::find(surrounding_edges->begin(), surrounding_edges->end(), curr_adj->theEdge()) == surrounding_edges->end()) {
            surrounding_edges->push_back(curr_adj->theEdge());
        }
    })
}

bool VisualGraph::test_shelling_pairs(FaceItem *reference_face, const vector<node> &shelling_sequence) const
{
    // bleiben über die gesamten Tests erhalten
    vector<FaceItem*> reference_faces_front;
    vector<edge> surrounding_edges_front;
    reference_faces_front.push_back(reference_face);
    compute_surrounding_edges(reference_face, &surrounding_edges_front);

    // setzen sich aus permament gemergten Faces der Anfangssequenz und der gelegentlich
    // auf 0 reduzierten Endsequenz zusammen
    vector<FaceItem*> all_reference_faces;
    vector<edge> all_surrounding_edges;
    all_reference_faces.push_back(reference_face);
    all_surrounding_edges = surrounding_edges_front;

    // teste die Paare
    int r = shelling_sequence.size();
    for (int i = 0; i < r - 1; ++i) {
        node u_i = shelling_sequence[i];
        all_reference_faces.clear();
        all_reference_faces = reference_faces_front;
        all_surrounding_edges.clear();
        all_surrounding_edges = surrounding_edges_front;
        for (int j = r - 1; j > i; --j) {
            node u_j = shelling_sequence[j];
            // teste das Shelling-Paar
            bool u_i_found = false;
            bool u_j_found = false;
            for (edge surrounding_edge : all_surrounding_edges) {
                node source = surrounding_edge->source();
                node target = surrounding_edge->target();
                if (source == u_i || target == u_i) {
                    u_i_found = true;
                }
                if (source == u_j || target == u_j) {
                    u_j_found = true;
                }
                if (u_i_found && u_j_found) {
                    break;
                }
            }
            if (!(u_i_found && u_j_found)) {
                return false;
            }
            delete_vertex_for_shelling_tests(u_j, &all_reference_faces, &all_surrounding_edges);
        }
        delete_vertex_for_shelling_tests(u_i, &reference_faces_front, &surrounding_edges_front);
    }
    return true;
}

void VisualGraph::compute_outer_vertices(const vector<FaceItem*>& reference_faces, node u_last,
    const vector<node>& other_vertices, vector<node>* outer_vertices) const
{
    for (auto f_item : reference_faces) {
        adjEntry first_adj = f_item->first_adjacency();
        adjEntry curr_adj = nullptr;
        forall_adjEdges(curr_adj, first_adj, {
            node vertex = curr_adj->theNode();
            if (!m_planrep->isCrossingType(vertex)
                && std::find(other_vertices.begin(), other_vertices.end(), vertex) == other_vertices.end()
                && vertex != u_last
                && std::find(outer_vertices->begin(), outer_vertices->end(), vertex) == outer_vertices->end())
            {
                outer_vertices->push_back(vertex);
            }
        })
    }
}

int VisualGraph::delete_vertex_for_shelling_tests(node outer_vertex, vector<FaceItem*>* reference_faces,
    vector<edge>* surrounding_edges) const
{
    // berechne die Menge der nun zusätzlich gelöschten Segmente
    vector<edge> all_deleted_segments;
    edge deleted_edge;
    node v_orig = m_planrep->original(outer_vertex);
    forall_adj_edges(deleted_edge, v_orig) {
        auto deleted_segments = m_planrep->chain(deleted_edge);
        for (auto deleted_segment : deleted_segments) {
            all_deleted_segments.push_back(deleted_segment);
        }
    }
    int num_additional_faces = 0;
    // gehe wiederholt die Kanten des Knotens ab und prüfe, ob sie nun benachbart
    // zu einem Referenz-Face sind, füge evtl. fehlendes Face ein
    bool changed = true;
    while (changed) {
        changed = false;
        for (auto deleted_segment : all_deleted_segments) {
            if (std::find(surrounding_edges->begin(), surrounding_edges->end(), deleted_segment) != surrounding_edges->end()) {
                adjEntry adj = deleted_segment->adjSource();
                FaceItem* right_f_item = m_adj_to_f_item.at(adj);
                if (std::find(reference_faces->begin(), reference_faces->end(), right_f_item) == reference_faces->end()) {
                    reference_faces->push_back(right_f_item);
                    compute_surrounding_edges(right_f_item, surrounding_edges);
                    num_additional_faces += 1;
                    changed = true;
                }
                adj = adj->twin();
                right_f_item = m_adj_to_f_item.at(adj);
                if (std::find(reference_faces->begin(), reference_faces->end(), right_f_item) == reference_faces->end()) {
                    reference_faces->push_back(right_f_item);
                    compute_surrounding_edges(right_f_item, surrounding_edges);
                    num_additional_faces += 1;
                    changed = true;
                }
            }
        }
    }
    return num_additional_faces;
}

bool VisualGraph::compute_shellings_rec(int r, node u_last,
    vector<node>* curr_tested_shelling,
    vector<FaceItem*>* reference_faces, vector<edge>* surrounding_edges,
    vector<vector<node>>* shellings) const
{
    // suche rekursiv die nächsten Knoten in der r-Shell
    vector<node> outer_vertices;
    compute_outer_vertices(*reference_faces, u_last, *curr_tested_shelling, &outer_vertices);
    vector<edge> all_surrounding_edges = *surrounding_edges;
    bool any_shelling_found = false;
    for (node outer_vertex : outer_vertices) {
        curr_tested_shelling->push_back(outer_vertex);
        if (r == (int) curr_tested_shelling->size() + 1) {
            // Rekursionsende
            curr_tested_shelling->push_back(u_last);
            bool shelling_found = test_shelling_pairs(reference_faces->front(), *curr_tested_shelling);
            if (shelling_found) {
                shellings->push_back(*curr_tested_shelling);
                any_shelling_found = true;
            }
            curr_tested_shelling->pop_back();
            curr_tested_shelling->pop_back();
            continue;
        }
        int num_additional_faces = delete_vertex_for_shelling_tests(outer_vertex, reference_faces, &all_surrounding_edges);
        bool shelling_found = compute_shellings_rec(r, u_last, curr_tested_shelling,  reference_faces, &all_surrounding_edges, shellings);
        if (shelling_found) {
            any_shelling_found = true;
        }

        // mache Auswahlen für diesen Knoten rückgängig;
        curr_tested_shelling->pop_back();
        for (int i = 0; i < num_additional_faces; ++i) { reference_faces->pop_back(); }
        all_surrounding_edges = *surrounding_edges;
    }
    return any_shelling_found;
}

bool VisualGraph::compute_shellings(FaceItem* f_item, int r, node u_1, node u_r, vector<vector<node>>* shellings) {
    // Verfahren:
    // (1) suche rekursiv an F anliegende Knoten u_2, ..., u_(r-1),
    //		indem voriger Knoten indirekt gelöscht wird (merke dir gemergte Faces)
    // (2) teste bei gefundener Folge, ob Bedingung für Paare (u_1, u_(r-1)), (u_1, u_(r-2)), ...,
    //		(u_1, u_2), (u_2, u_(r-1)), ..., (u_(r-2), u_(r-1)) auch erfüllt ist	

    vector<FaceItem*> reference_faces;
    reference_faces.push_back(f_item);
    vector<edge> surrounding_edges;
    compute_surrounding_edges(f_item, &surrounding_edges);
    vector<node> curr_tested_shelling;
    curr_tested_shelling.push_back(u_1);
    delete_vertex_for_shelling_tests(u_1, &reference_faces, &surrounding_edges);
    shellings->clear();
    bool shelling_found = compute_shellings_rec(r, u_r, &curr_tested_shelling, &reference_faces, &surrounding_edges, shellings);
    return shelling_found;
}

bool VisualGraph::compute_removal_orders_rec(int r, node other_end_vertex,
    vector<node>* curr_tested_order,
    vector<FaceItem*>* reference_faces, vector<edge>* surrounding_edges,
    vector<vector<node>>* removal_orders) const
{
    // suche rekursiv die nächsten Knoten in der r-Ordnung
    vector<node> outer_vertices;
    compute_outer_vertices(*reference_faces, other_end_vertex, *curr_tested_order, &outer_vertices);
    vector<edge> all_surrounding_edges = *surrounding_edges;
    bool any_order_found = false;
    for (node outer_vertex : outer_vertices) {
        curr_tested_order->push_back(outer_vertex);
        if (r == (int) curr_tested_order->size() - 1) {
            // Rekursionsende
            removal_orders->push_back(*curr_tested_order);
            curr_tested_order->pop_back();
            any_order_found = true;
            continue;
        }

        // eigentliche rekursive Bestimmung
        int num_additional_faces = delete_vertex_for_shelling_tests(outer_vertex, reference_faces, &all_surrounding_edges);
        bool removal_order_found = compute_removal_orders_rec(r, other_end_vertex, curr_tested_order, reference_faces, &all_surrounding_edges, removal_orders);
        if (removal_order_found) {
            any_order_found = true;
        }

        // mache Auswahlen für diesen Knoten rückgängig;
        curr_tested_order->pop_back();
        for (int i = 0; i < num_additional_faces; ++i) { reference_faces->pop_back(); }
        all_surrounding_edges = *surrounding_edges;
    }

    return any_order_found;
}

bool VisualGraph::compute_bishellings_from_removal_orders(const vector<vector<node>>& first_removal_orders,
    const vector<vector<node>>& second_removal_orders, vector<vector<node>>* bishellings) const
{
    bool bishelling_found;
    bool any_bishelling_found = false;
    for (const auto& first_order : first_removal_orders) {
        for (const auto& second_order : second_removal_orders) {
            assert(first_order.size() == second_order.size());
            bishelling_found = true;
            // teste Disjunktheit von a_0,...,a_i und b_r-i,...,b_0
            auto b_r_i_iter = second_order.rbegin();
            for (node a_i : first_order) {
                if (std::find(b_r_i_iter, second_order.rend(), a_i) != second_order.rend()) {
                    bishelling_found = false;
                    break;
                }
                ++b_r_i_iter;
            }
            assert(!bishelling_found || b_r_i_iter == second_order.rend());
            if (bishelling_found) {
                any_bishelling_found = true;
                vector<node> bishelling = first_order;
                bishelling.insert(bishelling.end(), second_order.begin(), second_order.end());
                bishellings->emplace_back(bishelling);
            }
        }
    }
    return any_bishelling_found;
}

bool VisualGraph::compute_bishellings(FaceItem* f_item, int r, node a_0, node b_0, vector<vector<node>>* bishellings) {
    // Verfahren:
    // (1) suche rekursiv an F anliegende Knoten a_1, ..., a_r,
    //		indem voriger Knoten indirekt gelöscht wird (merke dir gemergte Faces)
    // (1) suche rekursiv an F anliegende Knoten b_1, ..., b_r,
    //		indem voriger Knoten indirekt gelöscht wird (merke dir gemergte Faces)
    // (2) teste paarweise für alle Sequenzen von a und b:
    //		für alle i=0..r: a_0,...,a_i und b_r-i,...,b_0 disjunkt

    vector<FaceItem*> reference_faces;
    reference_faces.push_back(f_item);
    vector<edge> surrounding_edges;
    compute_surrounding_edges(f_item, &surrounding_edges);
    delete_vertex_for_shelling_tests(a_0, &reference_faces, &surrounding_edges);
    vector<node> curr_tested_shelling;
    curr_tested_shelling.push_back(a_0);
    vector<vector<node>> first_removal_orders;
    bool shelling_found = compute_removal_orders_rec(r, b_0, &curr_tested_shelling, &reference_faces, &surrounding_edges, &first_removal_orders);
    if (!shelling_found) {
        return false;
    }
    reference_faces.clear();
    reference_faces.push_back(f_item);
    surrounding_edges.clear();
    compute_surrounding_edges(f_item, &surrounding_edges);
    delete_vertex_for_shelling_tests(b_0, &reference_faces, &surrounding_edges);
    curr_tested_shelling.clear();
    curr_tested_shelling.push_back(b_0);
    vector<vector<node>> second_removal_orders;
    shelling_found = compute_removal_orders_rec(r, a_0, &curr_tested_shelling, &reference_faces, &surrounding_edges, &second_removal_orders);
    if (!shelling_found) {
        return false;
    }
    bishellings->clear();
    shelling_found = compute_bishellings_from_removal_orders(first_removal_orders, second_removal_orders, bishellings);
    return shelling_found;
}

FaceItem* VisualGraph::face_item_for_adjacency(adjEntry adj) const {
    FaceItem* original_face = m_adj_to_f_item.at(adj);
    return original_face;
}

void VisualGraph::delete_face_item(FaceItem* f_item) {
    assert(f_item);
    adjEntry adj;
    forall_adjEdges(adj, f_item->first_adjacency(), {
        auto edge_found_iter = m_adj_to_f_item.find(adj);
        if (edge_found_iter != m_adj_to_f_item.end()) {
            m_adj_to_f_item.erase(edge_found_iter);
        }
    });
    if (f_item == m_external_face) {
        m_external_face = nullptr;
    }
    m_faces.erase(f_item);
    m_scene->removeItem(f_item);
    // FIXME: Heapkorruption, wenn f_item gelöscht wird; vorübergehender Fix: memory leak
    //delete f_item;
}

void VisualGraph::delete_edge_item(edge e) {
    EdgeItem* e_item = m_edge_to_e_item[e];
    assert(e_item);
    auto edge_found_iter = m_adj_to_f_item.find(e->adjSource());
    if (edge_found_iter != m_adj_to_f_item.end()) {
        m_adj_to_f_item.erase(edge_found_iter);
    }
    edge_found_iter = m_adj_to_f_item.find(e->adjTarget());
    if (edge_found_iter != m_adj_to_f_item.end()) {
        m_adj_to_f_item.erase(edge_found_iter);
    }
    m_edge_to_e_item.erase(e);
    m_edges.erase(e_item);
    m_scene->removeItem(e_item);
    delete e_item;
}

void VisualGraph::delete_vertex_item(node v, bool not_crossing) {
    VertexItem* v_item = m_node_to_v_item[v];
    assert(v_item);
    m_node_to_v_item.erase(v);
    if (not_crossing) {
        m_vertices.erase(v_item);
    }
    else {
        m_crossings.erase(v_item);
    }
    m_scene->removeItem(v_item);
    delete v_item;
}

// Hilfsfunktion: entfernt angegebene Adjazenzen aus der Menge.
void remove_from_set(std::set<adjEntry>* map, adjEntry adj1, adjEntry adj2) {
    auto adj = map->find(adj1);
    if (adj != map->end()) {
        map->erase(adj);
    }
    adj = map->find(adj2);
    if (adj != map->end()) {
        map->erase(adj);
    }
}

GraphChange VisualGraph::remove_edge(VertexItem* first_v_item, VertexItem* second_v_item) {
    node u_copy = first_v_item->node();
    node v_copy = second_v_item->node();
    if (u_copy->index() > v_copy->index()) {
        swap(u_copy, v_copy);
    }
    node u_orig = m_planrep->original(u_copy);
    node v_orig = m_planrep->original(v_copy);
    edge e_orig = m_graph->searchEdge(u_orig, v_orig);
    assert(e_orig != nullptr);

    bool ref_face_external = m_reference_face == m_external_face;
    auto ref_face_polygon = m_reference_face->polygon();
    auto ref_face_adjs = m_reference_face->adjacencies();

    vector<adjEntry> adjs_of_new_faces;
    set<adjEntry> other_direction;
    auto segments = m_planrep->chain(e_orig);
    map<adjEntry, DPolyline> lines;
    for (edge segment : segments) {
        FaceItem* left_face = m_adj_to_f_item[segment->adjSource()];
        FaceItem* right_face = m_adj_to_f_item[segment->adjTarget()];

        // merke Adjazenzen von zu erstellenden FaceItems
        adjEntry adj_of_new_face = segment->adjTarget()->cyclicSucc();
        node crossing = segment->target();
        if (m_planrep->isCrossingType(crossing)) {
            // Implementierungsdetail: sorge dafür, dass Adjazenz noch später existiert
            if (adj_of_new_face != crossing->firstAdj()
                && adj_of_new_face != crossing->firstAdj()->succ())
            {
                adj_of_new_face = adj_of_new_face->pred()->pred();
                other_direction.insert(adj_of_new_face);
            }
        }
        if (adj_of_new_face != segment->adjTarget()) {
            // es handelt sich nicht um die einzige Kante
            adjs_of_new_faces.push_back(adj_of_new_face);
        }

        // entferne alle internen Zuweisungen von und zu den zu entfernenden Items
        delete_face_item(left_face);
        if (left_face != right_face) {
            delete_face_item(right_face);
        }
        delete_edge_item(segment);
    }

    for (adjEntry segment_adj : adjs_of_new_faces) {
        // entferne die Kreuzung und die entsprechenden Items
        if (m_planrep->isCrossingType(segment_adj->theNode())) {
            edge left_segment = segment_adj->theEdge();
            edge right_segment = segment_adj->cyclicSucc()->cyclicSucc()->theEdge();
            if (other_direction.find(segment_adj) != other_direction.end()) {
                swap(left_segment, right_segment);
            }
            EdgeItem* right_segment_item = m_edge_to_e_item[right_segment];
            EdgeItem* left_segment_item = m_edge_to_e_item[left_segment];
            DPolyline merged_line;
            if (left_segment_item->edge()->target() == right_segment_item->edge()->source()) {
                merged_line = left_segment_item->all_points();
                merged_line.popBack();
                auto all_other_points = right_segment_item->all_points();
                all_other_points.popFront();
                for (auto pos : all_other_points) {
                    merged_line.pushBack(pos);
                }
            }
            else {
                merged_line = right_segment_item->all_points();
                merged_line.popBack();
                auto all_other_points = left_segment_item->all_points();
                all_other_points.popFront();
                for (auto pos : all_other_points) {
                    merged_line.pushBack(pos);
                }
            }
            lines[segment_adj] = merged_line;
            delete_edge_item(right_segment);
            delete_edge_item(left_segment);

            delete_vertex_item(segment_adj->theNode(), false);
            m_planrep->removeCrossing(segment_adj->theNode());
        }
    }

    // entferne die Kante vollständig
    assert(m_planrep->chain(e_orig).size() == 1);
    m_planrep->delEdge(m_planrep->copy(e_orig));
    m_graph->delEdge(e_orig);
    assert(m_planrep->representsCombEmbedding());

    // erstelle fehlende Kanten
    for (adjEntry adjacency : adjs_of_new_faces) {
        if (m_edge_to_e_item.find(adjacency->theEdge()) == m_edge_to_e_item.end()) {
            EdgeItem* e_item = new EdgeItem(adjacency->theEdge(), lines.at(adjacency), EdgeItem::thickness());
            m_edge_to_e_item[adjacency->theEdge()] = e_item;
            m_edges.insert(e_item);
            m_scene->addItem(e_item);
        }
    }

    // erstelle fehlende Flächen
    vector<adjEntry> twin_adjacencies;
    for (adjEntry adj : adjs_of_new_faces) {
        twin_adjacencies.push_back(adj->twin());
    }
    for (adjEntry adj : twin_adjacencies) {
        adjs_of_new_faces.push_back(adj);
    }
    if (u_copy->degree() == 0 || v_copy->degree() == 0) {
        // laufe alle Flächen ab, um Sonderfall zu berücksichtigen
        edge e;
        forall_edges(e, *m_planrep) {
            adjs_of_new_faces.push_back(e->adjSource());
            adjs_of_new_faces.push_back(e->adjTarget());
        }
    }
    vector<adjEntry> adjs_of_new_face;
    for (adjEntry adjacency : adjs_of_new_faces) {
        if (m_adj_to_f_item.find(adjacency) != m_adj_to_f_item.end()) {
            continue;
        }
        adjs_of_new_face.clear();
        adjEntry adj;
        forall_adjEdges(adj, adjacency, {
            adjs_of_new_face.push_back(adj);
        });
        QPolygonF polygon;
        for (adjEntry adj : adjs_of_new_face) {
            edge e = adj->theEdge();
            if (e->source() == adj->theNode()) {
                for (auto pos : m_edge_to_e_item[e]->all_points()) {
                    polygon.push_back(Point(pos));
                }
            }
            else {
                auto all_points = m_edge_to_e_item[e]->all_points();
                for (auto pos_iter = all_points.rbegin(); pos_iter != all_points.rend(); --pos_iter) {
                    auto pos = *pos_iter;
                    polygon.push_back(Point(pos));
                }
            }
            //polygon.pop_back();
        }
        FaceItem* f_item = new FaceItem(adjacency, false, polygon);
        for (adjEntry adj : adjs_of_new_face) {
            m_adj_to_f_item[adj] = f_item;
        }
        m_faces.insert(f_item);
        m_scene->addItem(f_item);
    }

    // suche äußeres Face
    bool covers_all;
    for (FaceItem* f_item : m_faces) {
        covers_all = true;
        for (FaceItem* other_f_item : m_faces) {
            if (f_item->polygon().intersected(other_f_item->polygon()).isEmpty()) {
                covers_all = false;
                break;
            }
        }
        if (covers_all) {
            f_item->set_external(true);
            m_external_face = f_item;
        }
    }
    assert(m_external_face->is_external());

    reset_overlapping_items();

    set_reference_face(ref_face_polygon, ref_face_external);

    assert_consistencies();
    GraphChange diff;
    diff.type = GraphChange::Type::EDGE_REMOVAL;
    diff.vertices = node_pair(u_copy->index(), v_copy->index());
    return diff;
}

/// Sucht eine Adjazenz aus f_from, die an Adjazenz aus f_to liegt, gibt letzere zurück.
adjEntry find_common_adjEntry(const std::set<adjEntry>& f_from,
    const std::set<adjEntry>& f_to)
{
    for (auto adj : f_from) {
        auto found = f_to.find(adj->twin());
        if (found != f_to.end()) {
            return adj->twin();
        }
    }
    // kann erreicht werden, falls es keine gemeinsame Kante gibt
    return nullptr;
}

/// Findet alle Adjazenz aus f_from, die an Adjazenzen aus f_to liegen, und gibt letzere zurück.
vector<adjEntry> find_all_common_adjacencies(const std::set<adjEntry>& f_from,
    const std::set<adjEntry>& f_to)
{
    vector<adjEntry> common_adjacencies;
    for (auto adj : f_from) {
        auto found = f_to.find(adj->twin());
        if (found != f_to.end()) {
            common_adjacencies.push_back(adj->twin());
        }
    }
    // kann erreicht werden, falls es keine gemeinsame Kante gibt
    return common_adjacencies;
}

bool VisualGraph::original_edge_exists(VertexItem* u_item, VertexItem* v_item) const {
    node u_orig = m_planrep->original(u_item->node());
    node v_orig = m_planrep->original(v_item->node());
    return m_graph->searchEdge(u_orig, v_orig) != nullptr;
}

adjEntry VisualGraph::crossed_adjacency(Point pos1, Point pos2, FaceItem* f1_item, FaceItem* f2_item) {
    // um die richtige Kante zu finden, muss erstmal bestimmt werden, in welchen Faces die Punkte liegen
    std::set<adjEntry> f1_adjacencies;
    std::set<adjEntry> f2_adjacencies;
    adjEntry adj;
    forall_adjEdges(adj, f1_item->first_adjacency(), {
        f1_adjacencies.insert(adj);
    });
    forall_adjEdges(adj, f2_item->first_adjacency(), {
        f2_adjacencies.insert(adj);
    });

    auto all_common_adjacencies = find_all_common_adjacencies(f1_adjacencies, f2_adjacencies);
    for (auto common_adjacency : all_common_adjacencies) {
        edge edge_between_faces = common_adjacency->theEdge();
        DPolyline line = m_edge_to_e_item[edge_between_faces]->all_points();

        // finde das geschnittene Segment und den Kreuzungspunkt
        auto pos_it = line.begin();
        Point prev_pos = *pos_it;
        for (++pos_it; pos_it.valid(); ++pos_it) {
            Point curr_pos = *pos_it;
            auto ret = crossing_position(pos1, pos2, prev_pos, curr_pos);
            if (0 < ret.first && ret.first < 1) {
                return common_adjacency;
            }
            prev_pos = curr_pos;
        }
    }

    return nullptr;
}

// Sammle die Knicke einer Kante in einer Map (beidseitig).
void add_bendings(edge e, DPolyline bendings, map<node_pair, pair<node, DPolyline>>* edge_positions) {
    // hole dir die Knickpunkte des zweiten Segments
    node v1 = e->source();
    node v2 = e->target();
    // speichere die Knickinformationen
    node_pair v_pair = {v1->index(), v2->index()};
    (*edge_positions)[v_pair] = { v1, bendings };
    v_pair = { v_pair.second, v_pair.first };
    bendings.reverse();
    (*edge_positions)[v_pair] = { v2, bendings };
}

// Hilfsfunktion: findet die Adjazenz in u, die zum Face gehört, ansonsten nullptr.
adjEntry adjacency_of_vertex_to_face(const std::map<FaceItem*, std::set<adjEntry>>& mapping, node u, FaceItem* face) {
    auto set = mapping.at(face);
    adjEntry adj;
    forall_adj(adj, u) {
        if (set.find(adj) != set.end()) {
            return adj;
        }
    }
    // nicht erreichbar
    return nullptr;
}

void VisualGraph::reset_overlapping_items() {
    for (auto item : m_faces) { m_scene->removeItem(item); }
    for (auto item : m_edges) { m_scene->removeItem(item); }
    for (auto item : m_vertices) { m_scene->removeItem(item); }
    for (auto item : m_crossings) { m_scene->removeItem(item); }
    for (auto item : m_faces) { m_scene->addItem(item); }
    for (auto item : m_edges) { m_scene->addItem(item); }
    for (auto item : m_vertices) { m_scene->addItem(item); }
    for (auto item : m_crossings) { m_scene->addItem(item); }
}

GraphChange VisualGraph::add_edge(node u_copy, node v_copy, const vector<FaceItem*>& chosen_faces,
    const vector<adjEntry>& crossed_adjacencies,
    const vector<vector<Point>>& bend_positions)
{
    assert(chosen_faces.size() == bend_positions.size());
    assert(crossed_adjacencies.size() == chosen_faces.size() - 1);
    bool ref_face_external = (m_reference_face == m_external_face);
    auto ref_face_polygon = m_reference_face->polygon();
    auto ref_face_adjs = m_reference_face->adjacencies();

    // merke alle zu einem Face gehörenden Adjazenzen
    std::map<FaceItem*, std::set<adjEntry>> face_to_adjs_map;
    for (auto f : m_faces) {
        adjEntry first = f->first_adjacency();
        adjEntry adj;
        forall_adjEdges (adj, first, {
            face_to_adjs_map[f].insert(adj);
        })
    }

    // bestimme die Adjazenzen, mit denen später die richtigen Faces identifiziert werden können
    vector<adj_id> graph_diff_adjacencies;
    graph_diff_adjacencies.push_back(chosen_faces.front()->first_adjacency()->index());
    for (adjEntry adj : crossed_adjacencies) {
        graph_diff_adjacencies.push_back(adj->index());
    }

    // füge neue Kante hinzu
    node u_orig = m_planrep->original(u_copy);
    node v_orig = m_planrep->original(v_copy);
    edge e_orig = m_graph->newEdge(u_orig, v_orig);

    // finde die Folge der Adjazenzen für die Einbettung der Kante
    adjEntry rot1_before = adjacency_of_vertex_to_face(face_to_adjs_map, u_copy, chosen_faces.front());
    adjEntry rot2_before = adjacency_of_vertex_to_face(face_to_adjs_map, v_copy, chosen_faces.back());
    SList<adjEntry> crossed_segments;
    for (auto adj : crossed_adjacencies) {
        crossed_segments.pushBack(adj);
    }

    // lösche alte FaceItems zu den geteilten Faces
    map<FaceItem*, bool> already_deleted;
    for (auto f_item : chosen_faces) {
        if (!already_deleted[f_item]) {
            delete_face_item(f_item);
            already_deleted[f_item] = true;
        }
    }

    // bette die neue Kante korrekt ein
    vector<node> right_nodes;
    map<node_pair, DPolyline> old_edge_bendings;
    for (adjEntry adj : crossed_segments) {
        right_nodes.push_back(adj->theNode());
        edge e = adj->theEdge();
        auto bendings = m_edge_to_e_item[e]->all_points();
        old_edge_bendings[{e->source()->index(), e->target()->index()}] = bendings;
        bendings.reverse();
        old_edge_bendings[{e->target()->index(), e->source()->index()}] = bendings;
        delete_edge_item(e);
    }
    m_planrep->insertEdgePath(e_orig, crossed_segments);
    auto new_segments = m_planrep->chain(e_orig);
    auto new_segments_iter = new_segments.begin();
    edge last_segment = *new_segments_iter;
    ++new_segments_iter;
    for (node right_node : right_nodes) {
        // stelle sicher, dass die Adjazenzen eine lokal korrekte Rotation darstellen
        node last_node = last_segment->source();
        edge new_segment = *new_segments_iter;
        node crossing_node = new_segment->source();
        assert(m_planrep->isCrossingType(crossing_node));
        adjEntry first_adj = crossing_node->firstAdj();
        adjEntry second_adj = first_adj->succ();
        adjEntry third_adj = second_adj->succ();
        adjEntry fourth_adj = third_adj->succ();
        if (!(first_adj->theEdge() == last_segment && first_adj->twinNode() == last_node)) {
            if (second_adj->theEdge() == last_segment && second_adj->twinNode() == last_node) {
                m_planrep->moveAdjBefore(second_adj, first_adj);
                std::swap(second_adj, first_adj);
            }
            else if (third_adj->theEdge() == last_segment && third_adj->twinNode() == last_node) {
                m_planrep->moveAdjBefore(third_adj, first_adj);
                std::swap(third_adj, second_adj);
                std::swap(second_adj, first_adj);
            }
            else {
                assert(fourth_adj->theEdge() == last_segment && fourth_adj->twinNode() == last_node);
                m_planrep->moveAdjBefore(fourth_adj, first_adj);
                std::swap(fourth_adj, third_adj);
                std::swap(third_adj, second_adj);
                std::swap(second_adj, first_adj);
            }
        }
        assert(e_orig == m_planrep->original(first_adj->theEdge()));
        if (m_planrep->original(first_adj->theEdge()) != m_planrep->original(third_adj->theEdge())) {
            if (m_planrep->original(first_adj->theEdge()) == m_planrep->original(second_adj->theEdge())) {
                m_planrep->moveAdjAfter(second_adj, third_adj);
                std::swap(second_adj, third_adj);
            }
            else {
                m_planrep->moveAdjBefore(fourth_adj, third_adj);
                std::swap(fourth_adj, third_adj);
            }
        }
        assert(last_segment == first_adj->theEdge());
        assert(new_segment == third_adj->theEdge());
        // und jetzt soll die Rotation global korrekt werden
        if (right_node != fourth_adj->twinNode()) {
            // kehre an der Kreuzung falsch eingehende Kante um
            m_planrep->moveAdjBefore(fourth_adj, third_adj);
            m_planrep->moveAdjAfter(second_adj, third_adj);
            std::swap(second_adj, fourth_adj);
            assert(third_adj == second_adj->succ());
            assert(third_adj->succ() == fourth_adj);
        }
        assert(m_planrep->isCrossingType(crossing_node));
        assert(m_planrep->original(first_adj->theEdge()) == m_planrep->original(third_adj->theEdge()));
        assert(m_planrep->original(second_adj->theEdge()) == m_planrep->original(fourth_adj->theEdge()));
        assert(fourth_adj->cyclicSucc() == first_adj);
        last_segment = new_segment;
        ++new_segments_iter;
    }
    adjEntry first_adj = m_planrep->chain(e_orig).front()->adjSource();
    adjEntry last_adj = m_planrep->chain(e_orig).back()->adjTarget();
    if (rot1_before) {
        m_planrep->moveAdjAfter(first_adj, rot1_before);
    }
    if (rot2_before) {
        m_planrep->moveAdjAfter(last_adj, rot2_before);
    }
    assert(m_planrep->representsCombEmbedding());

    // ordne die Knicke nach tatsächlicher Richtung der Kopie
    List<vector<Point>> ordered_bend_positions;
    node source_copy = u_copy;
    node target_copy = v_copy;
    if (u_copy == m_planrep->copy(e_orig)->source()) {
        for (auto positions : bend_positions) {
            ordered_bend_positions.pushBack(positions);
        }
    }
    else {
        swap(source_copy, target_copy);
        for (auto positions : bend_positions) {
            std::reverse(positions.begin(), positions.end());
            ordered_bend_positions.pushFront(positions);
        }
    }
        
    // erstelle neue Visualisierungselemente
    VertexItem* u_item = m_node_to_v_item[source_copy];
    VertexItem* v_item = m_node_to_v_item[target_copy];
    auto new_edge_segments = m_planrep->chain(e_orig);
    VertexItem* w_before_item = u_item;
    auto new_edge_segments_iter = new_edge_segments.begin();
    std::set<adjEntry> adjs_of_new_faces;
    VertexItem* w_item;
    for (ListIterator<vector<Point>> ordered_bend_positions_iter = ordered_bend_positions.begin();
        new_edge_segments_iter.valid();
        ++new_edge_segments_iter, ++ordered_bend_positions_iter)
    {
        edge segment = *new_edge_segments_iter;
        vector<Point> segment_bendings = *ordered_bend_positions_iter;
        Point last_bend_in_segment = segment_bendings.back();

        // erstelle neues Item für Kreuzung
        node w = segment->target();
        if (w != target_copy) {
            // für die Kreuzungsposition
            Point current_bend = last_bend_in_segment;
            auto next_bend_positions_it = ordered_bend_positions_iter;
            
            Point next_bend = (*(++next_bend_positions_it)).front();
            // wähle Adjazenzen der an dieser Stelle geschnittenen Kante
            auto adj1_crossed_edge = segment->adjTarget()->cyclicSucc();
            auto adj2_crossed_edge = adj1_crossed_edge->cyclicSucc()->cyclicSucc();

            // bestimme den Kantenverlauf zwischen der Endpunkte der geschnittenen Kante
            node u_crossed = adj1_crossed_edge->twinNode();
            node v_crossed = adj2_crossed_edge->twinNode();
            auto crossed_edge_positions = old_edge_bendings.at({u_crossed->index(), v_crossed->index()});

            // finde das geschnittene Segment und den Kreuzungspunkt
            Point prev_pos = {-1000000, -1000000};
            Point pos_cross = {-1, -1};
            bool crossed_segment_found = false;
            for (auto curr_pos : crossed_edge_positions) {
                if (prev_pos != Point {-1000000, -1000000}) {
                    auto ret = crossing_position(current_bend, next_bend, prev_pos, curr_pos);
                    if (0 < ret.first && ret.first < 1) {
                        pos_cross = ret.second;
                        crossed_segment_found = true;
                        prev_pos = curr_pos;
                        break;
                    }
                }
                prev_pos = curr_pos;
            }
            assert(crossed_segment_found);

            // setze korrekten Verlauf für geschnittene Kante (bzw. entstandene 2 Segmente)
            // Merke dir die Linien zunächst ausgehend von u1 und u2
            DPolyline first_segment_positions;
            DPolyline second_segment_positions;
            auto pos_iter = crossed_edge_positions.begin();
            while (*pos_iter != prev_pos) {
                first_segment_positions.pushBack(*pos_iter);
                ++pos_iter;
            }
            first_segment_positions.pushBack(pos_cross);
            second_segment_positions.pushBack(pos_cross);
            while (pos_iter.valid()) {
                second_segment_positions.pushBack(*pos_iter);
                ++pos_iter;
            }
            second_segment_positions.reverse();

            // zwei neue EdgeItems für die gekreuzte Kante
            edge e_u1 = adj1_crossed_edge->theEdge();
            assert(!m_edge_to_e_item[e_u1]);
            if (u_crossed != e_u1->source()) {
                first_segment_positions.reverse();
            }
            EdgeItem* e_item1 = new EdgeItem(e_u1, first_segment_positions, m_edge_width);
            edge e_u2 = adj2_crossed_edge->theEdge();
            assert(!m_edge_to_e_item[e_u2]);
            if (v_crossed != e_u2->source()) {
                second_segment_positions.reverse();
            }
            EdgeItem* e_item2 = new EdgeItem(e_u2, second_segment_positions, m_edge_width);

            m_scene->addItem(e_item1);
            m_scene->addItem(e_item2);
            m_edges.insert(e_item1);
            m_edges.insert(e_item2);
            m_edge_to_e_item[e_u1] = e_item1;
            m_edge_to_e_item[e_u2] = e_item2;
            m_scene->addItem(e_item1);
            m_scene->addItem(e_item2);

            // erstelle neues Kreuzungsknotenitem;
            double width = m_vertex_width;
            w_item = new VertexItem(w, QString::number(w->index()), false, pos_cross.x, pos_cross.y, width);
            m_crossings.insert(w_item);
            m_node_to_v_item[w] = w_item;
            m_scene->addItem(w_item);
        }
        else {
            w_item = v_item;
        }

        // erstelle neues Item für aktuelles Kantensegment
        DPolyline line;
        line.pushBack(DPoint(w_before_item->x_pos(), w_before_item->y_pos()));
        for (auto pos : segment_bendings) {
            line.pushBack(pos);
        }
        line.pushBack(DPoint(w_item->x_pos(), w_item->y_pos()));
        EdgeItem* e_item = new EdgeItem(segment, line, m_edge_width);

        m_edges.insert(e_item);
        m_edge_to_e_item[segment] = e_item;
        m_scene->addItem(e_item);

        adjEntry first1 = segment->adjSource();
        adjEntry first2 = first1->twin();
        adjs_of_new_faces.insert(first1);
        adjs_of_new_faces.insert(first2);

        w_before_item = w_item;
    }

    // erstelle die FaceItems
    for (adjEntry new_adj : adjs_of_new_faces) {
        if (m_adj_to_f_item[new_adj]) {
            continue;
        }
        DPolyline line = positions_of_face_vertices(new_adj);
        QPolygonF polygon;
        for (auto pos: line) {
            polygon.append(Point(pos));
        }
        assert(polygon.isClosed());
        FaceItem* f_item = new FaceItem(new_adj, false, polygon);
        
        m_faces.insert(f_item);
        adjEntry adj;
        forall_adjEdges(adj, new_adj, {
            m_adj_to_f_item[adj] = f_item;
        });
        m_scene->addItem(f_item);
    }

    // bestimme äußeres Face
    for (FaceItem* tested_face : m_faces) {
        bool f1_extern = true;
        // das äußere Polygon schneidet alle inneren Polygone
        for (FaceItem* other_face : m_faces) {
            QPolygonF intersected = tested_face->polygon().intersected(other_face->polygon());
            if (tested_face != other_face && intersected.size() == 0) {
                f1_extern = false;
                break;
            }
        }
        if (f1_extern) {
            m_external_face = tested_face;
            m_external_face->set_external(true);
            m_external_face->setVisible(false);
            m_external_face->setEnabled(false);
            break;
        }
    }
    assert(m_external_face);

    reset_overlapping_items();
    set_reference_face(ref_face_polygon, ref_face_external);
    
    assert_consistencies();
    node_pair graph_diff_edge = { u_copy->index(), v_copy->index() };
    GraphChange diff;
    diff.type = GraphChange::Type::EDGE_ADDITION;
    diff.vertices = graph_diff_edge;
    diff.adjacencies = std::move(graph_diff_adjacencies);
    diff.bend_positions = bend_positions;
    return diff;
}

int VisualGraph::number_connected_components() {
    NodeArray<int> component_ids(*m_graph);
    int num_connected_components = ogdf::connectedComponents(*m_graph, component_ids);
    return num_connected_components;
}

vector<VertexItem*> VisualGraph::adjacent_v_items(VertexItem* v_item) {
    vector<VertexItem*> adjacent_items;
    node v_orig = m_planrep->original(v_item->node());
    adjEntry curr_adj;
    forall_adj(curr_adj, v_orig) {
        node other_orig = curr_adj->twinNode();
        node other_copy = m_planrep->copy(other_orig);
        auto other_v_item = m_node_to_v_item[other_copy];
        adjacent_items.push_back(other_v_item);
    }
    return adjacent_items;
}

DPolyline VisualGraph::positions_of_face_vertices(adjEntry first) {
    DPolyline line;
    edge e = first->theEdge();
    EdgeItem* e_item = m_edge_to_e_item[e];
    DPolyline edge_line = e_item->all_points();
    if (e->source() != first->theNode()) {
        edge_line.reverse();
    }
    edge_line.popBack();
    for (auto pos : edge_line) {
        line.pushBack(pos);
    }
    for (adjEntry next = first->faceCycleSucc(); next != first; next = next->faceCycleSucc()) {
        e = next->theEdge();
        e_item = m_edge_to_e_item[e];
        edge_line = e_item->all_points();
        if (e->source() != next->theNode()) {
            edge_line.reverse();
        }
        edge_line.popBack();
        for (auto pos : edge_line) {
            line.pushBack(pos);
        }
    }
    auto point = line.front();
    line.pushBack(point);
    return line;
}

bool VisualGraph::set_reference_face(const QPolygonF& ref_face_polygon, bool is_ref_face_external)
{
    m_reference_face->set_reference(false);
    FaceItem* ref_face_candidate = m_external_face;
    bool unique_face_is_found = true;
    if (!is_ref_face_external) {
        for (auto f_item : face_items()) {
            auto ref_polygon = f_item->polygon();
            auto intersection = ref_polygon.intersected(ref_face_polygon);
            if (!intersection.empty()) {
                if (f_item->is_external()) {
                    if (!ref_face_candidate) {
                        ref_face_candidate = f_item;
                    }
                }
                else if (ref_face_candidate && !ref_face_candidate->is_external()) {
                    // Konflikt: mind. zwei Kandidaten für Referenzfläche
                    ref_face_candidate = external_face_item();
                    unique_face_is_found = false;
                    break;
                }
                else {
                    //debug_color(f_item, 1);
                    ref_face_candidate = f_item;
                }
            }
        }
    }
    assert(ref_face_candidate);
    ref_face_candidate->set_reference(true);
    m_reference_face = ref_face_candidate;
    return unique_face_is_found;
}

void VisualGraph::set_reference_face(FaceItem* ref_face)
{
    m_reference_face->set_reference(false);
    ref_face->set_reference(true);
    m_reference_face = ref_face;
}

bool VisualGraph::are_faces_adjacent(FaceItem* f1, FaceItem* f2) const {
    adjEntry first_adj = f1->first_adjacency();
    adjEntry adj;
    forall_adjEdges(adj, first_adj, {
        if (m_adj_to_f_item.at(adj->twin()) == f2) {
            return true;
        }
    })
    return false;
}

bool VisualGraph::is_face_adjacent_to_vertex(VertexItem* u_item, FaceItem* f_item) const {
    Point position(u_item->x_pos(), u_item->y_pos());
    if (f_item->contains(position)
        || (f_item->is_external() && !f_item->contains(position))) {
        // Knoten liegt im Face und keine ausgehenden Kanten
        return true;
    }
    node u = u_item->node();
    adjEntry adj;
    forall_adj(adj, u) {
        if (m_adj_to_f_item.at(adj) == f_item || m_adj_to_f_item.at(adj->twin()) == f_item) {
            return true;
        }
    }
    return false;
}

void VisualGraph::compute_initial_layout(QGraphicsScene* scene, const GraphAttributes& attrs, adjEntry external_adj) {
    // Layout wird berechnet
    create_faces(scene, attrs, external_adj);
    create_edges(scene, attrs);
    create_vertices(scene, attrs);
    create_crossings(scene, attrs);
    assert_consistencies();
}

bool VisualGraph::is_drawing_good() const {
    // sehr ähnlich zu "determine_simple_drawing_violations"
    // 1) Kanten kreuzen sich nicht selbst.
    // (kann einfach vermieden werden, wird nicht überprüft (Test intern auch noch nicht implementiert))
    edge e;
    ogdf::List<edge> segments;
    EdgeArray<EdgeArray<int>> num_edge_crossings(*m_graph, EdgeArray<int>(*m_graph, 0));
    forall_edges(e, *m_graph) {
        segments = m_planrep->chain(e);
        for (const auto& segment : segments) {
            auto crossing = segment->target();
            if (!m_planrep->isCrossingType(crossing)) {
                break;
            }
            adjEntry crossed_segment = segment->adjTarget()->cyclicSucc();
            edge crossed_edge = m_planrep->original(crossed_segment->theEdge());

            // 2) Abhängige Kanten kreuzen sich nicht gegenseitig.
            if (e->source() == crossed_edge->source() || e->source() == crossed_edge->target()
                || e->target() == crossed_edge->source() || e->target() == crossed_edge->target())
            {
                return false;
            }

            // 3) 2 Kanten kreuzen sich maximal einfach.
            int num_crossings = num_edge_crossings[e][crossed_edge];
            if (num_crossings <= 2) {
                num_edge_crossings[e][crossed_edge] = num_crossings + 1;
                num_edge_crossings[crossed_edge][e] = num_crossings + 1;
            }
            else {
                return false;
            }
        }
    }
    return true;
}

vector<string> VisualGraph::determine_good_drawing_violations () const {
    // sehr ähnlich zu "is_drawing_simple"
    // 1) Kanten kreuzen sich nicht selbst.
    // (kann einfach vermieden werden, wird nicht überprüft)
    vector<string> violations;
    set<pair<edge, edge>> violations2;
    set<pair<edge, edge>> violations3;
    edge e;
    ogdf::List<edge> segments;
    EdgeArray<EdgeArray<int>> num_edge_crossings(*m_graph, EdgeArray<int>(*m_graph, 0));
    forall_edges(e, *m_graph) {
        segments = m_planrep->chain(e);
        for (const auto& segment : segments) {
            auto crossing = segment->target();
            if (!m_planrep->isCrossingType(crossing)) {
                break;
            }
            adjEntry crossed_segment = segment->adjTarget()->cyclicSucc();
            edge crossed_edge = m_planrep->original(crossed_segment->theEdge());

            // 2) Abhängige Kanten kreuzen sich nicht gegenseitig.
            if (e->source() == crossed_edge->source() || e->source() == crossed_edge->target()
                || e->target() == crossed_edge->source() || e->target() == crossed_edge->target())
            {
                edge e_min = min(e, crossed_edge);
                edge e_max = max(e, crossed_edge);
                violations2.insert({ e_min, e_max });
            }

            // 3) 2 Kanten kreuzen sich maximal einfach.
            int num_crossings = num_edge_crossings[e][crossed_edge];
            if (num_crossings <= 2) {
                num_edge_crossings[e][crossed_edge] = num_crossings + 1;
                num_edge_crossings[crossed_edge][e] = num_crossings + 1;
            }
            else {
                edge e_min = min(e, crossed_edge);
                edge e_max = max(e, crossed_edge);
                violations3.insert({ e_min, e_max });
            }
        }
    }
    for (auto& violation : violations2) {
        edge e1 = violation.first;
        edge e2 = violation.second;
        stringstream violation_string;
        violation_string << "Kreuzende (" << e1->source()->index() << "," << e1->target()->index() << ") ";
        violation_string << "und (" << e2->source()->index() << "," << e2->target()->index() << ") ";
        violation_string << "sind zum selben Knoten inzident.";
        violations.push_back(violation_string.str());
    }
    for (auto& violation : violations3) {
        edge e1 = violation.first;
        edge e2 = violation.second;
        stringstream violation_string;
        violation_string << "Kreuzende (" << e1->source()->index() << "," << e1->target()->index() << ") ";
        violation_string << "und (" << e2->source()->index() << "," << e2->target()->index() << ") ";
        violation_string << "kreuzen sich mehr als einmal.";
        violations.push_back(violation_string.str());
    }

    return violations;
}

void VisualGraph::compute_initial_layout(QGraphicsScene* scene, const PlanarizedDrawing& drawing, adjEntry external_adj) {
    GraphAttributes attrs = GraphAttributes(*m_planrep, 
        GraphAttributes::nodeGraphics | GraphAttributes::nodeType |
        GraphAttributes::nodeStyle |
        GraphAttributes::edgeGraphics |
        GraphAttributes::edgeStyle | GraphAttributes::edgeType);

    // Positionen der Knoten und Kanten werden berechnet
    node v;
    forall_nodes(v, *m_planrep) {
        Point pos = drawing.position(v->index());
        attrs.x(v) = pos.x;
        attrs.y(v) = pos.y;
    }
    edge e;
    forall_edges(e, *m_planrep) {
        node u_copy = e->adjSource()->theNode();
        node v_copy = e->adjTarget()->theNode();

        attrs.bends(e).pushBack(DPoint(attrs.x(u_copy), attrs.y(u_copy)));
        auto bends = drawing.bendings(e);
        for (Point pos : bends) {
            attrs.bends(e).pushBack(DPoint(pos.x, pos.y));
        }
        attrs.bends(e).pushBack(DPoint(attrs.x(v_copy), attrs.y(v_copy)));
    }

    compute_initial_layout(scene, attrs, external_adj);
}

void VisualGraph::create_faces(QGraphicsScene* scene, const GraphAttributes &attrs,
    adjEntry external_adj)
{
    std::vector<face> real_faces;
    const PlanRep &planrep_ = *m_planrep;
    const ConstCombinatorialEmbedding embedding(planrep_);
    search_for_faces(embedding, &real_faces);
    for (face f : real_faces) {
        bool is_external = false;
        QVector<QPointF> points;
        adjEntry first = f->firstAdj();
        adjEntry current_adj;
        forall_adjEdges(current_adj, first, {
            if (current_adj == external_adj) {
                is_external = true;
            }
            node u = current_adj->theNode();
            // behandle alle Kantenknicke
            DPolyline bends = attrs.bends(current_adj->theEdge());
            if (current_adj->theEdge()->source() == u) {
                for (auto bend : bends) {
                    points.push_back(Point(bend));
                }
                points.pop_back();
            }
            else {
                // andere Richtung der Kante
                QVector<QPointF> points_temp;
                for (auto bend : bends) {
                    points_temp.push_front(Point(bend));
                }
                for (auto point : points_temp) {
                    points.push_back(point);
                }
                points.pop_back();
            }
        });
        auto point = points.first();
        points.push_back(point);

        // erstelle Polygon in der Szene
        FaceItem* item = new FaceItem(first, is_external, QPolygonF(points));
        if (is_external) {
            m_external_face = item;
            item->set_reference(true);
            m_reference_face = item;
        }
        assert(item->polygon().isClosed());
        adjEntry adj;
        forall_adjEdges(adj, first, {
            m_adj_to_f_item[adj] = item;
        })
        m_faces.insert(item);
        scene->addItem(item);
    }
    assert(m_external_face);
    assert(m_reference_face);
}

void VisualGraph::create_edges(QGraphicsScene* scene, const GraphAttributes& attrs) {
    edge e;
    forall_edges(e, *m_planrep) {
        DPolyline line = attrs.bends(e);

        EdgeItem* item = new EdgeItem(e, line, m_edge_width);

        m_edges.insert(item);
        m_edge_to_e_item[e] = item;
        scene->addItem(item);
    }
}

void VisualGraph::set_vertex_width(double new_thickness) {
    m_vertex_width = new_thickness;
    for (VertexItem* item : m_vertices) {
        item->set_width(m_vertex_width);
    }
    for (VertexItem* item : m_crossings) {
        item->set_width(m_vertex_width);
    }
}

void VisualGraph::set_edge_width(double new_thickness) {
    m_edge_width = new_thickness;
    for (EdgeItem* item : m_edges) {
        item->set_width(m_edge_width);
    }
}

void VisualGraph::create_vertices(QGraphicsScene* scene, const GraphAttributes& attrs) {
    node u_orig;
    forall_nodes(u_orig, *m_graph) {
        node u_copy = m_planrep->copy(u_orig);
        qreal x = attrs.x(u_copy);
        qreal y = attrs.y(u_copy);

        VertexItem* item = new VertexItem(u_copy, QString::number(u_orig->index()), true, x, y, m_vertex_width); 
        m_vertices.insert(item);
        m_node_to_v_item[u_copy] = item;
        scene->addItem(item);
    }
}

void VisualGraph::create_crossings(QGraphicsScene* scene, const GraphAttributes& attrs) {
    node u;
    forall_nodes(u, *m_planrep) {
        if (m_planrep->original(u) != nullptr) {
            continue;
        }

        qreal x = attrs.x(u);
        qreal y = attrs.y(u);

        VertexItem* item = new VertexItem(u, QString::number(u->index()), false, x, y, m_vertex_width); 
        m_crossings.insert(item);
        m_node_to_v_item[u] = item;
        scene->addItem(item);
    }
}

void VisualGraph::mark_incident_edges(const VertexItem& v_item) {
    node orig_v = m_planrep->original(v_item.node());
    edge incident_edge;
    forall_adj_edges(incident_edge, orig_v) {
        for (auto segment : m_planrep->chain(incident_edge)) {
            auto e_item = m_edge_to_e_item[segment];
            e_item->set_pen(EdgeItem::incident_edge_pen());
        }
    }
}


UnprocessedDrawing VisualGraph::to_crossingless_drawing() const {
    int num_vertices = m_graph->numberOfNodes();
    UnprocessedDrawing drawing(num_vertices);

    // sorge dafür, dass alle Knoten lückenlos aufsteigend nummeriert sind
    map<node, node_id> compacted;
    node v_orig;
    forall_nodes(v_orig, *m_graph) {
        node v_copy = m_planrep->copy(v_orig);
        int compacted_id = compacted[v_orig] = compacted.size();
        drawing.set_position(compacted_id, 
            { m_node_to_v_item.at(v_copy)->x_pos(), m_node_to_v_item.at(v_copy)->y_pos() });
    }
    edge e_orig;
    forall_edges(e_orig, *m_graph) {
        node u_orig = e_orig->source();
        node v_orig = e_orig->target();
        node u_copy = m_planrep->copy(u_orig);
        node v_copy = m_planrep->copy(v_orig);
        node_id compacted_u_id = compacted[u_orig];
        node_id compacted_v_id = compacted[v_orig];
        drawing.add_edge({ compacted_u_id, compacted_v_id });
        auto edge_path = m_planrep->chain(e_orig);
        bool reverse_directions =
            ((compacted_u_id > compacted_v_id && u_copy->index() < v_copy->index())
            || (compacted_u_id < compacted_v_id && u_copy->index() > v_copy->index()));
        deque<Point> bendings;
        for (edge segment : edge_path) {
            auto segment_item = m_edge_to_e_item.at(segment);
            auto polyline = segment_item->all_points();
            polyline.popFront();
            polyline.popBack();
            if (!reverse_directions) {
                for (auto point : polyline) {
                    bendings.push_back(point);
                }
            }
            else {
                for (auto point : polyline) {
                    bendings.push_front(point);
                }
            }
        }
        for (auto bend : bendings) {
            drawing.add_bending({ compacted_u_id, compacted_v_id },
                bend);
        }
    }
    return drawing;
}


/// DF-Suche aller Pfade von u nach v bestimmter Länge.
/// distances_from_sink: Hilfsarray, um gewisse Knoten aufgrund zu weiter Entfernung direkt auszuschließen
/// current_path: Knoten vom Ursprungsknoten bis hierhin.
/// visited: Hilfsarray, um bereits auf diesem Pfad besuchte Knoten zu markieren
/// paths: Ausgabeliste für gefundene Pfade
void search_path_of_length(node u, node v, int length, const NodeArray<double>& distances_from_sink, vector<node> current_path, NodeArray<bool>* visited, vector<vector<node>>* paths) {
    (*visited)[u] = true;
    current_path.push_back(u);
    if (u == v) {
        // Weg der gewünschten Länge gefunden
        paths->push_back(current_path);
    }
    else if (length == 0) {
        // Weg hat nicht zum Ziel geführt
    }
    else {
        adjEntry adj;
        forall_adj(adj, u) {
            node u_next = adj->twinNode();
            if ((*visited)[u_next] || distances_from_sink[u_next] > length - 1) {
                continue;
            }
            search_path_of_length(u_next, v, length - 1, distances_from_sink, current_path, visited, paths);
        }
    }
    (*visited)[u] = false;
}

std::vector<EdgeItem*> VisualGraph::edge_path(VertexItem* u_item, VertexItem* v_item) const {
    node u_copy = u_item->node();
    node v_copy = v_item->node();
    node u_orig = m_planrep->original(u_copy);
    node v_orig = m_planrep->original(v_copy);
    edge e_orig = m_graph->searchEdge(u_orig, v_orig);
    auto segments = m_planrep->chain(e_orig);
    vector<EdgeItem*> segment_items;
    for (auto seg : segments) {
        EdgeItem* e_item = m_edge_to_e_item.at(seg);
        segment_items.push_back(e_item);
    }
    return segment_items;
}

vector<vector<FaceItem*>> VisualGraph::compute_alternative_courses(
    VertexItem* u_item, VertexItem* v_item, bool only_extended_flips) const
{
    node u_copy = u_item->node();
    node v_copy = v_item->node();
    node u_orig = m_planrep->original(u_copy);
    node v_orig = m_planrep->original(v_copy);
    edge e_orig = m_graph->searchEdge(u_orig, v_orig);
    if (e_orig->source() != u_orig) {
        swap(u_copy, v_copy);
        swap(u_orig, v_orig);
    }

    List<edge> segments = m_planrep->chain(e_orig);

    // unmittelbar links und rechts befindliche Facefolgen sind uninteressant
    CombinatorialEmbedding embedding(*m_planrep);
    ogdf::DualGraph dual_embedding(embedding);
    vector<FaceItem*> left_faces;
    vector<FaceItem*> right_faces;
    for (edge segment : segments) {
        left_faces.push_back(face_item_for_adjacency(segment->adjSource()));
        right_faces.push_back(face_item_for_adjacency(segment->adjTarget()));
    }

    // modifiziert den dualen Graphen so, dass Wege über die Faces von u nach v gesucht werden können
    GraphCopy modified_graph(dual_embedding);
    face u_face = dual_embedding.dualFace(u_copy);
    node u_modified = modified_graph.newNode();
    adjEntry adj, first = u_face->firstAdj();
    forall_adjEdges(adj, first, {
        node face_vertex = adj->theNode();
        modified_graph.newEdge(u_modified, modified_graph.copy(face_vertex));
    });
    face v_face = dual_embedding.dualFace(v_copy);
    node v_modified = modified_graph.newNode();
    first = v_face->firstAdj();
    forall_adjEdges(adj, first, {
        node face_vertex = adj->theNode();
        modified_graph.newEdge(modified_graph.copy(face_vertex), v_modified);
    });

    // berechne alle Wege von u nach v
    vector<vector<node>> face_paths;
    vector<node> current_path;
    int num_crossings = segments.size() - 1;
    current_path.reserve(num_crossings + 2);
    NodeArray<bool> visited(modified_graph, false);
    NodeArray<double> distances_from_sink(modified_graph);
    EdgeArray<double> edge_costs(modified_graph, 1.);
    ogdf::dijkstra_SPSS(v_modified, modified_graph, distances_from_sink, edge_costs);
    search_path_of_length(u_modified, v_modified, num_crossings + 2,
        distances_from_sink, current_path, &visited, &face_paths);

    // filter die zum aktuellen Kantenverlauf benachbarte Facefolgen aus
    // berücksichtige, ob nur erweiterte Kantenflips erwünscht sind
    vector<vector<FaceItem*>> face_item_paths;
    vector<FaceItem*> face_item_path;
    bool is_different_from_sides;
    for (auto path : face_paths) {
        is_different_from_sides = false;
        // kürzere Wege sind auf jeden Fall erwünscht
        if (path.size()-2 != left_faces.size()) {
            is_different_from_sides = true;
        }
        face_item_path.clear();
        if (only_extended_flips) {
            // merke, falls aktueller Pfad ein erweiterter Kantenflip ist (siehe Notizen)
            node face_vertex = path[1];
            face primal_face = dual_embedding.primalFace(modified_graph.original(face_vertex));
            FaceItem* f_item = face_item_for_adjacency(primal_face->firstAdj());
            if (f_item != left_faces[0] && f_item != right_faces[0]) {
                is_different_from_sides = true;
            }
        }
        // schmeiße die Dummyknoten am Anfang und Ende raus
        for (size_t i = 1; i < path.size() - 1; ++i) {
            node face_vertex = path[i];
            face primal_face = dual_embedding.primalFace(modified_graph.original(face_vertex));
            FaceItem* f_item = face_item_for_adjacency(primal_face->firstAdj());
            if (!only_extended_flips) {
                // merke, falls aktueller Pfad nicht vollkommen anderen Pfad deckt
                if (f_item != left_faces[i - 1] && f_item != right_faces[i - 1]) {
                    is_different_from_sides = true;
                }
            }
            face_item_path.push_back(f_item);
        }

        if (is_different_from_sides) {
            face_item_paths.push_back(face_item_path);
        }
    }
    return face_item_paths;
}

void VisualGraph::assert_consistencies() {
#ifdef CONSISTENCE
    // Kanten und Adjazenzen
    for (EdgeItem* e_item : m_edges) {
        edge e = e_item->edge();
        assert(m_edge_to_e_item[e] == e_item);
    }
    assert(m_edges.size() == m_planrep->numberOfEdges());
    assert(m_edges.size() == m_edge_to_e_item.size());
    int num_adjacencies = 0;
    for (FaceItem* f_item : m_faces) {
        adjEntry adj;
        forall_adjEdges(adj, f_item->first_adjacency(), {
            num_adjacencies += 1;
            assert(m_adj_to_f_item[adj] == f_item);
        })
    }
    assert(num_adjacencies == 2 * m_planrep->numberOfEdges());
    assert(num_adjacencies == m_adj_to_f_item.size());
    // echte und Kreuzungsknoten
    for (VertexItem* v_item : m_vertices) {
        node v = v_item->node();
        assert(m_node_to_v_item[v] == v_item);
        assert(!m_planrep->isCrossingType(v));
    }
    assert(m_vertices.size() == m_graph->numberOfNodes());
    for (VertexItem* v_item : m_crossings) {
        node v = v_item->node();
        assert(m_node_to_v_item[v] == v_item);
        assert(m_planrep->isCrossingType(v));
    }
    assert(m_crossings.size() + m_vertices.size() == m_planrep->numberOfNodes());
    assert(m_crossings.size() + m_vertices.size() == m_node_to_v_item.size());
    // andere Objekte
    assert(m_external_face->is_external());
    assert(m_reference_face->is_reference());
    assert(m_planrep->representsCombEmbedding());
#endif
}
