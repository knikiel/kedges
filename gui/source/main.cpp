#include <QApplication>
#include <QPushButton>
#include <QVBoxLayout>
#include <QTextEdit>

#include "KEdgesWindow.h"

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);
    QApplication::setOrganizationName("LS 11");
    QApplication::setApplicationName("kEdges");
    QLocale::setDefault(QLocale::system().name());

    KEdgesWindow window;
    window.show();

    return app.exec();
}
